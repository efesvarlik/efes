﻿using System;
using Efes.DataAccessLayer;
using Efes.Models.Krm;

namespace Efes.UIPC
{
    public class KrmUipc
    {
        public File GetKrmFileInformationsByDate()
        {
            return new KrmDal().GetKrmFileInformationsByDate();
        }
    }
}
