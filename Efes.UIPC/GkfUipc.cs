﻿using System;
using System.Collections.Generic;
using Efes.DataAccessLayer;
using Efes.Entities.Obligor;

namespace Efes.UIPC
{
    public class GkfUipc
    {
        private readonly GkfDal _gkfDal;
        public GkfUipc()
        {
            _gkfDal = new GkfDal();
        }
        public List<Gkf> GetGkfList(int userId, DateTime date, string type)
        {
            return _gkfDal.GetGkfList(userId, date, type);
        }
    }
}
