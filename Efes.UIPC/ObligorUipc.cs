﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading;
using Efes.DataAccessLayer;
using Efes.Entities.Obligor;
using Efes.Entities.User;
using Efes.Models;
using Efes.Models.Obligor;
using Efes.Models.Obligor.Tabs;
using Efes.Models.Obligor.Tabs.SmsHistory;
using Efes.Utility;

namespace Efes.UIPC
{
    public class ObligorUipc
    {
        private readonly ObligorDal _obligorDal;
        private readonly ProtocolUipc _protocolUipc;
        private readonly PhoneUipc _phoneUipc;
        private readonly ReminderUipc _reminderUipc;

        public ObligorUipc()
        {
            _obligorDal = new ObligorDal();
            _protocolUipc = new ProtocolUipc();
            _phoneUipc = new PhoneUipc();
            _reminderUipc = new ReminderUipc();
        }

        #region General Methods

        public List<Obligor> GetObligorList(string searchString, ObligorSearchType obligorSearchType, int userId)
        {
            return new ObligorDal().GetObligorList(searchString, obligorSearchType, userId);
        }

        public ObligorModel GetObligorByAccountId(string accountId, string contactId, int userId, List<Role> roles)
        {
            var obligorModel = _obligorDal.GetObligorByAccountId(accountId, contactId, userId);
            if (obligorModel != null)
            {
                obligorModel.ProtocolCampaignModelList = _protocolUipc.GetProtocolCampaignModelList(obligorModel.AccountId, roles);
                obligorModel.Last5ImportantNotes = GetLast5ImportantNotes(userId, obligorModel.Id);
                obligorModel.TabList = _obligorDal.GetTabList(userId);
                obligorModel.ProtocolCancelReasonList = _protocolUipc.GetProtocolCancelReasonList();
                obligorModel.Phones = _phoneUipc.GetPhones(obligorModel.Id);
                obligorModel.FakeNumbers = _phoneUipc.GetFakeNumbers();
                obligorModel.ReminderTypes = _reminderUipc.GetReminderTypes();
                obligorModel.CallResults = _obligorDal.GetCallResults();
                obligorModel.PaymentPlanTypes = new PaymentPlanUipc().GetPaymentPlanTypes();
            }

            new TimeoutUipc().Process();
            return obligorModel;
        }

        public ObligorModel GetObligorByAccountId(string accountId, int userId)
        {
            return _obligorDal.GetObligorByAccountId(accountId, null, userId);
        }

        public string GetAccountIdByObligorId(string obligorId)
        {
            return _obligorDal.GetAccountIdByObligorId(obligorId);
        }

        public List<User> GetUsersForAccountOwnership(int userId)
        {
            return _obligorDal.GetUsersForAccountOwnership(userId);
        }

        public bool ChangeAccountOwner(int newAccountOwnerId, int userId, string accountId)
        {
            return _obligorDal.ChangeAccountOwner(newAccountOwnerId, userId, accountId);
        }

        public bool ChangePaymentOwner(int newAccountOwnerId, int userId, string accountId, string recId)
        {
            return _obligorDal.ChangePaymentOwner(newAccountOwnerId, userId, accountId, recId);
        }

        public bool SetRating(int userId, string obligorId, string communicationHistoryId, int score)
        {
            return _obligorDal.SetRating(userId, obligorId, communicationHistoryId, score);
        }

        public bool CloseAccountByObligorId(string obligorId, int userId, string name)
        {
            var result = _obligorDal.CloseAccountByObligorId(obligorId, userId);
            if (!result) return false;

            var accountId = GetAccountIdByObligorId(obligorId);
            new ExternalMuhasebeUipc().CollactionAlacakGuncelle(accountId, name, userId.ToString());
            return true;
        }

        public bool SetApproveSpecialExecution(string obligorId, int userId, string reason)
        {
            return _obligorDal.SetApproveSpecialExecution(obligorId, userId, reason);
        }
        
        #endregion

        #region Tabs

        public List<YapilacakIs> GetYapilacakIsler(int userId, string obligorId)
        {
            return _obligorDal.GetYapilacakIsler(userId, obligorId);
        }

        public List<TumPortfoydekiBorclari> GetTumPortfoydekiBorclari(int userId, string obligorId)
        {
            return _obligorDal.GetTumPortfoydekiBorclari(userId, obligorId);
        }

        public List<HpHacizliMal> GetHpHacizliMal(int userId, string obligorId)
        {
            return _obligorDal.GetHpHacizliMal(userId, obligorId);
        }

        public List<HpTeminat> GetHpTeminat(int userId, string obligorId)
        {
            return _obligorDal.GetHpTeminat(userId, obligorId);
        }

        public List<SmsHistory> GetSmsHistory(int userId, string obligorId)
        {
            return _obligorDal.GetSmsHistory(userId, obligorId);
        }

        public Dictionary<int, string> GetPhoneDictionary(string obligorId)
        {
            return _obligorDal.GetPhoneDictionary(obligorId);
        }

        public List<IletisimTarihce> GetIletisimTarihce(int userId, string obligorId)
        {
            return _obligorDal.GetIletisimTarihce(userId, obligorId);
        }

        public List<ProtokolTarihce> GetProtokolTarihce(int userId, string obligorId)
        {
            return _obligorDal.GetProtokolTarihce(userId, obligorId);
        }

        public List<ProtokolTaksit> GetAktifProtokolTaksit(int userId, string obligorId)
        {
            return _obligorDal.GetAktifProtokolTaksit(userId, obligorId);
        }

        public List<ProtokolTaksit> GetProtocolInstalmentsById(int userId, string protocolId)
        {
            return _obligorDal.GetProtocolInstalmentsById(userId, protocolId);
        }

        public List<AllPayments> GetAllPayments(int userId, string obligorId)
        {
            return _obligorDal.GetAllPayments(userId, obligorId);
        }

        public List<Adres> GetAdres(int userId, string obligorId)
        {
            return _obligorDal.GetAdres(userId, obligorId);
        }

        public List<KisiBilgi> GetKisiBilgi(int userId, string obligorId)
        {
            return _obligorDal.GetKisiBilgi(userId, obligorId);
        }

        public List<Borc> GetBorc(int userId, string obligorId)
        {
            return _obligorDal.GetBorc(userId, obligorId);
        }

        public List<DosyaEk> GetDosyaEk(int userId, string obligorId)
        {
            return _obligorDal.GetDosyaEk(userId, obligorId);
        }

        public List<IslemTarihce> GetIslemTarihce(int userId, string obligorId)
        {
            return _obligorDal.GetIslemTarihce(userId, obligorId);
        }

        public List<YapilacakIsAdmin> GetYapilacakIsAdmin(int userId, string obligorId)
        {
            return _obligorDal.GetYapilacakIsAdmin(userId, obligorId);
        }

        public List<KisininIliskideOlduguTumBorc> GetKisininIliskideOlduguTumBorc(int userId, string obligorId)
        {
            return _obligorDal.GetKisininIliskideOlduguTumBorc(userId, obligorId);
        }

        public List<BorcunIliskideOlduguTumKisi> GetBorcunIliskideOlduguTumKisi(int userId, string obligorId)
        {
            return _obligorDal.GetBorcunIliskideOlduguTumKisi(userId, obligorId);
        }

        public List<DosyaSahiplikTarihce> GetDosyaSahiplikTarihce(int userId, string obligorId)
        {
            return _obligorDal.GetDosyaSahiplikTarihce(userId, obligorId);
        }

        public List<PriorityGroupTracking> GetPriorityGroupTracking(int userId, string obligorId)
        {
            return _obligorDal.GetPriorityGroupTracking(userId, obligorId);
        }

        public List<PostaGonderi> GetPostaGonderi(int userId, string obligorId)
        {
            return _obligorDal.GetPostaGonderi(userId, obligorId);
        }

        public List<KkbScore> GetKkbScoreList(int userId, string obligorId)
        {
            return _obligorDal.GetKkbScoreList(userId, obligorId);
        }

        public List<ObligorProcessHistory> GetObligorProcessHistoryList(int userId, string obligorId)
        {
            return _obligorDal.GetObligorProcessHistoryList(userId, obligorId);
        }

        #endregion

        #region Note

        public List<Note> GetNot(int userId, string obligorId)
        {
            return _obligorDal.GetNot(userId, obligorId);
        }

        public List<FiveImportantNote> GetLast5ImportantNotes(int userId, string obligorId)
        {
            return _obligorDal.GetLast5ImportantNotes(userId, obligorId);
        }

        public bool AddNote(Note note, string obligorId, string rate)
        {
            return _obligorDal.AddNote(note, obligorId, rate);
        }

        public DeleteNoteResult DeleteNote(string id, int userId)
        {
            return _obligorDal.DeleteNote(id, userId);
        }

        public bool AddCallResult(int userId, string phone, string obligorId, int resultId, string resultText)
        {
            return _obligorDal.AddCallResult(userId, phone, obligorId, resultId, resultText);
        }
        public List<CallResult> GetCallResults()
        {
            return _obligorDal.GetCallResults();
        }

        #endregion

        #region Phone

        public bool AddPhone(Phone phone, string obligorId)
        {
            return _obligorDal.AddPhone(phone, obligorId);
        }

        public bool UpdatePhone(Phone phone)
        {
            return _obligorDal.UpdatePhone(phone);
        }

        public Phone GetPhone(string id)
        {
            return _obligorDal.GetPhone(id);
        }

        public List<Phone> GetPhones(int userId, string obligorId)
        {
            return _obligorDal.GetPhones(userId, obligorId);
        }

        public List<GsmNumber> GetGsmNumbers(int userId, string obligorId)
        {
            return new ObligorDal().GetGsmNumbers(userId, obligorId);
        }

        #endregion

        #region Attachment

        public bool AddAttachment(string obligorId, string desc, int userId, string path, string fileName, bool isGkf)
        {
            return _obligorDal.AddAttachment(obligorId, desc, userId, path, fileName, isGkf);
        }

        public bool RemoveFile(string recId, int userId)
        {
            return _obligorDal.RemoveFile(recId, userId);
        }

        #endregion

        #region Address

        public bool AddAddress(AddressModel adres, string obligorId, int userId)
        {
            return _obligorDal.AddAddress(adres, obligorId, userId);
        }

        #endregion

        #region Posta Gonderileri

        public bool SendPostaGonderiToOperation(int userId, string id)
        {
            return _obligorDal.SendPostaGonderiToOperation(userId, id);
        }

        #endregion

        #region SmsHistory
        public bool InsertSms(int userId, string phoneNumber, string smsText, string obligorId)
        {
            return _obligorDal.InsertSms(userId, phoneNumber, smsText, obligorId);
        }

        #endregion

    }
}
