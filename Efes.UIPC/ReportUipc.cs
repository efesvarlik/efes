﻿using Efes.DataAccessLayer;
using Efes.Entities.Report;
using Efes.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.UIPC
{
    public class ReportUipc
    {
        private readonly ReportDal _reportDal;
        public ReportUipc()
        {
            _reportDal = new ReportDal();
        }
        public List<BeklentiTahsilat> GetBeklentiTahsilat(int userId, int month, int year)
        {
            return _reportDal.GetBeklentiTahsilat(userId, month, year);
        }

        public List<Tahsilat> GetTahsilatList(int userId, int month, int year)
        {
            return _reportDal.GetTahsilatList(userId, month, year);
        }

        public CikarilanGKF GetCikarilanGkf(int userId, int month, int year)
        {
            return _reportDal.GetCikarilanGkf(userId, month, year);
        }

        public List<int> GetAgentListesiYearlist()
        {
            return _reportDal.GetAgentListesiYearlist();
        }

        public List<User> GetAgentListesiAgentList(int userId)
        {
            return _reportDal.GetAgentListesiAgentList(userId);
        }
    }
}
