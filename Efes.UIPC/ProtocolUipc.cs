﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Efes.DataAccessLayer;
using Efes.Entities.Protocol;
using Efes.Entities.User;
using Efes.Models;
using Efes.Utility;

namespace Efes.UIPC
{
    public class ProtocolUipc
    {
        private readonly ProtocolDal _protocolDal = new ProtocolDal();
        public readonly log4net.ILog Logger = LogHelper.GetLogger();

        public List<ProtocolCampaignModel> GetProtocolCampaignModels()
        {
            return _protocolDal.GetProtocolCampaignModels();
        }

        public ProtocolCampaignModel GetProtocolCampaignModel()
        {
            var protocolCampaignModel = new ProtocolCampaignModel();
            var roles = new UserUipc().GetRolesHasRightToMakeCampaign();
            foreach (var r in roles)
            {
                protocolCampaignModel.Details.Add(new ProtocolCampaignDetails() {RoleId = r.Id, RoleName = r.Value});
            }
            protocolCampaignModel.Portfolios = new PortfolioDal().GetPortfolios();

            return protocolCampaignModel;

        }

        public bool CreateProtocolCampaign(ProtocolCampaign protocolCampaign, List<string> obligorList, int userId)
        {
            //Discount Percentage ve Maximum Installment Count degerlerinden yeni bir tablo olusturulmasi
            var dtDetails = Utilities.GetNewdatatable(new Dictionary<string, string>
            {
                {"MaxInstallmentCount", "System.Int32"},
                {"DiscountPercentage", "System.Int32"},
                {"RoleId", "System.Int32"},
                {"ObligorTypeId", "System.Int32"},
            });

            foreach (var detail in protocolCampaign.Details)
            {
                var dr = dtDetails.NewRow();
                dr["MaxInstallmentCount"] = detail.MaxInstallmentCount;
                dr["DiscountPercentage"] = detail.DiscountPercentage;
                dr["RoleId"] = detail.RoleId;
                dr["ObligorTypeId"] = (int) detail.ObligorType;
                dtDetails.Rows.Add(dr);
            }

            switch (protocolCampaign.ProtocolCampaignType)
            {
                case ProtocolCampaignType.ObligorList:
                    var dtObligorList =
                        Utilities.GetNewdatatable(new Dictionary<string, string> {{"Id", "System.String"}});
                    //obligor List
                    foreach (var obligor in obligorList)
                    {
                        var dr = dtObligorList.NewRow();
                        dr["Id"] = obligor;
                        dtObligorList.Rows.Add(dr);
                    }
                    return _protocolDal.CreateProtocolCampaign(protocolCampaign, dtObligorList, dtDetails, userId);

                case ProtocolCampaignType.AgePortfolio:
                    return _protocolDal.CreateProtocolCampaignByAgeAndPortfolio(protocolCampaign, dtDetails, userId);
            }
            return false;
        }

        public bool InsertProtocolAndDetail(ProtocolModel protocol, DataTable protocolDetail)
        {
            return _protocolDal.InsertProtocolAndDetail(protocol, protocolDetail);
        }

        public List<ProtocolCampaign> GetProtocolCampaignListByAccountId(string accountId)
        {
            return _protocolDal.GetProtocolCampaignListByAccountId(accountId);
        }

        public List<ProtocolCampaignModel> GetProtocolCampaignModelList(string accountId, List<Role> roles)
        {
            try
            {
                var protocolCampaignList = GetProtocolCampaignListByAccountId(accountId);
                return protocolCampaignList.Select(p =>
                {
                    var pcm = new ProtocolCampaignModel()
                    {
                        Id = p.Id,
                        Description = p.Description,
                        StartDate = p.StartDate,
                        EndDate = p.EndDate,
                        CreatorId = p.CreatorId,
                        CreatedDate = p.CreatedDate,
                        UpdaterId = p.UpdaterId,
                        UpdatedDate = p.UpdatedDate,
                        MaxAdvancePaymentDay = p.MaxAdvancePaymentDay,
                        MaxInstallmentDay = p.MaxInstallmentDay,
                        MinAmount = p.MinAmount,
                        MaxAmount = p.MaxAmount,
                        StartDateView = p.StartDate.ToString("dd MMMM, yyyy"),
                        EndDateView = p.EndDate.ToString("dd MMMM, yyyy"),
                        FirstInstallmentDateView = p.MaxInstallmentDay.Convert<DateTime>().ToString("dd MMMM, yyyy"),
                        FirstAdvancePaymentDateView =
                            p.MaxAdvancePaymentDay.Convert<DateTime>().ToString("dd MMMM, yyyy"),
                        CashClosingAmount = p.CashClosingAmount
                    };
                    var details = p.Details.Where(c => roles.Select(x => x.Id).Contains(c.RoleId)).ToList();
                    var detail = details.OrderByDescending(c => c.DiscountPercentage).FirstOrDefault();
                    if (detail != null)
                    {
                        pcm.RoleMakingProtocol = detail.RoleId;
                        pcm.Details = details; //cshtml de kontrol edilebilsin diye set edildi.
                        pcm.DiscountPercentage = detail.DiscountPercentage;
                        pcm.MaxInstallmentCount = detail.MaxInstallmentCount;
                    }
                    return pcm;
                }).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetProtocolCampaignModelList-UIPC metodunda hata oluştu.", ex);
                return new List<ProtocolCampaignModel>();
            }
        }

        public List<string> GetProtocolCancelReasonList()
        {
            var cancelReasonList = new List<string>();
            string key = "GetProtocolCancelReasonList";
            if (CacheManager.Default.Contains(key))
            {
                var result = CacheManager.Default.Get(key) as List<string>;
                if (result != null)
                {
                    return result;
                }
            }

            cancelReasonList = _protocolDal.GetProtocolCancelReasonList();
            CacheManager.Default.Add(key, cancelReasonList, DateTime.Now.AddSeconds(5));
            return cancelReasonList;
        }

        public bool DeleteProtocol(int userId, string recId, string reason)
        {
            return _protocolDal.DeleteProtocol(userId, recId, reason);
        }

        public List<ProtocolApprovementModel> GetProtocolApprovements(int userId)
        {
            return _protocolDal.GetProtocolApprovements(userId);
        }

        public bool CancelProtocol(int protocolId, int userId)
        {
            return _protocolDal.CancelProtocol(protocolId, userId);
        }

        public bool ApproveProtocol(int protocolId, int userId)
        {
            return _protocolDal.ApproveProtocol(protocolId, userId);
        }

        public List<ProtocolListModel> DailyProtocolList(int userId)
        {
            return _protocolDal.DailyProtocolList(userId);
        }

        public bool AttachToProtocol(string recId, int planId, int subPlanId, int userId)
        {
            return _protocolDal.AttachToProtocol(recId, planId, subPlanId, userId);
        }

        public bool DeAttachToProtocol(string recId, bool deAttachObligor, int userId)
        {
            return _protocolDal.DeAttachToProtocol(recId, deAttachObligor,  userId);
        }

        public bool InsertPaymentPromise(PaymentPromise paymentPromise)
        {
            return _protocolDal.InsertPaymentPromise(paymentPromise);
        }

        public bool CancelPaymentPromise(int userId, int promiseId)
        {
            return _protocolDal.CancelPaymentPromise(userId, promiseId);
        }

        public List<PaymentPromise> GetPaymentPromiseReport(int userId, DateTime startDate, DateTime endDate)
        {
            return _protocolDal.GetPaymentPromiseReport(userId, startDate, endDate);
        }
    }
}
