﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.DataAccessLayer;
using Efes.Entities;

namespace Efes.UIPC
{
    public class PaymentPlanUipc
    {
        private readonly PaymentPlanDal _paymentPlanDal;
        public PaymentPlanUipc()
        {
            _paymentPlanDal = new PaymentPlanDal();
        }
        public List<PaymentPlanType> GetPaymentPlanTypes()
        {
            return _paymentPlanDal.GetPaymentPlanTypes();
        }

        public List<PaymentSubPlanType> GetPaymentPlanSubTypes(int paymentPlanTypeId)
        {
            return _paymentPlanDal.GetPaymentPlanSubTypes(paymentPlanTypeId);
        }
    }
}
