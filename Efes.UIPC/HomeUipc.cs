﻿using Efes.DataAccessLayer;
using Efes.Models;

namespace Efes.UIPC
{
    public class HomeUipc
    {
        public DashboardModel GetDashBoardModel(int userId)
        {
            var homeDal = new HomeDal();
            return new DashboardModel()
            {
                SummaryList = homeDal.GetDashBoardSummaryList(userId),
                CallCount = homeDal.GetCallCount(userId),
                MoneyCollection = homeDal.GetMoneyCollection(userId),
                MoneyCollectionBottomStatictics = homeDal.GetMoneyCollectionBottomStatistics(userId),
                TargetAndActualCollectionList = homeDal.GetTargetAndActualCollections(userId),
            };
        }
    }
}
