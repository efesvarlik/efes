﻿using Efes.DataAccessLayer;
using Efes.Entities.LawOffice;
using Efes.Models.Law;

namespace Efes.UIPC
{
    public class LawOfficeUserUipc
    {
        private readonly UserUipc _userUipc;
        private readonly LawOfficeUserDal _lawOfficeUserDal;
        private readonly LawOfficeUipc _lawOfficeUipc;

        public LawOfficeUserUipc()
        {
            _userUipc = new UserUipc();
            _lawOfficeUipc = new LawOfficeUipc();
            _lawOfficeUserDal = new LawOfficeUserDal();
        }

        public OfficeUserAssignmentModel GetOfficeUserAssignmentModel(int officeId)
        {
            var lawOffice = _lawOfficeUipc.GetLawOfficeById(officeId);
            var officeUserAssignmentModel = new OfficeUserAssignmentModel()
            {
                OfficeId = officeId,
                OfficeName = lawOffice.Name,
                AllUsers = _userUipc.GetAllLawUserUnassigned(officeId),
                OfficeUsers = _userUipc.GetUserListByLawOfficeId(officeId)
            };

            return officeUserAssignmentModel;
        }

        public bool InsertOrUpdateLawOfficeUser(OfficeUser officeUser, int byId, bool assigned)
        {
            return _lawOfficeUserDal.InsertOrUpdateLawOfficeUser(officeUser, byId, assigned);
        }
    }
}
