﻿using System.Collections.Generic;
using Efes.DataAccessLayer;
using Efes.Entities.Obligor;
using Efes.Models.Obligor;
using System;

namespace Efes.UIPC
{
    public class ReminderUipc
    {
        private readonly ReminderDal _reminderDal;

        public ReminderUipc()
        {
            _reminderDal = new ReminderDal();
        }

        public bool InsertReminder(Reminder reminder)
        {
            return _reminderDal.InsertReminder(reminder);
        }

        public List<ReminderType> GetReminderTypes()
        {
            return _reminderDal.GetReminderTypes();
        }

        public List<ReminderModel> GetReminderByUserId(int userId, DateTime date )
        {
            return _reminderDal.GetReminderByUserId(userId, date);
        }
    }
}
