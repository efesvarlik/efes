﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Efes.DataAccessLayer;
using Efes.Entities.Portfolio;

namespace Efes.UIPC
{
    public class PortfolioUipc
    {
        private readonly PortfolioDal _portfolioDal;

        public PortfolioUipc()
        {
            _portfolioDal = new PortfolioDal();
        }

        public List<Portfolio> GetPortfolios()
        {
            return _portfolioDal.GetPortfolios();
        }
    }
}
