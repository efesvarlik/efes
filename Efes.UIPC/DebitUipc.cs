﻿using Efes.DataAccessLayer;

namespace Efes.UIPC
{
    public class DebitUipc
    {
        private readonly DebitDal _debitDal; 
        public DebitUipc()
        {
            _debitDal=  new DebitDal();
        }
        public bool DoExecution(int userId, string recId)
        {
            return _debitDal.DoExecution(userId, recId);
        }
    }
}
