﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.DataAccessLayer;
using Efes.Entities;
using Efes.Models;

namespace Efes.UIPC
{
    public class NotificationUipc
    {
        public List<NotificationModel> GetNotifications(int memberId)
        {
            var notificationModelList = new List<NotificationModel>();
            var notificationList = new NotificationDal().GetNotifications(memberId);
            if (notificationList.Any())
            {
                notificationModelList.AddRange(notificationList.Select(notification => new NotificationModel
                {
                    Id = notification.Id,
                    IsRead = notification.IsRead,
                    Title = notification.Title,
                    From = notification.From,
                    Body = notification.Body,
                    TimeText = notification.TimeText,
                    IconType = GetTypeText(notification.NotificationType),
                    IconBgType = GetBgTypeText(notification.NotificationType),
                    Type = notification.TypeId
                }));
            }
            return notificationModelList;
        }

        public void Read(int id)
        {
            new NotificationDal().Read(id);
        }

        public void Confirm(int id, int currentUserId)
        {
            new NotificationDal().Confirm(id, currentUserId);
        }

        private string GetTypeText(NotificationType notificationType)
        {
            string ret = "bell-o";
            switch (notificationType)
            {
                case NotificationType.Information:
                    ret = "info";
                    break;
                case NotificationType.Warning:
                    ret = "warning";
                    break;
                case NotificationType.Error:
                    ret = "remove";
                    break;
                case NotificationType.Verification:
                    ret = "bolt";
                    break;
                case NotificationType.General:
                    ret = "bell-o";
                    break;
            }
            return ret;

        }
        private string GetBgTypeText(NotificationType notificationType)
        {
            string ret = "success";
            switch (notificationType)
            {
                case NotificationType.Information:
                    ret = "success";
                    break;
                case NotificationType.Warning:
                    ret = "warning";
                    break;
                case NotificationType.Error:
                    ret = "danger";
                    break;
                case NotificationType.Verification:
                    ret = "danger";
                    break;
                case NotificationType.General:
                    ret = "info";
                    break;
            }

            return ret;

        }
    }
}
