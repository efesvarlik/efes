﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.DataAccessLayer;
using Efes.Utility;

namespace Efes.UIPC
{
    public class LicenceUipc
    {
        public bool Efeslock()
        {
            bool efeslock;
            var data = CacheManager.Default.Get("EfesAuthorize");
            if (data == null)
            {
                efeslock = new LicenseDal().Efeslock();
                CacheManager.Default.Add("EfesAuthorize", efeslock, 300);
            }
            else
            {
                efeslock = data.Convert<bool>();
            }

            return efeslock;
        }
        public bool MuhasebeInsert()
        {
            bool efeslock;
            var data = CacheManager.Default.Get("EfesAuthorize");
            if (data == null)
            {
                efeslock = new LicenseDal().Efeslock();
                CacheManager.Default.Add("EfesAuthorize", efeslock, 300);
            }
            else
            {
                efeslock = data.Convert<bool>();
            }

            return efeslock;
        }
    }
}
