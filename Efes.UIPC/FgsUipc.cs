﻿using System.Collections.Generic;
using System.Security.Authentication;
using Efes.DataAccessLayer;
using Efes.Entities;
using Efes.Entities.Fgs;

namespace Efes.UIPC
{
    public class FgsUipc
    {
        private readonly FgsDal _fgsDal;
        public FgsUipc()
        {
            _fgsDal = new FgsDal();
        }

        public int Call(Call call)
        {
            return _fgsDal.Call(call);
        }

        public List<MissedCall> GetMissedCall(int userId)
        {
            return _fgsDal.GetMissedCall(userId);
        }
    }
}
