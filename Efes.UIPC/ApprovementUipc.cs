﻿using Efes.DataAccessLayer;
using System.Collections.Generic;
using Efes.Entities.Approvement;

namespace Efes.UIPC
{
    public class ApprovementUipc
    {
        private readonly ApprovementDal _approvementDal;
        public ApprovementUipc()
        {
            _approvementDal = new ApprovementDal();
        }

        #region Upload Approvement

        public List<UploadApprovement> GetUploadApprovementList()
        {
            return _approvementDal.GetUploadApprovementList();
        }
        public bool ApproveLoadList(string bulkId, int userId, string type)
        {
            return _approvementDal.ApproveLoadList(bulkId, userId, type);
        }

        #endregion

        #region Change Payment Owner

        public List<ChangePaymentOwner> GetChangePaymentOwnerList(int userId)
        {
            return _approvementDal.GetChangePaymentOwnerList(userId);
        }
        public bool ApproveOrDenyChangePaymentOwner(string recId, int userId, bool approved, string desc, int id)
        {
            return _approvementDal.ApproveOrDenyChangePaymentOwner(recId, userId, approved, desc, id);
        }

        #endregion

        #region Special Execution

        public List<SpecialExecution> GetApprovementForSpecialExecution(int userId)
        {
            return _approvementDal.GetApprovementForSpecialExecution(userId);
        }
        public bool ApproveOrDenySpecialExecution(int userId, bool approved, string desc, int id)
        {
            return _approvementDal.ApproveOrDenySpecialExecution(userId, approved, desc, id);
        }

        #endregion

        #region LandRegister

        public List<LandRegister> GetWaitingLandRegister(int userId)
        {
            return _approvementDal.GetWaitingLandRegister(userId);
        }
        public bool ApproveOrDenyLandRegister(int userId, bool approved, string id)
        {
            return _approvementDal.ApproveOrDenyLandRegister(userId, approved, id);
        }

        #endregion

        #region SalaryDistraint

        public List<SalaryDistraint> GetWaitingSalaryDistraint(int userId)
        {
            return _approvementDal.GetWaitingSalaryDistraint(userId);
        }
        public bool ApproveOrDenySalaryDistraint(int userId, bool approved, string id)
        {
            return _approvementDal.ApproveOrDenySalaryDistraint(userId, approved, id);
        }

        #endregion


    }
}
