﻿using System.Collections.Generic;
using System.Data;
using Efes.DataAccessLayer;
using Efes.Entities.SmartCalling;
using Efes.Entities.Sms;
using Efes.Models.SmartCalling;
using Efes.Utility;

namespace Efes.UIPC
{
    public class SmartCallingUipc
    {
        private readonly SmartCallingDal _smartCallingDal;
        private readonly PhoneDal _phoneDal;
        private readonly PortfolioDal _portfolioDal;

        public SmartCallingUipc()
        {
            _smartCallingDal = new SmartCallingDal();
            _phoneDal = new PhoneDal();
            _portfolioDal = new PortfolioDal();
        }

        public NewAppointedModel GetNewAppointedList(int userId, int type)
        {
            return _smartCallingDal.GetNewAppointedList(userId, type);
        }

        public TodayPaymentsModel GetTodayPaymentsList(int userId)
        {
            return _smartCallingDal.GetTodayPaymentsList(userId);
        }

        public CancelProtocolModel GetCancelProtocolList(int userId)
        {
            return _smartCallingDal.GetCancelProtocolList(userId);
        }

        public ExpiredModel GetExpiredList(int userId)
        {
            return _smartCallingDal.GetExpiredList(userId);
        }

        public ByCapitalGroupModel GetByCapitalGroupList(int userId, int debtGroupType, int portfolioId)
        {
            var byCapitalGroupModel = new ByCapitalGroupModel
            {
                ByCapitalGroupList = _smartCallingDal.GetByCapitalGroupList(userId, debtGroupType, portfolioId),
                FakeNumbers = _phoneDal.GetFakeNumbers(),
                DebtGroups = GetDebtGroup(userId),
                Portfolios = _portfolioDal.GetPortfolios()
            };
            return byCapitalGroupModel;
        }

        public CollectionWithoutProtocolModel GetCollectionWithoutProtocolList(int userId)
        {
            return _smartCallingDal.GetCollectionWithoutProtocolList(userId);
        }

        public Dictionary<int, string> GetDebtGroup(int userId)
        {
            var key = "debtGroupKey_" + userId;
            var debtGroupExists = CacheManager.Default.Contains(key);

            if (debtGroupExists)
            {
                return CacheManager.Default.Get(key) as Dictionary<int, string>;
            }
            var debtGroups = _smartCallingDal.GetDebtGroup(userId);
            CacheManager.Default.Add(key, debtGroups, 60);
            return debtGroups;
        }

        #region Dialer

        public bool StartDialer(DataTable dtAgents, DataTable dtAccounts, int userId, string bandwidth, int maxWaitingTime, bool useFct)
        {
            return _smartCallingDal.StartDialer(dtAgents, dtAccounts, userId, bandwidth, maxWaitingTime, useFct);
        }

        public DialerStatus GetCurrentDialerStatus()
        {
            return _smartCallingDal.GetCurrentDialerStatus();
        }

        public List<DialerCall> GetDialerCallList()
        {
            return _smartCallingDal.GetDialerCallList();
        }

        public DialerDetail GetCurrentDialerDetail()
        {
            return _smartCallingDal.GetCurrentDialerDetail();
        }

        public bool ChangeDialerCallStatus(int userId, int status, string recId)
        {
            return _smartCallingDal.ChangeDialerCallStatus(userId, status, recId);
        }

        public PersonalAutoDialerModel GetPersonalAutoDialerList(int userId)
        {
            return _smartCallingDal.GetPersonalAutoDialerList(userId);
        }

        public List<DialerAgent> GetDialerAgentList()
        {
            return _smartCallingDal.GetDialerAgentList();
        }

        public bool RemoveAgentFromDialerList(string agentName, int userId)
        {
            return _smartCallingDal.RemoveAgentFromDialerList(agentName, userId);
        }

        #endregion
    }
}
