﻿using System;
using System.Collections.Generic;
using System.Data;
using Efes.DataAccessLayer;
using Efes.Entities;

namespace Efes.UIPC
{
    public class ToolUipc
    {
        public List<Phone> GetPhoneNumbers(string accountId)
        {
            return new ToolDal().GetPhoneNumbers(accountId);
        }

        public bool ChangePhoneStatus(string accountId, string phoneNumber, bool isActive)
        {
            return new ToolDal().ChangePhoneStatus(accountId, phoneNumber, isActive);
        }

        public bool PassivePhoneList(DataTable dt, int memberId)
        {
            return new ToolDal().PassivePhoneList(dt, memberId);
        }

        public void InsertFeedback(string feedback, int memberId)
        {
            new ToolDal().InsertFeedback(feedback, memberId);
        }

        public List<Word> GetTopTenWords()
        {
            return new ToolDal().GetTopTenWords();
        }
    }
}
