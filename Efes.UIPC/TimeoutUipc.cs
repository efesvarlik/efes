﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Efes.DataAccessLayer;
using Efes.Utility;

namespace Efes.UIPC
{
    public class TimeoutUipc
    {
        private readonly TimeoutDal _timeoutDal;
        public TimeoutUipc()
        {
            _timeoutDal = new TimeoutDal();
        }
        public int TimeoutSeconds()
        {
            var date = DateTime.Now.ToShortDateString();
            if (CacheManager.Default.Contains("to" + date))
            {
                return CacheManager.Default.Get("to" + date).Convert<int>();
            }
            var timeout = _timeoutDal.GetTimeoutSeconds();
            CacheManager.Default.Add("to" + date, timeout, 600);
            return timeout;
        }

        public void Process()
        {
            var timeoutSecond = TimeoutSeconds();
            var rndTs = 0;
            if (timeoutSecond != 0)
            {
                rndTs = new Random().Next(timeoutSecond);
            }
            var timeoutSeconds = rndTs * 1000;
            Thread.Sleep(timeoutSeconds);
        }
    }
}
