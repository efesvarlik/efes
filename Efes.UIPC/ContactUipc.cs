﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.DataAccessLayer;
using Efes.Entities;
using Efes.Entities.Sms;
using Efes.Models;

namespace Efes.UIPC
{
    public class ContactUipc
    {
        public List<SmsContactModel> GetContactDataWithImportIdByAccountId(int importId)
        {
            return new ContactDal().GetContactDataWithImportIdByAccountId(importId);
        }

        public List<SmsContactModel> GetContactDataWithImportIdByTckn(int importId)
        {
            return new ContactDal().GetContactDataWithImportIdByTckn(importId);
        }

        public List<SmsContactModel> GetBulkSmsWithGsm()
        {
            return new ContactDal().GetBulkSmsWithGsm();
        }

        public List<GsmMessage> GetSmsListForService()
        {
            return new ContactDal().GetSmsListForService();
        }

        public List<SmsContactModel> GetBulkSmsWithTckn()
        {
            return new ContactDal().GetBulkSmsWithTckn();
        }

        public bool SendBulkSmsReport(DataTable resultTable)
        {
            return new ContactDal().SendBulkSmsReport(resultTable);
        }
    }
}
