﻿using System.Collections.Generic;
using Efes.DataAccessLayer;
using Efes.Entities;

namespace Efes.UIPC
{
    public class LocationUipc
    {
        private LocationDal _locationDal;
        public LocationUipc()
        {
             _locationDal = new LocationDal();
        }
        public List<City> GetCities()
        {
           return _locationDal.GetCities();
        }

        public List<County> GetCountiesByCityId(int cityId)
        {
            return _locationDal.GetCountiesByCityId(cityId);
        }
    }
}
