﻿using System.Collections.Generic;
using Efes.DataAccessLayer;
using Efes.Entities.Sms;
using Efes.Utility;

namespace Efes.UIPC
{
    public class SmsUipc
    {
        public List<Template> GetSmsTemplates()
        {
            var smsTemplates = CacheManager.Default.Get("smsTemplates") as List<Template>;
            if (smsTemplates == null)
            {
                smsTemplates = new SmsDal().GetSmsTemplates();
                CacheManager.Default.Add("smsTemplates", smsTemplates, 60);    
            }
            return smsTemplates;
        }

        public List<string> GetBannedWords()
        {
            var bannedWords = CacheManager.Default.Get("bannedWords") as List<string>;
            if (bannedWords == null)
            {
                bannedWords = new SmsDal().GetBannedWords();
                CacheManager.Default.Add("bannedWords", bannedWords, 600);
            }
            return bannedWords;
        }
    }
}
