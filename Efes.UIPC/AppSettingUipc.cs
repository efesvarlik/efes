﻿using System;
using Efes.DataAccessLayer;
using Efes.Entities;
using Efes.Utility;

namespace Efes.UIPC
{
    public class AppSettingUipc
    {
        public string DataportSmsAccountNumber
        {
            get { return GetAppSettingByName("DataportSmsAccountNumber").Value.Trim(); }
        }
        public string DataportSmsUsername
        {
            get { return GetAppSettingByName("DataportSmsUsername").Value.Trim(); }
        }
        public string DataportSmsPassword
        {
            get { return GetAppSettingByName("DataportSmsPassword").Value.Trim(); }
        }
        public string DataportSmsAlphanumeric
        {
            get { return GetAppSettingByName("DataportSmsAlphanumeric").Value.Trim(); }
        }
        public string DataportSmsAlphanumericTckn
        {
            get { return GetAppSettingByName("DataportSmsAlphanumericTckn").Value.Trim(); }
        }
        public string DataportSmsServiceCode
        {
            get { return GetAppSettingByName("DataportSmsServiceCode").Value.Trim(); }
        }
        public int DataportSmsBulkLimitPerTime
        {
            get { return GetAppSettingByName("DataportSmsBulkLimitPerTime").Value.Convert<int>(); }
        }
        public int AuthTokenExpireInSecond
        {
            get
            {
                try
                {
                    return GetAppSettingByName("AuthTokenExpireInSecond").Value.Convert<int>();
                }
                catch (Exception ex)
                {
                    ex.Log("AuthTokenExpireInSecond");
                    return 1200;
                }
            }
        }
        public string CurrentGsmOperatorForSms
        {
            get { return GetAppSettingByName("CurrentGsmOperatorForSms").Value.Convert<string>(); }
        }
        public string SelectedTabId
        {
            get { return GetAppSettingByName("SelectedTabId").Value.Convert<string>(); }
        }
        public  AppSetting GetAppSettingById(int id)
        {
            return new AppSettingDal().GetAppSettingById(id);
        }
        public  AppSetting GetAppSettingByName(string name)
        {
            return new AppSettingDal().GetAppSettingByName(name);
        }
    }
}
