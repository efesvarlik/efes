﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.DataAccessLayer;
using Efes.Entities;
using Efes.Models;

namespace Efes.UIPC
{
    public class AuditUipc
    {
        public List<Audit> GetAuditsLast30Minutes()
        {
            return new AuditDal().GetAuditsLast30Minutes();
        }

        public List<AuditModel> GetAuditModelListLast30Minutes()
        {
            var auditList = new AuditDal().GetAuditsLast30Minutes();
            if (auditList == null || !auditList.Any()) return null;

            var auditModelList = new List<AuditModel>();
            foreach (var audit in auditList)
            {
                auditModelList.Add(new AuditModel()
                {
                    Id = audit.Id,
                    AccessedObjectsList = audit.AccessedObjectsList,
                    ApplicationName = audit.ApplicationName,
                    ClientHostName = audit.ClientHostName,
                    DatabaseId = audit.DatabaseId,
                    DatabaseName = audit.DatabaseName,
                    Duration = audit.Duration,
                    Error = audit.Error,
                    EventTypeId = audit.EventTypeId,
                    IsInternalEvent = audit.IsInternalEvent,
                    ServerName = audit.ServerName,
                    StartTime = audit.StartTime,
                    RowVersion = audit.RowVersion,
                    LoginName = audit.LoginName,
                    Success = audit.Success,
                    ObjectName = audit.ObjectName,
                    SchemaName = audit.SchemaName,
                    TransactionId = audit.TransactionId,
                    TextData = audit.TextData,
                    TextDataSmall = !string.IsNullOrWhiteSpace(audit.TextData) && audit.TextData.Length > 25 ? audit.TextData.Substring(0, 25) + "..." : audit.TextData,
                    Operation = audit.Operation
                });
            }
            return auditModelList;
            
        }
    }
}
