﻿using Efes.DataAccessLayer;
using Efes.Entities.Portfolio;
using Efes.Utility;

namespace Efes.UIPC
{
    public class PortfolioStageUipc
    {
        private readonly PortfolioStageDal _portfolioDal;

        public PortfolioStageUipc()
        {
            _portfolioDal = new PortfolioStageDal();
        }

        public Portfolio GetWaitingPortfolio()
        {
            return _portfolioDal.GetWaitingPortfolio();
        }

        public bool WorkTables(string bankName, string name)
        {
            return _portfolioDal.WorkTables(bankName, name);
        }

        public bool SourceAndSourceset(string bankName, string name)
        {
            return _portfolioDal.SourceAndSourceset(bankName, name);
        }

        public bool ContactUpdate(string bankName, string name)
        {
            return _portfolioDal.ContactUpdate(bankName, name);
        }

        public bool UpdateAccount(string bankName, string name)
        {
            return _portfolioDal.UpdateAccount(bankName, name);
        }

        public bool PhoneTable(string bankName, string name)
        {
            return _portfolioDal.PhoneTable(bankName, name);
        }

        public bool AddressTable(string bankName, string name)
        {
            return _portfolioDal.AddressTable(bankName, name);
        }

        public bool NormalizePhoneNumbers()
        {
            return _portfolioDal.NormalizePhoneNumbers();
        }

        public bool DebtDetailTable(string bankName, string name)
        {
            return _portfolioDal.DebtDetailTable(bankName, name);
        }

        public bool MoneyDebtDetailsToAccount()
        {
            return _portfolioDal.MoneyDebtDetailsToAccount();
        }

        public bool ContactRelationsTable(string bankName, string name)
        {
            return _portfolioDal.ContactRelationsTable(bankName, name);
        }

        public bool DataToTable(string name)
        {
            return _portfolioDal.DataToTable(name);
        }

        public bool ReleaseData(string name)
        {
            return _portfolioDal.ReleaseData(name);
        }

        public bool AddressActive(string name)
        {
            return _portfolioDal.AddressActive(name);
        }

        public bool CreateBop(string name)
        {
            return _portfolioDal.CreateBop(name);
        }

        public bool GetMaxId(string name)
        {
            var maxid = _portfolioDal.GetMaxId();
            return maxid != null && _portfolioDal.PreLoads2(name, maxid.Convert<int>());
        }
    }
}
