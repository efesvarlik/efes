﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Efes.DataAccessLayer;
using Efes.Entities;
using Efes.Entities.User;
using Efes.Utility;
using Rule = Efes.Entities.User.Rule;

namespace Efes.UIPC
{
    public class UserUipc
    {
        private readonly UserDal _userDal;

        #region Contructor
        public UserUipc()
        {
            _userDal = new UserDal();
        }

        #endregion

        #region General User Methods

        public User GetUser(string userName, string password)
        {
            var ds = _userDal.GetUser(userName, password);
            return MapUser(ds);
        }

        public User GetUser(int id)
        {
            var ds = _userDal.GetUser(id);
            return MapUser(ds);
        }

        public User GetUser(string email)
        {
            var ds = _userDal.GetUser(email);
            if (ds.HasData() && ds.Tables[0].Rows.Count == 1)
            {
                return new User()
                {
                    Id = ds.Tables[0].Rows[0]["Id"].Convert<int>(),
                    Name = ds.Tables[0].Rows[0]["Name"].Convert<string>(),
                    LoginName = ds.Tables[0].Rows[0]["LoginName"].Convert<string>(),
                };
            }
            return null;
        }

        public List<User> GetAllUsers()
        {
            return _userDal.GetAllUsers();
        }

        private static User MapUser(DataSet ds)
        {
            if (ds.HasData())
            {
                if (ds.Tables[0].Rows[0]["LoginCount"].Convert<int>() == 1)
                {
                    var dr = ds.Tables[0].Rows[0];
                    var uiInfo = new User
                    {
                        Id = dr["UserId"].Convert<int>(),
                        Name = dr["Name"].Convert<string>(),
                        LoginName = dr["LoginName"].Convert<string>(),
                        ImagePath = dr["ImagePath"].Convert<string>(),
                        Description = dr["Description"].Convert<string>(),
                        Email = dr["Email"].Convert<string>(),
                        IsAdmin = dr["RoleId"].Convert<int>() == 1,
                        IsSecurePassword = dr["IsSecurePassword"].Convert<bool>(),
                        //IsSecurePassword = false,
                        IsLockedOut = dr["IsLockedOut"].Convert<bool>(),
                        PhoneNumber = dr["PhoneNumber"].Convert<string>(""),
                        Rules = new List<Rule>(),
                        Roles = new List<Role>(),
                        IsActive = dr["IsActive"].Convert<bool>()
                    };


                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {
                        if (drow["RoleId"] != DBNull.Value)
                        {
                            var roleId = drow["RoleId"].Convert<int>();
                            var roleValue = drow["Role"].Convert<string>();
                            if (uiInfo.Roles.All(r => r.Id != roleId))
                            {
                                uiInfo.Roles.Add(new Role()
                                {
                                    Id = roleId,
                                    Value = roleValue
                                });
                            }
                        }

                        if (drow["RuleId"] != DBNull.Value)
                        {
                            var ruleId = drow["RuleId"].Convert<int>();
                            var ruleValue = drow["Rule"].Convert<string>();
                            if (uiInfo.Rules.All(r => r.Id != ruleId))
                            {
                                uiInfo.Rules.Add(new Rule()
                                {
                                    Id = ruleId,
                                    Value = ruleValue
                                });
                            }
                        }
                    }
                    return uiInfo;
                }
            }
            return null;
        }

        public bool InserUser(User user, int creatorId)
        {
            return _userDal.InsertUser(user, creatorId);
        }

        public bool UpdateUser(User user, int updaterId)
        {
            return _userDal.UpdateUser(user, updaterId);
        }

        public List<User> GetUserListHasApproveRight()
        {
            return _userDal.GetUserListHasApproveRight();
        }

        public List<User> GetUserListByLawOfficeId(int lawOfficeId)
        {
            return _userDal.GetUserListByLawOfficeId(lawOfficeId);
        }

        public List<User> GetAllLawUserUnassigned(int lawOfficeId)
        {
            return _userDal.GetAllLawUserUnassigned(lawOfficeId);
        }

        public List<User> GetUsersByRole(Roles role)
        {
            return _userDal.GetUsersByRole(role);
        }

        public List<User> GetUsersHasOnlyOneRoleByRoleRoleId(Roles role)
        {
            return _userDal.GetUsersHasOnlyOneRoleByRoleRoleId(role);
        }

        #endregion

        #region MemberLog
        public void InsertMemberLog(int userId, UserLogType logType)
        {
            _userDal.InsertMemberLog(userId, (int)logType);
        }

        #endregion

        #region Password

        public string ChangePassword(string userName, string password, string newPassword)
        {
            return _userDal.ChangePassword(userName, password, newPassword);
        }

        public bool ResetPassword(User user, int userId, string userDesc)
        {
            return _userDal.ResetPassword(user, userId, userDesc);
        }

        #endregion

        #region Rule

        public List<Rule> GetAllRules()
        {
            return _userDal.GetAllRules();
        }

        public void InsertRule(string value)
        {
            _userDal.InsertRule(value);
        }

        public void UpdateRule(int id, string value, bool isActive)
        {
            _userDal.UpdateRule(id, value, isActive);
        }

        #endregion

        #region Role

        public Role GetRole(int id)
        {
            return _userDal.GetRole(id);
        }


        public List<Role> GetAllRoles()
        {
            return _userDal.GetAllRoles();
        }

        public void InsertRole(Role role)
        {
            _userDal.InsertRole(role);
        }

        public void UpdateRole(Role role)
        {
            _userDal.UpdateRole(role);
        }

        public List<Role> GetRolesHasRightToMakeCampaign()
        {
            return _userDal.GetRolesHasRightToMakeCampaign();
        }

        #endregion

        #region UserRoles

        public List<Role> GetUserRolesById(int userId)
        {
            return _userDal.GetUserRolesById(userId);
        }

        public bool InsertUserRole(int userId, int roleId, int creatorId)
        {
            return _userDal.InsertUserRole(userId, roleId, creatorId);
        }

        public bool DeleteUserRole(int userId, int roleId, int creatorId)
        {
            return _userDal.DeleteUserRole(userId, roleId, creatorId);
        }

        #endregion

        #region RoleRules

        public List<Rule> GetRoleRulesById(int roleId)
        {
            return _userDal.GetRoleRulesById(roleId);
        }

        public bool InsertRoleRule(int roleId, int ruleId, int creatorId)
        {
            return _userDal.InsertRoleRule(roleId, ruleId, creatorId);
        }

        public bool DeleteRoleRule(int roleId, int ruleId, int creatorId)
        {
            return _userDal.DeleteRoleRule(roleId, ruleId, creatorId);
        }

        #endregion

        public List<LoginHistory> GetLoginHistory(int userId, string userName)
        {
            return _userDal.GetLoginHistory(userId, userName);
        }

    }
}
