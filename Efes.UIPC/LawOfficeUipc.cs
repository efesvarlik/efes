﻿using Efes.DataAccessLayer;
using Efes.Entities;
using Efes.Models.Law;
using System.Collections.Generic;
using Efes.Entities.Law;
using System;

namespace Efes.UIPC
{
    public class LawOfficeUipc
    {
        private readonly LawOfficeDal _lawOfficeDal;
        private readonly LocationUipc _locationUipc;
        public LawOfficeUipc()
        {
            _lawOfficeDal = new LawOfficeDal();
            _locationUipc = new LocationUipc();
        }

        public OfficeListModel GetLawOfficeModelList(int userId)
        {
            var officeListModel = new OfficeListModel();
            officeListModel.OfficeModelList = _lawOfficeDal.GetLawOfficeList(userId);
            return officeListModel;
        }

        public bool CreateLawOffice(OfficeModel officeModel)
        {
            return _lawOfficeDal.CreateLawOffice(officeModel);
        }

        public bool UpdateLawOffice(OfficeModel officeModel)
        {
            return _lawOfficeDal.UpdateLawOffice(officeModel);
        }

        public OfficeModel GetLawOfficeById(int lawOfficeId)
        {
            return _lawOfficeDal.GetLawOfficeById(lawOfficeId);
        }

        public OfficeAccountListModel GetLawOfficeAccountListModelByOfficeId(int officeId)
        {
            var officeAccountListModel = new OfficeAccountListModel();
            officeAccountListModel.OfficeAccountModelList = _lawOfficeDal.GetLawOfficeAccountListModelByOfficeId(officeId);
            var officeModel = GetLawOfficeById(officeId);
            officeAccountListModel.OfficeId = officeId;
            officeAccountListModel.OfficeName = string.Format("{0} ({1})", officeModel.Name, officeModel.CityName);
            return officeAccountListModel;
        }

        public OfficeAccountListModel GetLawOfficeAccountListModelByUserId(int userId)
        {
            var officeAccountListModel = new OfficeAccountListModel();
            officeAccountListModel.OfficeAccountModelList = _lawOfficeDal.GetLawOfficeAccountListModelByUserId(userId);
            if(officeAccountListModel.OfficeAccountModelList.Count > 0)
            {
                var officeModel = GetLawOfficeById(officeAccountListModel.OfficeAccountModelList[0].LawOfficeId);
                officeAccountListModel.OfficeId = officeModel.Id;
                officeAccountListModel.OfficeName = string.Format("{0} ({1})", officeModel.Name, officeModel.CityName);
            }
            return officeAccountListModel;
        }

        public OfficeAccountModel GetLawOfficeAccountByOfficeId(int id)
        {
            return _lawOfficeDal.GetLawOfficeAccountByOfficeId(id);
        }

        public bool AddAccountToLawOffice(OfficeAccount officeAccount)
        {
            return _lawOfficeDal.AddAccountToLawOffice(officeAccount);
        }

        public bool UpdateLawOfficeAccount(OfficeAccount officeAccount)
        {
            return _lawOfficeDal.UpdateLawOfficeAccount(officeAccount);
        }

        public List<Collection> GetCollectionByLawOfficeId(int userId, DateTime startDate, DateTime endDate)
        {
            return _lawOfficeDal.GetCollectionByLawOfficeId(userId, startDate, endDate);
        }
    }
}
