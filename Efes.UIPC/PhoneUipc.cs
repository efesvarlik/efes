﻿using System.Collections.Generic;
using Efes.DataAccessLayer;
using Efes.Entities;

namespace Efes.UIPC
{
    public class PhoneUipc
    {
        private readonly PhoneDal _phoneDal;
        public PhoneUipc()
        {
            _phoneDal = new PhoneDal();
        }
        public List<Phone> GetPhones(string obligorId)
        {
            return _phoneDal.GetPhones(obligorId);
        }

        public List<Phone> GetFakeNumbers()
        {
            return _phoneDal.GetFakeNumbers();
        }
    }
}
