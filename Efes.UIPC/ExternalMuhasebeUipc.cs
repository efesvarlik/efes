﻿using System;
using Efes.DataAccessLayer;

namespace Efes.UIPC
{
    public class ExternalMuhasebeUipc
    {
        private readonly ExternalMuhasebeDal _externalMuhasebeDal;
        private readonly LicenceUipc _licenceUipc;

        public ExternalMuhasebeUipc()
        {
            _externalMuhasebeDal = new ExternalMuhasebeDal();
            _licenceUipc = new LicenceUipc();
        }

        private bool AllowedInsert()
        {
            return _licenceUipc.MuhasebeInsert();
        }

        public void CollactionAlacakGuncelle(string musteriNo, string agentAd, string agentSoyad)
        {
            if (AllowedInsert())
            {
                musteriNo = GetAccountNumberWithoutDash(musteriNo);
                _externalMuhasebeDal.CollactionAlacakGuncelle(musteriNo, agentAd, agentSoyad);
            }
        }

        public void CollactionOdemeplaniEkle(string musteriNo, DateTime odemeplanitarihi, decimal odemeplanitutari, int taksitSayisi)
        {
            if (AllowedInsert())
            {
                musteriNo = GetAccountNumberWithoutDash(musteriNo);
                _externalMuhasebeDal.CollactionOdemeplaniEkle(musteriNo, odemeplanitarihi, odemeplanitutari,
                taksitSayisi);
            }
        }

        public void CollactionOdemeplaniIptal(string id)
        {
            if (AllowedInsert())
            {
               new ProtocolDal().SendCancelProtocolToCollaction(id);
            }
        }

        private string GetAccountNumberWithoutDash(string accountId)
        {
            return accountId.Contains("-") ? accountId.Split('-')[0] : accountId;
        }
    }
}
