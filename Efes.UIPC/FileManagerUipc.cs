using System;
using System.Data;
using Efes.DataAccessLayer;
using Efes.Entities;
using Efes.Entities.Excel;
using Exception = System.Exception;

namespace Efes.UIPC
{
    public class FileManagerUipc
    {
        public bool FileUpload(DataImportType dataImportType, DataTable dt, int memberId)
        {
            try
            {
                return new FileManagerDal().FileUpload(dataImportType, dt, memberId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool FileImportForBulkSms(SmsImportType smsImportType, DataTable dt, int userId, bool activeProtocol, bool protocolApprove, bool closeAccount, bool salary)
        {
            return new FileManagerDal().FileImportForBulkSms(smsImportType, dt, userId, activeProtocol, protocolApprove, closeAccount, salary);
        }

        public bool InterestAndCostUpload(DataTable dt, int userId)
        {
            try
            {
                return new FileManagerDal().InterestAndCostUpload(dt, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool LandRegister(DataTable dt, int userId, string fileName)
        {
            try
            {
                return new FileManagerDal().LandRegister(dt, userId, fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SalaryDistraint(DataTable dt, int userId, string fileName)
        {
            try
            {
                return new FileManagerDal().SalaryDistraint(dt, userId, fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}