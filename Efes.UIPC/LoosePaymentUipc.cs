﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.DataAccessLayer;
using Efes.Models;

namespace Efes.UIPC
{
    public class LoosePaymentUipc
    {
        public LoosePaymentModel GetLoosePaymentModel()
        {
            var loosePaymentModel = new LoosePaymentModel
                {
                    LoosePaymentList = new LoosePaymentDal().GetLoosePaymentList(),
                    PaymentPlanTypes = new PaymentPlanUipc().GetPaymentPlanTypes()
        };
            return loosePaymentModel;
        }

        public bool UpdateProccessedPayment(int userId, string recId, string value)
        {
            return new LoosePaymentDal().UpdateProccessedPayment(userId, recId, value);
        }
    }
}
