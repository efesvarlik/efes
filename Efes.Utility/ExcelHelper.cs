﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Web;
using System.Configuration;
using System.Linq;
using System.Web.Hosting;

namespace Efes.Utility
{
    public class ExcelHelper
    {
        public static List<string> ExcelImport(List<string> necessaryColumnList, HttpRequestBase request, int userId, string cacheName)
        {

            var ds = new DataSet();
            var httpPostedFileBase = request.Files[0];
            if (httpPostedFileBase.ContentLength <= 0) return necessaryColumnList;
            var fileExtension = System.IO.Path.GetExtension(request.Files[0].FileName);

            if (fileExtension != ".xls" && fileExtension != ".xlsx") return necessaryColumnList;
                    var mapPath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["ExcelFileMapPath"]);
            var exists = System.IO.Directory.Exists(mapPath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(mapPath);
            }
            var fileLocation = string.Format("{0}{1}_{2}_{3}", mapPath,
                request.Files[0].FileName, userId, DateTime.Now.ToLongDateString());
            if (System.IO.File.Exists(fileLocation))
            {
                System.IO.File.Delete(fileLocation);
            }
            request.Files[0].SaveAs(fileLocation);

            var excelConnectionString = string.Empty;
            if (fileExtension == ".xls" || fileExtension == ".xlsx")
            {
                switch (fileExtension)
                {
                    case ".xls":
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                                                fileLocation +
                                                ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        break;
                    case ".xlsx":
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                                fileLocation +
                                                ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        break;
                }
            }
            else
            {
                return null;
            }

            var excelConnection = new OleDbConnection(excelConnectionString);
            excelConnection.Open();

            var dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (dt == null)
            {
                return null;
            }

            var excelSheets = new String[dt.Rows.Count];

            var t = 0;
            foreach (DataRow row in dt.Rows)
            {
                excelSheets[t] = row["TABLE_NAME"].ToString();
                t++;
            }
            var excelConnection1 = new OleDbConnection(excelConnectionString);

            var query = string.Format("SELECT * FROM [{0}]", excelSheets[0]);
            using (var dataAdapter = new OleDbDataAdapter(query, excelConnection1))
            {
                dataAdapter.Fill(ds);
            }


            CacheManager.Default.Add(cacheName + request.BoUniqueId(), ds.Tables[0]);
            var gonnaRemove = (from DataColumn column in ds.Tables[0].Columns select column.ColumnName).ToList();

            excelConnection.Close();
            excelConnection.Dispose();
            return necessaryColumnList.Where(c => !gonnaRemove.Contains(c)).ToList();
        }
    }
}
