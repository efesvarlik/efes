﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Utility
{
    public class CertainLength : Attribute
    {
        public int Length;

        public CertainLength(int length)
        {
            this.Length = length;
        }
    }
}
