﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;

namespace Efes.Utility
{
    public class Utilities
    {
        public static DataTable ConvertToDatatable<T>(List<T> data)
        {
            var props = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            for (var i = 0; i < props.Count; i++)
            {
                var prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            var values = new object[props.Count];
            foreach (T item in data)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public const string RuntimeCacheKeyPrefix = "RuntimeCache";

        public static string ComputeMd5Hash(string str)
        {
            var md5 = new MD5CryptoServiceProvider();
            var result = md5.ComputeHash(new ASCIIEncoding().GetBytes(str));

            return ConvertByteArrayToHexString(result);
        }

        public static string ConvertByteArrayToHexString(byte[] array)
        {
            var sb = new StringBuilder(array.Length * 2);
            foreach (var t in array)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString().ToLower();
        }

        public static string ConvertToRuntimeCacheKey(string cacheKey)
        {
            return String.Format("{0}|{1}", RuntimeCacheKeyPrefix, cacheKey);
        }

        public static void SetAdoNetAppenderConnectionStrings()
        {
            var hier = (Hierarchy)LogManager.GetRepository();
            if (hier != null)
            {
                var appenders = hier.GetAppenders().OfType<AdoNetAppender>();
                foreach (var appender in appenders)
                {
                    appender.ConnectionString = ConfigurationManager.ConnectionStrings["efesCs"].ConnectionString.Base64Decode();
                    appender.ActivateOptions();
                }
            }
        }

        public static DataTable GetNewdatatable(Dictionary<string, string> columnTypeAndNames )
        {
            var dt = new DataTable();

            foreach (var d in columnTypeAndNames.Where(d => d.Value != null))
            {
                dt.Columns.Add(d.Key, Type.GetType(d.Value));
            }
            return dt;
        }
    }
}
