﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;

namespace Efes.Utility
{
    public class CacheManager
    {
        private static readonly object CacheManagerSyncLock = new object();
        static CacheManager _cacheManager = null;

        readonly ICacheManager _cache = null;

        public CacheManager()
        {
            _cache = CacheFactory.GetCacheManager();
        }

        public CacheManager(string cacheManagerName)
        {
            _cache = CacheFactory.GetCacheManager(cacheManagerName);
        }

        public static CacheManager Default
        {
            get
            {
                if (_cacheManager == null)
                {
                    lock (CacheManagerSyncLock)
                    {
                        if (_cacheManager == null)
                        {
                            _cacheManager = new CacheManager();
                        }
                    }
                }

                return _cacheManager;
            }
        }

        public static CacheManager GetCacheManager(string cacheManagerName)
        {
            return new CacheManager(cacheManagerName);
        }

        public void Add(string key, object value)
        {
            _cache.Add(key, value);
        }

        public void Add(string key, object value, int expireInSeconds)
        {
            this.Add(key, value, Convert.ToDouble(expireInSeconds));
        }

        public void Add(string key, object value, int expireInSeconds, bool useHashedKey)
        {
            key = useHashedKey ? Utilities.ComputeMd5Hash(key) : key;
            this.Add(key, value, Convert.ToDouble(expireInSeconds));
        }

        public void Add(string key, object value, double expireInSeconds)
        {
            if (expireInSeconds == 0)
            {
                _cache.Add(key, value);
            }
            else if (expireInSeconds > 0)
            {
                _cache.Add(key, value, CacheItemPriority.Normal, null, new SlidingTime(TimeSpan.FromSeconds(expireInSeconds)));
            }

            // DO NOT add if expiration is less than zero < Readonlycache Implementation
        }

        public void Add(string key, object value, DateTime expireDate)
        {
            _cache.Add(key, value, CacheItemPriority.Normal, null, new SlidingTime(TimeSpan.FromSeconds((expireDate - DateTime.Now).TotalSeconds)));
        }

        public bool Contains(string key)
        {
            return _cache.Contains(key);
        }

        public object Get(string key)
        {
            return _cache.GetData(key);
        }

        public object Get(string key, bool useHashedKey)
        {
            key = useHashedKey ? Utilities.ComputeMd5Hash(key) : key;
            return _cache.GetData(key);
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }

        public void Flush()
        {
            _cache.Flush();
        }
    }
}
