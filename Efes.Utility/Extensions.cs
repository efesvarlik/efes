﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Web;
using log4net.Repository.Hierarchy;

namespace Efes.Utility
{
    public static class Extensions
    {
        public static readonly log4net.ILog Logger = LogHelper.GetLogger();
        public static CultureInfo CultureInfoTR = CultureInfo.GetCultureInfo("tr-TR");


        public static bool IsNull(this object obj)
        {
            return obj == null || obj == DBNull.Value;
        }

        public static bool IsNotNull(this object obj)
        {
            return !obj.IsNull();
        }
        public static bool IsNullOrWhitespace(this string s)
        {
            return String.IsNullOrWhiteSpace(s);
        }

        public static string WithComma(this string str)
        {
            return !string.IsNullOrWhiteSpace(str) ? string.Format("{0}, ", str) : string.Empty;
        }
        public static string FormatMonthAndDay(this string str)
        {
            return str.Length == 1 ? "0" + str : str;
        }
        public static string FormatMonthAndDay(this int str)
        {
            return FormatMonthAndDay(str.ToString());
        }

        public static string SafeToString(this object item)
        {
            if (item == null || item == DBNull.Value)
            {
                return string.Empty;
            }
            return item.ToString();
        }

        public static string SafeToStringWithInvariantCulture(this decimal item)
        {
            return item.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        public static string SafeToStringWithTRCulture(this decimal item)
        {
            return item.ToString(CultureInfoTR);
        }

        public static double ToDouble(this object obj, double defaultValue = 0)
        {
            if (obj.IsNull())
                return defaultValue;

            double res = double.TryParse(obj.ToString(), out res) ? res : defaultValue;
            return res;
        }

        public static double ToDouble(this string str, System.Globalization.CultureInfo culture, double defaultValue = 0)
        {
            if (str.IsNull())
            {
                return defaultValue;
            }
            if (culture == null)
            {
                culture = System.Globalization.CultureInfo.CurrentCulture;
            }

            double res = double.TryParse(System.Convert.ToString(str, culture), System.Globalization.NumberStyles.Any, culture, out res) ? res : defaultValue;
            return res;
        }

        public static bool ToBoolean(this string str)
        {
            if (str.IsNullOrWhitespace())
                return false;

            if (str.Trim() == "1")
                return true;

            bool ret;
            bool.TryParse(str, out ret);
            return ret;
        }

        public static int ToInt32(this object item, int defaultValue = 0)
        {
            if (item.IsNull())
            {
                return defaultValue;
            }
            int val = int.TryParse(item.ToString(), out val) ? val : defaultValue;
            return val;
        }

        public static float ToFloat(this object str)
        {
            if (str.IsNull())
            {
                return 0;
            }
            float res = float.TryParse(str.ToString(), out res) ? res : 0;
            return res;
        }

        public static long ToLong(this object obj, long defaultValue = 0)
        {
            if (obj.IsNull())
                return defaultValue;

            long res = long.TryParse(obj.ToString(), out res) ? res : defaultValue;
            return res;
        }

        public static string ToDefaultString(this object obj, string defaultValue = "")
        {
            if (obj.IsNull())
                return defaultValue;

            return obj.ToString();
        }

        public static decimal ToDecimal(this object obj, decimal defaultValue = 0)
        {
            if (obj.IsNull())
            {
                return defaultValue;
            }

            decimal res = decimal.TryParse(System.Convert.ToString(obj, CultureInfo.InvariantCulture)
                , System.Globalization.NumberStyles.Any
                , CultureInfo.InvariantCulture
                , out res) ? res : defaultValue;

            return res;
        }

        public static DateTime ToDateTime(this object datetime)
        {
            DateTime dt = DateTime.TryParse(datetime.SafeToString(), out dt) ? dt : DateTime.MinValue;
            return dt;
        }

        public static bool ToBoolean(this object str)
        {
            var strVal = str.SafeToString();
            if (strVal.Trim().Equals("1"))
                return true;

            return ToBoolean(strVal);
        }

        public static T Convert<T>(this object obj)
        {
            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(T));
                if (obj != null && obj != DBNull.Value && !string.IsNullOrEmpty(obj.ToString()))
                {
                    return (T)converter.ConvertFromString(obj.ToString());
                }
                return default(T);
            }
            catch (Exception exception)
            {
                return default(T);
            }
        }
        public static T Convert<T>(this object obj, object defaultObj)
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));
            try
            {
                if (obj != null && obj != DBNull.Value && !string.IsNullOrWhiteSpace(obj.ToString()))
                {
                    return (T)converter.ConvertFromString(obj.ToString());
                }
                if (defaultObj != null && defaultObj != DBNull.Value)
                {
                    return (T)converter.ConvertFromString(defaultObj.ToString());
                }
                return default(T);
            }
            catch (System.Exception)
            {
                if (defaultObj != null && defaultObj != DBNull.Value)
                {
                    return (T)converter.ConvertFromString(defaultObj.ToString());
                }
                return default(T);
            }
        }
        public static T Convert<T>(this object obj, bool checkDbNull = false)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                if (checkDbNull && obj == DBNull.Value)
                {
                    return default(T);
                }
                return (T)converter.ConvertFromString(obj.ToString());
            }


            return default(T);
        }

        public static bool HasData(this DataSet ds)
        {
            return ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0;
        }
        public static bool HasData(this DataTable dt)
        {
            return dt != null && dt.Rows.Count > 0;
        }
        public static string GetValueOrDefault(this Dictionary<string, string> dictionary, string key, string defaultValue = "")
        {
            string value;
            return dictionary.TryGetValue(key, out value) ? value.Trim() : defaultValue;
        }

        public static string GetDescription(this Enum enumValue)
        {
            var enumValueAsString = enumValue.ToString();

            var type = enumValue.GetType();
            var fieldInfo = type.GetField(enumValueAsString);
            var attributes = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
            {
                var attribute = (DescriptionAttribute)attributes[0];
                return attribute.Description;
            }

            return enumValueAsString;
        }
        public static string BoUniqueId(this HttpRequest request)
        {
            var httpCookie = request.Cookies["boid"];
            if (httpCookie != null)
            {
                return httpCookie.Value;
            }

            Logger.Fatal("BO Unique Id is empty");
            return string.Empty;
        }
        public static string BoUniqueId(this HttpRequestBase request)
        {
            var httpCookie = request.Cookies["boid"];
            if (httpCookie != null)
            {
                return httpCookie.Value;
            }

            Logger.Fatal("BO Unique Id is empty");
            return string.Empty;
        }

        public static void Log(this Exception exception)
        {
            Logger.Error(exception);
        }

        public static void Log(this Exception exception, string description)
        {
            Logger.Error(description, exception);
        }

        public static void Fatal(this Exception exception)
        {
            Logger.Fatal(exception);
        }

        public static void Fatal(this Exception exception, string description)
        {
            Logger.Fatal(description, exception);
        }

        public static void Info(this string str)
        {
            Logger.Info(str);
        }

        public static string EncryptDecrypt(string szPlainText, int szEncryptionKey)
        {
            var szInputStringBuild = new StringBuilder(szPlainText);
            var szOutStringBuild = new StringBuilder(szPlainText.Length);
            for (var iCount = 0; iCount < szPlainText.Length; iCount++)
            {
                var textch = szInputStringBuild[iCount];
                textch = (char)(textch ^ szEncryptionKey);
                szOutStringBuild.Append(textch);
            }
            return szOutStringBuild.ToString();
        }

        public static string Base64Encode(this string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            var base64 = System.Convert.ToBase64String(plainTextBytes);
            var encBase64 = EncryptDecrypt(base64, 348);

            return encBase64;
        }

        public static string Base64EncKeyDecode(this string base64EncodedData)
        {
            //enc ve decryp aynı sekılde cagrılması gerekıyor.
            var base64 = EncryptDecrypt(base64EncodedData, 348);
            var base64EncodedBytes = System.Convert.FromBase64String(base64);

            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string Base64Decode(this string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static Dictionary<string, string> GetAttrDictionary(this object obj)
        {
            var dict = new Dictionary<string, string>();
            var t = obj.GetType();
            var props = t.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    string asda = "asda";
                }
            }

            return dict;
        }

        public static string ExpandStringPadRight(this string str, int length, char paddingChar = ' ')
        {
            return str.Replace("\n", string.Empty).Replace("\r", string.Empty).PadRight(length, paddingChar).Substring(0, length);
        }
        public static string ExpandStringPadLeft(this string str, int length, char paddingChar = ' ')
        {
            return str.Replace("\n", string.Empty).Replace("\r", string.Empty).PadLeft(length, paddingChar).Substring(0, length);
        }

        public static TValue GetAttributeValue<TAttribute, TValue>(this Type type, Func<TAttribute, TValue> valueSelector) where TAttribute : Attribute
        {
            var att = type.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;
            if (att != null)
            {
                return valueSelector(att);
            }
            return default(TValue);
        }

        public static string FormatToYYYYMMGG(this DateTime date)
        {
            return string.Format("{0}{1}{2}", date.Year, date.Month.FormatMonthAndDay(), date.Day.FormatMonthAndDay());
        }

        public static string FormatToYYYYMMGGKrm(this DateTime date)
        {
            return date.Year == 1 ? "        " : string.Format("{0}{1}{2}", date.Year, date.Month.FormatMonthAndDay(), date.Day.FormatMonthAndDay());
        }

        public static string FormatToYYYYMMGGKrmZero(this DateTime date)
        {
            return date.Year == 1 ? "00000000" : string.Format("{0}{1}{2}", date.Year, date.Month.FormatMonthAndDay(), date.Day.FormatMonthAndDay());
        }

        public static List<string> ToStringList(this DataTable dt, string columnName)
        {
            var list = new List<string>();
            if (!dt.HasData()) { return list; }
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(dt.Rows[i][columnName].Convert<string>());
            }
            return list;
        }

        public static List<string> ToStringListTwoColumns(this DataTable dt, string columnName1, string columnName2, string delimeter)
        {
            var list = new List<string>();
            if (!dt.HasData()) { return list; }
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(dt.Rows[i][columnName1].Convert<string>() + delimeter + dt.Rows[i][columnName2].Convert<string>());
            }
            return list;
        }

        
    }
}
