﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using ICSharpCode.SharpZipLib.Zip;

namespace Efes.Utility
{
    public static class ZipHelper
    {
        public static void Zip(string srcFile, string dstFile, int bufferSize)
        {
            var fileStreamIn = new FileStream
                (srcFile, FileMode.Open, FileAccess.Read);
            var fileStreamOut = new FileStream
                (dstFile, FileMode.Create, FileAccess.Write);
            var zipOutStream = new ZipOutputStream(fileStreamOut);
            var buffer = new byte[bufferSize];
            var entry = new ZipEntry(Path.GetFileName(srcFile));
            zipOutStream.PutNextEntry(entry);
            int size;
            do
            {
                size = fileStreamIn.Read(buffer, 0, buffer.Length);
                zipOutStream.Write(buffer, 0, size);
            } while (size > 0);
            zipOutStream.Close();
            fileStreamOut.Close();
            fileStreamIn.Close();
        }

        public static void UnZip(string srcFile, string dstFile, int bufferSize)
        {
            var fileStreamIn = new FileStream
                (srcFile, FileMode.Open, FileAccess.Read);
            var zipInStream = new ZipInputStream(fileStreamIn);
            var entry = zipInStream.GetNextEntry();
            var fileStreamOut = new FileStream
                (dstFile + @"\" + entry.Name, FileMode.Create, FileAccess.Write);
            int size;
            var buffer = new byte[bufferSize];
            do
            {
                size = zipInStream.Read(buffer, 0, buffer.Length);
                fileStreamOut.Write(buffer, 0, size);
            } while (size > 0);
            zipInStream.Close();
            fileStreamOut.Close();
            fileStreamIn.Close();
        }
    }
}