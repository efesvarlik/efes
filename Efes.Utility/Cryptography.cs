﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Efes.Utility
{
    public class Cryptography
    {
        private static byte[] _key = { };
        private static readonly byte[] Iv = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };
        private const string SEncryptionKey = "efesvarl";
        //private const string SEncryptionKey = "£fE$";

        public static string Decrypt(string stringToDecrypt)
        {
            try
            {
                _key = Encoding.UTF8.GetBytes(SEncryptionKey);
                var des = new DESCryptoServiceProvider();
                var inputByteArray = Convert.FromBase64String(stringToDecrypt);
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms,
                  des.CreateDecryptor(_key, Iv), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                var encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Encrypt(string stringToEncrypt)
        {
            try
            {
                _key = Encoding.UTF8.GetBytes(SEncryptionKey);
                var des = new DESCryptoServiceProvider();
                var inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, des.CreateEncryptor(_key, Iv), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
