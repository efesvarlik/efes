﻿using System;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace Efes.Utility
{
    public class MailManager
    {
        public static bool Send(string subject, string emailTo, string body)
        {
            try
            {
                var smtpAddress = ConfigurationManager.AppSettings["SmtpAddress"];
                var portNumber = ConfigurationManager.AppSettings["PortNumber"].Convert<int>();
                var enableSsl = ConfigurationManager.AppSettings["EnableSsl"].Convert<bool>();
                var emailFrom = ConfigurationManager.AppSettings["EmailFrom"];
                var password = ConfigurationManager.AppSettings["Password"];
                var emailName = ConfigurationManager.AppSettings["EmailName"];
               
                using (var mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom, emailName);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    
                  
                    using (var smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.EnableSsl = enableSsl;
                        smtp.Send(mail);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
