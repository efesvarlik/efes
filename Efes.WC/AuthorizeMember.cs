using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Efes.WC
{
    public class AuthorizeMember : AuthorizeAttribute
    {
        public bool IsAuthenticated
        {
            get { return AuthManager.IsAuthenticated; }
        }
        public string Rules { get; set; }
        public string RedirectController { get; set; }
        public string RedirectAction { get; set; }
        public string Message { get; set; }
        private bool IsAuthorized { get; set; }


        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            IsAuthorized = true;
            var success = AuthManager.IsAuthenticated;
            if (!success) return false;
            IsAuthorized = Rules == null || AuthManager.IsAuthorized(Rules) || Authentication.UserInfo.IsAdmin; //Yeni olusturulmus bir kullanicinin hic rolu olmadigindan kullanici giris yaptiginda hataya dusuyor diye 
            // (Roles != null ||) kodu eklendi
            if (!IsAuthorized)
            {
                Message = "��lemi yapmak i�in yetkiniz bulunmamaktad�r.";
            }

            return IsAuthorized;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var request = httpContext.Request;
            var response = httpContext.Response;

            if (request.IsAjaxRequest())
            {
                //filterContext.Result = new ContentResult();
                //response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                //response.SuppressFormsAuthenticationRedirect = true;
                var viewResult = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data =
                     (new
                     {
                         isFault = true,
                         isAuthorized = IsAuthorized,
                         description = "Unauthorized Ajax Request"
                     }),
                };
                filterContext.Result = viewResult;
            }
            else
            {
                var reUrl = request.Url == null ? "/" : request.Url.PathAndQuery;
                if (!string.IsNullOrWhiteSpace(RedirectController) && !string.IsNullOrWhiteSpace(RedirectAction))
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = RedirectController,
                                action = RedirectAction,
                                r = ""
                            })
                    );
                }
                else
                {

                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                           new
                           {
                               controller = "Account",
                               action = "Login",
                               r = reUrl
                           })
                       );
                }
            }
            //}
        }
    }
}