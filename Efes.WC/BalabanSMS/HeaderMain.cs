﻿using System;
namespace Efes.WC.BalabanSMS
{
    [Serializable]
    public class HeaderMain
    {
        public HeaderProp Header { get; set; }
    }

    [Serializable]
    public class HeaderProp
    {
        public string From { get; set; }
        public int Route { get; set; }
        public DateTime ScheduledDeliveryTime { get; set; }
        public int ValidityPeriod { get; set; }
    }
}