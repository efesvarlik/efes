﻿using System;

namespace Efes.WC.BalabanSMS
{
    [Serializable]
    public class SmsSubmitResponse
    {
        public SmsSubResponse Response { get; set; }
    }

    [Serializable]
    public class SmsSubResponse
    {
        public int MessageId { get; set; }
        public SmsSubmitStatus Status { get; set; }
    }

    [Serializable]
    public class SmsSubmitStatus
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
}