﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.WC.BalabanSMS
{
    [Serializable]
    public class LoginResponse
    {
        public Response Response { get; set; }
    }

    [Serializable]
    public class Response
    {
        public Identifier Identifier { get; set; }
        public Status Status { get; set; }
    }

    [Serializable]
    public class Identifier
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int OwnerId { get; set; }
    }

    [Serializable]
    public class Status
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
}
