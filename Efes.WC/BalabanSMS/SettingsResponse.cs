﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.WC.BalabanSMS
{
    [Serializable]
    public class SettingsResponse
    {
        public SetResponse Response { get; set; }
    }

    [Serializable]
    public class SetResponse
    {
        public Settings Settings { get; set; }
        public SetStatus Status { get; set; }
    }

    [Serializable]
    public class Settings
    {
        public Balance Balance { get; set; }
        public List<Keyword> Keywords { get; set; }
        public OperatorSettings OperatorSettings { get; set; }
        public List<Sender> Senders { get; set; }
    }

    [Serializable]
    public class Balance
    {
        public decimal Limit { get; set; }
        public decimal Main { get; set; }
    }

    [Serializable]
    public class Keyword
    {
        public string ServiceNumber { get; set; }
        public DateTime Timestamp { get; set; }
        public int Validity { get; set; }
        public string Value { get; set; }
    }

    [Serializable]
    public class OperatorSettings
    {
        public string Account { get; set; }
        public string MSISDN { get; set; }
        public string ServiceId { get; set; }
        public int UnitPrice { get; set; }
        public string VariantId { get; set; }
    }

    [Serializable]
    public class Sender
    {
        public bool Default { get; set; }
        public string Value { get; set; }
    }

    [Serializable]
    public class SetStatus
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
}
