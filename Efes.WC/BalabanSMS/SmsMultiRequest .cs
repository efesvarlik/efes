﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.WC.BalabanSMS
{
    [Serializable]
    public class SmsMultiRequest
    {
        public CredentialMain Credential { get; set; }
        public string DataCoding { get; set; }
        public HeaderMain Header { get; set; }
        public List<EnvelopeMain> Envelopes { get; set; }

        public SmsMultiRequest()
        {
            Envelopes = new List<EnvelopeMain>();
        }
    }

    [Serializable]
    public class EnvelopeMain
    {
        public string Message { get; set; }
        public string To { get; set; }
    }
}