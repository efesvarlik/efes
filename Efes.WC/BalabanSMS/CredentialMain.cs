﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.WC.BalabanSMS
{

    [Serializable]
    public class CredentialMain
    {
        public CredentialProp Credential { get; set; }
    }

    [Serializable]
    public class CredentialProp
    {
        public string Password { get; set; }
        public string Username { get; set; }
    }
}
