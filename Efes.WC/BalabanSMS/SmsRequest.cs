﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.WC.BalabanSMS
{
    [Serializable]
    public class SmsRequest
    {
        public CredentialMain Credential { get; set; }
        public string DataCoding { get; set; }
        public HeaderMain Header { get; set; }
        public string Message { get; set; }
        public string[] d2p1 { get; set; }
    }
}
