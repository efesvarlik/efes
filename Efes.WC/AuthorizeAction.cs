using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Efes.WC
{
    public class AuthorizeAction : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (Authentication.UserInfo == null)
            {
                return false;
            }
            if (Authentication.UserInfo.IsAdmin)
            {
                return true;
            }
            var necessaryRules = Rules.Split(',').ToList();
            var found = false;
            foreach (var rule in necessaryRules)
            {
                if (Authentication.UserInfo.Rules.Select(c => c.Value).ToList().Any(r => r == rule))
                {
                    found = true;
                }
            }

            return found;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var request = httpContext.Request;
            var response = httpContext.Response;

            if (request.IsAjaxRequest())
            {
                filterContext.Result = new ContentResult();
                response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                response.SuppressFormsAuthenticationRedirect = true;
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new
                        {
                            controller = "Account",
                            action = "Login",
                            r = request.Url == null ? "/" : request.Url.PathAndQuery
                        })
                    );
            }
        }

        public string Rules { get; set; }

    }
}