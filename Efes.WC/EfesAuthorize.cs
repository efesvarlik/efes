﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using Efes.UIPC;
using Efes.Utility;

namespace Efes.WC
{
    public class EfesAuthorize : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var efeslock = new LicenceUipc().Efeslock();

            if (!efeslock)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new
                        {
                            controller = "Account",
                            action = "Logout"
                        })
                );
            }
        }
    }
}
