﻿using System;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc.Html;
using System.Configuration;
using Efes.Entities;
using Efes.Entities.User;
using Efes.UIPC;
using Efes.Utility;
using Newtonsoft.Json;

namespace Efes.WC
{
    public class AuthManager
    {
        private const string AutCookieName = "bols";
        //818sn. Google Analytics Avg.Session Duration (13dk 38sn)
        private static int TokenExpireInSecond
        {
            get
            {
                var expire = new AppSettingUipc().AuthTokenExpireInSecond;
                return expire == 0 ? 1200 : expire;
            }
        }

        public static string GenerateToken(int id, string userName, DateTime expireDate)
        {
            AuthToken authToken = new AuthToken()
            {
                Id = id,
                UserName = userName,
                Exp = expireDate
            };

            var token = JsonConvert.SerializeObject(authToken);

            var signedToken = AuthSecurity.SignWithHashHMAC(token);

            var encryptedToken = AuthSecurity.Encrypt(signedToken);

            var urlEncodedToken = HttpUtility.UrlEncode(encryptedToken);

            return urlEncodedToken;
        }
        // + %2b / %2f = %3d
        static readonly Regex Rgx = new Regex("[+/=]"); //base64 special chars
        public static AuthToken GetAuthToken()
        {
            var cookieToken = HttpContext.Current.Request.Cookies[AutCookieName];

            if (cookieToken != null && !string.IsNullOrWhiteSpace(cookieToken.Value))
            {
                var urlDecodedToken = cookieToken.Value;

                //Android telefonlarda default browserda duplicate cookie sorunu oldugu icin eklendi, cookie virgulle duplicate sekilde gelmektedir
                //https://code.google.com/p/android/issues/detail?id=78796
                //bozuk cookie degerinde = oldugu icin split islemi urldecode kontrolunden once yapıldi
                if (urlDecodedToken.Contains(","))
                {
                    urlDecodedToken = urlDecodedToken.Split(',')[0];
                }


                //Firecookie gibi cookie extension yuklu browserlar Cookie degerini urldecoded gondermektedir, 
                //cookie degeri  urldecoded gelirse dogrudan decrypt islemine alınmalı,
                if (!Rgx.IsMatch(urlDecodedToken)) //urldecode edilerek gelmistir.
                {
                    urlDecodedToken = HttpUtility.UrlDecode(urlDecodedToken);
                }

                var decryptedToken = AuthSecurity.Decrypt(urlDecodedToken);

                if (!string.IsNullOrWhiteSpace(decryptedToken))
                {
                    var token = AuthSecurity.ValidateHMACAndGetPlainMessage(decryptedToken);

                    if (!string.IsNullOrWhiteSpace(token))
                    {
                        return JsonConvert.DeserializeObject<AuthToken>(token);
                    }
                }
            }

            return null;
        }
        public static int Id
        {
            get
            {
                var id = HttpContext.Current.Items["AuthMemberId"] as int?;

                if (id != null || !IsAuthenticated) return id ?? 0;
                var authToken = GetAuthToken();

                id = authToken == null ? 0 : authToken.Id;

                HttpContext.Current.Items["AuthMemberId"] = id;

                return id ?? 0;
            }
        }
        public static bool IsAuthenticated
        {
            get
            {
                //Request based cache,  bir requestte birden fazla kez kontrol edilir
                bool? isAuthenticated = HttpContext.Current.Items["IsAuthenticated"] as bool?;

                if (isAuthenticated == null)
                {
                    AuthToken authToken = GetAuthToken();

                    isAuthenticated = (authToken != null && authToken.Exp > DateTime.UtcNow);

                    if (isAuthenticated == true)//token slide edili
                    {
                        SlideAuthToken(authToken);
                    }
                    else //var olan token silinir
                    {
                        RemoveAuthCookie();
                    }

                    HttpContext.Current.Items["IsAuthenticated"] = isAuthenticated;
                }

                return isAuthenticated ?? false;
            }
        }

        public static bool IsAuthorized(string roles)
        {
            if (string.IsNullOrWhiteSpace(roles))
            {
                return false;
            }

            var roleList = roles.Split('|');
            //var userrules = new UserUipc().GetUser(Id).Rules;
            return roleList.Join(Authentication.UserRules, c => c, d => d, (c, d) => c).Any();
        }

        //public static string MembershipCacheKey
        //{
        //    get
        //    {
        //        return string.Format("member_{0}", Id);
        //    }
        //}
        private static void SlideAuthToken(AuthToken authToken)
        {
            SetAuthCookie(authToken.Id, authToken.UserName);
        }
        //Authenticate Member and Set MemberInfo To Cache 
        public static void AuthenticateMember(User user, string userName)
        {
            if (user != null && user.Id > 0)
            {
                SetAuthCookie(user.Id, userName);
                CacheManager.Default.Add(string.Format("UserInfo_{0}", HttpContext.Current.Request.BoUniqueId()), user, TokenExpireInSecond);
            }
        }
        private static void SetAuthCookie(int memberId, string userName)
        {
            var expireAt = DateTime.UtcNow.AddSeconds(TokenExpireInSecond);

            var authToken = GenerateToken(memberId, userName, expireAt);

            HttpCookie cookie = new HttpCookie(AutCookieName)
            {
                Value = authToken,
                Secure = false,
                HttpOnly = true
            };

            HttpContext.Current.Response.Cookies.Set(cookie);
            //Response servera gitmeden uye authenticate edilir ve IsAuthenticated methodu kullanılırsa true donulmeli
            HttpContext.Current.Items["IsAuthenticated"] = true;
        }
        private static void RemoveAuthCookie()
        {
            if (HttpContext.Current.Request.Cookies[AutCookieName] != null)
            {
                HttpContext.Current.Response.Cookies[AutCookieName].Expires = DateTime.Now.AddDays(-1);
            }
        }
        public static void LogoutMember()
        {
            //Licence
            var date = DateTime.Now.ToShortDateString();
            
            //Sleep Timeout
            var timeout = 0;
            if (CacheManager.Default.Contains("to" + date))
            {
                timeout = CacheManager.Default.Get("to" + date).Convert<int>();
            }

            CacheManager.Default.Flush();
            CacheManager.Default.Add("to" + date, timeout, 600);

            RemoveAuthCookie();
        }

    }
}
