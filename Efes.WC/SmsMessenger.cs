﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using biz.biotekno.sms.xml.helper;
using Efes.Entities.Sms;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC.BalabanSMS;
using Efes.WC.com.isnet.api;
using log4net;
using Newtonsoft.Json;

namespace Efes.WC
{
    public class SmsMessenger
    {
        #region Properties

        public readonly ILog Logger = LogHelper.GetLogger();
        private static readonly string SpMsisdn = ConfigurationManager.AppSettings["ServiceProviderMSISDN"];
        private static readonly string SpName = ConfigurationManager.AppSettings["ServiceProviderName"];
        private static readonly string SpPassword = ConfigurationManager.AppSettings["ServiceProviderPassword"];
        private static readonly string CpMsisdn = ConfigurationManager.AppSettings["CustomerProviderMSISDN"];
        private static readonly string CpName = ConfigurationManager.AppSettings["CustomerProviderName"];
        private static readonly string CpPassword = ConfigurationManager.AppSettings["CustomerProviderPassword"];
        private static readonly string OriginatingAddress = ConfigurationManager.AppSettings["OriginatingAddress"];
        private static readonly int bulkSmsLimitPertime = ConfigurationManager.AppSettings["BulkSmsLimitPertime"].Convert<int>();
        private static string sessionId = "";

        #endregion

        #region Contructor

        //public SmsMessenger()
        //{
        //    try
        //    {
        //        var service = new ResellerPlatformWebService_V2PortTypeClient();
        //        var response = service.login(SpMsisdn, SpPassword);
        //        sessionId = response.sessionId;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        #endregion

        //static SmsMultiRequest GetMultiSmsRequest(List<GsmMessage> messageList)
        //{
        //    var smsMultiRequest = new SmsMultiRequest
        //    {
        //        Credential = new CredentialMain
        //        {
        //            Credential = new CredentialProp
        //            {
        //                Username = "String",
        //                Password = "String"
        //            }
        //        },
        //        DataCoding = "Default",
        //        Header = new HeaderMain
        //        {
        //            Header = new HeaderProp
        //            {
        //                From = "Efes",
        //                Route = 0,
        //                ScheduledDeliveryTime = DateTime.Now,
        //                ValidityPeriod = 0
        //            }
        //        }
        //    };

        //    foreach (var item in messageList)
        //    {
        //        smsMultiRequest.Envelopes.Add(
        //            new EnvelopeMain()
        //            {
        //                Message = item.Content,
        //                To = item.GsmNumber
        //            }
        //        );
        //    }

        //    return smsMultiRequest;
        //}

        //static StringContent GetSmsPostData(SmsMultiRequest smsRequest)
        //{
        //    var myContent = JsonConvert.SerializeObject(smsRequest);
        //    return new StringContent(myContent, Encoding.UTF8, "application/json");
        //}

        //static async Task<string> MaraditSms(StringContent postData)
        //{
        //    var response = new HttpClient().PostAsync("http://gw.maradit.net/api/json/reply/SubmitMulti", postData);
        //    return await response.Result.Content.ReadAsStringAsync();
        //}

        #region Methods

        public List<string> SendSmsJetSmsWithGsmNo(List<GsmMessage> messageList)
        {
            var resultList = new List<string>();
            var iterationCount = (messageList.Count() / bulkSmsLimitPertime) +
                (messageList.Count % bulkSmsLimitPertime > 0 ? 1 : 0);

            for (var j = 0; j < iterationCount; j++)
            {
                var smsMultiSender = new SmsMultiSender
                {
                    Username = "efsvarlik",
                    Password = "Hb.5267",
                    Originator = "EFES VARLIK",
                    Onlengthproblem = SmsBase.enmOnLengthProblem.SendAllPackage,
                    
                };

                var dtTemp = CreateResponseTable();

                foreach (var message in messageList.Skip(j * bulkSmsLimitPertime).Take(bulkSmsLimitPertime))
                {
                    smsMultiSender.AddMessage(message.GsmNumber, message.Content);

                    var dr = dtTemp.NewRow();
                    dr["RecId"] = message.RecId;
                    dr["SessionId"] = sessionId;
                    dr["ErrorDesc"] = "";
                    dr["MessageId"] = "";
                    dr["Status"] = "";
                    dtTemp.Rows.Add(dr);
                }
                try
                {
                    var responseFromBioTekno = smsMultiSender.sendMessage();
                    for (var i = 0; i < dtTemp.Rows.Count; i++)
                    {
                        dtTemp.Rows[i]["Status"] = responseFromBioTekno.Code;
                        dtTemp.Rows[i]["MessageId"] = responseFromBioTekno.Message;
                    }

                }
                catch (SmsSendException sse)
                {
                    //Sms Gönderilirken sistemsel olarak alýnabilecek hata
                    for (var i = 0; i < dtTemp.Rows.Count; i++)
                    {
                        dtTemp.Rows[i]["Status"] = sse.StackTrace;
                    }
                   
                }
                catch (Exception e)
                {
                    for (var i = 0; i < dtTemp.Rows.Count; i++)
                    {
                        dtTemp.Rows[i]["Status"] = e.StackTrace;
                    }
                }


                new ContactUipc().SendBulkSmsReport(dtTemp);
            }

            return resultList;
        }

        //public static List<string> SendSmsMaraditWithGsmNo(List<GsmMessage> messageList)
        //{
        //    var recIdArray = GetRecIdArray(messageList);
        //    var resultList = new List<string>();
        //    var iterationCount = (messageList.Count() / bulkSmsLimitPertime) +
        //        (messageList.Count % bulkSmsLimitPertime > 0 ? 1 : 0);

        //    for (var j = 0; j < iterationCount; j++)
        //    {
        //        var messages = messageList.Skip(j * bulkSmsLimitPertime).Take(bulkSmsLimitPertime).ToList();
        //        var multiSmsReq = GetMultiSmsRequest(messages);
        //        var stringContent = GetSmsPostData(multiSmsReq);
        //        var responseText = MaraditSms(stringContent).GetAwaiter().GetResult();
        //        var ssr = JsonConvert.DeserializeObject<SmsSubmitResponse>(responseText);

        //        if (ssr.Response.Status.Code != 200)
        //        {
        //            resultList.Add(string.Format("errorcode:{0}", ssr.Response.Status.Code.ToString()));
        //        }
        //        //if (wsSendMessageResponse.errorDescription.Any())
        //        //{
        //        //    resultList.AddRange(wsSendMessageResponse.errorDescription.Select(r => string.Format("errordesc:{0}", r)));
        //        //}
        //        //if (wsSendMessageResponse.statusCode.Any())
        //        //{
        //        //    resultList.AddRange(wsSendMessageResponse.statusCode.Select(r => string.Format("statuscode:{0}", r)));
        //        //}

        //        var itrRecList = recIdArray.Skip(j * bulkSmsLimitPertime).Take(bulkSmsLimitPertime).ToArray();
        //        var dtTemp = CreateResponseTable();
        //        for (var i = 0; i < messages.Count; i++)
        //        {
        //            var dr = dtTemp.NewRow();
        //            dr["RecId"] = itrRecList[i];
        //            dr["SessionId"] = sessionId;
        //            dr["ErrorDesc"] = ssr.Response.Status.Code +
        //                "-" + ssr.Response.Status.Description;
        //            dr["MessageId"] = ssr.Response.Status.Code;
        //            dr["Status"] = ssr.Response.Status.Code.ToString();

        //            dtTemp.Rows.Add(dr);
        //        }

        //        new ContactUipc().SendBulkSmsReport(dtTemp);
        //    }

        //    return resultList;
        //}

        public List<string> SendSmsWithGsmNo(List<GsmMessage> messageList)
        {
            var wsContentArray = messageList.Select(c => c.Content).ToArray();
            var wsGsmArray = messageList.Select(c => c.GsmNumber).ToArray();
            var recIdArray = GetRecIdArray(messageList);
            var startTime = DateTime.Now.ToString("yyyy.MM.dd HH:mm");
            var endTime = DateTime.Now.AddDays(1).ToString("yyyy.MM.dd HH:mm");

            if (wsGsmArray.Length != wsContentArray.Length)
            {
                const string error = "Gonderilmek istenen message sayisi ile GSM numarasi sayisi birbirinden farklidir. Lutfen dosyayi kontrol ediniz.";
                Logger.Error(error);
                return new List<string>() { error };
            }

            var resultList = new List<string>();
            var iterationCount = (messageList.Count() / bulkSmsLimitPertime) + (messageList.Count % bulkSmsLimitPertime > 0 ? 1 : 0);

            for (var j = 0; j < iterationCount; j++)
            {
                var service = new ResellerPlatformWebService_V2PortTypeClient();
                var messageBody = wsContentArray.Skip(j * bulkSmsLimitPertime).Take(bulkSmsLimitPertime).ToArray();
                var toReceivers = wsGsmArray.Skip(j * bulkSmsLimitPertime).Take(bulkSmsLimitPertime).ToArray();
                SendSMSOutput wsSendMessageResponse;
                using (new OperationContextScope(service.InnerChannel))
                {
                    OperationContext.Current.OutgoingMessageHeaders.Add(
                        MessageHeader.CreateHeader("sessionId", "", sessionId));
                    wsSendMessageResponse = service.sendShortMessage("1", CpMsisdn, startTime, endTime, OriginatingAddress, toReceivers, messageBody);
                }

                if (wsSendMessageResponse.errorCode.Any())
                {
                    resultList.AddRange(wsSendMessageResponse.errorCode.Select(r => string.Format("errorcode:{0}", r)));
                }
                if (wsSendMessageResponse.errorDescription.Any())
                {
                    resultList.AddRange(wsSendMessageResponse.errorDescription.Select(r => string.Format("errordesc:{0}", r)));
                }
                if (wsSendMessageResponse.statusCode.Any())
                {
                    resultList.AddRange(wsSendMessageResponse.statusCode.Select(r => string.Format("statuscode:{0}", r)));
                }

                var itrRecList = recIdArray.Skip(j * bulkSmsLimitPertime).Take(bulkSmsLimitPertime).ToArray();
                var dtTemp = CreateResponseTable();
                for (var i = 0; i < wsGsmArray.Length; i++)
                {
                    var dr = dtTemp.NewRow();
                    dr["RecId"] = itrRecList[i];
                    dr["SessionId"] = sessionId;
                    dr["ErrorDesc"] = wsSendMessageResponse.errorCode[i] +
                        "-" + wsSendMessageResponse.errorDescription[i];
                    dr["MessageId"] = wsSendMessageResponse.errorCode[i];
                    dr["Status"] = wsSendMessageResponse.statusCode[i].ToString();

                    dtTemp.Rows.Add(dr);
                }

                new ContactUipc().SendBulkSmsReport(dtTemp);
            }

            return resultList;
        }

        private static DataTable CreateResponseTable()
        {
            var sampleDataTable = new DataTable();

            sampleDataTable.Columns.Add("RecId", typeof(string));
            sampleDataTable.Columns.Add("SessionId", typeof(string));
            sampleDataTable.Columns.Add("ErrorDesc", typeof(string));
            sampleDataTable.Columns.Add("Status", typeof(string));
            sampleDataTable.Columns.Add("MessageId", typeof(string));
            return sampleDataTable;
        }

        private static string[] GetRecIdArray(IEnumerable<GsmMessage> list)
        {
            var recIds = list.Select(s => s.RecId).ToArray();
            var recIdlist = new string[recIds.Length];
            for (var i = 0; i < recIds.Length; i++)
            {
                recIdlist[i] = recIds[i];
            }
            return recIdlist;
        }

        #endregion
    }
}
