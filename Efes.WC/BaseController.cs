﻿using System.Web.Mvc;
using Efes.Utility;

namespace Efes.WC
{
    public class BaseController : Controller
    {

        public readonly log4net.ILog Logger = LogHelper.GetLogger();

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        public string MembersClientIp
        {
            get
            {
                return Request.ServerVariables["HTTP_CLIENT_IP"] ?? Request.UserHostAddress ?? string.Empty;
            }
        }

        protected string ForwardedIp
        {
            get
            {
                return Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? string.Empty;
            }
        }

        protected string ServerIp
        {
            get
            {
                return Request.ServerVariables["LOCAL_ADDR"] ?? string.Empty;
            }
        }

        private int _referer;
        public int Referer
        {
            get
            {
                if (CacheManager.Default.Get("Referer"+ Request.BoUniqueId()) != null)
                {
                    _referer = CacheManager.Default.Get("Referer" + Request.BoUniqueId()).Convert<int>();
                }

                return _referer;
            }
        }
    }
}
