﻿using System;
using System.Linq;
using System.Web;
using Efes.Entities.User;
using Efes.UIPC;
using Efes.Utility;

namespace Efes.WC
{
    public class Authentication
    {
        private static readonly UserUipc _userUipc;

        public Authentication()
        {

        }

        #region Static Properties

        /// <summary>
        /// Session'da tutulan üyelik bilgilerine erisimi saglar
        /// </summary>
        public static User UserInfo
        {
            get
            {
                //CacheManager.Default.Add(string.Format("member_{0}", user.Id), user, TokenExpireInSecond);
                var user = CacheManager.Default.Get("UserInfo_" + HttpContext.Current.Request.BoUniqueId()) as User;
                if (user == null)
                {
                    var authToken = AuthManager.GetAuthToken();
                    if (authToken != null)
                    {
                        UserInfo = new UserUipc().GetUser(authToken.Id);
                        return UserInfo;
                    }
                }
                return user;
            }
            set
            {
                CacheManager.Default.Add("UserInfo_" + HttpContext.Current.Request.BoUniqueId(), value, new AppSettingUipc().AuthTokenExpireInSecond);
            }
        }

        public static String[] UserRules {
            get
            {
                return UserInfo.Rules.Any() ? UserInfo.Rules.Select(r => r.Value.Replace(" ", "")).ToArray() : new string[] { };
            }
        }

        public static String[] UserRoles
        {
            get
            {
                return UserInfo.Roles.Any() ? UserInfo.Roles.Select(r => r.Value.Replace(" ", "")).ToArray() : new string[] { };
            }
        }

        public static bool IsAllowedToSee(string rules)
        {
            return rules.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries).Any(x => UserRules.Contains(x.Trim()) || UserInfo.IsAdmin);
        }

        #endregion

        public void Logout()
        {
            AuthManager.LogoutMember();
        }

        public static User AuthenticateUser(string userName, string password, string sessionId)
        {
            User userInfo;
            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                userInfo = new User
                {
                    ExceptionInfo =
                    {
                        Code = "0", 
                        Message = "UserName and/or Password are empty",
                        UserMessage = "Kullanici ismi ve/veya Sifre bos"
                    }
                };

                return userInfo;
            }

            userInfo = new UserUipc().GetUser(userName, password);

            if (userInfo != null && !userInfo.IsLockedOut && userInfo.IsSecurePassword)
            {
                if (HttpContext.Current != null)
                {
                    //Licence
                    var date = DateTime.Now.ToShortDateString();
                    var lresult = false;
                    if (CacheManager.Default.Contains("li" + date))
                    {
                        lresult = CacheManager.Default.Get("li" + date).Convert<bool>();
                    }

                    //Sleep Timeout
                    var timeout = 0;
                    if (CacheManager.Default.Contains("to" + date))
                    {
                        timeout = CacheManager.Default.Get("to" + date).Convert<int>();
                    }

                    CacheManager.Default.Flush();
                    CacheManager.Default.Add("li" + date, lresult, 600);
                    CacheManager.Default.Add("to" + date, timeout, 600);

                    AuthManager.AuthenticateMember(userInfo, userName);
                }
                new UserUipc().InsertMemberLog(userInfo.Id, UserLogType.Login);
            }
            return userInfo;
        }
    }
}
