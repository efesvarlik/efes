﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Efes.Utility;

namespace Efes.WC
{
    //Public yapmayiniz, site icersindeki islemler icin CryptoUtilities classini kullaniniz
    internal class AuthSecurity
    {
        public static readonly log4net.ILog log4 = LogHelper.GetLogger();
        private static byte[] EnKey
        {
            get
            {
                var tokenCypher = string.Empty;
                // key degisiminde farkli sunucularda farkli key olmamasi icin token key memcacheden okunur
                //var tokenCypher = CacheManager.Default.Get("TokenCypher") as string;

                if (string.IsNullOrWhiteSpace(tokenCypher))
                {
                    //Appsettings runtime cacheden alınır
                    //cache suresi 2dakikadir
                    //tokenCypher = AppSettings.GetValue("TokenCypher");

                    //memcache, httpRuntime veya  db'de null ise static set edilir
                    if (string.IsNullOrWhiteSpace(tokenCypher))
                    {
                        tokenCypher = "p?b!QnTr*5p@<oMq!u8R1vK&+@8ToaHP";
                    }

                    //runtime cacheden okundugu icin memcache suresi kisa tutulmustur
                    //CacheManager.Default.Add("TokenCypher", tokenCypher, 30);
                }

                return Encoding.UTF8.GetBytes(tokenCypher);
            }
        }

        private static byte[] HMACKey
        {
            get
            {
                return Encoding.UTF8.GetBytes("!rXNmq@W");
            }
        }

        public static string SignWithHashHMAC(string plainMessage)
        {
            try
            {
                byte[] binaryMessage = Encoding.UTF8.GetBytes(plainMessage);

                var hMac = HashHMAC(binaryMessage);

                return Convert.ToBase64String(binaryMessage) + "." + hMac;

            }
            catch (Exception e)
            {
                LogAuthException(e, "SignWithHashHMAC", plainMessage);
            }

            return null;
        }


        public static string ValidateHMACAndGetPlainMessage(string signedMessage)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(signedMessage))
                {
                    var tokenAndSign = signedMessage.Split('.');

                    string clientBase64Token = tokenAndSign[0];

                    string clientHMACSign = tokenAndSign[1];

                    byte[] clientBinaryToken = Convert.FromBase64String(clientBase64Token);

                    string serverHMACSign = HashHMAC(clientBinaryToken);

                    if (clientHMACSign == serverHMACSign)
                    {
                        return Encoding.UTF8.GetString(clientBinaryToken);
                    }
                }
            }
            catch (Exception e)
            {
                LogAuthException(e, "ValidateHMACAndGetPlainMessage", signedMessage);
            }

            return null;
        }


        private static string HashHMAC(byte[] bmessage)
        {
            try
            {
                var hash = new HMACSHA256(HMACKey);
                var hMac = hash.ComputeHash(bmessage);
                return Convert.ToBase64String(hMac);
            }
            catch (Exception e)
            {
                LogAuthException(e, "HashHMAC");
            }

            return null;
        }

        private static SymmetricAlgorithm CreateCrypto()
        {
            SymmetricAlgorithm algorithm = new AesManaged();
            algorithm.Key = EnKey;
            algorithm.IV = new byte[algorithm.IV.Length];
            return algorithm;
        }

        public static string Decrypt(string cipher)
        {
            if (string.IsNullOrEmpty(cipher))
            {
                return null;
            }

            try
            {
                using (var algorithm = CreateCrypto())
                {
                    return Encoding.UTF8.GetString(Transform(algorithm.CreateDecryptor(), Convert.FromBase64String(cipher)));
                }
            }
            catch (Exception e)
            {
                LogAuthException(e, "Decrypt", cipher);
            }

            return null;
        }



        public static string Encrypt(string plain)
        {
            if (string.IsNullOrEmpty(plain))
            {
                return null;
            }

            try
            {
                using (var algorithm = CreateCrypto())
                {
                    return Convert.ToBase64String(Transform(algorithm.CreateEncryptor(), Encoding.UTF8.GetBytes(plain)));
                }
            }
            catch (Exception e)
            {
                LogAuthException(e, "Encrypt", plain);
            }

            return null;
        }


        private static byte[] Transform(ICryptoTransform transformer, byte[] data)
        {
            byte[] buffer;

            using (var stream = new MemoryStream())
            {
                using (var stream2 = new CryptoStream(stream, transformer, CryptoStreamMode.Write))
                {
                    stream2.Write(data, 0, data.Length);
                    stream2.FlushFinalBlock();
                    buffer = stream.ToArray();
                }
            }
            return buffer;
        }

        private static void LogAuthException(Exception e, string funtionTitle, string affectedText = "")
        {
            try
            {
                var originalHeader = HttpContext.Current.Request.ServerVariables["ALL_HTTP"] ?? "";

                affectedText = affectedText ?? "";

                var message = funtionTitle + " isleminde hata alindi.{0}" + e.ToString() + "{0}AffectedText:{0}" + affectedText + "{0}ALL_HTTP:{0}" + originalHeader;

                message = string.Format(message, Environment.NewLine + Environment.NewLine);

                log4.Fatal( "AuthSecurity - EventLogAndEmail" + message);
            }
            catch (Exception ex)
            {
                log4.Fatal(ex);
            }
        }

    }
}
