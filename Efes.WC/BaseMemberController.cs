using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Efes.UIPC;

namespace Efes.WC
{
    public class BaseMemberController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (AuthManager.IsAuthenticated)
            {
                return;
            }

            var httpContext = filterContext.HttpContext;
            var request = httpContext.Request;

            filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(
                       new
                       {
                           controller = "Account",
                           action = "Login",
                           r = request.Url == null ? "/" : request.Url.PathAndQuery
                       })
                   );
            var response = HttpContext.Response;
            response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
            response.SuppressFormsAuthenticationRedirect = true;
        }

    }
}