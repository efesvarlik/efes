﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Utility;

namespace Efes.SQLOperations
{
    class Program
    {
        static int Main(string[] args)
        {
            var retval = 0;
            if (args.Length == 0)
            {
                return 0;
            }

            //log4net.Config.XmlConfigurator.Configure();
            //Utilities.SetAdoNetAppenderConnectionStrings();

            switch (args[0])
            {
                case "/SendMemberSms":
                    var o = new Classes.SendMemberSms();
                    retval = (o.Execute() ? 0 : 1);
                    break;
                case "/Krm":
                    var k = new Classes.SendKrmReport();
                    retval = (k.Execute() ? 0 : 1);
                    break;
            }
            return retval;
        }
    }
}
