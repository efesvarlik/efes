﻿using System;
using System.Collections.Generic;
using System.Linq;
using Efes.Entities.Sms;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;

namespace Efes.SQLOperations.Classes
{
    public class SendMemberSms
    {
        public bool Execute()
        {
            try
            {
                var contactUipc = new ContactUipc();
                var smsList = contactUipc.GetSmsListForService();

                //var smsList = new List<GsmMessage> {
                //    new GsmMessage()
                //    {
                //        GsmNumber = "5427136371",
                //        Content = "Deneme Mesaji",
                //        RecId = "123123123"
                //    },
                //    new GsmMessage()
                //    {
                //        GsmNumber = "5414280199",
                //        Content = "Deneme Mesaji",
                //        RecId = "123123123"
                //    }
                //};

                if (smsList.Any(s => !string.IsNullOrWhiteSpace(s.GsmNumber)))
                {
                    var smsMessenger = new SmsMessenger();
                    smsMessenger.SendSmsJetSmsWithGsmNo(smsList);
                }

                return true;
            }
            catch (Exception ex)
            {
                ex.Log();
                return false;
            }

        }
    }
}
