﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using Efes.UIPC;
using Efes.Utility;
using log4net;

namespace Efes.SQLOperations.Classes
{
    [Serializable]
    public class SendKrmReport
    {
        private const string DirectoryName = @"C:\";
        private const int BufferSize = 4096;
        public readonly ILog Logger = LogHelper.GetLogger();
        private string _txtFileName = "";
        private string _zipFileName = "";
        public bool Execute()
        {
            var file = new KrmUipc().GetKrmFileInformationsByDate();
            if (file != null)
            {
                _txtFileName = string.Format("{0}{1}txt", DirectoryName, GetFileName());
                _zipFileName = string.Format("{0}{1}zip", DirectoryName, GetFileName());
                File.Create(_txtFileName).Close();
                using (var address = new StreamWriter(_txtFileName, true, Encoding.GetEncoding(1252)))
                {
                    //Header
                    address.Write(file.Header.FileHeaderRecord);
                    address.Write(file.Header.VersionNumber);
                    address.Write(file.Header.MemberCode);
                    address.Write(file.Header.ReserveField1);
                    address.Write(file.Header.MemberName);
                    address.Write(file.Header.CreationDate);
                    address.Write(file.Header.DeclarationDate);
                    address.Write(file.Header.ReserveField2);
                    address.Write(Environment.NewLine);

                    foreach (var csvym in file.CSVYMList)
                    {
                        //CSVYM
                        address.Write(csvym.RecordType);
                        address.Write(csvym.VersionNumber);
                        address.Write(csvym.MemberCode);
                        address.Write(csvym.IdentityType);
                        address.Write(csvym.IdentityNumber);

                        address.Write(csvym.Name);
                        address.Write(csvym.SecondaryName);
                        address.Write(csvym.Surname);
                        address.Write(csvym.Address1);
                        address.Write(csvym.Address2);
                        address.Write(csvym.PhoneNumber1);
                        address.Write(csvym.PhoneNumber2);
                        address.Write(csvym.PhoneNumber3);
                        address.Write(csvym.Email1);
                        address.Write(csvym.Email2);
                        address.Write(csvym.PaymentPlanDate);
                        address.Write(csvym.PaymentPlanInstallmentCount);
                        address.Write(csvym.PayedInstallmentCount);
                        address.Write(csvym.PayedDebt);
                        address.Write(csvym.RemainingDebt);
                        address.Write(csvym.CurrencyCode);

                        address.Write(csvym.RevisedPaymentPlanDate);
                        address.Write(csvym.RevisedPaymentPlanInstallmentCount);
                        address.Write(csvym.RevisedPayedInstallmentCount);
                        address.Write(csvym.RevisedRemainingDebt);

                        address.Write(csvym.FirstExecutiveDate);
                        address.Write(csvym.LastExecutiveDate);
                        address.Write(csvym.ExecutiveAmount);
                        address.Write(csvym.DebtStatus);
                        address.Write(csvym.DebtStatusAlterationStatus);
                        address.Write(csvym.ClosureDate);
                        address.Write(csvym.ReserveField);
                        address.Write(Environment.NewLine);

                        //CSVYD
                        foreach (var csvyd in csvym.CSVYDList)
                        {
                            address.Write(csvyd.RecordType);
                            address.Write(csvyd.VersionNumber);
                            address.Write(csvyd.MemberCode);
                            address.Write(csvyd.IdentityType);
                            address.Write(csvyd.IdentityNumber);

                            address.Write(csvyd.AccountNumber);
                            address.Write(csvyd.TransferDate);
                            address.Write(csvyd.TransferMemberCode);
                            address.Write(csvyd.PrincipalBalanceTransferedIn);
                            address.Write(csvyd.CurrencyCode);
                            address.Write(csvyd.AccountOpeningDate);
                            address.Write(csvyd.ReserveField);
                            address.Write(Environment.NewLine);
                        }
                    }

                    //Footer

                    address.Write(file.Footer.FooterIdentifier);
                    address.Write(file.Footer.VersionNumber);
                    address.Write(file.Footer.MemberCode);
                    address.Write(file.Footer.ReserveField1);
                    address.Write(file.Footer.TotalCountOfCustomerRecords);
                    address.Write(file.Footer.ReserveField2);

                }
                ZipHelper.Zip(_txtFileName, _zipFileName, BufferSize);
                //SendFileViaFtp();

                return true;
            }

            Logger.Error("KRM dosya gonderimi veritabanindan kayit gelmediginden dolayi yapilamadi.");
            return false;

        }

        private void SendFileViaFtp()
        {
            var krmFtpUrl = ConfigurationManager.AppSettings["KrmFtpUrl"] + "/BIREYSEL/GUNLUK_BILDIRIM/DATA";
            var krmFtpUserName = ConfigurationManager.AppSettings["KrmFtpUserName"];
            var krmFtpPassword = ConfigurationManager.AppSettings["KrmFtpPassword"];
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            var request = (FtpWebRequest)WebRequest.Create(string.Format("{0}/{1}", krmFtpUrl, Path.GetFileName(_zipFileName))) ;
            request.EnableSsl = true;
            request.Method = WebRequestMethods.Ftp.UploadFile;

            request.Credentials = new NetworkCredential(krmFtpUserName, krmFtpPassword);

            var sourceStream = new StreamReader(_zipFileName);
            var fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            sourceStream.Close();
            request.ContentLength = fileContents.Length;

            using (var requestStream = request.GetRequestStream())
            {
                using (var input = File.OpenRead(_zipFileName))
                {
                    input.CopyTo(requestStream);
                }
                requestStream.Write(fileContents, 0, fileContents.Length);
            }

            var response = (FtpWebResponse)request.GetResponse();
            response.Close();

        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string GetFileName()
        {
            var dt = DateTime.Now;
            return string.Format("KRSGUN_00858_{0}{1}{2}.", dt.Year, dt.Month.FormatMonthAndDay(), dt.Day.FormatMonthAndDay());
        }
    }
}
