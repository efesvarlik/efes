﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities;
using Efes.Entities.User;

namespace Efes.Models
{
    public class UserHistoryLoginModel
    {
        public List<User> UserList { get; set; }
        public string SelectedAgent { get; set; }
        public List<LoginHistory> LoginHistoryList { get; set; }

        public UserHistoryLoginModel()
        {
            LoginHistoryList = new List<LoginHistory>();
        }
    }
}
