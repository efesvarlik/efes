﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities;

namespace Efes.Models
{
    public class LoosePaymentModel
    {
        public List<LoosePayment> LoosePaymentList { get; set; }
        public List<PaymentPlanType> PaymentPlanTypes { get; set; }

        public LoosePaymentModel()
        {
            LoosePaymentList = new List<LoosePayment>();
            PaymentPlanTypes = new List<PaymentPlanType>();
        }
    }
}
