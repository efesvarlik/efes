﻿using Efes.Entities.Protocol;

namespace Efes.Models
{
    public class ProtocolModel: Protocol
    {
        public int UserId { get; set; }
        public int Status { get; set; }
        public int ObligorTypeId { get; set; }
        public int RoleIdMakingProtocol { get; set; }
        public bool SendInstallmentContract { get; set; }
        public bool SendSulhContract { get; set; }
        public bool SendPaymentPlanToObligor { get; set; }
        public bool SmsProtocolReminder { get; set; }
    }
}
