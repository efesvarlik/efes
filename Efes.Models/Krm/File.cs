﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities.Krm;

namespace Efes.Models.Krm
{
    public class File
    {
        public Header Header { get; set; }
        public List<CSVYM> CSVYMList { get; set; }
        public Footer Footer { get; set; }

        public File()
        {
            Header = new Header();
            CSVYMList = new List<CSVYM>();
            Footer = new Footer();
        }
    }
}
