﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities.Portfolio;
using Efes.Entities.Protocol;
using Efes.Entities.User;

namespace Efes.Models
{
    [Serializable]
    public class ProtocolCampaignModel : ProtocolCampaign
    {
        public int ObligorCount { get; set; }
        public string StartDateView { get; set; }
        public string EndDateView { get; set; }
        public string FirstAdvancePaymentDateView { get; set; }
        public string FirstInstallmentDateView { get; set; }
        public List<Role> RolesHasRightToMakeCampaign { get; set; }
        public int DiscountPercentage { get; set; }
        public int MaxInstallmentCount { get; set; }
        public List<Portfolio> Portfolios { get; set; }
        public string Creator { get; set; }
        public string Updater { get; set; }
        public string Portfolio { get; set; }
        public int RoleMakingProtocol { get; set; }
        public ProtocolCampaignModel()
        {
            RolesHasRightToMakeCampaign = new List<Role>();
        }
    }
}
