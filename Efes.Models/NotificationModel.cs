﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Models
{
    public class NotificationModel
    {
        public int Id { get; set; }
        public bool IsRead { get; set; }
        public string Title { get; set; }
        public string From { get; set; }
        public string Body { get; set; }
        public string TimeText { get; set; }
        public string IconType { get; set; }
        public string IconBgType { get; set; }
        public int Type { get; set; }
    }
}
