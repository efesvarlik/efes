﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Models
{
    public class AuditModel
    {
        public int Id { get; set; }
        public string ServerName { get; set; }
        public DateTime StartTime { get; set; }
        public TimestampInformation RowVersion { get; set; }
        public int Duration { get; set; }
        public string ApplicationName { get; set; }
        public string ClientHostName { get; set; }
        public string LoginName { get; set; }
        public string DatabaseName { get; set; }
        public int DatabaseId { get; set; }
        public bool Success { get; set; }
        public int ObjectId { get; set; }
        public string SchemaName { get; set; }
        public string ObjectName { get; set; }
        public long TransactionId { get; set; }
        public string TextData { get; set; }
        public string TextDataSmall { get; set; }
        public int Operation { get; set; }
        public string AccessedObjectsList { get; set; }
        public int Error { get; set; }
        public int IsInternalEvent { get; set; }
        public int EventTypeId { get; set; }
    }
}
