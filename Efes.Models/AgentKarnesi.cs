﻿using Efes.Entities.Report;
using Efes.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Models
{
    public class AgentKarnesiModel
    {
        public int SelectedAgent { get; set; }
        public int SelectedYear { get; set; }
        public int SelectedMonth { get; set; }
        public List<User> UserList { get; set; }
        public List<int> Years { get; set; }
        public List<BeklentiTahsilat> BeklentiTahsilatList { get; set; }
        public List<Tahsilat> TahsilatList { get; set; }
        public CikarilanGKF CikarilanGKF { get; set; }
        public string TahsilatOperasyonOzet { get; set; }
    }
}
