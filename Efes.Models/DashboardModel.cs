﻿using System.Collections.Generic;
using Efes.Entities.Home;

namespace Efes.Models
{
    public class DashboardModel
    {
        public List<DashboardSummary> SummaryList { get; set; }
        public Dictionary<string, int> CallCount { get; set; }
        public Dictionary<string, int> MoneyCollection { get; set; }
        public Dictionary<string, int> MoneyCollectionBottomStatictics { get; set; }
        public List<TargetAndActualCollection> TargetAndActualCollectionList { get; set; }
    }
}
