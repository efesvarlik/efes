﻿using System.Collections.Generic;
using Efes.Entities.SmartCalling;
using Efes.Entities.User;

namespace Efes.Models
{
    public class CreateDialerModel
    {
        public List<User> UnselectedUsers { get; set; }
        public List<User> SelectedUsers { get; set; }
        public List<string> AccountIdList { get; set; }
        public DialerStatus DialerStatus { get; set; }
    }
}
