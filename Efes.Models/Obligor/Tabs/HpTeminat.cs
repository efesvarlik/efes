﻿using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class HpTeminat
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string FoyNo { get; set; }
        public string KisiKurumAdi { get; set; }
        public string TeminatTuru { get; set; }
        public string TeminatCinsi { get; set; }
        public string Tasinir { get; set; }
        public string Tasinmaz { get; set; }
        public string Takyidat { get; set; }
        public string ExpertizDegeri { get; set; }
        public string ExpertizTarihi { get; set; }
        public string Tutari { get; set; }
        public string BulunduguYer { get; set; }
        public string Derece { get; set; }
        public string Il { get; set; }
        public string KTR { get; set; }
        public string Maliki { get; set; }
        public string Marka { get; set; }
        public string Model { get; set; }
        public string Muhafaza { get; set; }
        public string Plaka { get; set; }
        public string RehinSozlesmeTarihi { get; set; }
        public string SatisGunu1 { get; set; }
        public string SatisGunu2 { get; set; }
        public string SatisSonucu { get; set; }
    }
}