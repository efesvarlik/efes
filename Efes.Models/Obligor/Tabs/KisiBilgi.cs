using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class KisiBilgi
    {
        public string Id { get; set; }
        public string Unvan { get; set; }
        public string GorunenAd { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string DogumTarihi { get; set; }
        public string KisiTipiTanimi { get; set; }
        public string BireyKurum { get; set; }
        public string PortfoyNo { get; set; }
        public string TcKimlikNo { get; set; }
        public string VergiNo { get; set; }
    }
}