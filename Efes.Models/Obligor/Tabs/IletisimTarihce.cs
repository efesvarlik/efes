using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class IletisimTarihce
    {
        public string Id { get; set; }
        public string Tarih { get; set; } //27/11/2015 15:13
        public string Agent { get; set; } //emavzer
        public string Telefon { get; set; } //53312124123
        public string Sonuc { get; set; } //7.Borcluya ulasildi
        public string KonusulanKisi { get; set; }//Borclu
        public string KonusulanKisiAdi { get; set; } //Gokhan Ozen
        public string GorunenNumara { get; set; }
        public string NumaramiGoster { get; set; } //Isnet
        public string GorunenNumaraKaynagi { get; set; }
        public int DegerlendirmeNotu { get; set; } //0.00
        public string FilePath { get; set; } //0.00
        public string ModifiedFilePath
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    if (FilePath.StartsWith("sesware.rec.efesvarlik.gm"))
                    {
                        return "http://" + FilePath;
                    }
                    else
                    {
                        return "http://old.rec.efesvarlik.gm" + FilePath;
                    }
                }
                return FilePath;
            }
        }

        //public string ShortenedFilePath
        //{
        //    get
        //    {
        //        if (string.IsNullOrWhiteSpace(FilePath)) return string.Empty;

        //        return FilePath.Length > 50 ? string.Format("{0}...", this.FilePath.Substring(0, 49)) : this.FilePath;
        //    }
        //} //0.00
    }
}