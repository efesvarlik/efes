using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class Adres
    {
        public string Id { get; set; }
        public string Amac { get; set; }
        public string Address { get; set; }
        public string Ilce { get; set; }
        public string Sehir { get; set; }
        public string PostaKodu { get; set; }
        public string Ulke { get; set; }
        public string OlusturanKisi { get; set; }
        public string OlusturmaTarihi { get; set; }
        public string DegistirenKisi { get; set; }
        public string DegistirmeTarihi { get; set; }
    }
}