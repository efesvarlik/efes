using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class ProtokolTarihce
    {
        public string RecId { get; set; }
        public int PayPlanId { get; set; }
        public DateTime PlanDate { get; set; }
        public DateTime PesinDate { get; set; }
        public DateTime InstallmentStartDate { get; set; }
        public string PlanDateStr
        {
            get { return PlanDate.ToShortDateString(); }
        }
        public string PesinDateStr
        {
            get { return PesinDate.ToShortDateString(); }
        }
        public string InstallmentStartDateStr
        {
            get { return InstallmentStartDate.ToShortDateString(); }
        }

        public decimal PesinAmount { get; set; }
        public decimal InstallmentAmount { get; set; }
        public decimal MaxInstallmentCount { get; set; }
        public decimal PlanAmount { get; set; }
        public string StatusText { get; set; }
        public string Owner { get; set; }
        public string StatusReason { get; set; }
        public int CampaignId { get; set; }
        public string Description { get; set; }
        public decimal DebtAmount { get; set; }
        public decimal DebtFees { get; set; }
        public decimal DebtInterest { get; set; }
        public decimal DebtPrincipal { get; set; }
        public decimal CreditAmount { get; set; }


    }
}