﻿using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class TumPortfoydekiBorclari
    {
        public string Id { get; set; }
        public string GorunenAd { get; set; }
        public string BorcIleIliskisi { get; set; }
        public string IcraDairesi { get; set; }
        public string IcraDosya { get; set; }
        public string Banka { get; set; }
        public string Portfoy { get; set; }
        public string BorcDurum { get; set; }
        public string BorcDurumDetay { get; set; }
        public string DosyaSahibi { get; set; }
        public decimal ToplamAnapara { get; set; }
        public decimal BorcAnapara { get; set; }
        public decimal BakiyeAnapara { get; set; }
        public bool MasterDept { get; set; }
        public string KisiDurumu { get; set; }
    }
}