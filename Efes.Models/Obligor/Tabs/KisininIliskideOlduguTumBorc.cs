using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class KisininIliskideOlduguTumBorc
    {
        public string Id { get; set; }
        public string PriorutyGroup { get; set; }
        public string BorcIleIliskisi { get; set; }
        public string EnBuyukBorc { get; set; }
        public string AktifTaskSayisi { get; set; }
        public string PasifTaskSayisi { get; set; }
        public string GorunenAd { get; set; }
        public string OncekiAksiyonSonucu { get; set; }
        public string AccountId { get; set; }
        public string ContactTcknNo { get; set; }
        public string IcraDairesi { get; set; }
        public string IcraDosyaNumarasi { get; set; }
        public string Banka { get; set; }
        public string Portfoy { get; set; }
        public string BorcDurum { get; set; }
        public string BorcDurumDetay { get; set; }
        public string DosyaSahibi { get; set; }
        public decimal ToplamAnapara { get; set; }
        public decimal BorcAnapara { get; set; }
        public decimal BakiyeAnapara { get; set; }
        public string VergiNo { get; set; }
        public string BabaAdi { get; set; }
        public string KisiDurumu { get; set; }
    }
}