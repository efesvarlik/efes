using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class Borc
    {
        public string RecId { get; set; }
        public string AccountId { get; set; }
        public string BorcTipi { get; set; }
        public decimal ToplamRisk { get; set; }
        public decimal Anapara { get; set; }
        public decimal Faiz { get; set; }
        public decimal Masraflar { get; set; }
        public string Durum { get; set; }
        public string IcraDaireNo { get; set; }
        public string IcraFileId { get; set; }
        public decimal Masraf { get; set; }
        public DateTime TemerrutTarihi { get; set; }
        public DateTime DevirTarihi { get; set; }
        public string TemerrutTarihiStr 
        {
            get { return TemerrutTarihi.ToShortDateString(); }
        }
        public string DevirTarihiStr
        {
            get { return DevirTarihi.ToShortDateString(); }
        }
        public string Sube { get; set; }
        public string BorcNo { get; set; }
		public string KrediKartNo { get; set; }
		public string FoyNo { get; set; }
    }
}