﻿using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class HpHacizliMal
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string FoyNo { get; set; }
        public string TeminatTuru { get; set; }
        public string TeminatCinsi { get; set; }
        public string SatisGunu1 { get; set; }
        public string SatisGunu2 { get; set; }
        public string HacizTarihi { get; set; }
        public string Tasinir { get; set; }
        public string Tasinmaz { get; set; }
    }
}