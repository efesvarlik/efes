using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class Phone
    {
        public string Id { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string LastResultCode { get; set; }
        [Required]
        public string RelatedPerson { get; set; }
        [Required]
        public string CalledPerson { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Extension { get; set; }
        [Required]
        public string InformationSource { get; set; }
        [Required]
        public string Creator { get; set; }
        [Required]
        public string Updater { get; set; }
        [Required]
        public string CreatedDate { get; set; }
        [Required]
        public string LastCommunicationDate { get; set; }
    }
}