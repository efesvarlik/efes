﻿using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class Note
    {
        public string Id { get; set; }
        public string NotTarihi { get; set; }
        public string Detay { get; set; }
        public string Olusturan { get; set; }
        public int Tip { get; set; }
        public int UserId { get; set; }
        public bool IsDanger { get; set; }
    }

    [Serializable]
    public class DeleteNoteResult {
        public bool IsDeleted { get; set; }
        public string Message { get; set; }
    }
}