using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class Ozellik
    {
        public string Id { get; set; }
        public string Property { get; set; }
        public string Value { get; set; }
    }
}