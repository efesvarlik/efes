using System;

namespace Efes.Models.Obligor.Tabs
{
	[Serializable]
	public class PostaGonderi
	{
		public string Id { get; set; }
		public string AccountId { get; set; }
		public string AdSoyad { get; set; }
		public string GonderiTipi { get; set; }
		public string Adres { get; set; }
		public string Il { get; set; }
		public string Ilce { get; set; }
		public string ReportPage { get; set; }
		public string CreatedBy { get; set; }
		public string CreatedDateTime { get; set; }
		public bool IsSendToOperation { get; set; }
	}
}