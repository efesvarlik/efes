﻿using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class YapilacakIs
    {
        public int Id { get; set; }
        public string OncekiSonuc { get; set; }
        public string YapilacakIsDesc  { get; set; }
        public string SorumluEkip { get; set; }
        public string SorumluKisi { get; set; }
        public string GorevlendirmeTarihi { get; set; }
        public string UstlenmeTarihi { get; set; }
        public string HedefTarihi { get; set; }
        public string Durum { get; set; }
    }
}
