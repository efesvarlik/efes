using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class IslemTarihce
    {
        public string Id { get; set; }
        public string GorevlendirmeTarihi { get; set; }
        public string UstlenmeTarihi { get; set; }
        public string TamamlamaTarihi { get; set; }
        public string Amac { get; set; }
        public string Sonuc{ get; set; }
        public string SorumluEkip { get; set; }
        public string Uzman { get; set; }
        public string Durum { get; set; }
        public string OncekiUzman { get; set; }
        public string OncekiEkip { get; set; }
        public string OncekiSonuc { get; set; }
        public string OncekiSonucKodu { get; set; }

    }
}