using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class DosyaEk
    {
        public string RecId { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaAciklamasi { get; set; }
        public string OlusturmaTarihi { get; set; }
        public string Olusturan { get; set; }
        public string AccountLinkRecId { get; set; }
        public string AttachmentPath { get; set; }
    }
}