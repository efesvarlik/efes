using System;
using System.Xml.Schema;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class AllPayments
    {
        public string Id { get; set; }
        public string PaymentDate { get; set; }
        public decimal Value{ get; set; }
        public string Bank { get; set; }
        public string CollectionDesc { get; set; }
        public string EfsCollectionRow { get; set; }
        public string NameSurname { get; set; }
        public string PayerTckn { get; set; }
        public string FileOwner { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string CollectionJournalNumber { get; set; } //tahsilat yevmiye numaras�
        public string AccountingVoucherNumber { get; set; } //Muhasebe Fis No
        public string LastUpdaterUser { get; set; }
        public string LastUpdateDate { get; set; }
        public bool HasProtocoled { get; set; }
        public string HasProtocoledText { get; set; }
        public string PlanType { get; set; }
        public string PlanSubType { get; set; }
        public string CID { get; set; }
        public string StatusText { get; set; }
        public string MessageText { get; set; }
        public string FullName { get; set; }
        public string Tads { get; set; }
        public string AccountId { get; set; }
    }
}