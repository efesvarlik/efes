using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class PriorityGroupTracking
    {
        public string Id { get; set; }
        public string Degistiren { get; set; }
        public string CreatedDateTime { get; set; }
        public string CurrentPriority { get; set; }
        public string PriorityGroup { get; set; }
    }
}