using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class ProtokolTaksit
    {
        public string Id { get; set; }
        public string TaksitTarihiVadeli { get; set; }
        public string TaksitNo { get; set; }
        public decimal TaksitTutari { get; set; }
        public decimal Odenen { get; set; }
        public decimal Kalan { get; set; }
        public decimal TaksitTutariVfsiz { get; set; }
        public string Durum { get; set; }
        public string OrjinalTaksitTarihi { get; set; }
        public string TaksitDegistirmeNotu { get; set; }
        public string Desc { get; set; }
    }
}