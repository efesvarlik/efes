using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class BorcunIliskideOlduguTumKisi
    {
        public string Id { get; set; }
        public string GorunenAd { get; set; }
        public string ContactTypeText { get; set; }
        public decimal SorumluOlduguBorcMiktari { get; set; }
        public string IcraDairesi { get; set; }
        public string IcraDosyaNumarasi { get; set; }
        public decimal BalanceAmount { get; set; }
        public string SourceText { get; set; }
        public string SourceSet { get; set; }
        public string KisiDurumu { get; set; }
    }
}