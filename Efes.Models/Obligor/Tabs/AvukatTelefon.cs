using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class AvukatTelefon
    {
        public string Id { get; set; }
        public string Tip { get; set; }
        public string Telefon { get; set; }
        public string Dahili { get; set; }
        public string Olusturan { get; set; }
        public string Tarih { get; set; }
        public string SonIletisim { get; set; }
    }
}