﻿using System;

namespace Efes.Models.Obligor.Tabs.SmsHistory
{
    [Serializable]
    public class SmsHistory
    {
        public string Id { get; set; }
        public string Number { get; set; } 
        public string SmsTemplate { get; set; }
        public string SmsText { get; set; }
        public string ShortSmsText { get; set; }  
        public string Status { get; set; } 
        public int ErrorCode { get; set; } 
        public string ErrorDesc { get; set; } 
        public string PlannedSendDate { get; set; } 
        public string QueuedDate { get; set; } 
        public string ResultDate { get; set; } 
        public string SmsStatus { get; set; } 
        public string User { get; set; }
    }
}