using System;

namespace Efes.Models.Obligor.Tabs
{
    [Serializable]
    public class DosyaSahiplikTarihce
    {
        public string RecId { get; set; }
        public int Order { get; set; }
        public string AdSoyad { get; set; }
        public string LoginId { get; set; }
        public string Olusturan { get; set; }
        public string OlusturanTarih { get; set; }
        public string Degistiren { get; set; }
        public string DegistirenTarih { get; set; }
    }
}