﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Models.Obligor.Tabs;

namespace Efes.Models.Obligor
{
    public class AddressModel : Adres
    {
        public string AddressType { get; set; }
        public int CityId { get; set; }
        public int CountyId { get; set; }
    }
}
