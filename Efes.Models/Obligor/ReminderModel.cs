﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities.Obligor;

namespace Efes.Models.Obligor
{
    public class ReminderModel : Reminder
    {
        public string TypeName { get; set; }
        public bool IsPast { get; set; }
        public string Date { get; set; }
    }
}
