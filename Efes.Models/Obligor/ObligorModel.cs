﻿using System;
using System.Collections.Generic;
using Efes.Entities;
using Efes.Entities.Obligor;
using Efes.Entities.Protocol;
using Efes.Models.Obligor.Tabs;

namespace Efes.Models.Obligor
{
    [Serializable]
    public class ObligorModel : Entities.Obligor.Obligor
    {
        public ObligorModel()
        {
            this.TabList = new List<Tab>();
            //this.ProtocolList = new List<Protocol>();
            this.SmsTemplateDictionary = new Dictionary<string, string>();
            this.ProtocolCampaignModelList = new List<ProtocolCampaignModel>();
        }

        public string Kisi { get; set; }
        public string BorcTakipId { get; set; }
        public string DosyaNo { get; set; }
        public string BankaAdi { get; set; }
        public string Portfoy { get; set; }
        public string DosyaSahibi { get; set; }
        public string DabToplam { get; set; }
        public string DabAnapara { get; set; }
        public string DabFaiz { get; set; }
        public string DabMasraf { get; set; }
        public string DabEkMasraflar { get; set; }
        public string TeToplam { get; set; }
        public string TeAnapara { get; set; }
        public string TeFaiz { get; set; }
        public string TeMasraf { get; set; }
        public string TeSonTahsilatTarihi { get; set; }
        public string TeIadeAnapara { get; set; }
        public string BToplam { get; set; }
        public string BAnapara { get; set; }
        public string BFaiz { get; set; }
        public string BMasraf { get; set; }
        public string BHesFaiz { get; set; }
        public string BHesFaizTarihi { get; set; }
        public string KisiDurumu { get; set; }
        public string BankaMusteriNo { get; set; }
        public string Cinsiyet { get; set; }
        public string DogumTarihi { get; set; }
        public string AnneKizlikSoyadi { get; set; }
        public string BabaAdi { get; set; }
        public string EsininAdi { get; set; }
        public string IlkSoyadi { get; set; }
        public string MedeniDurumu { get; set; }
        public string AktifProtokolNo { get; set; }
        public string ProtokolToplam { get; set; }
        public string SonTahsilatTarihi { get; set; }
        public string AtananHukukBurosu { get; set; }
        public string AtanmaTarihi { get; set; }
        public string TemlikDuzeltmeTarihi { get; set; }
        public string Meslek { get; set; }
        public string CalismaDurumu { get; set; }
        public string OrtalamaGelir { get; set; }
        public string CocukSayisi { get; set; }
        public string Sgk { get; set; }
        public string SgkSicilNo { get; set; }
        public string IsyeriKodu { get; set; }
        public int KisiTipiId { get; set; }
        public string IcraDaireDosya { get; set; }
        public string DosyaAnaKisisi { get; set; }
        public Dictionary<string, string> SmsTemplateDictionary { get; set; }
        public List<Tab> TabList { get; set; }
        public List<ProtocolCampaignModel> ProtocolCampaignModelList { get; set; }
        public List<FiveImportantNote> Last5ImportantNotes { get; set; }
        public decimal Score { get; set; }
        public string ScoreColorCode { get; set; }
        public bool Status { get; set; }
        public List<string> ProtocolCancelReasonList { get; set; }
        public List<Entities.Phone> Phones { get; set; }
        public List<Entities.Phone> FakeNumbers { get; set; }
        public bool HasActiveProtocol { get; set; }
        public bool HasWaitingProtocol { get; set; }
        public List<ReminderType> ReminderTypes { get; set; }
        public string ConRelRecID { get; set; }
        public string ProtocolValue { get; set; }
        public bool IsClosable { get; set; }
        public string BorcTipi { get; set; }
        public DateTime DevirTarihi { get; set; }
        public List<CallResult> CallResults { get; set; }
        public List<PaymentPlanType> PaymentPlanTypes { get; set; }
        public string KbbSkor { get; set; }
        public string ToplamTakipBakiyesi { get; set; }
        public string DosyaVekili { get; set; }
        public bool TapuKaydi { get; set; }
        public bool MaasHaczi { get; set; }
        public bool HasPaymentPromise { get; set; }
        public string PromiseDate { get; set; }
        public string PromiseCreator { get; set; }

    }
}
