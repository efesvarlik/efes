﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Efes.Models.Obligor.Tabs;

namespace Efes.Models.Obligor
{
    public class FiveImportantNote
    {
        public string Text { get; set; }
        public string ShortenedDetail { get; set; }
        public int Type { get; set; }
    }
}
