﻿using System.Collections.Generic;
using Efes.Entities.SmartCalling;

namespace Efes.Models.SmartCalling
{
    public class CollectionWithoutProtocolModel : SmartCallingModel
    {
        public List<CollectionWithoutProtocol> CollectionWithoutProtocolList { get; set; }
        public CollectionWithoutProtocolModel()
        {
            CollectionWithoutProtocolList = new List<CollectionWithoutProtocol>();
        }
    }
}