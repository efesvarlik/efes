﻿using System.Collections.Generic;
using Efes.Entities.SmartCalling;

namespace Efes.Models.SmartCalling
{
    public class NewAppointedModel : SmartCallingModel
    {
        public List<NewAppointed> NewAppointedList { get; set; }
        public NewAppointedModel()
        {
            NewAppointedList = new List<NewAppointed>();
        }
    }
}