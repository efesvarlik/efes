﻿using System.Collections.Generic;
using Efes.Entities.SmartCalling;

namespace Efes.Models.SmartCalling
{
    public class PersonalAutoDialerModel : SmartCallingModel
    {
        public List<PersonalAutoDialer> PersonalAutoDialerList { get; set; }
        public PersonalAutoDialerModel()
        {
            PersonalAutoDialerList = new List<PersonalAutoDialer>();
        }
    }
}