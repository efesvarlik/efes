﻿using System.Collections.Generic;
using Efes.Entities.SmartCalling;

namespace Efes.Models.SmartCalling
{
    public class TodayPaymentsModel : SmartCallingModel
    {
        public List<TodayPayments> TodayPaymentsList { get; set; }
        public TodayPaymentsModel()
        {
            TodayPaymentsList = new List<TodayPayments>();
        }
    }
}
