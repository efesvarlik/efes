﻿using System.Collections.Generic;
using Efes.Entities.SmartCalling;
using Efes.Entities.Portfolio;

namespace Efes.Models.SmartCalling
{
    public class ByCapitalGroupModel : SmartCallingModel
    {
        public List<ByCapitalGroup> ByCapitalGroupList { get; set; }
        public Dictionary<int, string> DebtGroups { get; set; }

        public List<Portfolio> Portfolios { get; set; }

        public ByCapitalGroupModel()
        {
            ByCapitalGroupList = new List<ByCapitalGroup>();
            DebtGroups = new Dictionary<int, string>();
            Portfolios = new List<Portfolio>();
        }

        public int SelectedDebtGroup { get; set; }
        public int SelectedPortfolio { get; set; }
    }
}