﻿using System.Collections.Generic;
using Efes.Entities.SmartCalling;

namespace Efes.Models.SmartCalling
{
    public class ExpiredModel : SmartCallingModel
    {
        public List<Expired> ExpiredList { get; set; }

        public ExpiredModel()
        {
            ExpiredList = new List<Expired>();
        }
    }
}