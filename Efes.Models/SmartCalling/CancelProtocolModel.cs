﻿using System.Collections.Generic;
using Efes.Entities.SmartCalling;

namespace Efes.Models.SmartCalling
{
    public class CancelProtocolModel : SmartCallingModel
    {
        public List<CancelProtocol> CancelProtocolist { get; set; }
        public CancelProtocolModel()
        {
            CancelProtocolist = new List<CancelProtocol>();
        }
    }
}