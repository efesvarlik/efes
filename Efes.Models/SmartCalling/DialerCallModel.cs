﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities.SmartCalling;

namespace Efes.Models.SmartCalling
{
    public class DialerCallModel
    {
        public List<DialerCall> DialerCallList { get; set; }
        public DialerStatus DialerStatus { get; set; }
        public DialerDetail DialerDetail { get; set; }

        public DialerCallModel()
        {
            DialerCallList = new List<DialerCall>();
        }
    }
}
