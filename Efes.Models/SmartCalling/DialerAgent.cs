﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Models.SmartCalling
{
    public class DialerAgent
    {
        public string AgentName { get; set; }
        public DateTime LastCalledDate { get; set; }
        public string LastCalledDateToString =>  LastCalledDate.ToString("dd.MM.yyyy - HH:mm:ss");
    }
}
