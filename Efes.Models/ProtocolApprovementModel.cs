﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities;
using Efes.Entities.Protocol;

namespace Efes.Models
{
    public class ProtocolApprovementModel: ProtocolApprovement
    {
        public string Colour { get; set; }
    }
}
