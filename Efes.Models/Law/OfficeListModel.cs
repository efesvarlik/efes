﻿using System.Collections.Generic;

namespace Efes.Models.Law
{
    public class OfficeListModel
    {
        public List<OfficeModel> OfficeModelList { get; set; }
    }
}
