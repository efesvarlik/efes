﻿using Efes.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Efes.Entities.Law;

namespace Efes.Models.Law
{
    public class OfficeAccountModel : OfficeAccount
    {
        [Display(Name = "Oluşturan")]
        public string CreatedByName { get; set; }
        [Display(Name = "Değiştiren")]
        public string UpdatedByName { get; set; }
    }
}
