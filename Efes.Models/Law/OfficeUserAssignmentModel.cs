﻿using Efes.Entities.LawOffice;
using Efes.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Models.Law
{
    public class OfficeUserAssignmentModel
    {
        public string OfficeName { get; set; }
        public int OfficeId { get; set; }
        public List<User> AllUsers { get; set; }
        public List<User> OfficeUsers { get; set; }

    }
}
