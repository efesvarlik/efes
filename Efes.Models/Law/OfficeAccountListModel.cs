﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Efes.Models.Law
{
    public class OfficeAccountListModel
    {
        [Display(Name = "OfficeId")]
        public int OfficeId { get; set; }
        [Display(Name = "OfficeName")]
        public string OfficeName { get; set; }
        public List<OfficeAccountModel> OfficeAccountModelList { get; set; }

        public OfficeAccountListModel()
        {
            OfficeAccountModelList = new List<OfficeAccountModel>();
        }
    }
}
