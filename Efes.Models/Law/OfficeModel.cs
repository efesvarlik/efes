﻿using Efes.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Efes.Models.Law
{
    public class OfficeModel: Office
    {
        [Display(Name = "Şehir")]
        public string CityName { get; set; }
        [Display(Name = "Aktif mi?")]
        public string IsActiveName { get; set; }
        [Display(Name = "Şehir Listesi")]
        public List<City> CityList { get; set; }

        public OfficeModel()
        {
            CityList = new List<City>();
        }
    }
}
