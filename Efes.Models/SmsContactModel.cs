﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities;

namespace Efes.Models
{
    public class SmsContactModel: Contact
    {
        public int SmsLogId { get; set; }
        public int BulkSmsImportId { get; set; }
        public string Sms { get; set; }
        public string RecId { get; set; }
    }
}
