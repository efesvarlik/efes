﻿using Efes.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Models
{
    public class CreateUserModel
    {
        public User User { get; set; }
        public List<User> TeamLeaderList { get; set; }
        public CreateUserModel()                                                                                                                                                                                           
        {
            TeamLeaderList = new List<User>();
            User = new User();
        }
    }
}
