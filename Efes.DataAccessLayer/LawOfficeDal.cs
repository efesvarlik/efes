﻿using System;
using System.Collections.Generic;
using System.Data;
using Efes.Entities.Law;
using Efes.Models.Law;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class LawOfficeDal : BaseDal
    {
        #region LawOffice
        public List<OfficeModel> GetLawOfficeList(int userId)
        {
            List<OfficeModel> lawOfficeList = new List<OfficeModel>();
            try
            {
                var command = Db.GetStoredProcCommand("Usp_GetLawOfficeList");
                IDataReader dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        lawOfficeList.Add(new OfficeModel()
                        {
                            Id = dr["Id"].Convert<int>(),
                            Name = dr["Name"].Convert<string>(),
                            CityId = dr["CityId"].Convert<int>(),
                            IsActive = dr["IsActive"].Convert<bool>(),
                            PhoneNumber = dr["PhoneNumber"].Convert<string>(),
                            CityName = dr["CityName"].Convert<string>(),
                            IsActiveName = dr["IsActive"].Convert<bool>() ? "Aktif" : "Pasif",
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetLawOfficeList metodunda hata oluştu.", ex);

            }

            return lawOfficeList;
        }

        public bool CreateLawOffice(OfficeModel officeModel)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_CreateLawOffice");
                Db.AddInParameter(dbCommand, "@Name", DbType.String, officeModel.Name);
                Db.AddInParameter(dbCommand, "@CityId", DbType.String, officeModel.CityId);
                Db.AddInParameter(dbCommand, "@PhoneNumber", DbType.String, officeModel.PhoneNumber);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("CreateLawOffice metodunda hata oluştu.");
                return false;
            }
        }

        public OfficeModel GetLawOfficeById(int lawOfficeId)
        {
            try
            {
                var command = Db.GetStoredProcCommand("Usp_GetLawOfficeById");
                Db.AddInParameter(command, "@LawOfficeId", DbType.Int32, lawOfficeId);
                IDataReader dr = Db.ExecuteReader(command);
                using (dr)
                {
                    if (dr.Read())
                    {
                        return new OfficeModel()
                        {
                            Id = dr["Id"].Convert<int>(),
                            Name = dr["Name"].Convert<string>(),
                            CityId = dr["CityId"].Convert<int>(),
                            IsActive = dr["IsActive"].Convert<bool>(),
                            PhoneNumber = dr["PhoneNumber"].Convert<string>(),
                            CityName = dr["CityName"].Convert<string>(),
                            IsActiveName = dr["IsActive"].Convert<bool>() ? "Aktif" : "Pasif",
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetLawOfficeByLawOfficeId metodunda hata oluştu.", ex);
            }
            return null;
        }

        public bool UpdateLawOffice(OfficeModel officeModel)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_UpdateLawOffice");
                Db.AddInParameter(dbCommand, "@Id", DbType.Int32, officeModel.Id);
                Db.AddInParameter(dbCommand, "@Name", DbType.String, officeModel.Name);
                Db.AddInParameter(dbCommand, "@CityId", DbType.String, officeModel.CityId);
                Db.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, officeModel.IsActive);
                Db.AddInParameter(dbCommand, "@PhoneNumber", DbType.String, officeModel.PhoneNumber);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("UpdateLawOffice metodunda hata oluştu.");
                return false;
            }
        }

        #endregion

        #region LawOfficeAccount
        public List<OfficeAccountModel> GetLawOfficeAccountListModelByOfficeId(int officeId)
        {
            List<OfficeAccountModel> lawOfficeFileList = new List<OfficeAccountModel>();
            try
            {
                var dbCommand = Db.GetStoredProcCommand("Usp_GetLawOfficeAccountListByOfficeId");
                Db.AddInParameter(dbCommand, "@Id", DbType.Int32, officeId);
                IDataReader dr = Db.ExecuteReader(dbCommand);
                using (dr)
                {
                    while (dr.Read())
                    {
                        lawOfficeFileList.Add(new OfficeAccountModel()
                        {
                            Id = dr["Id"].Convert<int>(),
                            AccountId = dr["AccountId"].Convert<string>(),
                            LawOfficeId = dr["LawOfficeId"].Convert<int>(),
                            IsActive = dr["IsActive"].Convert<bool>(),
                            CreatedDate = dr["CreatedDate"].Convert<DateTime>(),
                            CreatedBy = dr["CreatedBy"].Convert<int>(),
                            UpdatedDate = dr["UpdatedDate"].Convert<DateTime>(),
                            UpdatedBy = dr["UpdatedBy"].Convert<int>(),
                            CreatedByName = dr["CreatedByName"].Convert<string>(),
                            UpdatedByName = dr["UpdatedByName"].Convert<string>(),
                            AccountOwner = dr["AccountOwner"].Convert<string>()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetLawOfficeAccountListModelByOfficeId metodunda hata oluştu.", ex);

            }
            return lawOfficeFileList;
        }

        public List<OfficeAccountModel> GetLawOfficeAccountListModelByUserId(int userId)
        {
            List<OfficeAccountModel> lawOfficeFileList = new List<OfficeAccountModel>();
            try
            {
                var dbCommand = Db.GetStoredProcCommand("Usp_GetLawOfficeAccountByUserId");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                IDataReader dr = Db.ExecuteReader(dbCommand);
                using (dr)
                {
                    while (dr.Read())
                    {
                        lawOfficeFileList.Add(new OfficeAccountModel()
                        {
                            Id = dr["Id"].Convert<int>(),
                            AccountId = dr["AccountId"].Convert<string>(),
                            LawOfficeId = dr["LawOfficeId"].Convert<int>(),
                            IsActive = dr["IsActive"].Convert<bool>(),
                            CreatedDate = dr["CreatedDate"].Convert<DateTime>(),
                            CreatedBy = dr["CreatedBy"].Convert<int>(),
                            UpdatedDate = dr["UpdatedDate"].Convert<DateTime>(),
                            UpdatedBy = dr["UpdatedBy"].Convert<int>(),
                            CreatedByName = dr["CreatedByName"].Convert<string>(),
                            UpdatedByName = dr["UpdatedByName"].Convert<string>(),
                            AccountOwner = dr["AccountOwner"].Convert<string>()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetLawOfficeAccountListModelByUserId metodunda hata oluştu.", ex);
            }

            return lawOfficeFileList;
        }

        public bool AddAccountToLawOffice(OfficeAccount officeFile)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_AddAccountToLawOffice");
                Db.AddInParameter(dbCommand, "@AccountId", DbType.String, officeFile.AccountId);
                Db.AddInParameter(dbCommand, "@LawOfficeId", DbType.Int32, officeFile.LawOfficeId);
                Db.AddInParameter(dbCommand, "@CreatedBy", DbType.Int32, officeFile.CreatedBy);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("AddAccountToLawOffice metodunda hata oluştu.");
                return false;
            }
        }

        public bool UpdateLawOfficeAccount(OfficeAccount officeFile)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_UpdateLawOfficeAccount");
                Db.AddInParameter(dbCommand, "@AccountId", DbType.String, officeFile.AccountId);
                Db.AddInParameter(dbCommand, "@LawOfficeId", DbType.Int32, officeFile.LawOfficeId);
                Db.AddInParameter(dbCommand, "@UpdatedBy", DbType.Int32, officeFile.UpdatedBy);
                Db.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, officeFile.IsActive);
                Db.AddInParameter(dbCommand, "@Id", DbType.Int32, officeFile.Id);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("UpdateLawOfficeAccount metodunda hata oluştu.");
                return false;
            }
        }

        public OfficeAccountModel GetLawOfficeAccountByOfficeId(int id)
        {
            try
            {
                var command = Db.GetStoredProcCommand("Usp_GetLawOfficeAccountByOfficeId");
                Db.AddInParameter(command, "@Id", DbType.Int32, id);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    if (dr.Read())
                    {
                        return new OfficeAccountModel()
                        {
                            Id = dr["Id"].Convert<int>(),
                            AccountId = dr["AccountId"].Convert<string>(),
                            LawOfficeId = dr["LawOfficeId"].Convert<int>(),
                            IsActive = dr["IsActive"].Convert<bool>(),
                            CreatedDate = dr["CreatedDate"].Convert<DateTime>(),
                            CreatedBy = dr["CreatedBy"].Convert<int>(),
                            UpdatedDate = dr["UpdatedDate"].Convert<DateTime>(),
                            UpdatedBy = dr["UpdatedBy"].Convert<int>(),
                            CreatedByName = dr["CreatedByName"].Convert<string>(),
                            UpdatedByName = dr["UpdatedByName"].Convert<string>(),
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetLawOfficeAccountByOfficeId metodunda hata oluştu.", ex);

            }
            return null;
        }
        #endregion

        #region LawOfficeCollection

        public List<Collection> GetCollectionByLawOfficeId(int userId, DateTime startDate, DateTime endDate)
        {
            var collectionList = new List<Collection>();
            try
            {
                var dbCommand = Db.GetStoredProcCommand("Usp_CollectionByLawOffice");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@StartDate", DbType.DateTime, startDate);
                Db.AddInParameter(dbCommand, "@EndDate", DbType.DateTime, endDate);

                IDataReader dr = Db.ExecuteReader(dbCommand);
                using (dr)
                {
                    while (dr.Read())
                    {
                        collectionList.Add(new Collection()
                        {
                            Id = dr["Id"].Convert<string>(),
                            AccountId = dr["AccountId"].Convert<string>(),
                            PaymentDate = dr["PaymentDate"].Convert<DateTime>(),
                            Amount = dr["Amount"].Convert<decimal>(),
                            PaymentBank = dr["PaymentBank"].Convert<string>(),
                            PaymentDesc = dr["PaymentDesc"].Convert<string>(),
                            StatusText = dr["StatusText"].Convert<string>(),
                            EmployeeLoginID = dr["EmployeeLoginID"].Convert<string>(),
                            LawOffice = dr["LawOffice"].Convert<string>(),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetCollectionByLawOfficeId metodunda hata oluştu.", ex);

            }

            return collectionList;
        }

        #endregion
    }
}
