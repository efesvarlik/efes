﻿using System;
using System.Collections.Generic;
using System.Data;
using Efes.Entities.Home;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class HomeDal : BaseDal
    {
        public List<DashboardSummary> GetDashBoardSummaryList(int userId)
        {
            var dashboardSummaryList = new List<DashboardSummary>();
            try
            {
                var key = "GetDashBoardSummaryList_" + userId;
                if(CacheManager.Default.Contains(key)){
                    return CacheManager.Default.Get(key) as List<DashboardSummary>;
                }

                var command = Db.GetStoredProcCommand("Usp_GetDashboardSummary");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        dashboardSummaryList.Add(new DashboardSummary()
                        {
                            Id = dr["DetailId"].Convert<int>(),
                            TextKey = dr["TextKey"].Convert<string>(),
                            TextValue = dr["TextValue"].Convert<string>()
                        });
                    }
                }

                CacheManager.Default.Add(key, dashboardSummaryList, 300);

            }
            catch (Exception ex)
            {
                Logger.Error("GetDashBoardSummaryList metodunda hata oluştu.", ex);
                
            }
            
            return dashboardSummaryList;
        }

        public Dictionary<string, int> GetCallCount(int userId)
        {
            var callCountDictionary = new Dictionary<string, int>();
            try
            {
                string key = "GetCallCount_" + userId;
                if (CacheManager.Default.Contains(key))
                {
                    var cache = CacheManager.Default.Get(key) as Dictionary<string, int>;
                    if (cache != null)
                    {
                        return cache;
                    }
                }

                var command = Db.GetStoredProcCommand("GetCallCount");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        callCountDictionary.Add(dr["MonthName"].Convert<string>(), dr["Value"].Convert<int>());
                    }
                }
                var result = callCountDictionary.Count > 0 ? callCountDictionary : null;
                CacheManager.Default.Add(key, result, 300);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCallCount metodunda hata oluştu.", ex);
                return null;
            }
        }

        public Dictionary<string, int> GetMoneyCollection(int userId)
        {
            var moneyCollectionDictionary = new Dictionary<string, int>();
            try
            {
                string key = "GetMoneyCollection_" + userId;
                if (CacheManager.Default.Contains(key))
                {
                    var cache = CacheManager.Default.Get(key) as Dictionary<string, int>;
                    if (cache != null)
                    {
                        return cache;
                    }
                }

                var command = Db.GetStoredProcCommand("GetMoneyCollection");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        moneyCollectionDictionary.Add(dr["MonthName"].Convert<string>(), dr["Value"].Convert<int>());
                    }
                }
                var result = moneyCollectionDictionary.Count > 0 ? moneyCollectionDictionary : null;
                CacheManager.Default.Add(key, result, 300);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("GetMoneyCollection metodunda hata oluştu.", ex);
                return null;
            }
        }

        public Dictionary<string, int> GetMoneyCollectionBottomStatistics(int userId)
        {
            var moneyCollectionBottomStatisticsDictionary = new Dictionary<string, int>();
            try
            {
                string key = "GetMoneyCollectionBottomStatistics_" + userId;
                if (CacheManager.Default.Contains(key))
                {
                    var cache = CacheManager.Default.Get(key) as Dictionary<string, int>;
                    if (cache != null)
                    {
                        return cache;
                    }
                }

                var command = Db.GetStoredProcCommand("GetMoneyCollectionBottomStatistics");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        moneyCollectionBottomStatisticsDictionary.Add(dr["Name"].Convert<string>(), dr["Value"].Convert<int>());
                    }
                }
                var result = moneyCollectionBottomStatisticsDictionary.Count > 0 ? moneyCollectionBottomStatisticsDictionary : null;
                CacheManager.Default.Add(key, result, 300);

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("GetMoneyCollectionBottomStatistics metodunda hata oluştu.", ex);
                return null;
            }
        }

        public List<TargetAndActualCollection> GetTargetAndActualCollections(int userId)
        {
            var targetAndActualCollections = new List<TargetAndActualCollection>();
            try
            {
                string key = "GetTargetAndActualCollections_" + userId;
                if (CacheManager.Default.Contains(key))
                {
                    var result = CacheManager.Default.Get(key) as List<TargetAndActualCollection>;
                    if (result != null)
                    {
                        return result;
                    }
                }

                var command = Db.GetStoredProcCommand("GetTargetAndActualCollections");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        targetAndActualCollections.Add(new TargetAndActualCollection()
                        {
                            TargetAndActualCollectionType = (TargetAndActualCollection.TargetAndActualCollectionTypeEnum)dr["Type"].Convert<int>(),
                            Target = dr["Target"].Convert<int>(),
                            Actual = dr["Actual"].Convert<int>()
                        });
                    }
                }
                CacheManager.Default.Add(key, targetAndActualCollections, 300);
            }
            catch (Exception ex)
            {
                Logger.Error("GetTargetAndActualCollections metodunda hata oluştu.", ex);

            }

            return targetAndActualCollections;
        }

    }
}
