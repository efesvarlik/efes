﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Efes.Entities.Sms;
using Efes.Models;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class ContactDal : BaseDal
    {
        public List<SmsContactModel> GetContactDataWithImportIdByAccountId(int importId)
        {
            try
            {
                var contactList = new List<SmsContactModel>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetContactDataWithImportIdByAccountId");
                Db.AddInParameter(dbCommand, "@ImportId", DbType.Int32, importId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        contactList.Add(ContactMapper(reader));
                    }
                }
                return contactList;
            }
            catch (Exception ex)
            {
                Logger.Error("Usp_GetCustomerDataWithImportId metodunda hata oluştu.", ex);
                return new List<SmsContactModel>();
            }

        }

        public List<SmsContactModel> GetContactDataWithImportIdByTckn(int importId)
        {
            try
            {
                var contactList = new List<SmsContactModel>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetContactDataWithImportIdByTckn");
                Db.AddInParameter(dbCommand, "@ImportId", DbType.Int32, importId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        contactList.Add(ContactMapper(reader));
                    }
                }
                return contactList;
            }
            catch (Exception ex)
            {
                Logger.Error("Usp_GetCustomerDataWithImportId metodunda hata oluştu.", ex);
                return new List<SmsContactModel>();
            }
        }

        public List<GsmMessage> GetSmsListForService()
        {
            try
            {
                var gsmMessageList = new List<GsmMessage>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetSmsListForService");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        var message = new GsmMessage()
                        {
                            GsmNumber = reader["Gsm"].Convert<string>(),
                            Content = reader["Sms"].Convert<string>(),
                            RecId = reader["RecId"].Convert<string>()
                        };

                        gsmMessageList.Add(message);
                    }
                }
                return gsmMessageList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetSmsListForService metodunda hata oluştu.", ex);
                return new List<GsmMessage>();
            }
        }

        public List<SmsContactModel> GetBulkSmsWithGsm()
        {
            try
            {
                var contactList = new List<SmsContactModel>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetBulkSmsWithGsm");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        contactList = new List<SmsContactModel>
                        {
                            new SmsContactModel()
                            {
                                Gsm = reader["Gsm"].Convert<string>(),
                                Sms = reader["Sms"].Convert<string>()
                            }
                        };
                    }
                }
                return contactList;
            }
            catch (Exception ex)
            {
                Logger.Error("Usp_GetBulkSmsWithGsm metodunda hata oluştu.", ex);
                return new List<SmsContactModel>();
            }
        }

        public List<SmsContactModel> GetBulkSmsWithTckn()
        {
            try
            {
                var contactList = new List<SmsContactModel>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetBulkSmsWithTckn");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        contactList = new List<SmsContactModel>
                        {
                            new SmsContactModel()
                            {
                                Tckn = reader["Tckn"].Convert<string>(),
                                Sms = reader["Sms"].Convert<string>(),
                            }
                        };
                    }
                }
                return contactList;
            }
            catch (Exception ex)
            {
                Logger.Error("Usp_GetBulkSmsWithTckn metodunda hata oluştu.", ex);
                return new List<SmsContactModel>();
            }
        }

        public bool SendBulkSmsReport(DataTable dataTable)
        {
            try
            {
                DbCommand objDbCmd = Db.GetStoredProcCommand("Usp_UpdateSmsStatusForService");
                objDbCmd.Parameters.Add(new SqlParameter("@TableType", dataTable) { SqlDbType = SqlDbType.Structured });
                Db.ExecuteNonQuery(objDbCmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("SendBulkSmsReport metodunda hata oluştu.", ex);
                return false;
            }
        }

        private static SmsContactModel ContactMapper(IDataReader reader)
        {
            return new SmsContactModel()
            {
                Id = reader["CustomerId"].Convert<int>(),
                AccountId = reader["AccountId"].Convert<string>(),
                Tckn = reader["Tckn"].Convert<string>(),
                Name = reader["Name"].Convert<string>(),
                Surname = reader["Surname"].Convert<string>(),
                Gsm = reader["Gsm"].Convert<string>(),
                SmsLogId = reader["SmsLogId"].Convert<int>(),
                BulkSmsImportId = reader["BulkSmsImportId"].Convert<int>(),
                Sms = reader["Sms"].Convert<string>(),
            };
        }
    }
}
