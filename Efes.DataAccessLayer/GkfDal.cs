﻿using System;
using System.Collections.Generic;
using System.Data;
using Efes.Entities.Obligor;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class GkfDal : BaseDal
    {
        public List<Gkf> GetGkfList(int userId, DateTime date, string type)
        {
            try
            {
                var missedCallList = new List<Gkf>();
                var dbCommand = Db.GetStoredProcCommand("dbo.GetGkfList_V0001");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Date", DbType.DateTime, date);
                Db.AddInParameter(dbCommand, "@Type", DbType.String, type);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        missedCallList.Add(new Gkf()
                        {
                            AccountId = reader["AccountID"].Convert<string>(),
                            SourceSet = reader["SourceSet"].Convert<string>(),
                            DisplayName = reader["PrimaryContactDisplayName"].Convert<string>(),
                            CreatedBy = reader["CreatedBy"].Convert<string>(),
                            CreatedDateTime = reader["CreatedDateTime"].Convert<string>(),
                            SendType = reader["GonderiTipi"].Convert<string>(),
                            ReportPage = reader["ReportPage"].Convert<string>(),
                        });
                    }
                }
                return missedCallList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetGkfList metodunda hata oluştu.", ex);
                return new List<Gkf>();
            }
        }
    }
}
