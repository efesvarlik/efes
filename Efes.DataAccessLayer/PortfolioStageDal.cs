﻿using System;
using System.Collections.Generic;
using System.Data;
using Efes.Entities.Portfolio;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class PortfolioStageDal : BaseDal
    {
        public PortfolioStageDal() : base(DbTypeEnum.EfesStage)
        {

        }
        public Portfolio GetWaitingPortfolio()
        {
            try
            {
                var portfolio = new Portfolio();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetWaitingPortfolio");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        portfolio = new Portfolio()
                        {
                            BankName = reader["Source"].Convert<string>(),
                            Name = reader["SourceSet"].Convert<string>(),
                            BorcluSayisi = reader["FileCount"].Convert<string>(),
                            KefilSayisi = reader["ContactCount"].Convert<string>(),
                            BorcSayisi = reader["DebtCount"].Convert<string>(),
                            AdresSayisi = reader["AddressCount"].Convert<string>(),
                            TelefonSayisi = reader["PhoneCount"].Convert<string>(),
                            IsActive = reader["IsActive"].Convert<bool>(),
                        };
                    }
                }
                return portfolio;
            }
            catch (Exception ex)
            {
                Logger.Error("GetWaitingPortfolio metodunda hata oluştu.", ex);
                return null;
            }
        }

        public bool WorkTables(string bankName, string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_WorkTables");
                Db.AddInParameter(dbCommand, "@Source", DbType.String, bankName);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("WorkTables metodunda hata oluştu.");
                return false;
            }
        }

        public bool SourceAndSourceset(string bankName, string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_SourceAndSourceSet");
                Db.AddInParameter(dbCommand, "@Source", DbType.String, bankName);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("SourceAndSourceset metodunda hata oluştu.");
                return false;
            }
        }

        public bool ContactUpdate(string bankName, string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ContactUpdate");
                Db.AddInParameter(dbCommand, "@Source", DbType.String, bankName);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ContactUpdate metodunda hata oluştu.");
                return false;
            }
        }

        public bool UpdateAccount(string bankName, string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_UpdateAccount");
                Db.AddInParameter(dbCommand, "@Source", DbType.String, bankName);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("UpdateAccount metodunda hata oluştu.");
                return false;
            }
        }

        public bool PhoneTable(string bankName, string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_PhoneTable");
                Db.AddInParameter(dbCommand, "@Source", DbType.String, bankName);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("PhoneTable metodunda hata oluştu.");
                return false;
            }
        }

        public bool AddressTable(string bankName, string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_AddressTable");
                Db.AddInParameter(dbCommand, "@Source", DbType.String, bankName);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("AddressTable metodunda hata oluştu.");
                return false;
            }
        }

        public bool NormalizePhoneNumbers()
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_NormalizePhoneNumbers");
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("NormalizePhoneNumbers metodunda hata oluştu.");
                return false;
            }
        }

        public bool DebtDetailTable(string bankName, string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_DebtDetailTable");
                Db.AddInParameter(dbCommand, "@Source", DbType.String, bankName);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("DebtDetailTable metodunda hata oluştu.");
                return false;
            }
        }

        public bool MoneyDebtDetailsToAccount()
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_MoneyDebtDetailsToAccount");
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("MoneyDebtDetailsToAccount metodunda hata oluştu.");
                return false;
            }
        }

        public bool ContactRelationsTable(string bankName, string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ContactRelationsTable");
                Db.AddInParameter(dbCommand, "@Source", DbType.String, bankName);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ContactRelationsTable metodunda hata oluştu.");
                return false;
            }
        }

        public bool DataToTable(string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_DataToTable");
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                Db.AddInParameter(dbCommand, "@DatabaseName", DbType.String, "EFESITSM");
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("DataToTable metodunda hata oluştu.");
                return false;
            }
        }

        public bool ReleaseData(string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ReleaseData");
                Db.AddInParameter(dbCommand, "@TableName", DbType.String, "");
                Db.AddInParameter(dbCommand, "@NewSourceSet", DbType.String, name);
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                Db.AddInParameter(dbCommand, "@DatabaseName", DbType.String, "EFESITSM");
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ReleaseData metodunda hata oluştu.");
                return false;
            }
        }

        public bool AddressActive(string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.EFESITSM.[dbo].[BS_sp_AddressActive]");
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("AddressActive metodunda hata oluştu.");
                return false;
            }
        }

        public bool CreateBop(string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.EFESITSM..BS_sp_CreateBOP");
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("CreateBop metodunda hata oluştu.");
                return false;
            }
        }

        public int? GetMaxId()
        {
            try
            {
                int? retValue = null;
                var dbCommand = Db.GetSqlStringCommand(
                @"SELECT (TOP 1 Cast(LEFT(accountid, Charindex(‘-’, accountid)-1) AS INT) 
                + 1000) as maxid
                FROM   efesitsm..bs_account
                WHERE  accountid LIKE ‘1 %’ 
                ORDER  BY accountid DESC ");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        retValue = reader["maxid"].Convert<int>(null);
                    }
                }

                return retValue;
            }
            catch (Exception ex)
            {
                ex.Log("CreateBop metodunda hata oluştu.");
                return null;
            }
        }

        public bool PreLoads2(string name, int maxid)
        {
            try
            {
                var dbCommand = Db.GetSqlStringCommand(
                    @"INSERT INTO efesitsm_preload2..bs_impparams 
                                    (sourceset, 
                                     paramname, 
                                     paramvalue, 
                                     paramdesc) 
                        VALUES     (@SourceSet, 
                                    ‘contactidstart’, 
                                    @MaxId, 
                                    ‘’)");
                Db.AddInParameter(dbCommand, "@SourceSet", DbType.String, name);
                Db.AddInParameter(dbCommand, "@MaxId", DbType.Int32, maxid);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("PreLoads2 metodunda hata oluştu.");
                return false;
            }
        }
    }
}
