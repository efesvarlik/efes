﻿using System;
using System.Collections.Generic;
using System.Data;
using Efes.Entities.Obligor;
using Efes.Models.Obligor;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class ReminderDal : BaseDal
    {
        public bool InsertReminder(Reminder reminder)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_InsertReminder");
                Db.AddInParameter(dbCommand, "@TypeId", DbType.Int32, reminder.TypeId);
                Db.AddInParameter(dbCommand, "@RemindDate", DbType.DateTime, reminder.RemindDate);
                Db.AddInParameter(dbCommand, "@Detail", DbType.String, reminder.Detail);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, reminder.UserId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, reminder.ObligorId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("InsertReminder metodunda hata oluştu.");
                return false;
            }
        }

        public List<ReminderType> GetReminderTypes()
        {
            try
            {
                var tmpList = new List<ReminderType>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetReminderTypes");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        tmpList.Add(new ReminderType()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Value = reader["Value"].Convert<string>()
                        });
                    }
                }
                return tmpList;
            }
            catch (Exception ex)
            {
                ex.Log("GetReminderTypes metodunda hata oluştu.");
                return new List<ReminderType>();
            }
        }

        public List<ReminderModel> GetReminderByUserId(int userId, DateTime date)
        {
            try
            {
                var tmpList = new List<ReminderModel>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetReminderByUserId");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Date", DbType.DateTime, date);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        tmpList.Add(new ReminderModel()
                        {
                            Id = reader["Id"].Convert<int>(),
                            TypeId = reader["TypeId"].Convert<int>(),
                            CreatedDate = reader["CreatedDate"].Convert<DateTime>(),
                            RemindDate = reader["RemindDate"].Convert<DateTime>(),
                            Detail = reader["Detail"].Convert<string>(),
                            TypeName = reader["Value"].Convert<string>(),
                            AccountId = reader["AccountId"].Convert<string>(),
                            IsPast = reader["IsPast"].Convert<bool>(),
                            Username = reader["Username"].Convert<string>()
                        });
                    }
                }
                return tmpList;
            }
            catch (Exception ex)
            {
                ex.Log("GetReminderTypes metodunda hata oluştu.");
                return new List<ReminderModel>();
            }
        }

    }
}
