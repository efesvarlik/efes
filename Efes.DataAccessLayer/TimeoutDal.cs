﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Utility;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Efes.DataAccessLayer
{
    public class TimeoutDal
    {
        public int GetTimeoutSeconds()
        {
            try
            {
                Database db = new SqlDatabase("Data Source=37.48.73.143,14133;Initial Catalog=efeslock;Application Name=EfesPortal;UId=webuser;Pwd=DA93B037-166B-43B3-8163-63ED0A5345AA");
                var dbCommand = db.GetStoredProcCommand("GetTimeoutSecond");
                var res = 0;
                var reader = db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        res = reader["Value"].ToString().Convert<int>();
                    }
                }

                return res;
            }
            catch
            {
                return 10;
            }
        }
    }
}
