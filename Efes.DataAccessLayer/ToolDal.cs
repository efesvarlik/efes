﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Efes.Entities;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class ToolDal : BaseDal
    {
        public List<Phone> GetPhoneNumbers(string accountId)
        {
            List<Phone> phoneList = new List<Phone>();
            DbCommand dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetPhoneNumberListByAccountId");
            Db.AddInParameter(dbCommand, "@AccountId", DbType.String, accountId);
            IDataReader reader = Db.ExecuteReader(dbCommand);
            using (reader)
            {
                while (reader.Read())
                {
                    Phone phone = new Phone
                    {
                        Number = reader["PhoneNumber"].Convert<string>(),
                        IsActive = reader["Status"].Convert<bool>(),
                        AccountId = accountId
                    };

                    phoneList.Add(phone);
                }
            }
            return phoneList;
        }

        public bool ChangePhoneStatus(string accountId, string phoneNumber, bool isActive)
        {
            try
            {
                DbCommand dbCommand = Db.GetStoredProcCommand("dbo.Usp_UpdatePhoneNumberStatus");
                Db.AddInParameter(dbCommand, "@AccountId ", DbType.String, accountId);
                Db.AddInParameter(dbCommand, "@Phone", DbType.String, phoneNumber);
                Db.AddInParameter(dbCommand, "@Status", DbType.Boolean, isActive);
                Db.ExecuteNonQuery(dbCommand);
                return true;

            }
            catch (Exception ex)
            {
                Logger.Error("ChangePhoneStatus metodunda hata oluştu.", ex);
                return false;
            }
        }

        public bool PassivePhoneList(DataTable dt, int memberId)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("Usp_BulkPhoneDisable");
                objDbCmd.Parameters.Add(new SqlParameter("@List", dt) { SqlDbType = SqlDbType.Structured });
                objDbCmd.Parameters.Add(new SqlParameter("@MemberId", memberId) { SqlDbType = SqlDbType.Int });
                Db.ExecuteNonQuery(objDbCmd);
                return true;

            }
            catch (Exception ex)
            {
                Logger.Error("PassivePhoneList metodunda hata oluştu.", ex);
            }

            return false;
        }

        public void InsertFeedback(string feedback, int userId)
        {
            var dbCommand = Db.GetStoredProcCommand("Usp_InsertFeedback");
            Db.AddInParameter(dbCommand, "@Feedback ", DbType.String, feedback);
            Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
            Db.ExecuteNonQuery(dbCommand);
        }

        public List<Word> GetTopTenWords()
        {
            var topTenWords = new List<Word>();
            DbCommand dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetNoteWords");
            IDataReader reader = Db.ExecuteReader(dbCommand);
            using (reader)
            {
                while (reader.Read())
                {
                    var word = new Word
                    {
                        WordText = reader["Word"].Convert<string>(),
                        WordCount = reader["WordCount"].Convert<int>()
                    };

                    topTenWords.Add(word);
                }
            }
            return topTenWords;
        }
    }
}
