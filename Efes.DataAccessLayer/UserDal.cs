﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Net;
using Efes.Entities;
using Efes.Entities.User;
using Efes.Utility;
using Rule = Efes.Entities.User.Rule;

namespace Efes.DataAccessLayer
{
    public class UserDal : BaseDal
    {
        #region User

        public DataSet GetUser(string username, string password)
        {
            var cmd = Db.GetStoredProcCommand("Usp_MemberLogin");

            Db.AddInParameter(cmd, "@LoginName", DbType.String, username);
            Db.AddInParameter(cmd, "@Password", DbType.String, password.Base64Encode());
            //Db.AddInParameter(cmd, "@HostName", DbType.String, System.Web.HttpContext.Current.Request.UserHostName);

            var ds = Db.ExecuteDataSet(cmd);

            return ds;
        }

        public DataSet GetUser(int id)
        {
            var cmd = Db.GetStoredProcCommand("Usp_GetUserById");
            Db.AddInParameter(cmd, "@Id", DbType.Int32, id);
            var ds = Db.ExecuteDataSet(cmd);

            return ds;
        }

        public DataSet GetUser(string email)
        {
            var cmd = Db.GetStoredProcCommand("Usp_GetUserByEmail");
            Db.AddInParameter(cmd, "@Email", DbType.String, email);
            var ds = Db.ExecuteDataSet(cmd);

            return ds;
        }

        public List<User> GetAllUsers()
        {
            List<User> memberList = new List<User>();
            var cmd = Db.GetStoredProcCommand("Usp_GetAllUsers");
            IDataReader dr = Db.ExecuteReader(cmd);
            using (dr)
            {
                while (dr.Read())
                {
                    memberList.Add(UserMap(dr));
                }
            }

            return memberList;
        }

        public List<User> GetAllLawUserUnassigned(int lawOfficeId)
        {
            var memberList = new List<User>();
            var cmd = Db.GetStoredProcCommand("Usp_GetAllLawUserUnassigned");
            Db.AddInParameter(cmd, "@LawOfficeId", DbType.Int32, lawOfficeId);
            var dr = Db.ExecuteReader(cmd);
            using (dr)
            {
                while (dr.Read())
                {
                    memberList.Add(new User()
                    {
                        Name = dr["Name"].Convert<string>(),
                        Id = dr["UserId"].Convert<int>()
                    });
                }
            }
            return memberList;
        }

        public List<User> GetUserListByLawOfficeId(int lawOfficeId)
        {
            var memberList = new List<User>();
            var cmd = Db.GetStoredProcCommand("Usp_GetUserListByLawOfficeId");
            Db.AddInParameter(cmd, "@LawOfficeId", DbType.Int32, lawOfficeId);
            var dr = Db.ExecuteReader(cmd);
            using (dr)
            {
                while (dr.Read())
                {
                    memberList.Add(new User()
                    {
                        Name = dr["Name"].Convert<string>(),
                        Id = dr["UserId"].Convert<int>()
                    });
                }
            }
            return memberList;
        }

        public bool InsertUser(User user, int creatorId)
        {
            try
            {
                var cmd = Db.GetStoredProcCommand("[user].[Usp_InsertUser]");

                Db.AddInParameter(cmd, "@Name", DbType.String, user.Name);
                Db.AddInParameter(cmd, "@LoginName", DbType.String, user.LoginName);
                Db.AddInParameter(cmd, "@Password", DbType.String, user.Password);
                Db.AddInParameter(cmd, "@Description", DbType.String, user.Description);
                Db.AddInParameter(cmd, "@CreatedBy", DbType.String, user.CreatedBy);
                Db.AddInParameter(cmd, "@CreatedById", DbType.Int32, creatorId);
                Db.AddInParameter(cmd, "@ImagePath", DbType.String, user.ImagePath);
                Db.AddInParameter(cmd, "@TeamLeaderId", DbType.Int32, user.TeamLeaderId);
                Db.AddInParameter(cmd, "@Email", DbType.String, user.Email);

                Db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool UpdateUser(User user, int updaterId)
        {
            try
            {
                var cmd = Db.GetStoredProcCommand("[user].[Usp_UpdateUser]");

                Db.AddInParameter(cmd, "@Name", DbType.String, user.Name);
                Db.AddInParameter(cmd, "@LoginName", DbType.String, user.LoginName);
                Db.AddInParameter(cmd, "@UpdatedBy", DbType.String, user.UpdatedBy);
                Db.AddInParameter(cmd, "@UpdatedById", DbType.Int32, user.UpdatedById);
                Db.AddInParameter(cmd, "@UserId", DbType.Int32, user.Id);
                Db.AddInParameter(cmd, "@ImagePath", DbType.String, user.ImagePath);
                Db.AddInParameter(cmd, "@Email", DbType.String, user.Email);
                Db.AddInParameter(cmd, "@IsActive", DbType.Boolean, user.IsActive);

                Db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool ResetPassword(User user, int updaterId, string updateDesc)
        {
            try
            {
                var cmd = Db.GetStoredProcCommand("[user].[Usp_ResetPassword]");

                Db.AddInParameter(cmd, "@UpdatedBy", DbType.String, updateDesc);
                Db.AddInParameter(cmd, "@UpdatedById", DbType.Int32, updaterId);
                Db.AddInParameter(cmd, "@UserId", DbType.Int32, user.Id);
                Db.AddInParameter(cmd, "@IsSecure", DbType.Boolean, false);
                Db.AddInParameter(cmd, "@Password", DbType.String, user.Password);

                Db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public List<User> GetUsersByRole(Roles role)
        {
            var memberList = new List<User>();
            var command = Db.GetStoredProcCommand("Usp_GetUsersByRole");
            Db.AddInParameter(command, "@RoleId", DbType.Int32, (int)role);
            var dr = Db.ExecuteReader(command);
            using (dr)
            {
                while (dr.Read())
                {
                    memberList.Add(UserMap(dr));
                }
            }
            return memberList;
        }

        public List<User> GetUsersHasOnlyOneRoleByRoleRoleId(Roles role)
        {
            var memberList = new List<User>();
            var command = Db.GetStoredProcCommand("Usp_GetUsersHasOnlyOneRoleByRoleRoleId");
            Db.AddInParameter(command, "@RoleId", DbType.Int32, (int)role);
            var dr = Db.ExecuteReader(command);
            using (dr)
            {
                while (dr.Read())
                {
                    memberList.Add(UserMap(dr));
                }
            }
            return memberList;
        }

        private static User UserMap(IDataReader dr)
        {
            return new User()
            {
                Id = dr["UserId"].Convert<int>(),
                Name = dr["Name"].Convert<string>(),
                LoginName = dr["LoginName"].Convert<string>(),
                Description = dr["Description"].Convert<string>(),
                CreatedBy = dr["CreatedBy"].Convert<string>(),
                Email = dr["Email"].Convert<string>(),
                CreatedDate = dr["CreatedDate"].Convert<DateTime>(),
                IsActive = dr["IsActive"].Convert<bool>(),
                ImagePath = dr["ImagePath"].Convert<string>(),
                IsSecurePassword = dr["IsSecurePassword"].Convert<bool>(),
                //IsSecurePassword = false,
                IsLockedOut = dr["IsLockedOut"].Convert<bool>(),
            };


        }

        public List<User> GetUserListHasApproveRight()
        {
            var memberList = new List<User>();
            var command = Db.GetStoredProcCommand("Usp_GetUserListHasApproveRight");
            var dr = Db.ExecuteReader(command);
            using (dr)
            {
                while (dr.Read())
                {
                    memberList.Add(UserMap(dr));
                }
            }
            return memberList;
        }

        #endregion

        #region MemberLog

        public void InsertMemberLog(int userId, int typeId)
        {
            var cmd = Db.GetStoredProcCommand("Usp_InsertMemberLog");

            Db.AddInParameter(cmd, "@UserId", DbType.Int32, userId);
            Db.AddInParameter(cmd, "@TypeId", DbType.Int32, typeId);

            Db.ExecuteNonQuery(cmd);
        }

        public string ChangePassword(string userName, string password, string newPassword)
        {
            try
            {
                var cmd = Db.GetStoredProcCommand("Usp_ChangePassword");

                Db.AddInParameter(cmd, "@UserName", DbType.String, userName);
                Db.AddInParameter(cmd, "@Password", DbType.String, password.Base64Encode());
                Db.AddInParameter(cmd, "@NewPassword", DbType.String, newPassword.Base64Encode());

                var dr = Db.ExecuteReader(cmd);
                using (dr)
                {
                    if (dr.Read())
                    {
                        return dr["Result"].ToString();
                    }
                }

                Logger.Error("Db'den result gelmiyor - " + userName + " - " + password + " - " + newPassword);
                return "Error";
            }
            catch (Exception ex)
            {
                Logger.Error("Şifre değiştirme esnasında hata oluştu - " + userName + " - " + password + " - " + newPassword, ex);
                return "Error";
            }
        }

        #endregion

        #region UserRole

        public List<Role> GetUserRolesById(int userId)
        {
            List<Role> roleList = new List<Role>();
            DbCommand command = Db.GetStoredProcCommand("user.Usp_GetUserRolesById");
            Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
            IDataReader dr = Db.ExecuteReader(command);
            using (dr)
            {
                while (dr.Read())
                {
                    roleList.Add(RoleMapper(dr));
                }
            }
            return roleList;
        }

        public bool InsertUserRole(int userId, int roleId, int creatorId)
        {
            try
            {
                var cmd = Db.GetStoredProcCommand("[user].[Usp_InsertUserRole]");

                Db.AddInParameter(cmd, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(cmd, "@RoleId", DbType.Int32, roleId);
                Db.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorId);

                Db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool DeleteUserRole(int userId, int roleId, int creatorId)
        {
            try
            {
                var cmd = Db.GetStoredProcCommand("[user].[Usp_DeleteUserRole]");

                Db.AddInParameter(cmd, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(cmd, "@RoleId", DbType.Int32, roleId);
                Db.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorId);

                Db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        #endregion

        #region RoleRules

        public List<Rule> GetRoleRulesById(int roleId)
        {
            var ruleList = new List<Rule>();
            var command = Db.GetStoredProcCommand("user.Usp_GetRoleRulesById");
            Db.AddInParameter(command, "@RoleId", DbType.Int32, roleId);
            var dr = Db.ExecuteReader(command);

            using (dr)
            {
                while (dr.Read())
                {
                    ruleList.Add(RuleMapper(dr));
                }
            }
            return ruleList;
        }

        public bool InsertRoleRule(int roleId, int ruleId, int creatorId)
        {
            try
            {
                var cmd = Db.GetStoredProcCommand("[user].[Usp_InsertRoleRule]");

                Db.AddInParameter(cmd, "@RoleId", DbType.Int32, roleId);
                Db.AddInParameter(cmd, "@RuleId", DbType.Int32, ruleId);
                Db.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorId);

                Db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool DeleteRoleRule(int roleId, int ruleId, int creatorId)
        {
            try
            {
                var cmd = Db.GetStoredProcCommand("[user].[Usp_DeleteRoleRule]");

                Db.AddInParameter(cmd, "@RoleId", DbType.Int32, roleId);
                Db.AddInParameter(cmd, "@RuleId", DbType.Int32, ruleId);
                Db.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorId);

                Db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        #endregion

        #region Rule

        public List<Rule> GetAllRules()
        {
            List<Rule> ruleList = new List<Rule>();
            DbCommand command = Db.GetStoredProcCommand("user.Usp_GetAllRules");
            IDataReader dr = Db.ExecuteReader(command);
            using (dr)
            {
                while (dr.Read())
                {
                    ruleList.Add(RuleMapper(dr));
                }
            }
            return ruleList;
        }

        public void InsertRule(string value)
        {
            var cmd = Db.GetStoredProcCommand("user.Usp_InsertRule");
            Db.AddInParameter(cmd, "@Value", DbType.String, value);
            Db.ExecuteNonQuery(cmd);
        }

        public void UpdateRule(int id, string value, bool isActive)
        {
            var cmd = Db.GetStoredProcCommand("user.Usp_UpdateRule");
            Db.AddInParameter(cmd, "@Id", DbType.Int32, id);
            Db.AddInParameter(cmd, "@Value", DbType.String, value);
            Db.AddInParameter(cmd, "@IsActive", DbType.Boolean, isActive);
            Db.ExecuteNonQuery(cmd);
        }

        private Rule RuleMapper(IDataReader dr)
        {
            return new Rule()
                        {
                            Id = dr["Id"].Convert<int>(),
                            Value = dr["Value"].Convert<string>(),
                            IsActive = dr["IsActive"].Convert<bool>()
                        };
        }

        #endregion

        #region Role

        public Role GetRole(int roleId)
        {
            Role role = null;
            var command = Db.GetStoredProcCommand("user.Usp_GetRole");
            Db.AddInParameter(command, "@RoleId", DbType.Int32, roleId);
            var dr = Db.ExecuteReader(command);
            using (dr)
            {
                if (dr.Read())
                {
                    role = RoleMapper(dr);
                }
            }
            return role;
        }

        public List<Role> GetAllRoles()
        {
            List<Role> roleList = new List<Role>();
            DbCommand command = Db.GetStoredProcCommand("user.Usp_GetAllRoles");
            IDataReader dr = Db.ExecuteReader(command);
            using (dr)
            {
                while (dr.Read())
                {
                    roleList.Add(RoleMapper(dr));
                }
            }
            return roleList;
        }

        public void InsertRole(Role role)
        {
            var cmd = Db.GetStoredProcCommand("user.Usp_InsertRole");
            Db.AddInParameter(cmd, "@Value", DbType.String, role.Value);
            Db.AddInParameter(cmd, "@ClosureRate", DbType.Decimal, role.ClosureRate);
            Db.ExecuteNonQuery(cmd);
        }

        public void UpdateRole(Role role)
        {
            var cmd = Db.GetStoredProcCommand("user.Usp_UpdateRole");
            Db.AddInParameter(cmd, "@Id", DbType.Int32, role.Id);
            Db.AddInParameter(cmd, "@Value", DbType.String, role.Value);
            Db.AddInParameter(cmd, "@IsActive", DbType.Boolean, role.IsActive);
            Db.AddInParameter(cmd, "@ClosureRate", DbType.Decimal, role.ClosureRate);
            Db.ExecuteNonQuery(cmd);
        }

        public List<Role> GetRolesHasRightToMakeCampaign()
        {
            List<Role> roleList = new List<Role>();
            DbCommand command = Db.GetStoredProcCommand("user.Usp_GetRolesHasRightToMakeCampaign");
            IDataReader dr = Db.ExecuteReader(command);
            using (dr)
            {
                while (dr.Read())
                {
                    roleList.Add(RoleMapper(dr));
                }
            }
            return roleList;
        }

        private Role RoleMapper(IDataReader dr)
        {
            return new Role()
                        {
                            Id = dr["Id"].Convert<int>(),
                            Value = dr["Value"].Convert<string>(),
                            IsAdmin = dr["IsAdmin"].Convert<bool>(),
                            IsActive = dr["IsActive"].Convert<bool>(),
                            ClosureRate = dr["ClosureRate"].Convert<decimal>()
                        };
        }

        #endregion


        public List<LoginHistory> GetLoginHistory(int userId, string userName)
        {
            var historyList = new List<LoginHistory>();
            var command = Db.GetStoredProcCommand("dbo.Usp_GetLoginHistory");
            Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
            Db.AddInParameter(command, "@UserName", DbType.String, userName);
            var dr = Db.ExecuteReader(command);

            using (dr)
            {
                while (dr.Read())
                {
                    historyList.Add(new LoginHistory()
                    {
                        Date = dr["LoginDate"].Convert<DateTime>().ToString("dd/MM/yyyy HH:mm:ss"),
                        Status = dr["Status"].Convert<string>()
                    });
                }
            }
            return historyList;
        }
    }
}
