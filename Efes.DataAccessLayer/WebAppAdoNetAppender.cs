﻿using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class WebAppAdoNetAppender : log4net.Appender.AdoNetAppender
    {
        public string ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["efesCs"].ConnectionString.Base64Decode();
            }
        }
    }
}
