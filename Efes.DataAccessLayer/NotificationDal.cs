﻿using Efes.Entities;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class NotificationDal : BaseDal
    {
        public void Confirm(int id, int currentUserId)
        {
            DbCommand dbCommand = Db.GetStoredProcCommand("dbo.Usp_SetConfirmation");
            Db.AddInParameter(dbCommand, "@NotificationId", DbType.Int32, id);
            //Db.AddInParameter(dbCommand, "@CurrentUserId", DbType.Int32, currentUserId);
            Db.ExecuteNonQuery(dbCommand);
        }

        public List<Notification> GetNotifications(int memberId)
        {
            List<Notification> notifications = new List<Notification>();
            DbCommand dbCommand = Db.GetStoredProcCommand("dbo.usp_GetMemberNotification");
            Db.AddInParameter(dbCommand, "@MemberId", DbType.Int32, memberId);
            IDataReader reader = Db.ExecuteReader(dbCommand);
            using (reader)
            {
                while (reader.Read())
                {
                    Notification notification = new Notification
                    {
                        Id = reader["NotificationId"].Convert<int>(),
                        TypeId = reader["TypeId"].Convert<int>(),
                        Title = reader["Title"].Convert<string>(),
                        From = reader["From"].Convert<string>(),
                        Body = reader["Body"].Convert<string>(),
                        TimeText = reader["TimeText"].Convert<string>(),
                        IsRead = reader["IsRead"].Convert<bool>(),
                        NotificationType = (NotificationType)reader["TypeId"].Convert<int>()
                    };

                    notifications.Add(notification);
                }
            }
            return notifications;
        }

        public void Read(int id)
        {
            DbCommand dbCommand = Db.GetStoredProcCommand("dbo.Usp_SetReadNotification");
            Db.AddInParameter(dbCommand, "@NotificationId", DbType.Int32, id);
            Db.ExecuteNonQuery(dbCommand);
        }
    }
}