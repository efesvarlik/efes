﻿using System;
using Efes.Utility;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Efes.DataAccessLayer
{
    public class BaseDal
    {
        protected Database Db;
        public readonly log4net.ILog Logger = LogHelper.GetLogger();

        public enum DbTypeEnum 
        {
            Efes = 1,
            Apex = 2,
            SesWare = 3,
            EfesStage = 4,
            ExternalMuhasebe = 5
        }

        public BaseDal(DbTypeEnum connectionDbType = DbTypeEnum.Efes)
        {
            try
            {
                string connectionString;
                switch (connectionDbType)
                {
                    case DbTypeEnum.Efes:
                        connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["efesCs"].ConnectionString.Base64Decode();
                        break;
                    case DbTypeEnum.ExternalMuhasebe:
                        connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["externalMuhasebe"].ConnectionString.Base64Decode();
                        break;
                    case DbTypeEnum.Apex:
                        connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["apexCs"].ConnectionString.Base64Decode();
                        break;
                    case DbTypeEnum.SesWare:
                        connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["sesWareCs"].ConnectionString.Base64Decode();
                        break;
                    case DbTypeEnum.EfesStage:
                        connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["efesCsStage"].ConnectionString.Base64Decode();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(connectionDbType), connectionDbType, null);
                }
                Db = new SqlDatabase(connectionString);
            }
            catch (Exception ex)
            {
                Logger.Fatal("BaseDal can not connect to the db", ex);
            }
            
        }
    }
}
