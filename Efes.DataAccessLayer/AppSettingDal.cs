﻿using System;
using System.Data;
using Efes.Entities;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class AppSettingDal : BaseDal
    {
        public AppSetting GetAppSettingById(int id)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetAppSettingById");
                Db.AddInParameter(dbCommand, "@Id", DbType.Int32, id);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        return new AppSetting
                        {
                            Id = reader["Id"].Convert<int>(),
                            Name = reader["Name"].Convert<string>(),
                            Value = reader["Value"].Convert<string>()
                        };
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("GetAppSettingById metodunda hata oluştu.", ex);
                return null;
            }
        }

        public  AppSetting GetAppSettingByName(string name)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetAppSettingByName");
                Db.AddInParameter(dbCommand, "@Name", DbType.String, name);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        return new AppSetting
                        {
                            Id = reader["Id"].Convert<int>(),
                            Name = reader["Name"].Convert<string>(),
                            Value = reader["Value"].Convert<string>()
                        };
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("GetAppSettingByName metodunda hata oluştu.", ex);
                return null;
            }
        }
    }
}
