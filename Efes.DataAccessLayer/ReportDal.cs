﻿using Efes.Entities.Report;
using Efes.Entities.User;
using Efes.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.DataAccessLayer
{
    public class ReportDal: BaseDal
    {
        public List<BeklentiTahsilat> GetBeklentiTahsilat(int userId, int month, int year)
        {
            var beklentiTahsilatList = new List<BeklentiTahsilat>();
            try
            {
                var command = Db.GetStoredProcCommand("Usp_GetAgnetKarne_TahsilatBeklenti");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(command, "@Year", DbType.Int32, year);
                Db.AddInParameter(command, "@Month", DbType.Int32, month);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        beklentiTahsilatList.Add(new BeklentiTahsilat()
                        {
                            Date = dr["REPORT_DATE"].Convert<DateTime>(),
                            Beklenti = dr["Beklenti"].Convert<decimal>(),
                            Tahsilat = dr["Tahsilat"].Convert<decimal>(),
                            CagriSayisi = dr["CagriSayisi"].Convert<int>(),
                            Ulasilan = dr["UlasilanCagri"].Convert<int>(),
                            Protokol = dr["ProtkolAdet"].Convert<int>(),
                            HattaKalma = dr["HattaKalma"].Convert<int>()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetBeklentiTahsilat metodunda hata oluştu.", ex);
            }

            return beklentiTahsilatList;
        }

        public List<Tahsilat> GetTahsilatList(int userId, int month, int year)
        {
            var tahsilatList = new List<Tahsilat>();
            try
            {
                var command = Db.GetStoredProcCommand("Usp_GetAgnetKarne_AnaparaGrubu");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(command, "@Year", DbType.Int32, year);
                Db.AddInParameter(command, "@Month", DbType.Int32, month);
                IDataReader dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        tahsilatList.Add(new Tahsilat()
                        {
                            AnaparaGrubu = dr["AnaparaGrubu"].Convert<string>(),
                            TahsilatAdedi = dr["TahsilatAdet"].Convert<int>(),
                            TahsilatTutar = dr["Tahsilattutar"].Convert<decimal>(),
                            Anapara = dr["Total"].Convert<decimal>(),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetTahsilatList metodunda hata oluştu.", ex);

            }

            return tahsilatList;
        }

        public CikarilanGKF GetCikarilanGkf(int userId, int month, int year)
        {
            CikarilanGKF cikarilanGkf = null;
            try
            {
                var command = Db.GetStoredProcCommand("Usp_GetAgnetKarne_GKF");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(command, "@Year", DbType.Int32, year);
                Db.AddInParameter(command, "@Month", DbType.Int32, month);
                IDataReader dr = Db.ExecuteReader(command);
                using (dr)
                {
                    if (dr.Read())
                    {
                        cikarilanGkf = new CikarilanGKF()
                        {
                            Adet = dr["GKFAdet"].Convert<int>(),
                            AnaparaToplami = dr["AnaparaToplami"].Convert<decimal>(),
                            KapamaBakiyesi = dr["KapamaBakiyesi"].Convert<decimal>(),
                            Yuzdesi = dr["Yuzdesi"].Convert<string>(),
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetCikarilanGkf metodunda hata oluştu.", ex);
            }

            return cikarilanGkf;
        }

        public List<int> GetAgentListesiYearlist()
        {
            var yearList = new List<int>();
            try
            {
                var command = Db.GetStoredProcCommand("Usp_GetAgnetKarne_Date");
                IDataReader dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        yearList.Add(dr["Year"].Convert<int>());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetAgentListesiYearlist metodunda hata oluştu.", ex);
            }

            return yearList;
        }

        public List<User> GetAgentListesiAgentList(int userId)
        {
            var agentList = new List<User>();
            try
            {
                var command = Db.GetStoredProcCommand("Usp_GetAgnetKarne_AgentList");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                IDataReader dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        agentList.Add(new User()
                        {
                            Id = dr["UserId"].Convert<int>(),
                            Name = dr["Name"].Convert<string>(),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetAgentListesiAgentList metodunda hata oluştu.", ex);
            }

            return agentList;
        }
    }
}
