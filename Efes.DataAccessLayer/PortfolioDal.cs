﻿using System;
using System.Collections.Generic;
using Efes.Entities.Portfolio;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class PortfolioDal : BaseDal
    {
        public List<Portfolio> GetPortfolios()
        {
            try
            {
                var portfolioList = new List<Portfolio>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetPortfolios");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        portfolioList.Add(new Portfolio()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Name = reader["Name"].Convert<string>(),
                        });
                    }
                }
                return portfolioList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetPortfolios metodunda hata oluştu.", ex);
                return new List<Portfolio>();
            }
        }
    }
}
