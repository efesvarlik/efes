﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Efes.Entities.Protocol;
using Efes.Models;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class ProtocolDal : BaseDal
    {
        public List<ProtocolCampaignModel> GetProtocolCampaignModels()
        {
            try
            {
                var protocolList = new List<ProtocolCampaignModel>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetProtocolCampaigns");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        protocolList.Add(new ProtocolCampaignModel()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Description = reader["Description"].Convert<string>(),
                            StartDate = reader["StartDate"].Convert<DateTime>(),
                            EndDate = reader["EndDate"].Convert<DateTime>(),
                            CreatorId = reader["CreatorId"].Convert<int>(),
                            CreatedDate = reader["CreatedDate"].Convert<DateTime>(),
                            UpdaterId = reader["UpdaterId"].Convert<int>(),
                            CampaignSuccessRate = reader["CampaignSuccessRate"].Convert<int>(),
                            Creator = reader["Creator"].Convert<string>(),
                            Updater = reader["Updater"].Convert<string>(),
                            Portfolio = reader["Portfolio"].Convert<string>(),
                            UpdatedDate = reader["UpdatedDate"].Convert<DateTime>(),
                            ObligorCount = reader["ObligorCount"].Convert<int>(),
                            StartDateView = reader["StartDate"].Convert<DateTime>().ToString("dd MMMM, yyyy"),
                            EndDateView = reader["EndDate"].Convert<DateTime>().ToString("dd MMMM, yyyy")
                        });
                    }
                }
                return protocolList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetProtocolCampaignModels metodunda hata oluştu.", ex);
                return new List<ProtocolCampaignModel>();
            }
        }

        public bool CreateProtocolCampaign(ProtocolCampaign protocolCampaign, DataTable dtObligor, DataTable dtDetails, int userId)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("[dbo].[Usp_InsertProtocolCampaigns]");
                objDbCmd.Parameters.Add(new SqlParameter("@Description", protocolCampaign.Description));
                objDbCmd.Parameters.Add(new SqlParameter("@StartDate", protocolCampaign.StartDate));
                objDbCmd.Parameters.Add(new SqlParameter("@EndDate", protocolCampaign.EndDate));
                objDbCmd.Parameters.Add(new SqlParameter("@CreatorId", userId));
                objDbCmd.Parameters.Add(new SqlParameter("@TableType", dtObligor) { SqlDbType = SqlDbType.Structured });
                objDbCmd.Parameters.Add(new SqlParameter("@MaxAdvancePaymentDay", protocolCampaign.MaxAdvancePaymentDay));
                objDbCmd.Parameters.Add(new SqlParameter("@MaxInstallmentDay", protocolCampaign.MaxInstallmentDay));
                objDbCmd.Parameters.Add(new SqlParameter("@MinAmount", protocolCampaign.MinAmount));
                objDbCmd.Parameters.Add(new SqlParameter("@MaxAmount", protocolCampaign.MaxAmount));
                objDbCmd.Parameters.Add(new SqlParameter("@Age", protocolCampaign.Age));
                objDbCmd.Parameters.Add(new SqlParameter("@PortfolioId", protocolCampaign.PortfolioId));
                objDbCmd.Parameters.Add(new SqlParameter("@ObligorList", protocolCampaign.ObligorList));
                objDbCmd.Parameters.Add(new SqlParameter("@Details", dtDetails) { SqlDbType = SqlDbType.Structured });

                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("CreateProtocolCampaign işlemi sırasında hata oluştu.", ex);
                return false;
            }
        }

        public bool CreateProtocolCampaignByAgeAndPortfolio(ProtocolCampaign protocolCampaign, DataTable dtDetails, int userId)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("[dbo].[Usp_InsertProtocolCampaignsByAgeAndPortfolio]");
                objDbCmd.Parameters.Add(new SqlParameter("@Description", protocolCampaign.Description));
                objDbCmd.Parameters.Add(new SqlParameter("@StartDate", protocolCampaign.StartDate));
                objDbCmd.Parameters.Add(new SqlParameter("@EndDate", protocolCampaign.EndDate));
                objDbCmd.Parameters.Add(new SqlParameter("@CreatorId", userId));
                objDbCmd.Parameters.Add(new SqlParameter("@MaxAdvancePaymentDay", protocolCampaign.MaxAdvancePaymentDay));
                objDbCmd.Parameters.Add(new SqlParameter("@MaxInstallmentDay", protocolCampaign.MaxInstallmentDay));
                objDbCmd.Parameters.Add(new SqlParameter("@MinAmount", protocolCampaign.MinAmount));
                objDbCmd.Parameters.Add(new SqlParameter("@MaxAmount", protocolCampaign.MaxAmount));
                objDbCmd.Parameters.Add(new SqlParameter("@Age", protocolCampaign.Age));
                objDbCmd.Parameters.Add(new SqlParameter("@PortfolioId", protocolCampaign.PortfolioId));
                objDbCmd.Parameters.Add(new SqlParameter("@ObligorList", protocolCampaign.ObligorList));
                objDbCmd.Parameters.Add(new SqlParameter("@Details", dtDetails) { SqlDbType = SqlDbType.Structured });

                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("CreateProtocolCampaignByAgeAndPortfolio işlemi sırasında hata oluştu.", ex);
                return false;
            }
        }

        public bool InsertProtocolAndDetail(ProtocolModel protocol, DataTable protocolDetail)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("[dbo].[Usp_InsertProtocolAndDetail]");
                Db.AddInParameter(objDbCmd, "@ObligorId", DbType.String, protocol.ObligorId);
                Db.AddInParameter(objDbCmd, "@UserId", DbType.String, protocol.UserId);
                Db.AddInParameter(objDbCmd, "@Status", DbType.Int32, protocol.Status);
                Db.AddInParameter(objDbCmd, "@CampaignId", DbType.Int32, protocol.CampaignId);
                Db.AddInParameter(objDbCmd, "@AdvancePaymentValue", DbType.Decimal, protocol.AdvancePaymentValue);
                Db.AddInParameter(objDbCmd, "@AdvancePaymentDate", DbType.Date, protocol.AdvancePaymentDate);
                Db.AddInParameter(objDbCmd, "@PlanAmount", DbType.Decimal, protocol.TotalPaymentValue);
                Db.AddInParameter(objDbCmd, "@RoleId", DbType.String, protocol.RoleIdMakingProtocol);
                Db.AddInParameter(objDbCmd, "@ObligorTypeId", DbType.String, protocol.ObligorTypeId);
                Db.AddInParameter(objDbCmd, "@Note", DbType.String, protocol.Note);
                Db.AddInParameter(objDbCmd, "@SendInstallmentContract", DbType.Boolean, protocol.SendInstallmentContract);
                Db.AddInParameter(objDbCmd, "@SendSulhContract", DbType.Boolean, protocol.SendSulhContract);
                Db.AddInParameter(objDbCmd, "@SendPaymentPlanToObligor", DbType.Boolean, protocol.SendPaymentPlanToObligor);
                Db.AddInParameter(objDbCmd, "@SmsProtocolReminder", DbType.Boolean, protocol.SmsProtocolReminder);
                Db.AddInParameter(objDbCmd, "@AgencyFee", DbType.Decimal, protocol.AgencyFee);
                objDbCmd.Parameters.Add(new SqlParameter("@ProtocolDetail", protocolDetail));

                Db.ExecuteNonQuery(objDbCmd);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("InsertProtocolAndDetail işlemi sırasında hata oluştu.", ex);
                return false;
            }
        }

        public List<ProtocolCampaign> GetProtocolCampaignListByAccountId(string accountId)
        {
            try
            {
                var protocolCampaignList = new List<ProtocolCampaign>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetProtocolCampaignListByAccountId");
                Db.AddInParameter(dbCommand, "@AccountId", DbType.String, accountId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        protocolCampaignList.Add(new ProtocolCampaign()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Description = reader["Description"].Convert<string>(),
                            StartDate = reader["StartDate"].Convert<DateTime>(),
                            EndDate = reader["EndDate"].Convert<DateTime>(),
                            CreatorId = reader["CreatorId"].Convert<int>(),
                            CreatedDate = reader["CreatedDate"].Convert<DateTime>(),
                            UpdaterId = reader["UpdaterId"].Convert<int>(),
                            UpdatedDate = reader["UpdatedDate"].Convert<DateTime>(),
                            MaxAdvancePaymentDay = reader["MaxAdvancePaymentDay"].Convert<int>(),
                            MaxInstallmentDay = reader["MaxInstallmentDay"].Convert<int>(),
                            MinAmount = reader["MinAmount"].Convert<int>(),
                            MaxAmount = reader["MaxAmount"].Convert<int>(),
                            CashClosingAmount = reader["CashClosingAmount"].Convert<decimal>(),
                            Details = GetProtocolCampaignDetailsById(reader["Id"].Convert<int>()),
                        });
                    }
                }
                return protocolCampaignList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetProtocolCampaignListByAccountId metodunda hata oluştu.", ex);
                return new List<ProtocolCampaign>();
            }
        }

        public List<ProtocolCampaignDetails> GetProtocolCampaignDetailsById(int protocolCampaignId)
        {
            try
            {
                var pcdList = new List<ProtocolCampaignDetails>();

                string key = "GetProtocolCampaignDetailsById" + protocolCampaignId;
                if (CacheManager.Default.Contains(key))
                {
                    var result = CacheManager.Default.Get(key) as List<ProtocolCampaignDetails>;
                    if (result != null)
                    {
                        return result;
                    }
                }

                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetProtocolCampaignDetailsById");
                Db.AddInParameter(dbCommand, "@Id", DbType.String, protocolCampaignId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        pcdList.Add(new ProtocolCampaignDetails()
                        {
                            Id = reader["Id"].Convert<int>(),
                            ProtocolCampaignId = reader["ProtocolCampaignId"].Convert<int>(),
                            MaxInstallmentCount = reader["MaxInstallmentCount"].Convert<int>(),
                            DiscountPercentage = reader["DiscountPercentage"].Convert<int>(),
                            RoleId = reader["RoleId"].Convert<int>()
                        });
                    }
                }

                CacheManager.Default.Add(key, pcdList, 60);

                return pcdList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetProtocolCampaignDetailsById metodunda hata oluştu.", ex);
                return new List<ProtocolCampaignDetails>();
            }
        }

        public List<Protocol> GetProtocolListByObligorId(string obligorId)
        {
            try
            {
                var contactList = new List<Protocol>();
                var dbCommand = Db.GetStoredProcCommand("dbo.GetProtocolListByObligorId");
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        contactList.Add(new Protocol()
                        {
                            Id = reader["Id"].Convert<int>(),
                            ObligorId = reader["ObligorId"].Convert<string>(),
                            CreatedDate = reader["CreatedDate"].Convert<DateTime>(),
                            ConfirmedId = reader["ConfirmedId"].Convert<int>(),
                            CampaignId = reader["CampaignId"].Convert<int>(),
                            AdvancePaymentValue = reader["AdvancePaymentValue"].Convert<int>(),
                            TotalPaymentValue = reader["TotalPaymentValue"].Convert<decimal>(),
                            InstallmentCount = reader["InstallmentCount"].Convert<int>(),
                            HasCompleted = reader["HasCompleted"].Convert<bool>(),
                            IsValid = reader["IsValid"].Convert<bool>()
                        });
                    }
                }
                return contactList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetProtocolListByObligorId metodunda hata oluştu.", ex);
                return new List<Protocol>();
            }
        }

        public List<string> GetProtocolCancelReasonList()
        {
            try
            {
                var list = new List<string>();
                var dbCommand = Db.GetStoredProcCommand("[dbo].[GetProtokolCancelReason]");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        list.Add(reader["Name"].Convert<string>());

                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Logger.Error("GetProtocolCancelReasonList metodunda hata oluştu.", ex);
                return new List<string>();
            }
        }

        public bool DeleteProtocol(int userId, string recId, string reason)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_CancelCurrentProtocol");
                Db.AddInParameter(dbCommand, "@CancelReason ", DbType.String, reason);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@HeaderRecId", DbType.String, recId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("DeleteProtocol metodunda hata oluştu.");
                return false;
            }
        }

        public List<ProtocolApprovementModel> GetProtocolApprovements(int userId)
        {
            try
            {
                var protocolApprovements = new List<ProtocolApprovementModel>();
                var dbCommand = Db.GetStoredProcCommand("dbo.GetProtocolApproval");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        protocolApprovements.Add(new ProtocolApprovementModel()
                        {
                            Id = reader["Id"].Convert<string>(),
                            ProtocolId = reader["ProtocolId"].Convert<int>(),
                            NameSurname = reader["NameSurname"].Convert<string>(),
                            Relation = reader["Relation"].Convert<string>(),
                            CreatedBy = reader["CreatedBy"].Convert<string>(),
                            DebtPrinciple = reader["DebtPrinciple"].Convert<decimal>(),
                            TotalDebtAmount = reader["TotalDebtAmount"].Convert<decimal>(),
                            TotalCreditAmount = reader["TotalCreditAmount"].Convert<decimal>(),
                            PlanAmount = reader["PlanAmount"].Convert<decimal>(),
                            InstallmentCount = reader["InstallmentCount"].Convert<int>(),
                            ObligorAge = reader["ObligorAge"].Convert<int>(),
                            ClosureRate = reader["ClosureRate"].Convert<int>(),
                            CreateDate = reader["CreateDate"].Convert<DateTime>(),
                            GkfUrl = reader["GkfUrl"].Convert<string>(),
                            Colour = (100 - reader["ClosureRate"].Convert<int>() < 50) ? "red" : (reader["ClosureRate"].Convert<int>() < 75 ? "yellow" : "green")
                        });
                    }
                }
                return protocolApprovements;
            }
            catch (Exception ex)
            {
                Logger.Error("GetProtocolApprovements metodunda hata oluştu.", ex);
                return new List<ProtocolApprovementModel>();
            }
        }

        public bool CancelProtocol(int protocolId, int userId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_CancelProtocolWaitingApprovement");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ProtocolId ", DbType.Int32, protocolId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("CancelProtocol metodunda hata oluştu.");
                return false;
            }
        }

        public bool ApproveProtocol(int protocolId, int userId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ApproveProtocolWaitingApprovement");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ProtocolId ", DbType.String, protocolId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ApproveProtocol metodunda hata oluştu.");
                return false;
            }
        }

        public List<ProtocolListModel> DailyProtocolList(int userId)
        {
            try
            {
                var protocolList = new List<ProtocolListModel>();
                var dbCommand = Db.GetStoredProcCommand("dbo.GetDailyProtocolList");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        protocolList.Add(new ProtocolListModel()
                        {
                            Id = reader["Id"].Convert<string>(),
                            ProtocolId = reader["ProtocolId"].Convert<int>(),
                            NameSurname = reader["NameSurname"].Convert<string>(),
                            Relation = reader["Relation"].Convert<string>(),
                            CreatedBy = reader["CreatedBy"].Convert<string>(),
                            DebtPrinciple = reader["DebtPrinciple"].Convert<decimal>(),
                            TotalDebtAmount = reader["TotalDebtAmount"].Convert<decimal>(),
                            TotalCreditAmount = reader["TotalCreditAmount"].Convert<decimal>(),
                            PlanAmount = reader["PlanAmount"].Convert<decimal>(),
                            InstallmentCount = reader["InstallmentCount"].Convert<int>(),
                            ObligorAge = reader["ObligorAge"].Convert<int>(),
                            ClosureRate = reader["ClosureRate"].Convert<int>(),
                            CreateDate = reader["CreateDate"].Convert<DateTime>(),
                            Colour = (100 - reader["ClosureRate"].Convert<int>() < 50) ? "red" : (reader["ClosureRate"].Convert<int>() < 75 ? "yellow" : "green")
                        });
                    }
                }
                return protocolList;
            }
            catch (Exception ex)
            {
                Logger.Error("DailyProtocolList metodunda hata oluştu.", ex);
                return new List<ProtocolListModel>();
            }
        }

        public bool AttachToProtocol(string recId, int planId, int subPlanId, int userId)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("[dbo].[Usp_OdemeyiProtokoleIsle]");
                objDbCmd.Parameters.Add(new SqlParameter("@PaymentRecId", recId));
                objDbCmd.Parameters.Add(new SqlParameter("@PlanType", planId));
                objDbCmd.Parameters.Add(new SqlParameter("@PlanSubType", subPlanId));
                objDbCmd.Parameters.Add(new SqlParameter("@UserId", userId));

                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("AttachToProtocol işlemi sırasında hata oluştu.", ex);
                return false;
            }
        }

        public bool DeAttachToProtocol(string recId, bool deAttachObligor, int userId)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("[dbo].[Usp_OdemeyiProtokoldenGeriCek]");
                objDbCmd.Parameters.Add(new SqlParameter("@PaymentRecId", recId));
                objDbCmd.Parameters.Add(new SqlParameter("@DeAttachObligor", deAttachObligor));
                objDbCmd.Parameters.Add(new SqlParameter("@UserId", userId));

                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("DeAttachToProtocol işlemi sırasında hata oluştu.", ex);
                return false;
            }
        }

        public bool InsertPaymentPromise(PaymentPromise paymentPromise)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("[dbo].[Usp_InsertPaymentPromise_V0001]");
                objDbCmd.Parameters.Add(new SqlParameter("@AdvancePayment", paymentPromise.AdvancePayment));
                objDbCmd.Parameters.Add(new SqlParameter("@Date", paymentPromise.Date));
                objDbCmd.Parameters.Add(new SqlParameter("@InstallmentCount", paymentPromise.InstallmentCount));
                objDbCmd.Parameters.Add(new SqlParameter("@PlanAmount", paymentPromise.PlanAmount));
                objDbCmd.Parameters.Add(new SqlParameter("@obligorId", paymentPromise.ObligorId));
                objDbCmd.Parameters.Add(new SqlParameter("@UserId", paymentPromise.UserId));

                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("InsertPaymentPromise işlemi sırasında hata oluştu.", ex);
                return false;
            }
        }

        public bool CancelPaymentPromise(int userId, int promiseId)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("[dbo].[Usp_CancelPaymentPromise]");
                objDbCmd.Parameters.Add(new SqlParameter("@UserId", userId));
                objDbCmd.Parameters.Add(new SqlParameter("@PromiseId", promiseId));

                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("CancelPaymentPromise işlemi sırasında hata oluştu.", ex);
                return false;
            }
        }

        public List<PaymentPromise> GetPaymentPromiseReport(int userId, DateTime startDate, DateTime endDate)
        {
            try
            {
                var paymentPromiseList = new List<PaymentPromise>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetPaymentPromise_V0001");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@StartDate", DbType.DateTime, startDate);
                Db.AddInParameter(dbCommand, "@EndDate", DbType.DateTime, endDate);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        paymentPromiseList.Add(new PaymentPromise()
                        {
                            Id = reader["Id"].Convert<int>(),
                            AccountId = reader["AccountID"].Convert<string>(),
                            Name = reader["Name"].Convert<string>(),
                            PlanAmount = reader["PlanAmount"].Convert<decimal>(),
                            AdvancePayment = reader["AdvancePayment"].Convert<decimal>(),
                            Date = reader["PromiseDate"].Convert<DateTime>(),
                            InstallmentCount = reader["InstallmentCount"].Convert<int>(),
                            CreateDate = reader["CreateDate"].Convert<DateTime>(),
                        });
                    }
                }
                return paymentPromiseList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetPaymentPromiseReport metodunda hata oluştu.", ex);
                return new List<PaymentPromise>();
            }
        }

        public int SendCancelProtocolToCollaction(string id)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("Usp_SendCancelProtocolToCollaction");
                Db.AddInParameter(dbCommand, "@Id", DbType.String, id);
                return Db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception ex)
            {
                ex.Log("SendCancelProtocolToCollaction metodunda hata oluştu.");
                return -1;
            }
        }

    }
}
