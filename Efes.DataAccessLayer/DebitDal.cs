﻿using System;
using System.Data;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class DebitDal : BaseDal
    {
        public bool DoExecution(int userId, string recId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_DoExecution");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@AccountRecId", DbType.String, recId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("DoExecution metodunda hata oluştu.");
                return false;
            }
        }
    }
}
