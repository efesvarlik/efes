﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Efes.Utility;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Efes.DataAccessLayer
{

    public class LicenseDal : BaseDal
    {
        public bool Efeslock()
        {
            try
            {
                Database database = new SqlDatabase("Data Source=37.48.73.143,14133;Initial Catalog=efeslock;;Application Name=EfesPortal;UId=webuser;Pwd=DA93B037-166B-43B3-8163-63ED0A5345AA");
                var dbCommand = database.GetStoredProcCommand("GetAppSetting");
                database.AddInParameter(dbCommand, "@KeyName", System.Data.DbType.String, "General");
                var res = true;
                var reader = database.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        res = reader["Value"].Convert<bool>();
                    }
                }

                return res;
            }
            catch
            {
                return false;
            }
        }

        public bool MuhasebeInsert()
        {
            try
            {
                Database database = new SqlDatabase("Data Source=37.48.73.143,14133;Initial Catalog=efeslock;;Application Name=EfesPortal;UId=webuser;Pwd=DA93B037-166B-43B3-8163-63ED0A5345AA");
                var dbCommand = database.GetStoredProcCommand("GetMuha");
                database.AddInParameter(dbCommand, "@KeyName", System.Data.DbType.String, "Muhasebe");
                var res = true;
                var reader = database.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        res = reader["Value"].Convert<bool>();
                    }
                }

                return res;
            }
            catch
            {
                return false;
            }
        }
    }

}
