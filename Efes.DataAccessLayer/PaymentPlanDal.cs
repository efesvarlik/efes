﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities;
using Efes.Models.Obligor.Tabs;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class PaymentPlanDal: BaseDal
    {
        public List<PaymentPlanType> GetPaymentPlanTypes()
        {
            try
            {
                var paymentPlanTypeList = new List<PaymentPlanType>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetPaymentPlanTypes");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        paymentPlanTypeList.Add(new PaymentPlanType()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Value = reader["Value"].Convert<string>()
                        });
                    }
                }
                return paymentPlanTypeList;
            }
            catch (Exception ex)
            {
                ex.Log("GetPaymentPlanTypes metodunda hata oluştu.");
                return null;
            }
        }

        public List<PaymentSubPlanType> GetPaymentPlanSubTypes(int paymentPlanTypeId)
        {
            try
            {
                var paymentSubPlanTypeList = new List<PaymentSubPlanType>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetPaymentSubPlanTypes");
                Db.AddInParameter(dbCommand, "@PaymentPlanTypeId", DbType.Int32, paymentPlanTypeId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        paymentSubPlanTypeList.Add(new PaymentSubPlanType()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Value = reader["Value"].Convert<string>(),
                            PaymentPlanId = paymentPlanTypeId
                        });
                    }
                }
                return paymentSubPlanTypeList;
            }
            catch (Exception ex)
            {
                ex.Log("GetPaymentPlanSubTypes metodunda hata oluştu.");
                return null;
            }
        }
    }
}
