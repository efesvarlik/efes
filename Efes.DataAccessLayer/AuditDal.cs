﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography.X509Certificates;
using Efes.Entities;
using Efes.Models;
using Efes.Utility;
using Exception = System.Exception;

namespace Efes.DataAccessLayer
{
    public class AuditDal: BaseDal
    {
        public AuditDal() : base(DbTypeEnum.Apex)
        {

        }
        public List<Audit> GetAuditsLast30Minutes()
        {
            try
            {
                var auditList = new List<Audit>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetEventViewData");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        //Audit audit = AuditMapper(reader);
                        auditList.Add(AuditMapper(reader));
                    }
                }
                return auditList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetAuditsLast30Minutes metodunda hata oluştu." , ex);
               return new List<Audit>();
            }
           
        }

        private static Audit AuditMapper(IDataReader reader)
        {
            return new Audit()
            {
                Id = reader["Id"].Convert<int>(),
                ServerName = reader["ServerName"].Convert<string>(),
                StartTime = reader["StartTime"].Convert<DateTime>(),
                ApplicationName = reader["ApplicationName"].Convert<string>(),
                ClientHostName = reader["ClientHostName"].Convert<string>(),
                LoginName = reader["LoginName"].Convert<string>(),
                DatabaseName = reader["DatabaseName"].Convert<string>(),
                SchemaName = reader["SchemaName"].Convert<string>(),
                ObjectName = reader["ObjectName"].Convert<string>(),
                TextData = reader["TextData"].Convert<string>(),
                Operation = reader["Operation"].Convert<int>(),
                AccessedObjectsList = reader["AccessedObjectsList"].Convert<string>(),
                RowVersion = reader["RowVersion"].Convert<TimestampInformation>(),
            };
        }
    }
}
