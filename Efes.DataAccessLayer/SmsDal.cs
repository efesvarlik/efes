﻿using System;
using System.Collections.Generic;
using Efes.Entities.Sms;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class SmsDal : BaseDal
    {
        public List<Template> GetSmsTemplates()
        {
            try
            {
                var tmpList = new List<Template>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetSmsTemplates");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        tmpList.Add(new Template
                        {
                            Id = reader["Id"].Convert<string>(),
                            Name = reader["Name"].Convert<string>(),
                            TemplateText = reader["TemplateText"].Convert<string>(),
                        });
                    }
                }
                return tmpList;
            }
            catch (Exception ex)
            {
                ex.Log("GetSmsTemplates metodunda hata oluştu.");
                return null;
            }
        }

        public List<string> GetBannedWords()
        {
            try
            {
                var tmpList = new List<string>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetBannedWords");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        tmpList.Add(reader["Word"].Convert<string>());
                    }
                }
                return tmpList;
            }
            catch (Exception ex)
            {
                ex.Log("GetBannedWords metodunda hata oluştu.");
                return null;
            }
        }
    }
}
