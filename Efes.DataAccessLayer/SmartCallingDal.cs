﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Efes.Entities.SmartCalling;
using Efes.Models.SmartCalling;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class SmartCallingDal : BaseDal
    {
        private readonly PhoneDal _phoneDal;
        public SmartCallingDal()
        {
            _phoneDal = new PhoneDal();
        }

        public NewAppointedModel GetNewAppointedList(int userId, int type)
        {
            try
            {
                var newAppointedModel = new NewAppointedModel();
                var newAppointedList = new List<NewAppointed>();
                var dbCommand = Db.GetStoredProcCommand("Usp_SmartCallingGetMyAccountsByDate");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Type", DbType.Int32, type);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        newAppointedList.Add(new NewAppointed
                        {
                            AccountId = reader["AccountID"].Convert<string>(),
                            AdSoyad = reader["AdSoyad"].Convert<string>(),
                            AtamaTarihi = reader["AtamaTarihi"].Convert<DateTime>(),
                            DosyaSahibi = reader["DosyaSahibi"].Convert<string>(),
                            Portfoy = reader["Portfoy"].Convert<string>(),
                            ToplamRisk = reader["ToplamRisk"].Convert<decimal>(),
                            Anapara = reader["Anapara"].Convert<decimal>(),
                            Phone = reader["PhoneNumber"].Convert<string>(),
                            IsCalledToday = reader["IsCalledToday"].Convert<bool>()
                        });
                    }
                }
                newAppointedModel.NewAppointedList = newAppointedList;
                newAppointedModel.FakeNumbers = _phoneDal.GetFakeNumbers();
                return newAppointedModel;
            }
            catch (Exception ex)
            {
                Logger.Error("GetNewAppointedList metodunda hata oluştu.", ex);
                return new NewAppointedModel();
            }
        }

        public TodayPaymentsModel GetTodayPaymentsList(int userId)
        {
            try
            {
                var todayPaymentModel = new TodayPaymentsModel();
                var todayPaymentsList = new List<TodayPayments>();
                var dbCommand = Db.GetStoredProcCommand("Usp_SmartCallingGetTodayPaymentsByAgent");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        todayPaymentsList.Add(new TodayPayments
                        {
                            AccountId = reader["AccountID"].Convert<string>(),
                            AdSoyad = reader["AdSoyad"].Convert<string>(),
                            TaksitNumarasi = reader["TaksitNumarasi"].Convert<int>(),
                            OdemeTarihi = reader["OdemeTarihi"].Convert<DateTime>(),
                            ToplamProtokolTutari = reader["ToplamProtokolTutari"].Convert<decimal>(),
                            OdenecekTutar = reader["OdenecekTutar"].Convert<decimal>(),
                            OdenenTutar = reader["OdenenTutar"].Convert<decimal>(),
                            KalanTutar = reader["KalanTutar"].Convert<decimal>(),
                            OdemeDurumu = reader["OdemeDurumu"].Convert<string>(),
                            ProtokolTarihi = reader["ProtokolTarihi"].Convert<DateTime>(),
                            ProtokolOlusturan = reader["ProtokolOlusturan"].Convert<string>(),
                            GecikmadekiTaksitAdedi = reader["GecikmadekiTaksitAdedi"].Convert<int>(),
                            PhoneNumber = reader["PhoneNumber"].Convert<string>(),
                            IsCalledToday = reader["IsCalledToday"].Convert<bool>(),
                            PaymnetStatus = reader["PaymentStatus"].Convert<bool>(),
                        });
                    }
                }

                todayPaymentModel.TodayPaymentsList = todayPaymentsList;
                todayPaymentModel.FakeNumbers = _phoneDal.GetFakeNumbers();
                return todayPaymentModel;
            }
            catch (Exception ex)
            {
                Logger.Error("GetTodayPaymentsList metodunda hata oluştu.", ex);
                return new TodayPaymentsModel();
            }
        }

        public CancelProtocolModel GetCancelProtocolList(int userId)
        {
            try
            {
                var cancelProtocolModel = new CancelProtocolModel();
                var cancelProtocolList = new List<CancelProtocol>();
                var dbCommand = Db.GetStoredProcCommand("Usp_SmartCallingGetObligorWithNotActiveProtocol");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        cancelProtocolList.Add(new CancelProtocol
                        {
                            AccountId = reader["AccountID"].Convert<string>(),
                            AdSoyad = reader["AdSoyad"].Convert<string>(),
                            Banka = reader["Banka"].Convert<string>(),
                            ToplamRisk = reader["ToplamRisk"].Convert<decimal>(),
                            AnaPara = reader["AnaPara"].Convert<decimal>(),
                            OdenenTutar = reader["OdenenTutar"].Convert<decimal>(),
                            Son3KritikNot = reader["Son3KritikNot"].Convert<string>(),
                            SonOdemeTarihi = reader["SonOdemeTarihi"].Convert<DateTime>(),
                            Phone = reader["PhoneNumber"].Convert<string>(),
                            IsCalledToday = reader["IsCalledToday"].Convert<bool>()
                        });
                    }
                }
                cancelProtocolModel.CancelProtocolist = cancelProtocolList;
                cancelProtocolModel.FakeNumbers = _phoneDal.GetFakeNumbers();
                return cancelProtocolModel;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCancelProtocolList metodunda hata oluştu.", ex);
                return new CancelProtocolModel();
            }
        }

        public ExpiredModel GetExpiredList(int userId)
        {
            try
            {
                var expiredModel = new ExpiredModel();
                var expiredList = new List<Expired>();
                var dbCommand = Db.GetStoredProcCommand("Usp_SmartCallingVadesiGecenDosyalar");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        expiredList.Add(new Expired
                        {
                            AccountId = reader["Id"].Convert<string>(),
                            Name = reader["Borclu"].Convert<string>(),
                            Portfolio = reader["Portföy"].Convert<string>(),
                            DebtPrinciple = reader["Anapara"].Convert<decimal>(),
                            ExpiredAmount = reader["GecikenOdeme"].Convert<decimal>(),
                            InstallmentCount = reader["TaksitSayisi"].Convert<int>(),
                            PhoneNumber = reader["PhoneNumber"].Convert<string>(),
                            IsCalledToday = reader["IsCalledToday"].Convert<bool>()
                        });
                    }
                }
                expiredModel.ExpiredList = expiredList;
                expiredModel.FakeNumbers = _phoneDal.GetFakeNumbers();
                return expiredModel;
            }
            catch (Exception ex)
            {
                Logger.Error("GetExpiredList metodunda hata oluştu.", ex);
                return new ExpiredModel();
            }
        }

        public List<ByCapitalGroup> GetByCapitalGroupList(int userId, int debtGroupType, int portfolioId)
        {
            try
            {
                var byCapitalGroupModel = new ByCapitalGroupModel();
                var byCapitalGroupList = new List<ByCapitalGroup>();
                var dbCommand = Db.GetStoredProcCommand("[dbo].[Usp_SmartCallingGetAccountByDebtGroup]");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@DebtGroupType", DbType.Int32, debtGroupType);
                Db.AddInParameter(dbCommand, "@PortfolioId", DbType.Int32, portfolioId);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        byCapitalGroupList.Add(new ByCapitalGroup
                        {
                            AccountId = reader["AccountID"].Convert<string>(),
                            Name = reader["AdSoyad"].Convert<string>(),
                            Portfolio = reader["Portfoy"].Convert<string>(),
                            TotalDebtAmount = reader["ToplamRisk"].Convert<decimal>(),
                            DebtPrinciple = reader["Anapara"].Convert<decimal>(),
                            PhoneNumber = reader["PhoneNumber"].Convert<string>(),
                            IsCalledToday = reader["IsCalledToday"].Convert<bool>()
                        });
                    }
                }

                byCapitalGroupModel.ByCapitalGroupList = byCapitalGroupList;

                return byCapitalGroupList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetByCapitalGroupList metodunda hata oluştu.", ex);
                return new List<ByCapitalGroup>();
            }
        }

        public Dictionary<int, string> GetDebtGroup(int userId)
        {
            try
            {
                var dic = new Dictionary<int, string>();
                var dbCommand = Db.GetStoredProcCommand("[dbo].[Usp_GetDebtGroup]");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        dic.Add(reader["Id"].Convert<int>(), reader["Description"].Convert<string>());
                    }
                }

                return dic;
            }
            catch (Exception ex)
            {
                Logger.Error("GetDebtGroup metodunda hata oluştu.", ex);
                return new Dictionary<int, string>();
            }
        }

        public CollectionWithoutProtocolModel GetCollectionWithoutProtocolList(int userId)
        {
            try
            {
                var collectionWithoutProtocolModel = new CollectionWithoutProtocolModel();
                var collectionWithoutProtocolList = new List<CollectionWithoutProtocol>();
                var dbCommand = Db.GetStoredProcCommand("Usp_SmartCallingGetObligorWithPaymentNotActiveProtocol");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        collectionWithoutProtocolList.Add(new CollectionWithoutProtocol
                        {
                            AccountId = reader["AccountID"].Convert<string>(),
                            AdSoyad = reader["AdSoyad"].Convert<string>(),
                            Banka = reader["Banka"].Convert<string>(),
                            ToplamRisk = reader["ToplamRisk"].Convert<decimal>(),
                            AnaPara = reader["AnaPara"].Convert<decimal>(),
                            OdenenTutar = reader["OdenenTutar"].Convert<decimal>(),
                            Son3KritikNot = reader["Son3KritikNot"].Convert<string>(),
                            SonOdemeTarihi = reader["SonOdemeTarihi"].Convert<DateTime>(),
                            Phone = reader["PhoneNumber"].Convert<string>(),
                            IsCalledToday = reader["IsCalledToday"].Convert<bool>()
                        });
                    }
                }
                collectionWithoutProtocolModel.CollectionWithoutProtocolList = collectionWithoutProtocolList;
                collectionWithoutProtocolModel.FakeNumbers = _phoneDal.GetFakeNumbers();

                return collectionWithoutProtocolModel;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCollectionWithoutProtocolList metodunda hata oluştu.", ex);
                return new CollectionWithoutProtocolModel();
            }
        }

        #region Dialer

        public bool StartDialer(DataTable dtAgents, DataTable dtAccounts, int userId, string bandwidth, int maxWaitingTime, bool useFct)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("[dbo].[Usp_StartDialler]");
                objDbCmd.CommandTimeout = 0;

                Db.AddInParameter(objDbCmd, "@UserId ", DbType.Int32, userId);
                objDbCmd.Parameters.Add(new SqlParameter("@AccountList", dtAccounts) { SqlDbType = SqlDbType.Structured });
                objDbCmd.Parameters.Add(new SqlParameter("@AgentList", dtAgents) { SqlDbType = SqlDbType.Structured });
                objDbCmd.Parameters.Add(new SqlParameter("@Bandwidth", bandwidth) { SqlDbType = SqlDbType.VarChar });
                objDbCmd.Parameters.Add(new SqlParameter("@MaxWaitingTime", maxWaitingTime) { SqlDbType = SqlDbType.Int });
                objDbCmd.Parameters.Add(new SqlParameter("@UseFct", useFct) { SqlDbType = SqlDbType.Bit });

                Db.ExecuteNonQuery(objDbCmd);
                return true;
            }
            catch (Exception ex)
            {
                //Logger.Error("StartDialer işlemi sırasında hata oluştu.", ex);
                return true;
            }
        }

        public DialerStatus GetCurrentDialerStatus()
        {
            try
            {
                var dialerStatus = DialerStatus.Passive;
                var dbCommand = Db.GetStoredProcCommand("[dbo].[Usp_GetCurrentDialerStatus]");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        dialerStatus = (DialerStatus)reader["Status"].Convert<int>();
                    }
                }
                return dialerStatus;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCurrentDialerStatus işlemi sırasında hata oluştu.", ex);
                return DialerStatus.Passive;
            }
        }

        public List<DialerCall> GetDialerCallList()
        {
            try
            {
                var dialerCallList = new List<DialerCall>();
                var dbCommand = Db.GetStoredProcCommand("[dbo].[Usp_GetDialerCallList]");

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        dialerCallList.Add(new DialerCall
                        {
                            AccountId = reader["AccountId"].Convert<string>(),
                            Agent = reader["Agent"].Convert<string>(),
                            PhoneNumber = reader["Phone"].Convert<string>(),
                            CallDuration = reader["CallDuration"].Convert<int>(),
                            CallStatus = reader["CallResult"].Convert<string>()
                        });
                    }
                }

                return dialerCallList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetDialerCallList metodunda hata oluştu.", ex);
                return new List<DialerCall>();
            }
        }

        public DialerDetail GetCurrentDialerDetail()
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("[dbo].[Usp_GetCurrentDialerDetail]");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        return new DialerDetail()
                        {
                            Total = reader["Total"].Convert<int>(),
                            Finished = reader["Finished"].Convert<int>(),
                            Remain = reader["Remain"].Convert<int>(),
                            StartDate = reader["StartDate"].Convert<DateTime>(),
                            DialerRecId = reader["DialerRecId"].Convert<string>()
                        };
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCurrentDialerDetail işlemi sırasında hata oluştu.", ex);
                return null;
            }
        }

        public bool ChangeDialerCallStatus(int userId, int status, string recId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ChangeDialerStatus");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@DialerRecId", DbType.String, recId);
                Db.AddInParameter(dbCommand, "@Status", DbType.String, status);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("InsertReminder metodunda hata oluştu.");
                return false;
            }
        }

        public PersonalAutoDialerModel GetPersonalAutoDialerList(int userId)
        {
            try
            {
                var personalAutoDialerModel = new PersonalAutoDialerModel();
                var personalAutoDialerList = new List<PersonalAutoDialer>();
                var dbCommand = Db.GetStoredProcCommand("");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        personalAutoDialerList.Add(new PersonalAutoDialer
                        {
                            //AccountId = reader["AccountId"].Convert<string>(),
                        });
                    }
                }
                personalAutoDialerModel.PersonalAutoDialerList = personalAutoDialerList;
                personalAutoDialerModel.FakeNumbers = _phoneDal.GetFakeNumbers();
                return personalAutoDialerModel;
            }
            catch (Exception ex)
            {
                Logger.Error("GetPersonalAutoDialerList metodunda hata oluştu.", ex);
                return new PersonalAutoDialerModel();
            }
        }

        public List<DialerAgent> GetDialerAgentList()
        {
            try
            {
                var dialerAgentList = new List<DialerAgent>();
                var dbCommand = Db.GetStoredProcCommand("[dbo].[Usp_GetActiveAgentListForDia]");

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        dialerAgentList.Add(new DialerAgent
                        {
                            AgentName = reader["AgentName"].Convert<string>(),
                            LastCalledDate = reader["LastCalledDate"].Convert<DateTime>(),
                        });
                    }
                }

                return dialerAgentList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetDialerAgentList metodunda hata oluştu.", ex);
                return new List<DialerAgent>();
            }
        }

        public bool RemoveAgentFromDialerList(string agentName, int userId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_RemoveAgentFromDia");
                Db.AddInParameter(dbCommand, "@agent", DbType.String, agentName);
                Db.AddInParameter(dbCommand, "@UserId ", DbType.Int32, userId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("RemoveAgentFromDialerList metodunda hata oluştu.");
                return false;
            }
        }
        #endregion
    }
}
