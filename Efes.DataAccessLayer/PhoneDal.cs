﻿using System;
using System.Collections.Generic;
using System.Data;
using Efes.Entities;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class PhoneDal : BaseDal
    {
        public List<Phone> GetPhones(string obligorId)
        {
            try
            {
                var phones = new List<Phone>();
                var dbCommand = Db.GetStoredProcCommand("[obligor].[Usp_GetPhoneList]");
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        var phone = new Phone()
                        {
                            Number = reader["Phone"].Convert<string>().Replace("\r", "").Replace("\n", ""),
                            Description = reader["Description"].Convert<string>()
                        };
                        if (phone.Number.Length != 11) { continue; }
                        phone.NumberText = phone.Number.Trim().Substring(0, 3) + " " +
                                           phone.Number.Trim().Substring(3, 3) + " " +
                                           phone.Number.Trim().Substring(6, 2) + " " +
                                           phone.Number.Trim().Substring(8, 2);
                        phone.NumberText += string.IsNullOrWhiteSpace(phone.Description) ? "" : " (" + phone.Description + ")";
                        phones.Add(phone);
                    }
                }
                return phones;
            }
            catch (Exception ex)
            {
                ex.Log("GetPhones metodunda hata oluştu.");
                return new List<Phone>();
            }
        }

        public List<Phone> GetFakeNumbers()
        {
            try
            {
                var phones = new List<Phone>();
                var dbCommand = Db.GetStoredProcCommand("[dbo].[Usp_GetFakeNumberList]");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        phones.Add(new Phone()
                        {
                            Number = reader["Phone"].Convert<string>().Replace("\r", "").Replace("\n", ""),
                            Fct = reader["Fct"].Convert<string>()
                        });

                    }
                }
                return phones;
            }
            catch (Exception ex)
            {
                ex.Log("GetFakeNumbers metodunda hata oluştu.");
                return new List<Phone>();
            }
        }
    }
}
