﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Efes.Entities.Obligor;
using Efes.Entities.User;
using Efes.Models.Obligor;
using Efes.Models.Obligor.Tabs;
using Efes.Models.Obligor.Tabs.SmsHistory;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class ObligorDal : BaseDal
    {
        #region General Methods

        public List<Obligor> GetObligorList(string searchString, ObligorSearchType obligorSearchType, int userId)
        {
            try
            {
                var obligorList = new List<Obligor>();
                var dbCommand = Db.GetStoredProcCommand("GetObligorListBySearchString");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@SearchString", DbType.String, searchString);
                Db.AddInParameter(dbCommand, "@SearchType", DbType.Int32, (int)obligorSearchType);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        obligorList.Add(new Obligor
                        {
                            AccountId = reader["AccountId"].Convert<string>(),
                            Gsm = reader["Gsm"].Convert<string>(),
                            Id = reader["Id"].Convert<string>(),
                            Name = reader["Name"].Convert<string>(),
                            Surname = reader["Surname"].Convert<string>(),
                            TcNo = reader["TCKN"].Convert<string>(),
                            Portfoy = reader["Portfoy"].Convert<string>(),
                            RelationAboutDept = reader["RelationAboutDept"].Convert<string>(),
                            ExecutionFile = reader["ExecutionFile"].Convert<string>(),
                            ExecutionDetail = reader["ExecutionDetail"].Convert<string>(),
                            TotalPrincipal = reader["TotalPrincipal"].Convert<string>(),
                            TotalRisk = reader["TotalRisk"].Convert<string>(),
                            //Bank = reader["Bank"].Convert<string>(),
                            IsBiggest = reader["IsBiggest"].Convert<bool>(),
                            Owner = reader["Owner"].Convert<string>(),
                            ContactRecId = reader["ContactRecId"].Convert<string>()
                        });
                    }
                }

                return obligorList.OrderBy(c => c.TcNo).ThenBy(t => t.TotalRisk).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetObligorList metodunda hata oluştu.", ex);
                return new List<Obligor>();
            }
        }

        public string GetAccountIdByObligorId(string obligorId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetAccountIdByObligorId");
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        return reader["AccountId"].Convert<string>();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                ex.Log("GetAccountIdByObligorId metodunda hata oluştu.");
                return null;
            }
        }

        public ObligorModel GetObligorByAccountId(string accountId, string contactId, int userId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("member.Usp_GetObligorByAccountId_V0001");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@AccountId", DbType.String, accountId);
                Db.AddInParameter(dbCommand, "@ContactId", DbType.String, contactId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        return ObligorModelMapper(reader);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                ex.Log("GetObligorByAccountId metodunda hata oluştu.");
                return null;
            }
        }

        public static string GetSearchSpNameAccordingToObligorSearchType(ObligorSearchType obligorSearchType)
        {
            var spName = "";

            switch (obligorSearchType)
            {
                case ObligorSearchType.NameAndSurname:
                    spName = "GetObligorListByNameSurname";
                    break;
                case ObligorSearchType.Tckn:
                    spName = "GetObligorListByTckn";
                    break;
                case ObligorSearchType.PhoneNumber:
                    spName = "GetObligorListByPhoneNumber";
                    break;
                case ObligorSearchType.BankAccountNumber:
                    spName = "GetObligorListByBankAccountNumber";
                    break;
                case ObligorSearchType.ProtocolNumber:
                    spName = "GetObligorListByProtocolNumber";
                    break;
                case ObligorSearchType.ExecutionFileNumber:
                    spName = "GetObligorListByExecutionFileNumber";
                    break;
                case ObligorSearchType.CollectionWage:
                    spName = "GetObligorListByCollectionWage";
                    break;
                case ObligorSearchType.AccountId:
                    spName = "GetObligorListByAccountId";
                    break;
                case ObligorSearchType.Score:
                    spName = "GetObligorListByScore";
                    break;
            }

            return spName;
        }

        private ObligorModel ObligorModelMapper(IDataReader reader)
        {
            var obligorModel = new ObligorModel
            {
                AccountId = reader["AccountId"].Convert<string>(),
                Gsm = reader["Gsm"].Convert<string>(),
                Id = reader["Id"].Convert<string>(),
                Name = reader["Name"].Convert<string>(),
                Surname = reader["Surname"].Convert<string>(),
                TcNo = reader["TCKN"].Convert<string>(),
                Kisi = reader["Kisi"].Convert<string>(),
                BorcTakipId = reader["BorcTakipId"].Convert<string>(),
                DosyaNo = reader["DosyaNo"].Convert<string>(),
                BankaAdi = reader["BankaAdi"].Convert<string>(),
                Portfoy = reader["Portfoy"].Convert<string>(),
                DosyaSahibi = reader["DosyaSahibi"].Convert<string>(),
                DabToplam = reader["DabToplam"].Convert<string>(),
                DabAnapara = reader["DabAnapara"].Convert<string>(),
                DabFaiz = reader["DabFaiz"].Convert<string>(),
                DabMasraf = reader["DabMasraf"].Convert<string>(),
                DabEkMasraflar = reader["DabEkMasraflar"].Convert<string>(),
                TeToplam = reader["TeToplam"].Convert<string>(),
                TeAnapara = reader["TeAnapara"].Convert<string>(),
                TeFaiz = reader["TeFaiz"].Convert<string>(),
                TeMasraf = reader["TeMasraf"].Convert<string>(),
                TeSonTahsilatTarihi = reader["TeSonTahsilatTarihi"].Convert<string>(),
                TeIadeAnapara = reader["TeIadeAnapara"].Convert<string>(),
                BToplam = reader["BToplam"].Convert<string>(),
                BAnapara = reader["BAnapara"].Convert<string>(),
                BFaiz = reader["BFaiz"].Convert<string>(),
                BMasraf = reader["BMasraf"].Convert<string>(),
                BHesFaiz = reader["BHesFaiz"].Convert<string>(),
                BHesFaizTarihi = reader["BHesFaizTarihi"].Convert<string>(),
                KisiDurumu = reader["KisiDurumu"].Convert<string>(),
                BankaMusteriNo = reader["BankaMusteriNo"].Convert<string>(),
                Cinsiyet = reader["Cinsiyet"].Convert<string>(),
                DogumTarihi = reader["DogumTarihi"].Convert<string>(),
                AnneKizlikSoyadi = reader["AnneKizlikSoyadi"].Convert<string>(),
                BabaAdi = reader["BabaAdi"].Convert<string>(),
                EsininAdi = reader["EsininAdi"].Convert<string>(),
                IlkSoyadi = reader["IlkSoyadi"].Convert<string>(),
                MedeniDurumu = reader["MedeniDurumu"].Convert<string>(),
                AktifProtokolNo = reader["AktifProtokolNo"].Convert<string>(),
                ProtokolToplam = reader["ProtokolToplam"].Convert<string>(),
                SonTahsilatTarihi = reader["SonTahsilatTarihi"].Convert<string>(),
                AtananHukukBurosu = reader["AtananHukukBurosu"].Convert<string>(),
                AtanmaTarihi = reader["AtanmaTarihi"].Convert<string>(),
                TemlikDuzeltmeTarihi = reader["TemlikDuzeltmeTarihi"].Convert<string>(),
                Meslek = reader["Meslek"].Convert<string>(),
                CalismaDurumu = reader["CalismaDurumu"].Convert<string>(),
                OrtalamaGelir = reader["OrtalamaGelir"].Convert<string>(),
                CocukSayisi = reader["CocukSayisi"].Convert<string>(),
                Sgk = reader["Sgk"].Convert<string>(),
                SgkSicilNo = reader["SgkSicilNo"].Convert<string>(),
                IsyeriKodu = reader["IsyeriKodu"].Convert<string>(),
                KisiTipiId = reader["KisiTipiId"].Convert<int>(),
                IcraDaireDosya = reader["IcraDaireDosya"].Convert<string>(),
                DosyaAnaKisisi = reader["DosyaAnaKisisi"].Convert<string>(),
                Status = reader["Status"].Convert<bool>(),
                SmsTemplateDictionary = new Dictionary<string, string>
                {//Tab4 te gosterilen sms Template ler icerisindeki degsikenler set ediliyor.
                    {"<TKDISPLAYNAME>", (reader["Name"].Convert<string>() + " " + reader["Surname"].Convert<string>()).Trim()},
                    //{"<TKAGENTTELNO>", "0" + Authentication.UserInfo.PhoneNumber},
                    {"<TKSOURCETEXT>", reader["Portfoy"].Convert<string>()},
                    {"<TKACCOUNTID>", reader["AccountId"].Convert<string>()},
                },
                HasActiveProtocol = reader["HasActiveProtocol"].Convert<bool>(),
                HasWaitingProtocol = reader["HasWaitingProtocol"].Convert<bool>(),
                ConRelRecID = reader["ConRelRecID"].Convert<string>(),
                ProtocolValue = reader["ProtocolValue"].Convert<string>(),
                IsClosable = reader["IsClosable"].Convert<bool>(),
                BorcTipi = reader["BorcTipi"].Convert<string>(),
                DevirTarihi = reader["DevirTarihi"].Convert<DateTime>(),
                Score = reader["Score"].Convert<decimal>(),
                KbbSkor = reader["KKBSkoru"].Convert<string>(),
                ToplamTakipBakiyesi = reader["KBBToplamTakipBakiyesi"].Convert<string>(),
                DosyaVekili = reader["SubstituteAgent"].Convert<string>(),
                TapuKaydi  = reader["HasLandRegister"].Convert<bool>(),
                MaasHaczi = reader["HasSalaryDistraint"].Convert<bool>(),
                HasPaymentPromise = reader["HasPaymentPromise"].Convert<bool>(),
                PromiseDate = reader["PromiseDate"].Convert<DateTime>().ToString("dd.MM.yyyy"),
                PromiseCreator = reader["PromiseCreator"].Convert<string>(),
            };

            obligorModel.ScoreColorCode = GetScoreColorCode(obligorModel.Score);
            return obligorModel;
        }

        private static string GetScoreColorCode(decimal score)
        {
            return score >= 0 ? "#36d393" : "#e7505a";
        }

        public List<Tab> GetTabList(int userId)
        {
            try
            {
                var tablist = new List<Tab>();
                var dbCommand = Db.GetStoredProcCommand("member.Usp_GetTablistById");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        tablist.Add(new Tab
                        {
                            Id = reader["Id"].Convert<int>(),
                            Header = reader["Header"].Convert<string>(),
                            RuleName = reader["RuleName"].Convert<string>("")
                        });
                    }
                }
                return tablist;
            }
            catch (Exception ex)
            {
                ex.Log("GetTabList metodunda hata oluştu.");
                return null;
            }
        }

        public bool CloseAccountByObligorId(string obligorId, int userId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_CloseAccountByObligorId");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("CloseAccountByObligorId metodunda hata oluştu.");
                return false;
            }
        }

        public bool SetApproveSpecialExecution(string obligorId, int userId, string reason)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_SetApproveSpecialExecution");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                Db.AddInParameter(dbCommand, "@Reason", DbType.String, reason);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("CloseAccountByObligorId metodunda hata oluştu.");
                return false;
            }
        }

        public List<User> GetUsersForAccountOwnership(int userId)
        {
            try
            {
                var userList = new List<User>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetUsersForAccountOwnership");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                var reader = Db.ExecuteReader(dbCommand);
                var dic = new Dictionary<int, string>();
                using (reader)
                {
                    while (reader.Read())
                    {
                        userList.Add(new User
                        {
                            Id = reader["UserId"].Convert<int>(),
                            Name = reader["Name"].Convert<string>(),
                            LoginName = reader["LoginName"].Convert<string>()
                        });
                    }
                }
                return userList;
            }
            catch (Exception ex)
            {
                ex.Log("GetUsersForAccountOwnership metodunda hata oluştu.");
                return null;
            }
        }

        public bool ChangeAccountOwner(int newAccountOwnerId, int userId, string accountId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ChangeAccountOwner");
                Db.AddInParameter(dbCommand, "@NewUserId", DbType.Int32, newAccountOwnerId);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@AccountId", DbType.String, accountId);
                var result = Db.ExecuteNonQuery(dbCommand) > 0;
                return result;
            }
            catch (Exception ex)
            {
                ex.Log("ChangeAccountOwner metodunda hata oluştu.");
                return false;
            }
        }

        public bool ChangePaymentOwner(int newAccountOwnerId, int userId, string accountId, string recId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ChangePaymentOwnerRequest");
                Db.AddInParameter(dbCommand, "@NewUserId", DbType.Int32, newAccountOwnerId);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@AccountId", DbType.String, accountId);
                Db.AddInParameter(dbCommand, "@RecId", DbType.String, recId);
                var result = Db.ExecuteNonQuery(dbCommand) > 0;
                return result;
            }
            catch (Exception ex)
            {
                ex.Log("ChangePaymentOwner metodunda hata oluştu.");
                return false;
            }
        }

        #endregion

        #region Tabs

        public List<YapilacakIs> GetYapilacakIsler(int userId, string obligorId)
        {
            try
            {
                var yapilacakIsAdminList = new List<YapilacakIs>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetYapilacakIsler");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.Int32, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        yapilacakIsAdminList.Add(new YapilacakIs
                        {
                            Id = reader["Id"].Convert<int>(),
                            OncekiSonuc = reader["OncekiSonuc"].Convert<string>(),
                            YapilacakIsDesc = reader["YapilacakIs"].Convert<string>(),
                            SorumluEkip = reader["SorumluEkip"].Convert<string>(),
                            SorumluKisi = reader["SorumluKisi"].Convert<string>(),
                            GorevlendirmeTarihi = reader["GorevlendirmeTarihi"].Convert<string>(),
                            UstlenmeTarihi = reader["UstlenmeTarihi"].Convert<string>(),
                            HedefTarihi = reader["HedefTarihi"].Convert<string>(),
                            Durum = reader["Durum"].Convert<string>(),
                        });
                    }
                }
                return yapilacakIsAdminList;
            }
            catch (Exception ex)
            {
                ex.Log("GetYapilacakIsAdmin metodunda hata oluştu.");
                return null;
            }
        }

        public List<TumPortfoydekiBorclari> GetTumPortfoydekiBorclari(int userId, string obligorId)
        {
            try
            {
                var tumPortfoydekiBorclari = new List<TumPortfoydekiBorclari>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetTumPortfoydekiBorclari");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        tumPortfoydekiBorclari.Add(new TumPortfoydekiBorclari
                        {
                            Id = reader["Id"].Convert<string>(),
                            GorunenAd = reader["GorunenAd"].Convert<string>(),
                            BorcIleIliskisi = reader["BorcIleIliskisi"].Convert<string>(),
                            IcraDairesi = reader["IcraDairesi"].Convert<string>(),
                            IcraDosya = reader["IcraDosya"].Convert<string>(),
                            Banka = reader["Banka"].Convert<string>(),
                            Portfoy = reader["Portfoy"].Convert<string>(),
                            BorcDurum = reader["BorcDurum"].Convert<string>(),
                            BorcDurumDetay = reader["BorcDurumDetay"].Convert<string>(),
                            DosyaSahibi = reader["DosyaSahibi"].Convert<string>(),
                            ToplamAnapara = reader["ToplamAnapara"].Convert<decimal>(),
                            BorcAnapara = reader["BorcAnapara"].Convert<decimal>(),
                            BakiyeAnapara = reader["BakiyeAnapara"].Convert<decimal>(),
                            MasterDept = reader["MasterDept"].Convert<bool>(),
                            KisiDurumu = reader["KisiDurumu"].Convert<string>()
                        });
                    }
                }
                return tumPortfoydekiBorclari;
            }
            catch (Exception ex)
            {
                ex.Log("GetTabList metodunda hata oluştu.");
                return null;
            }
        }

        public List<HpHacizliMal> GetHpHacizliMal(int userId, string obligorId)
        {
            try
            {
                var hpHacizliMalList = new List<HpHacizliMal>();
                var dbCommand = Db.GetStoredProcCommand("obligor.GetHpHacizliMal");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        hpHacizliMalList.Add(new HpHacizliMal
                        {
                            Id = reader["Id"].Convert<string>(),
                            AccountId = reader["AccountId"].Convert<string>(),
                            FoyNo = reader["FoyNo"].Convert<string>(),
                            TeminatTuru = reader["TeminatTuru"].Convert<string>(),
                            TeminatCinsi = reader["TeminatCinsi"].Convert<string>(),
                            SatisGunu1 = reader["SatisGunu1"].Convert<string>(),
                            SatisGunu2 = reader["SatisGunu2"].Convert<string>(),
                            HacizTarihi = reader["HacizTarih"].Convert<string>(),
                            Tasinir = reader["Tasinir"].Convert<string>(),
                            Tasinmaz = reader["Tasinmaz"].Convert<string>()
                        });
                    }
                }
                return hpHacizliMalList;
            }
            catch (Exception ex)
            {
                ex.Log("GetHpHacizliMal metodunda hata oluştu.");
                return null;
            }
        }

        public List<HpTeminat> GetHpTeminat(int userId, string obligorId)
        {
            try
            {
                var hpTeminatList = new List<HpTeminat>();
                var dbCommand = Db.GetStoredProcCommand("obligor.GetHpTeminat");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        hpTeminatList.Add(new HpTeminat
                        {
                            Id = reader["Id"].Convert<string>(),
                            AccountId = reader["AccountId"].Convert<string>(),
                            FoyNo = reader["FoyNo"].Convert<string>(),
                            KisiKurumAdi = reader["KisiKurumAdi"].Convert<string>(),
                            TeminatTuru = reader["TeminatTuru"].Convert<string>(),
                            TeminatCinsi = reader["TeminatCinsi"].Convert<string>(),
                            Tasinir = reader["Tasinir"].Convert<string>(),
                            Tasinmaz = reader["Tasinmaz"].Convert<string>(),
                            Takyidat = reader["Takyidat"].Convert<string>(),
                            ExpertizDegeri = reader["ExpertizDegeri"].Convert<string>(),
                            ExpertizTarihi = reader["ExpertizTarihi"].Convert<string>(),
                            Tutari = reader["Tutari"].Convert<string>(),
                            Derece = reader["Derece"].Convert<string>(),
                            Il = reader["Il"].Convert<string>(),
                            KTR = reader["KTR"].Convert<string>(),
                            Maliki = reader["Maliki"].Convert<string>(),
                            Marka = reader["Marka"].Convert<string>(),
                            Model = reader["Model"].Convert<string>(),
                            Muhafaza = reader["Muhafaza"].Convert<string>(),
                            Plaka = reader["Plaka"].Convert<string>(),
                            RehinSozlesmeTarihi = reader["RehinSozlesmeTarihi"].Convert<string>(),
                            SatisGunu1 = reader["SatisGunu1"].Convert<string>(),
                            SatisGunu2 = reader["SatisGunu2"].Convert<string>(),
                            SatisSonucu = reader["SatisSonucu"].Convert<string>(),
                        });
                    }
                }
                return hpTeminatList;
            }
            catch (Exception ex)
            {
                ex.Log("GetHpTeminat metodunda hata oluştu.");
                return null;
            }
        }

        public Dictionary<int, string> GetPhoneDictionary(string obligorId)
        {
            try
            {
                var phoneDictionary = new Dictionary<int, string>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetPhoneListByObligorId");
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        phoneDictionary.Add(reader["Id"].Convert<int>(), reader["Phone"].Convert<string>());
                    }
                }
                return phoneDictionary;
            }
            catch (Exception ex)
            {
                ex.Log("GetPhoneDictionary metodunda hata oluştu.");
                return null;
            }
        }

        public List<ProtokolTarihce> GetProtokolTarihce(int userId, string obligorId)
        {
            try
            {
                var protokolTarihceList = new List<ProtokolTarihce>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetProtokolTarihce");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        protokolTarihceList.Add(new ProtokolTarihce
                        {
                            RecId = reader["RecId"].Convert<string>(),
                            PayPlanId = reader["PayPlanID"].Convert<int>(),
                            PlanDate = reader["w_PlanDate"].Convert<DateTime>(),
                            PesinDate = reader["w_PesinDate"].Convert<DateTime>(),
                            InstallmentStartDate = reader["w_InstallmentStartDate"].Convert<DateTime>(),
                            PesinAmount = reader["w_PesinAmount"].Convert<decimal>(),
                            InstallmentAmount = reader["w_InstallmentAmount"].Convert<decimal>(),
                            MaxInstallmentCount = reader["MaxInstallmentCount"].Convert<decimal>(),
                            PlanAmount = reader["w_PlanAmount"].Convert<decimal>(),
                            StatusText = reader["StatusText"].Convert<string>(),
                            Owner = reader["Owner"].Convert<string>(),
                            StatusReason = reader["StatusReason"].Convert<string>(),
                            CampaignId = reader["CampaignId"].Convert<int>(),
                            Description = reader["CampaignSeg_03"].Convert<string>(),
                            DebtAmount = reader["DebtAmount"].Convert<decimal>(),
                            DebtFees = reader["DebtFees"].Convert<decimal>(),
                            DebtInterest = reader["DebtInterest"].Convert<decimal>(),
                            DebtPrincipal = reader["DebtPrincipal"].Convert<decimal>(),
                            CreditAmount = reader["CreditAmount"].Convert<decimal>(),
                        });
                    }
                }
                return protokolTarihceList;
            }
            catch (Exception ex)
            {
                ex.Log("GetProtokolTarihce metodunda hata oluştu.");
                return null;
            }
        }

        public List<ProtokolTaksit> GetAktifProtokolTaksit(int userId, string obligorId)
        {
            try
            {
                var aktifProtokolTaksitList = new List<ProtokolTaksit>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetAktifProtokolTaksit");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        aktifProtokolTaksitList.Add(new ProtokolTaksit
                        {
                            Id = reader["Id"].Convert<string>(),
                            TaksitTarihiVadeli = reader["TaksitTarihiVadeli"].Convert<string>(),
                            TaksitNo = reader["TaksitNo"].Convert<string>(),
                            TaksitTutari = reader["TaksitTutari"].Convert<decimal>(),
                            Odenen = reader["Odenen"].Convert<decimal>(),
                            Kalan = reader["Kalan"].Convert<decimal>(),
                            TaksitTutariVfsiz = reader["TaksitTutariVfsiz"].Convert<decimal>(),
                            Durum = reader["Durum"].Convert<string>(),
                            OrjinalTaksitTarihi = reader["OrjinalTaksitTarihi"].Convert<string>(),
                            TaksitDegistirmeNotu = reader["TaksitDegistirmeNotu"].Convert<string>(),
                            Desc = reader["Desc"].Convert<string>(),
                        });
                    }
                }
                return aktifProtokolTaksitList;
            }
            catch (Exception ex)
            {
                ex.Log("GetAktifProtokolTaksit metodunda hata oluştu.");
                return null;
            }
        }

        public List<ProtokolTaksit> GetProtocolInstalmentsById(int userId, string protocolId)
        {
            try
            {
                var protokolTaksitList = new List<ProtokolTaksit>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetProtokolDetailsByProtocolId");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ProtocolId", DbType.String, protocolId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        protokolTaksitList.Add(new ProtokolTaksit
                        {
                            Id = reader["Id"].Convert<string>(),
                            TaksitTarihiVadeli = reader["TaksitTarihiVadeli"].Convert<DateTime>().ToShortDateString(),
                            TaksitNo = reader["TaksitNo"].Convert<string>(),
                            TaksitTutari = reader["TaksitTutari"].Convert<decimal>(),
                            Odenen = reader["Odenen"].Convert<decimal>(),
                            Kalan = reader["Kalan"].Convert<decimal>(),
                            TaksitTutariVfsiz = reader["TaksitTutariVfsiz"].Convert<decimal>(),
                            Durum = reader["Durum"].Convert<string>(),
                            OrjinalTaksitTarihi = reader["OrjinalTaksitTarihi"].Convert<DateTime>().ToShortDateString(),
                            Desc = reader["Desc"].Convert<string>(),
                            //TaksitDegistirmeNotu = reader["TaksitDegistirmeNotu"].Convert<string>(),
                        });
                    }
                }
                return protokolTaksitList;
            }
            catch (Exception ex)
            {
                ex.Log("GetProtocolInstalmentsById metodunda hata oluştu.");
                return null;
            }
        }

        public List<AllPayments> GetAllPayments(int userId, string obligorId)
        {
            try
            {
                var kisiTumOdemeList = new List<AllPayments>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetAllPaymnets");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        kisiTumOdemeList.Add(new AllPayments
                        {
                            Id = reader["Id"].Convert<string>(),
                            PaymentDate = reader["PaymentDate"].Convert<string>(),
                            Value = reader["Amount"].Convert<decimal>(),
                            Bank = reader["PaymentBank"].Convert<string>(),
                            CollectionDesc = reader["PaymentDesc"].Convert<string>(),
                            EfsCollectionRow = reader["DBSLine"].Convert<string>(),
                            //NameSurname = reader["NameSurname"].Convert<string>(),
                            //PayerTckn = reader["PayerTckn"].Convert<string>(),
                            FileOwner = reader["FileOwner"].Convert<string>(),
                            Status = reader["Status"].Convert<string>(),
                            StatusText = reader["StatusText"].Convert<string>(),
                            //Description = reader["Description"].Convert<string>(),
                            CollectionJournalNumber = reader["JournalNumber"].Convert<string>(),
                            AccountingVoucherNumber = reader["AccountingNumber"].Convert<string>(),
                            //LastUpdaterUser = reader["LastUpdaterUser"].Convert<string>(),
                            //LastUpdateDate = reader["LastUpdateDate"].Convert<string>(),
                            HasProtocoled = reader["HasProtocoled"].Convert<bool>(),
                            PlanType = reader["PlanType"].Convert<string>(),
                            PlanSubType = reader["PlanSubType"].Convert<string>(),
                            //HasProtocoledText = reader["ProtocoledText"].Convert<string>()
                            CID = reader["CID"].Convert<string>(),
                            MessageText = reader["MessageText"].Convert<string>(),
                            FullName = reader["FullName"].Convert<string>(),
                            Tads = reader["TADS"].Convert<string>(),
                            AccountId = reader["AccountId"].Convert<string>(),
                        });
                    }
                }
                return kisiTumOdemeList;
            }
            catch (Exception ex)
            {
                ex.Log("GetKisiTumOdeme metodunda hata oluştu.");
                return null;
            }
        }

        public List<Adres> GetAdres(int userId, string obligorId)
        {
            try
            {
                var adresList = new List<Adres>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetAdres");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        adresList.Add(new Adres
                        {
                            Id = reader["Id"].Convert<string>(),
                            Amac = reader["Amac"].Convert<string>(),
                            Address = reader["Address"].Convert<string>(),
                            Ilce = reader["Ilce"].Convert<string>(),
                            Sehir = reader["Sehir"].Convert<string>(),
                            PostaKodu = reader["PostaKodu"].Convert<string>(),
                            Ulke = reader["Ulke"].Convert<string>(),
                            OlusturanKisi = reader["OlusturanKisi"].Convert<string>(),
                            OlusturmaTarihi = reader["OlusturmaTarihi"].Convert<string>(),
                            DegistirenKisi = reader["DegistirenKisi"].Convert<string>(),
                            DegistirmeTarihi = reader["DegistirmeTarihi"].Convert<string>(),
                        });
                    }
                }
                return adresList;
            }
            catch (Exception ex)
            {
                ex.Log("GetAdres metodunda hata oluştu.");
                return null;
            }
        }

        public List<KisiBilgi> GetKisiBilgi(int userId, string obligorId)
        {
            try
            {
                var kisiBilgiList = new List<KisiBilgi>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetKisiBilgi");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        kisiBilgiList.Add(new KisiBilgi
                        {
                            Id = reader["Id"].Convert<string>(),
                            Unvan = reader["Unvan"].Convert<string>(),
                            GorunenAd = reader["GorunenAd"].Convert<string>(),
                            Adi = reader["Adi"].Convert<string>(),
                            Soyadi = reader["Soyadi"].Convert<string>(),
                            DogumTarihi = reader["DogumTarihi"].Convert<string>(),
                            KisiTipiTanimi = reader["KisiTipiTanimi"].Convert<string>(),
                            BireyKurum = reader["BireyKurum"].Convert<string>(),
                            PortfoyNo = reader["PortfoyNo"].Convert<string>(),
                            TcKimlikNo = reader["TcKimlikNo"].Convert<string>(),
                            VergiNo = reader["VergiNo"].Convert<string>(),
                        });
                    }
                }
                return kisiBilgiList;
            }
            catch (Exception ex)
            {
                ex.Log("GetKisiBilgi metodunda hata oluştu.");
                return null;
            }
        }

        public List<Borc> GetBorc(int userId, string obligorId)
        {
            try
            {
                var borcList = new List<Borc>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetBorclar");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        borcList.Add(new Borc
                        {
                            RecId = reader["AccountRecId"].Convert<string>(),
                            AccountId = reader["AccountID"].Convert<string>(),
                            BorcTipi = reader["BorcTipi"].Convert<string>(),
                            ToplamRisk = reader["ToplamRisk"].Convert<decimal>(),
                            Anapara = reader["Anapara"].Convert<decimal>(),
                            Faiz = reader["Faiz"].Convert<decimal>(),
                            Masraflar = reader["Masraflar"].Convert<decimal>(),
                            Durum = reader["Durum"].Convert<string>(),
                            IcraDaireNo = reader["IcraDaireNo"].Convert<string>(),
                            TemerrutTarihi = reader["TemerrutTarihi"].Convert<DateTime>(),
                            DevirTarihi = reader["DevirTarihi"].Convert<DateTime>(),
                            IcraFileId = reader["IcraFileId"].Convert<string>(),
                            Masraf = reader["Masraf"].Convert<decimal>(),
                            FoyNo = reader["FoyNo"].Convert<string>(),
                            BorcNo = reader["BorcNo"].Convert<string>(),
                            KrediKartNo = reader["KrediKartNo"].Convert<string>(),
                        });
                    }
                }
                return borcList;
            }
            catch (Exception ex)
            {
                ex.Log("GetBorc metodunda hata oluştu.");
                return null;
            }
        }

        public List<DosyaEk> GetDosyaEk(int userId, string obligorId)
        {
            try
            {
                var dosyaEkList = new List<DosyaEk>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetDosyaEk");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        dosyaEkList.Add(new DosyaEk
                        {
                            RecId = reader["RecId"].Convert<string>(),
                            DosyaAdi = reader["ATTACHNAME"].Convert<string>(),
                            DosyaAciklamasi = reader["ATTACHDESC"].Convert<string>(),
                            Olusturan = reader["CreatedBy"].Convert<string>(),
                            OlusturmaTarihi = reader["CreatedDateTime"].Convert<string>(),
                            AccountLinkRecId = reader["AccountLink_RecID"].Convert<string>(),
                            AttachmentPath = Uri.EscapeUriString(reader["AttachmentPath"].Convert<string>())
                        });
                    }
                }
                return dosyaEkList;
            }
            catch (Exception ex)
            {
                ex.Log("GetDosyaEk metodunda hata oluştu.");
                return null;
            }
        }

        public List<IslemTarihce> GetIslemTarihce(int userId, string obligorId)
        {
            try
            {
                var islemTarihceList = new List<IslemTarihce>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetIslemTarihce");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        islemTarihceList.Add(new IslemTarihce
                        {
                            Id = reader["Id"].Convert<string>(),
                            GorevlendirmeTarihi = reader["GorevlendirmeTarihi"].Convert<string>(),
                            UstlenmeTarihi = reader["UstlenmeTarihi"].Convert<string>(),
                            TamamlamaTarihi = reader["TamamlamaTarihi"].Convert<string>(),
                            Amac = reader["Amac"].Convert<string>(),
                            Sonuc = reader["Sonuc"].Convert<string>(),
                            SorumluEkip = reader["SorumluEkip"].Convert<string>(),
                            Uzman = reader["Uzman"].Convert<string>(),
                            Durum = reader["Durum"].Convert<string>(),
                            OncekiUzman = reader["OncekiUzman"].Convert<string>(),
                            OncekiEkip = reader["OncekiEkip"].Convert<string>(),
                            OncekiSonuc = reader["OncekiSonuc"].Convert<string>(),
                            OncekiSonucKodu = reader["OncekiSonucKodu"].Convert<string>(),
                        });
                    }
                }
                return islemTarihceList;
            }
            catch (Exception ex)
            {
                ex.Log("GetIslemTarihce metodunda hata oluştu.");
                return null;
            }
        }

        public List<YapilacakIsAdmin> GetYapilacakIsAdmin(int userId, string obligorId)
        {
            try
            {
                var yapilacakIsAdminList = new List<YapilacakIsAdmin>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetYapilacakIsAdmin");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        yapilacakIsAdminList.Add(new YapilacakIsAdmin
                        {
                            Id = reader["Id"].Convert<string>(),
                            OncekiSonuc = reader["OncekiSonuc"].Convert<string>(),
                            YapilacakIs = reader["YapilacakIs"].Convert<string>(),
                            SorumluEkip = reader["SorumluEkip"].Convert<string>(),
                            SorumluKisi = reader["SorumluKisi"].Convert<string>(),
                            GorevlendirmeTarihi = reader["GorevlendirmeTarihi"].Convert<string>(),
                            UstlenmeTarihi = reader["UstlenmeTarihi"].Convert<string>(),
                            HedefTarihi = reader["HedefTarihi"].Convert<string>(),
                            Durum = reader["Durum"].Convert<string>(),
                        });
                    }
                }
                return yapilacakIsAdminList;
            }
            catch (Exception ex)
            {
                ex.Log("GetYapilacakIsAdmin metodunda hata oluştu.");
                return null;
            }
        }

        public List<KisininIliskideOlduguTumBorc> GetKisininIliskideOlduguTumBorc(int userId, string obligorId)
        {
            try
            {
                var kisininIliskideOlduguTumBorcList = new List<KisininIliskideOlduguTumBorc>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetKisininIliskideOlduguTumBorc");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        kisininIliskideOlduguTumBorcList.Add(new KisininIliskideOlduguTumBorc
                        {
                            Id = reader["Id"].Convert<string>(),
                            PriorutyGroup = reader["PriorutyGroup"].Convert<string>(),
                            BorcIleIliskisi = reader["BorcIleIliskisi"].Convert<string>(),
                            EnBuyukBorc = reader["EnBuyukBorc"].Convert<string>(),
                            AktifTaskSayisi = reader["AktifTaskSayisi"].Convert<string>(),
                            PasifTaskSayisi = reader["PasifTaskSayisi"].Convert<string>(),
                            GorunenAd = reader["GorunenAd"].Convert<string>(),
                            OncekiAksiyonSonucu = reader["OncekiAksiyonSonucu"].Convert<string>(),
                            AccountId = reader["AccountId"].Convert<string>(),
                            ContactTcknNo = reader["ContactTcknNo"].Convert<string>(),
                            IcraDairesi = reader["IcraDairesi"].Convert<string>(),
                            IcraDosyaNumarasi = reader["IcraDosyaNumarasi"].Convert<string>(),
                            Banka = reader["Banka"].Convert<string>(),
                            Portfoy = reader["Portfoy"].Convert<string>(),
                            BorcDurum = reader["BorcDurum"].Convert<string>(),
                            BorcDurumDetay = reader["BorcDurumDetay"].Convert<string>(),
                            DosyaSahibi = reader["DosyaSahibi"].Convert<string>(),
                            ToplamAnapara = reader["ToplamAnapara"].Convert<decimal>(),
                            BorcAnapara = reader["BorcAnapara"].Convert<decimal>(),
                            BakiyeAnapara = reader["BakiyeAnapara"].Convert<decimal>(),
                            VergiNo = reader["VergiNo"].Convert<string>(),
                            BabaAdi = reader["BabaAdi"].Convert<string>(),
                            KisiDurumu = reader["KisiDurumu"].Convert<string>(),
                        });
                    }
                }
                return kisininIliskideOlduguTumBorcList;
            }
            catch (Exception ex)
            {
                ex.Log("GetDosyaEk metodunda hata oluştu.");
                return null;
            }
        }

        public List<BorcunIliskideOlduguTumKisi> GetBorcunIliskideOlduguTumKisi(int userId, string obligorId)
        {
            try
            {
                var borcunIliskideOlduguTumKisiList = new List<BorcunIliskideOlduguTumKisi>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetBorcunIliskideOlduguTumKisi");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        borcunIliskideOlduguTumKisiList.Add(new BorcunIliskideOlduguTumKisi
                        {
                            Id = reader["Id"].Convert<string>(),
                            GorunenAd = reader["GorunenAd"].Convert<string>(),
                            ContactTypeText = reader["ContactTypeText"].Convert<string>(),
                            SorumluOlduguBorcMiktari = reader["SorumluOlduguBorcMiktari"].Convert<decimal>(),
                            IcraDairesi = reader["IcraDairesi"].Convert<string>(),
                            IcraDosyaNumarasi = reader["IcraDosyaNumarasi"].Convert<string>(),
                            BalanceAmount = reader["BalanceAmount"].Convert<decimal>(),
                            SourceText = reader["SourceText"].Convert<string>(),
                            SourceSet = reader["SourceSet"].Convert<string>(),
                            KisiDurumu = reader["KisiDurumu"].Convert<string>(),

                        });
                    }
                }
                return borcunIliskideOlduguTumKisiList;
            }
            catch (Exception ex)
            {
                ex.Log("GetBorcunIliskideOlduguTumKisi metodunda hata oluştu.");
                return null;
            }
        }

        public List<DosyaSahiplikTarihce> GetDosyaSahiplikTarihce(int userId, string obligorId)
        {
            try
            {
                var dosyaSahiplikTarihceList = new List<DosyaSahiplikTarihce>();
                var dbCommand = Db.GetStoredProcCommand("obligor.GetAccountOwnerTracking");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        dosyaSahiplikTarihceList.Add(new DosyaSahiplikTarihce
                        {
                            Order = reader["sira"].Convert<int>(),
                            AdSoyad = reader["AdSoyad"].Convert<string>(),
                            LoginId = reader["LoginId"].Convert<string>(),
                            Olusturan = reader["Olusturan"].Convert<string>(),
                            OlusturanTarih = reader["Tarih"].Convert<string>(),
                            Degistiren = reader["Degistiren"].Convert<string>(),
                            DegistirenTarih = reader["DegistirmeTarihi"].Convert<string>(),

                        });
                    }
                }
                return dosyaSahiplikTarihceList;
            }
            catch (Exception ex)
            {
                ex.Log("GetDosyaSahiplikTarihce metodunda hata oluştu.");
                return null;
            }
        }

        public List<PriorityGroupTracking> GetPriorityGroupTracking(int userId, string obligorId)
        {
            try
            {
                var priorityGroupTrackingList = new List<PriorityGroupTracking>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetPriorityGroupTracking");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        priorityGroupTrackingList.Add(new PriorityGroupTracking
                        {
                            Id = reader["Id"].Convert<string>(),
                            Degistiren = reader["Degistiren"].Convert<string>(),
                            CreatedDateTime = reader["CreatedDateTime"].Convert<string>(),
                            CurrentPriority = reader["CurrentPriority"].Convert<string>(),
                            PriorityGroup = reader["PriorityGroup"].Convert<string>(),

                        });
                    }
                }
                return priorityGroupTrackingList;
            }
            catch (Exception ex)
            {
                ex.Log("GetPriorityGroupTracking metodunda hata oluştu.");
                return null;
            }
        }

        public List<PostaGonderi> GetPostaGonderi(int userId, string obligorId)
        {
            try
            {
                var postaGonderiList = new List<PostaGonderi>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetPostaGonderi");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        postaGonderiList.Add(new PostaGonderi
                        {
                            Id = reader["Id"].Convert<string>(),
                            AccountId = reader["AccountId"].Convert<string>(),
                            AdSoyad = reader["AdSoyad"].Convert<string>(),
                            GonderiTipi = reader["GonderiTipi"].Convert<string>(),
                            Adres = reader["Adres"].Convert<string>(),
                            Il = reader["Il"].Convert<string>(),
                            Ilce = reader["Ilce"].Convert<string>(),
                            ReportPage = reader["ReportPage"].Convert<string>(),
                            CreatedBy = reader["CreatedBy"].Convert<string>(),
                            CreatedDateTime = reader["CreatedDateTime"].Convert<string>(),
                            IsSendToOperation = reader["IsSendToOperation"].Convert<bool>()
                        });
                    }
                }
                return postaGonderiList;
            }
            catch (Exception ex)
            {
                ex.Log("GetPostaGonderi metodunda hata oluştu.");
                return null;
            }
        }

        public List<KkbScore> GetKkbScoreList(int userId, string obligorId)
        {
            try
            {
                var list = new List<KkbScore>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_KKBScore");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        list.Add(new KkbScore
                        {
                            TckNo = reader["TCKNo"].Convert<string>(),
                            KkbSkoru = reader["KKBSkoru"].Convert<decimal>(),
                            KkbSorguTarihi = reader["KKBSorguTarihi"].Convert<DateTime>(),
                            ToplamHesapLimit = reader["ToplamHesapLimit"].Convert<decimal>(),
                            ToplamHesapBakiye = reader["ToplamHesapBakiye"].Convert<decimal>(),
                            KkbSonKanuniTakipTarihi = reader["KKBSonKanuniTakipTarihi"].Convert<DateTime>(),
                            ToplamKanuniTakipBakiye = reader["ToplamKanuniTakipBakiye"].Convert<decimal>(),
                            KanunTakipLimiti = reader["KanunTakipLimiti"].Convert<decimal>(),
                            ToplamTakipBakiyesi = reader["ToplamTakipBakiyesi"].Convert<decimal>(),
                            SonKrediVerilisTarihi = reader["SonKrediVerilisTarihi"].Convert<DateTime>(),
                        });
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                ex.Log("GetPostaGonderi metodunda hata oluştu.");
                return null;
            }
        }

        public List<ObligorProcessHistory> GetObligorProcessHistoryList(int userId, string obligorId)
        {
            try
            {
                var list = new List<ObligorProcessHistory>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetObligorProcessHistory");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        list.Add(new ObligorProcessHistory
                        {
                            Id = reader["Id"].Convert<string>(),
                            Order = reader["Order"].Convert<int>(),
                            Creator = reader["Creator"].Convert<string>(),
                            CreatedDatetime = reader["CreatedDatetime"].Convert<DateTime>(),
                            Process = reader["Process"].Convert<string>(),
                            ProcessSubCode = reader["ProcessSubCode"].Convert<string>(),
                            Desc = reader["Desc"].Convert<string>(),
                        });
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                ex.Log("GetObligorProcessHistoryList metodunda hata oluştu.");
                return null;
            }
        }

        #endregion

        #region Note

        public List<Note> GetNot(int userId, string obligorId)
        {
            try
            {
                var notList = new List<Note>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetNot");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        notList.Add(new Note
                        {
                            Id = reader["Id"].Convert<string>(),
                            NotTarihi = reader["NotTarihi"].Convert<string>(),
                            Detay = reader["Detay"].Convert<string>(),
                            Olusturan = reader["Olusturan"].Convert<string>(),
                            IsDanger = reader["IsDanger"].Convert<bool>(),
                        });
                    }
                }
                return notList;
            }
            catch (Exception ex)
            {
                ex.Log("GetNot metodunda hata oluştu.");
                return null;
            }
        }

        public List<FiveImportantNote> GetLast5ImportantNotes(int userId, string obligorId)
        {
            try
            {
                var notList = new List<FiveImportantNote>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetLastFiveImportantNotes");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        var note = new FiveImportantNote
                        {
                            Text = reader["Text"].Convert<string>(),
                            Type = reader["Type"].Convert<int>()
                        };

                        note.ShortenedDetail = !string.IsNullOrWhiteSpace(note.Text) && note.Text.Length > 75
                            ? note.Text.Substring(0, 74) + "..."
                            : note.Text;

                        notList.Add(note);
                    }
                }
                return notList;
            }
            catch (Exception ex)
            {
                ex.Log("GetLast5ImportantNotes metodunda hata oluştu.");
                return null;
            }
        }

        public bool AddNote(Note note, string obligorId, string rate)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_AddNote");
                Db.AddInParameter(dbCommand, "@Detail", DbType.String, note.Detay);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, note.UserId);
                Db.AddInParameter(dbCommand, "@TypeId", DbType.Int32, note.Tip);
                Db.AddInParameter(dbCommand, "@Id", DbType.String, obligorId);
                Db.AddInParameter(dbCommand, "@Rate", DbType.String, rate);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("AddNote metodunda hata oluştu.");
                return false;
            }
        }

        public DeleteNoteResult DeleteNote(string id, int userId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_DeleteNote");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@NoteId", DbType.String, id);
                DeleteNoteResult deleteNoteResult = null;
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        deleteNoteResult = new DeleteNoteResult();
                        deleteNoteResult.IsDeleted = reader["IsDeleted"].Convert<bool>();
                        deleteNoteResult.Message = reader["Message"].Convert<string>();
                    }
                }

                return deleteNoteResult;
            }
            catch (Exception ex)
            {
                ex.Log("DeleteNote metodunda hata oluştu.");
                return null;
            }
        }

        public bool AddCallResult(int userId, string phone, string obligorId, int resultId, string resultText)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_AddCallResult");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Phone", DbType.String, phone);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                Db.AddInParameter(dbCommand, "@ResultId", DbType.Int32, resultId);
                Db.AddInParameter(dbCommand, "@ResultText", DbType.String, resultText);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("AddCallResult metodunda hata oluştu.");
                return false;
            }
        }

        public List<CallResult> GetCallResults()
        {
            try
            {
                var callResults = new List<CallResult>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetCallResults");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        callResults.Add(new CallResult()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Value = reader["Text"].Convert<string>()
                        });
                    }
                }
                return callResults;
            }
            catch (Exception ex)
            {
                ex.Log("GetCallResults metodunda hata oluştu.");
                return null;
            }
        }

        #endregion

        #region Sms History

        public List<SmsHistory> GetSmsHistory(int userId, string obligorId)
        {
            try
            {
                var smsTarihceList = new List<SmsHistory>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetSmsHistory");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        var smsHistory = new SmsHistory
                        {
                            Id = reader["Id"].Convert<string>(),
                            Number = reader["Number"].Convert<string>(),
                            SmsTemplate = reader["SmsTemplate"].Convert<string>(),
                            SmsText = reader["SmsText"].Convert<string>(string.Empty),
                            Status = reader["Status"].Convert<string>(),
                            ErrorCode = reader["ErrorCode"].Convert<int>(),
                            ErrorDesc = reader["ErrorDesc"].Convert<string>(),
                            PlannedSendDate = reader["PlannedSendDate"].Convert<string>(),
                            QueuedDate = reader["QueuedDate"].Convert<string>(),
                            ResultDate = reader["ResultDate"].Convert<string>(),
                            SmsStatus = reader["SmsStatus"].Convert<string>(),
                            User = reader["User"].Convert<string>()
                        };
                        smsHistory.ShortSmsText = (smsHistory.SmsText.Length > 50) ? (smsHistory.SmsText.Substring(0, 49) + "...") : smsHistory.SmsText;

                        smsTarihceList.Add(smsHistory);
                    }
                }
                return smsTarihceList;
            }
            catch (Exception ex)
            {
                ex.Log("GetSmsHistory metodunda hata oluştu.");
                return null;
            }
        }

        public List<GsmNumber> GetGsmNumbers(int userId, string obligorId)
        {
            try
            {
                var list = new List<GsmNumber>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetGsmNumbers");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        list.Add(new GsmNumber
                        {
                            Id = reader["Id"].Convert<string>(),
                            Number = reader["Number"].Convert<string>()
                        });
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                ex.Log("GetGsmNumbers metodunda hata oluştu.");
                return null;
            }
        }

        public bool InsertSms(int userId, string phoneNumber, string smsText, string obligorId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_InsertSms");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@PhoneNumber", DbType.String, phoneNumber);
                Db.AddInParameter(dbCommand, "@SmsText", DbType.String, smsText);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("InsertSms metodunda hata oluştu.");
                return false;
            }
        }

        public List<SmsTemplate> GetSmsTemplates()
        {
            try
            {
                var list = new List<SmsTemplate>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetSmsTemplates");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        list.Add(new SmsTemplate
                        {
                            Id = reader["Id"].Convert<int>(),
                            Name = reader["Name"].Convert<string>(),
                            TemplateText = reader["TemplateText"].Convert<string>()
                        });
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                ex.Log("GetSmsTemplates metodunda hata oluştu.");
                return null;
            }
        }

        #endregion

        #region Communication History

        public bool SetRating(int userId, string obligorId, string communicationHistoryId, int score)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_SetRating");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                Db.AddInParameter(dbCommand, "@CommunicationHistoryId", DbType.String, communicationHistoryId);
                Db.AddInParameter(dbCommand, "@Score", DbType.Int32, score);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("SetRating metodunda hata oluştu.");
                return false;
            }
        }

        public List<IletisimTarihce> GetIletisimTarihce(int userId, string obligorId)
        {
            try
            {
                var iletisimTarihceList = new List<IletisimTarihce>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetIletisimTarihce");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        iletisimTarihceList.Add(new IletisimTarihce
                        {
                            Id = reader["Id"].Convert<string>(),
                            Tarih = reader["Tarih"].Convert<string>(),
                            Agent = reader["Agent"].Convert<string>(),
                            Telefon = reader["Telefon"].Convert<string>(),
                            Sonuc = reader["Sonuc"].Convert<string>(),
                            KonusulanKisi = reader["KonusulanKisi"].Convert<string>(),
                            KonusulanKisiAdi = reader["KonusulanKisiAdi"].Convert<string>(),
                            GorunenNumara = reader["GorunenNumara"].Convert<string>(),
                            NumaramiGoster = reader["NumaramiGoster"].Convert<string>(),
                            GorunenNumaraKaynagi = reader["GorunenNumaraKaynagi"].Convert<string>(),
                            DegerlendirmeNotu = reader["DegerlendirmeNotu"].Convert<int>(),
                            FilePath = reader["FilePath"].Convert<string>(),
                        });
                    }
                }
                return iletisimTarihceList;
            }
            catch (Exception ex)
            {
                ex.Log("GetIletisimTarihce metodunda hata oluştu.");
                return null;
            }
        }

        #endregion

        #region Phone

        public List<Phone> GetPhones(int userId, string obligorId)
        {
            try
            {
                var telefonList = new List<Phone>();
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetPhones");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        telefonList.Add(new Phone
                        {
                            Id = reader["Id"].Convert<string>(),
                            Status = reader["Status"].Convert<string>(),
                            Type = reader["Type"].Convert<string>(),
                            LastResultCode = reader["LastResultCode"].Convert<string>(),
                            RelatedPerson = reader["RelatedPerson"].Convert<string>(),
                            CalledPerson = reader["CalledPerson"].Convert<string>(),
                            PhoneNumber = reader["PhoneNumber"].Convert<string>().Replace("\r", "").Replace("\n", ""),
                            Extension = reader["Extension"].Convert<string>(),
                            InformationSource = reader["InformationSource"].Convert<string>(),
                            Creator = reader["Creator"].Convert<string>(),
                            CreatedDate = reader["CreatedDate"].Convert<string>(),
                            LastCommunicationDate = reader["LastCommunicationDate"].Convert<string>(),
                        });
                    }
                }
                return telefonList;
            }
            catch (Exception ex)
            {
                ex.Log("GetPhones metodunda hata oluştu.");
                return null;
            }
        }

        public bool AddPhone(Phone phone, string obligorId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_AddPhone");
                Db.AddInParameter(dbCommand, "@Status", DbType.String, phone.Status);
                Db.AddInParameter(dbCommand, "@Type", DbType.String, phone.Type);
                Db.AddInParameter(dbCommand, "@RelatedPerson", DbType.String, phone.RelatedPerson);
                Db.AddInParameter(dbCommand, "@CalledPerson", DbType.String, phone.CalledPerson);
                Db.AddInParameter(dbCommand, "@Phone", DbType.String, phone.PhoneNumber);
                Db.AddInParameter(dbCommand, "@Extension", DbType.String, phone.Extension);
                Db.AddInParameter(dbCommand, "@InformationSource", DbType.String, phone.InformationSource);
                Db.AddInParameter(dbCommand, "@Creator", DbType.String, phone.Creator);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("AddPhone metodunda hata oluştu.");
                return false;
            }
        }

        public bool UpdatePhone(Phone phone)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_UpdatePhone");
                Db.AddInParameter(dbCommand, "@Id", DbType.String, phone.Id);
                Db.AddInParameter(dbCommand, "@Status", DbType.String, phone.Status);
                Db.AddInParameter(dbCommand, "@Type", DbType.String, phone.Type);
                Db.AddInParameter(dbCommand, "@RelatedPerson", DbType.String, phone.RelatedPerson);
                Db.AddInParameter(dbCommand, "@CalledPerson", DbType.String, phone.CalledPerson);
                Db.AddInParameter(dbCommand, "@Phone", DbType.String, phone.PhoneNumber);
                Db.AddInParameter(dbCommand, "@Extension", DbType.String, phone.Extension);
                Db.AddInParameter(dbCommand, "@InformationSource", DbType.String, phone.InformationSource);
                Db.AddInParameter(dbCommand, "@Updater", DbType.String, phone.Updater);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("UpdatePhone metodunda hata oluştu.");
                return false;
            }
        }

        public Phone GetPhone(string id)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_GetPhone");
                Db.AddInParameter(dbCommand, "@Id", DbType.String, id);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    if (reader.Read())
                    {
                        return new Phone
                        {
                            Id = reader["Id"].Convert<string>(),
                            Status = reader["Status"].Convert<string>(),
                            Type = reader["Type"].Convert<string>(),
                            LastResultCode = reader["LastResultCode"].Convert<string>(),
                            RelatedPerson = reader["RelatedPerson"].Convert<string>(),
                            CalledPerson = reader["CalledPerson"].Convert<string>(),
                            PhoneNumber = reader["PhoneNumber"].Convert<string>(),
                            Extension = reader["Extension"].Convert<string>(),
                            InformationSource = reader["InformationSource"].Convert<string>(),
                            Creator = reader["Creator"].Convert<string>(),
                            CreatedDate = reader["CreatedDate"].Convert<string>(),
                            LastCommunicationDate = reader["LastCommunicationDate"].Convert<string>(),
                        };
                    }
                }

                return null;

            }
            catch (Exception ex)
            {
                ex.Log("GetPhone metodunda hata oluştu.");
                return null;
            }
        }

        #endregion

        #region Attachment

        public bool AddAttachment(string obligorId, string desc, int userId, string path, string fileName, bool isGkf)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_InsertDosyaEk");
                Db.AddInParameter(dbCommand, "@AttachmentPath", DbType.String, path);
                Db.AddInParameter(dbCommand, "@AttachmentDescription", DbType.String, desc);
                Db.AddInParameter(dbCommand, "@FileName", DbType.String, fileName);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                Db.AddInParameter(dbCommand, "@IsGkf", DbType.Boolean, isGkf);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("AddAttachment metodunda hata oluştu.");
                return false;
            }
        }

        public bool RemoveFile(string recId, int userId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_RemoveFile");
                Db.AddInParameter(dbCommand, "@RecId", DbType.String, recId);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("RemoveFile metodunda hata oluştu.");
                return false;
            }
        }

        #endregion

        #region Address

        public bool AddAddress(AddressModel adres, string obligorId, int userId)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("obligor.Usp_AddAddress");
                Db.AddInParameter(dbCommand, "@AddressType", DbType.String, adres.AddressType);
                Db.AddInParameter(dbCommand, "@CityId", DbType.Int32, adres.CityId);
                Db.AddInParameter(dbCommand, "@CountyId", DbType.Int32, adres.CountyId);
                Db.AddInParameter(dbCommand, "@Address", DbType.String, adres.Address);
                Db.AddInParameter(dbCommand, "@PostalCode", DbType.String, adres.PostaKodu);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@ObligorId", DbType.String, obligorId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("AddAddress metodunda hata oluştu.");
                return false;
            }
        }

        #endregion

        #region Posta Gonderileri
        public bool SendPostaGonderiToOperation(int userId, string id)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_SendPostaGonderiToOperation");
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Id", DbType.String, id);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("SendPostaGonderiToOperation metodunda hata oluştu.");
                return false;
            }
        }
        #endregion

    }
}
