﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Models;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class ExternalMuhasebeDal: BaseDal
    {
        public ExternalMuhasebeDal() : base(DbTypeEnum.ExternalMuhasebe)
        {
            
        }

        public int CollactionAlacakGuncelle(string musteriNo, string agentAd, string agentSoyad)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("sp_collaction_alacak_guncelle");
                Db.AddInParameter(dbCommand, "@musterino", DbType.String, musteriNo);
                Db.AddInParameter(dbCommand, "@alacak_statusu", DbType.String, "Kapandı");
                Db.AddInParameter(dbCommand, "@agent_ad", DbType.String, agentAd);
                Db.AddInParameter(dbCommand, "@agent_soyad", DbType.String, agentSoyad);
                Db.AddInParameter(dbCommand, "@avukat_ad", DbType.String, "");
                Db.AddInParameter(dbCommand, "@avukat_soyad", DbType.String, "");
                return Db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception ex)
            {
                ex.Log("CollactionAlacakGuncelle metodunda hata oluştu.");
                return -1;
            }
        }

        public int CollactionOdemeplaniEkle(string musteriNo, DateTime odemeplanitarihi, decimal odemeplanitutari, int taksitSayisi)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("sp_collaction_odemeplani_ekle");
                Db.AddInParameter(dbCommand, "@musterino", DbType.String, musteriNo);
                Db.AddInParameter(dbCommand, "@odemeplanitarihi", DbType.DateTime, odemeplanitarihi);
                Db.AddInParameter(dbCommand, "@odemeplanitutari", DbType.Decimal, odemeplanitutari);
                Db.AddInParameter(dbCommand, "@taksitsayisi", DbType.Int32, taksitSayisi);
                return Db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception ex)
            {
                ex.Log("CollactionOdemeplaniEkle metodunda hata oluştu.");
                return -1;
            }
        }

        public int CollactionOdemeplaniIptal(string musteriNo, DateTime odemeplanitarihi, decimal odemeplanitutari, int taksitSayisi)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("sp_collaction_odemeplani_iptal");
                Db.AddInParameter(dbCommand, "@musterino", DbType.String, musteriNo);
                Db.AddInParameter(dbCommand, "@odemeplanitarihi", DbType.DateTime, odemeplanitarihi);
                Db.AddInParameter(dbCommand, "@odemeplanitutari", DbType.Decimal, odemeplanitutari);
                Db.AddInParameter(dbCommand, "@taksitsayisi", DbType.Int32, taksitSayisi);
                return Db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception ex)
            {
                ex.Log("CollactionOdemeplaniIptal metodunda hata oluştu.");
                return -1;
            }
        }

        
    }
}
