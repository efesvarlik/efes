﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Efes.Entities.Krm;
using Efes.Models.Krm;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class KrmDal : BaseDal
    {
        public File GetKrmFileInformationsByDate()
        {
            try
            {
                var csvydList = GetCSVYDList();
                if (csvydList.Any())
                {

                    File file = null;
                    var dbCommand = Db.GetStoredProcCommand("dbo.Usp_KRMDailyReportGetData");
                    var reader = Db.ExecuteReader(dbCommand);
                    using (reader)
                    {
                        file = new File
                        {
                            Header =
                            {
                                FileHeaderRecord = "CS000",
                                VersionNumber = "01",
                                MemberCode = "858",
                                MemberName = "Efes Varlık Yönetim A.Ş.",
                                CreationDate = DateTime.Now.Convert<DateTime>().FormatToYYYYMMGG(),
                                DeclarationDate = DateTime.Now.Convert<DateTime>().FormatToYYYYMMGG()
                            },
                            Footer = { FooterIdentifier = "CS999", VersionNumber = "01", MemberCode = "858" }
                        };

                        while (reader.Read())
                        {
                            var identityNumber = reader["IdentityNumber"].Convert<string>(string.Empty);
                            var identityType = reader["IdentityType"].Convert<string>(string.Empty);
                            var recId = reader["RecId"].Convert<string>(string.Empty);

                            var csvymList = file.CSVYMList.Where(c => c.IdentityNumber == identityNumber && c.IdentityType == identityType);
                            var enumerable = csvymList as IList<CSVYM> ?? csvymList.ToList();
                            if (enumerable.Any())
                            {
                                var csvym = enumerable[0];
                                //csvym payedDebt ve remainingDebt degerlerini toplama
                                var remainingDebt = Math.Round(reader["RemainingDebt"].Convert<decimal>());
                                var payedDebt = Math.Round(reader["PayedDebt"].Convert<decimal>());
                                csvym.PayedDebt = Math.Round(csvym.PayedDebt.Convert<decimal>() + payedDebt).ToString();
                                csvym.RemainingDebt = Math.Round(csvym.RemainingDebt.Convert<decimal>() + remainingDebt).ToString();

                                //farkli recId ayni identity li detaylari onceden detayi olan listeye ekleme
                                var details = csvydList.Where(c => c.RecId == recId).OrderBy(c => c.TransferDateForOrder).ToList();
                                csvym.CSVYDList.AddRange(details); 
                            }
                            else
                            {
                                var csvym = new CSVYM
                                {
                                    RecordType = "CSVYM",
                                    VersionNumber = "01",
                                    MemberCode = "858",
                                    IdentityType = reader["IdentityType"].Convert<string>(string.Empty),
                                    IdentityNumber = reader["IdentityNumber"].Convert<string>(string.Empty),
                                    Name = reader["Name"].Convert<string>(string.Empty),
                                    SecondaryName = reader["SecondaryName"].Convert<string>(string.Empty),
                                    Surname = reader["Surname"].Convert<string>(string.Empty),
                                    Address1 = reader["Address1"].Convert<string>(string.Empty),
                                    Address2 = reader["Address2"].Convert<string>(string.Empty),
                                    PhoneNumber1 = reader["PhoneNumber1"].Convert<string>(string.Empty),
                                    PhoneNumber2 = reader["PhoneNumber2"].Convert<string>(string.Empty),
                                    PhoneNumber3 = reader["PhoneNumber3"].Convert<string>(string.Empty),
                                    Email1 = reader["Email1"].Convert<string>(string.Empty),
                                    Email2 = reader["Email2"].Convert<string>(string.Empty),
                                    PaymentPlanDate = reader["PaymentPlanDate"].Convert<DateTime>().FormatToYYYYMMGGKrmZero(),
                                    PaymentPlanInstallmentCount = reader["PaymentPlanInstallmentCount"].Convert<string>(string.Empty),
                                    PayedInstallmentCount = reader["PayedInstallmentCount"].Convert<string>(string.Empty),
                                    PayedDebt = Math.Round(reader["PayedDebt"].Convert<decimal>()).ToString(),
                                    RemainingDebt = Math.Round(reader["RemainingDebt"].Convert<decimal>()).ToString(),
                                    CurrencyCode = reader["CurrencyCode"].Convert<string>(string.Empty),
                                    RevisedPaymentPlanDate = reader["RevisedPaymentPlanDate"].Convert<DateTime>().FormatToYYYYMMGGKrmZero(),
                                    RevisedPaymentPlanInstallmentCount = reader["RevisedPaymentPlanInstallmentCount"].Convert<string>(string.Empty),
                                    RevisedPayedInstallmentCount = reader["RevisedPayedInstallmentCount"].Convert<string>(string.Empty),
                                    RevisedRemainingDebt = Math.Round(reader["RevisedRemainingDebt"].Convert<decimal>()).ToString(),
                                    FirstExecutiveDate = reader["FirstExecutiveDate"].Convert<DateTime>().FormatToYYYYMMGGKrmZero(),
                                    LastExecutiveDate = reader["LastExecutiveDate"].Convert<DateTime>().FormatToYYYYMMGGKrmZero(),
                                    ExecutiveAmount = Math.Round(reader["ExecutiveAmount"].Convert<decimal>()).ToString(),
                                    DebtStatus = reader["DebtStatus"].Convert<string>(string.Empty),
                                    DebtStatusAlterationStatus = reader["DebtStatusAlterationStatus"].Convert<string>(string.Empty),
                                    ClosureDate = reader["ClosureDate"].Convert<DateTime>().FormatToYYYYMMGGKrmZero(),
                                    RecId = reader["RecId"].Convert<string>(string.Empty)
                                };

                                csvym.CSVYDList = csvydList.Where(c => c.RecId == csvym.RecId).OrderBy(c => c.TransferDateForOrder).ToList();
                                file.CSVYMList.Add(csvym);
                            }

                        }
                        file.Footer.TotalCountOfCustomerRecords = file.CSVYMList.Count().ToString();
                    }
                    return file;
                }
                Logger.Error("Borclu Listesinden kayit donmedi. 'Usp_GetKrmFileCSVYDListByDate' sp row count is 0.");
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("GetFile metodunda hata oluştu.", ex);
                return null;
            }
        }

        private List<CSVYD> GetCSVYDList()
        {
            try
            {
                var CSVYDList = new List<CSVYD>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_KRMDailyReportGetDataForCSVYD");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        var csvyd = new CSVYD
                        {
                            RecordType = "CSVYD",
                            VersionNumber = "01",
                            MemberCode = "858",
                            IdentityType = reader["IdentityType"].Convert<string>(string.Empty),
                            IdentityNumber = reader["IdentityNumber"].Convert<string>(string.Empty),
                            AccountNumber = reader["AccountNumber"].Convert<string>(string.Empty),
                            TransferDate = reader["TransferDate"].Convert<DateTime>().FormatToYYYYMMGGKrm(),
                            TransferDateForOrder = reader["TransferDate"].Convert<DateTime>(),
                            TransferMemberCode = reader["TransferMemberCode"].Convert<string>(string.Empty),
                            PrincipalBalanceTransferedIn = reader["PrincipalBalanceTransferedIn"].Convert<string>(string.Empty),
                            CurrencyCode = reader["CurrencyCode"].Convert<string>(string.Empty),
                            AccountOpeningDate = reader["AccountOpeningDate"].Convert<DateTime>().FormatToYYYYMMGGKrm(),
                            RecId = reader["RecId"].Convert<string>(string.Empty)
                        };
                        CSVYDList.Add(csvyd);
                    }
                }
                return CSVYDList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCSVYDList metodunda hata oluştu.", ex);
                return null;
            }
        }
    }
}
