﻿using Efes.Entities.LawOffice;
using System;
using System.Data;

namespace Efes.DataAccessLayer
{
    public class LawOfficeUserDal: BaseDal
    {
        public bool InsertOrUpdateLawOfficeUser(OfficeUser officeUser, int byId, bool assigned)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_InsertOrUpdateLawOfficeUser");
                Db.AddInParameter(dbCommand, "@LawOfficeId", DbType.Int32, officeUser.LawOfficeId);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, officeUser.UserId);
                Db.AddInParameter(dbCommand, "@ById", DbType.Int32, byId);
                Db.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, officeUser.IsActive);
                Db.AddInParameter(dbCommand, "@Assigned", DbType.Boolean, assigned);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                Logger.Error("InsertOrUpdateLawOfficeUser metodunda hata oluştu.", ex);
                return false;
            }
        }
    }
}
