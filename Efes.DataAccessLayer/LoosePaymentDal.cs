﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities;
using Efes.Entities.Obligor;
using Efes.Models;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class LoosePaymentDal: BaseDal
    {
        public List<LoosePayment> GetLoosePaymentList()
        {
            try
            {
                var tmpList = new List<LoosePayment>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetUnProccessedPaymentList");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        tmpList.Add(new LoosePayment()
                        {
                            RecId = reader["RecId"].Convert<string>(),
                            ContactRecId = reader["ContactRecId"].Convert<string>(),
                            IsRelated = reader["IsReleated"].Convert<bool>(),
                            PaymentDate = reader["PaymentDate"].Convert<DateTime>(),
                            Amount = reader["Amount"].Convert<decimal>(),
                            PaymentBank = reader["PaymentBank"].Convert<string>(),
                            PaymentDesc = reader["PaymentDesc"].Convert<string>(),
                            DbsLine = reader["DBSLine"].Convert<string>(),
                            DisplayName = reader["DisplayName"].Convert<string>(),
                            Tckn = reader["TCKNO"].Convert<string>(),
                            EmployeeLoginId = reader["EmployeeLoginId"].Convert<string>(),
                            StatusText = reader["StatusText"].Convert<string>(),
                            MessageText = reader["MessageText"].Convert<string>(),
                            TahsilatYevmiyeNo = reader["TahsilatYevmiyeNo"].Convert<string>(),
                            MuhasebeFisNo = reader["MuhasebeFisNo"].Convert<string>(),
                        });
                    }
                }
                return tmpList;
            }
            catch (Exception ex)
            {
                ex.Log("GetLoosePaymnetList metodunda hata oluştu.");
                return new List<LoosePayment>();
            }
        }

        public bool UpdateProccessedPayment(int userId, string recId, string value)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_UpdateProccessedPayment");
                Db.AddInParameter(dbCommand, "@PaymentRecId", DbType.String, recId);
                Db.AddInParameter(dbCommand, "@Value", DbType.String, value);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                Logger.Error("UpdateProccessedPayment metodunda hata oluştu.", ex);
                return false;
            }
        }
    }
}
