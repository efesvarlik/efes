﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities;
using Efes.Entities.Portfolio;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class LocationDal: BaseDal
    {
        public List<City> GetCities()
        {
            try
            {
                var cityList = new List<City>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetCities");
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        cityList.Add(new City()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Name = reader["Name"].Convert<string>(),
                        });
                    }
                }
                return cityList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCities metodunda hata oluştu.", ex);
                return new List<City>();
            }
        }

        public List<County> GetCountiesByCityId(int cityId)
        {
            try
            {
                var countyList = new List<County>();
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_GetCountiesByCityId");
                Db.AddInParameter(dbCommand, "@CityId", DbType.String, cityId);
                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        countyList.Add(new County()
                        {
                            Id = reader["Id"].Convert<int>(),
                            Name = reader["Name"].Convert<string>(),
                            CityId = reader["CityId"].Convert<int>(),
                        });
                    }
                }
                return countyList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCountiesByCityId metodunda hata oluştu.", ex);
                return new List<County>();
            }
        }
    }
}
