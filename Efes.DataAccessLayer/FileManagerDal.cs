﻿using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Efes.Entities;
using Efes.Entities.Excel;
using Exception = System.Exception;

namespace Efes.DataAccessLayer
{
    public class FileManagerDal : BaseDal
    {
        public bool FileUpload(DataImportType dataImportType, DataTable dt, int memberId)
        {
            try
            {
                string spname = string.Empty;
                switch (dataImportType)
                {
                    case DataImportType.Address:
                        spname = "Usp_BulkInsertAddress";
                        break;
                    case DataImportType.Note:
                        spname = "Usp_BulkInsertNote";
                        break;
                    case DataImportType.Phone:
                        spname = "Usp_BulkInsertPhone";
                        break;
                    case DataImportType.AttachBulkFile:
                        spname = "Usp_BulkChangeAccountOwner";
                        break;
                    case DataImportType.Portfolio:
                        spname = "";
                        break;

                }
                DbCommand objDbCmd = Db.GetStoredProcCommand(spname);
                objDbCmd.Parameters.Add(new SqlParameter("@MemberId", memberId));
                objDbCmd.Parameters.Add(new SqlParameter("@TableType", dt) { SqlDbType = SqlDbType.Structured });
                Db.ExecuteNonQuery(objDbCmd);

                return true;

            }
            catch (Exception ex)
            {
                Logger.Error("Dosya Yukleme sirasinda hata oluştu.", ex);
                return false;
            }
        }

        public bool FileImportForBulkSms(SmsImportType smsImportType, DataTable dt, int userId, bool activeProtocol, bool protocolApprove, bool closeAccount, bool salary)
        {
            try
            {
                const string spname = "Usp_BulkInsertSmsLogWithAccountId";
                var objDbCmd = Db.GetStoredProcCommand(spname);
                objDbCmd.Parameters.Add(new SqlParameter("@UserId", userId));
                objDbCmd.Parameters.Add(new SqlParameter("@TableType", dt) { SqlDbType = SqlDbType.Structured });
                objDbCmd.Parameters.Add(new SqlParameter("@ActiveProtocol", activeProtocol ));
                objDbCmd.Parameters.Add(new SqlParameter("@ProtocolApprove", protocolApprove));
                objDbCmd.Parameters.Add(new SqlParameter("@CloseAccount", closeAccount));
                objDbCmd.Parameters.Add(new SqlParameter("@Salary", salary));
                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("Sms icin dosya yukleme sirasinda hata oluştu.", ex);
                return false;
            } 
        }

        public bool InterestAndCostUpload(DataTable dt, int userId)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("Usp_BulkUpdateFaizMasraf");
                objDbCmd.Parameters.Add(new SqlParameter("@UserId", userId));
                objDbCmd.Parameters.Add(new SqlParameter("@Ty_BulkFaizMasraf", dt) { SqlDbType = SqlDbType.Structured });
                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("InterestAndCostUpload Dosya Yukleme sirasinda hata oluştu.", ex);
                return false;
            }
        }

        public bool LandRegister(DataTable dt, int userId, string fileName)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("Usp_BulkLandRegister");
                objDbCmd.Parameters.Add(new SqlParameter("@UserId", userId));
                objDbCmd.Parameters.Add(new SqlParameter("@FileName", fileName));
                objDbCmd.Parameters.Add(new SqlParameter("@Ty_BulkTckn", dt) { SqlDbType = SqlDbType.Structured });
                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("LandRegister Dosya Yukleme sirasinda hata oluştu.", ex);
                return false;
            }
        }

        public bool SalaryDistraint(DataTable dt, int userId, string fileName)
        {
            try
            {
                var objDbCmd = Db.GetStoredProcCommand("Usp_BulkSalaryDistraint");
                objDbCmd.Parameters.Add(new SqlParameter("@UserId", userId));
                objDbCmd.Parameters.Add(new SqlParameter("@FileName", fileName));
                objDbCmd.Parameters.Add(new SqlParameter("@Ty_BulkTckn", dt) { SqlDbType = SqlDbType.Structured });
                Db.ExecuteNonQuery(objDbCmd);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("SalaryDistraint Dosya Yukleme sirasinda hata oluştu.", ex);
                return false;
            }
        }
    }
}
