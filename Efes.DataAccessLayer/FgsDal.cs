﻿using System;
using System.Collections.Generic;
using System.Data;
using Efes.Entities;
using Efes.Entities.Fgs;
using Efes.Utility;

namespace Efes.DataAccessLayer
{
    public class FgsDal: BaseDal
    {
        public FgsDal() : base(DbTypeEnum.SesWare)
        {

        }
        public int Call(Call call)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.efes_ADD_PHONENUMBER");
                Db.AddInParameter(dbCommand, "@TELNO", DbType.String, call.TelNo);
                Db.AddInParameter(dbCommand, "@CPN", DbType.String, call.Cpn);
                Db.AddInParameter(dbCommand, "@AGENT_NAME", DbType.String, call.AgentName);
                Db.AddInParameter(dbCommand, "@QUEUE_NO", DbType.Int32, call.QueueNo);
                Db.AddInParameter(dbCommand, "@RECORD_TYPE", DbType.Int32, (int)call.RecordType);
                Db.AddInParameter(dbCommand, "@NEXT_TRY_DATE", DbType.DateTime, call.NextTryDate);
                Db.AddInParameter(dbCommand, "@AD_SOYAD", DbType.String, call.CustomerName);
                Db.AddInParameter(dbCommand, "@FIRMA_ADI", DbType.String, call.CompanyName);
                Db.AddInParameter(dbCommand, "@ACCOUNT_ID", DbType.String, call.AccountId);
                var ds = Db.ExecuteDataSet(dbCommand);
                if (ds.HasData())
                {
                    return ds.Tables[0].Rows[0]["TEL_LIST_ONLINE_ID"].Convert<int>();
                }
                return -1;
            }
            catch (Exception ex)
            {
                ex.Log("Call metodunda hata oluştu.");
                return -2;
            }
        }

        public List<MissedCall> GetMissedCall(int userId)
        {
            try
            {
                var missedCallList = new List<MissedCall>();
                var dbCommand = Db.GetStoredProcCommand("dbo.cloud_wb_MISSED_CALLS");
                Db.AddInParameter(dbCommand, "@WEB_LOGIN_USERS_ID", DbType.Int32, 1);
                Db.AddInParameter(dbCommand, "@COMPANIES_ID", DbType.Int32, 0);
                Db.AddInParameter(dbCommand, "@DEPARTMENTS_ID", DbType.Int32, 0);
                Db.AddInParameter(dbCommand, "@TEAMS_ID", DbType.Int32, 0);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);

                var reader = Db.ExecuteReader(dbCommand);
                using (reader)
                {
                    while (reader.Read())
                    {
                        missedCallList.Add(new MissedCall()
                        {
                            CallTime = reader["CAGRI_ZAMANI"].Convert<DateTime>(),
                            CallId = reader["CAGRI_ID"].Convert<string>(),
                            CallingNumber = reader["ARAYAN_NUMARA"].Convert<string>(),
                            CalledNumber = reader["ARANAN_NUMARA"].Convert<string>(),
                            ClosingReason = reader["KAPANMA_NEDENI"].Convert<string>(),
                            UserCode = reader["KULLANICI_KODU"].Convert<string>(),
                            UserName = reader["KULLANICI_ADI"].Convert<string>(),
                            Number = reader["DAHILI_NUMARA"].Convert<string>(),
                        });
                    }
                }
                return missedCallList;
            }
            catch (Exception ex)
            {
                Logger.Error("GetMissedCall metodunda hata oluştu.", ex);
                return new List<MissedCall>();
            }
        }
    }
}
