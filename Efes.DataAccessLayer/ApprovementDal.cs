﻿using Efes.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using Efes.Entities.Approvement;

namespace Efes.DataAccessLayer
{
    [Serializable]
    public class ApprovementDal : BaseDal
    {
        #region Upload Approvement

        public List<UploadApprovement> GetUploadApprovementList()
        {
            var uploadApprovementList = new List<UploadApprovement>();
            try
            {
                var command = Db.GetStoredProcCommand("dbo.Usp_GetApprovableLoadList");
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        uploadApprovementList.Add(new UploadApprovement()
                        {
                            BulkId = dr["BulkId"].Convert<string>(),
                            Type = dr["Type"].Convert<string>(),
                            TotalCount = dr["TotalCount"].Convert<int>(),
                            UserName = dr["UserName"].Convert<string>(),
                            Desc = dr["Desc"].Convert<string>(),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetUploadApprovementList metodunda hata oluştu.", ex);

            }
            return uploadApprovementList;
        }

        public bool ApproveLoadList(string bulkId, int userId, string type)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ApproveLoadList");
                Db.AddInParameter(dbCommand, "@BulkId", DbType.String, bulkId);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Type", DbType.String, type);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ApproveLoadList metodunda hata oluştu.");
                return false;
            }
        }

        #endregion

        #region Change Payment Owner

        public List<ChangePaymentOwner> GetChangePaymentOwnerList(int userId)
        {
            var changePaymentOwnerList = new List<ChangePaymentOwner>();
            try
            {
                var command = Db.GetStoredProcCommand("dbo.Usp_GetApprovementForPaymentOwner");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        changePaymentOwnerList.Add(new ChangePaymentOwner()
                        {
                            Id = dr["Id"].Convert<int>(),
                            RecId = dr["RecId"].Convert<string>(),
                            FileId = dr["AccountId"].Convert<string>(),
                            PaymentDate = dr["PaymentDate"].Convert<string>(),
                            PaymentAmount = dr["Amount"].Convert<string>(),
                            OldOwner = dr["PreviousOwner"].Convert<string>(),
                            NewOwner = dr["NewOwner"].Convert<string>(),
                            Changer = dr["CreatedBy"].Convert<string>(),
                            ChangeDate = dr["CreatedDate"].Convert<string>()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetChangePaymentOwnerList metodunda hata oluştu.", ex);

            }
            return changePaymentOwnerList;
        }

        public bool ApproveOrDenyChangePaymentOwner(string recId, int userId, bool approved, string desc, int id)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ApproveOrDenyChangePaymentOwner");
                Db.AddInParameter(dbCommand, "@Id", DbType.Int32, id);
                Db.AddInParameter(dbCommand, "@RecId", DbType.String, recId);
                Db.AddInParameter(dbCommand, "@Desc", DbType.String, desc);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Approved", DbType.Boolean, approved);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ApproveOrDenyChangePaymentOwner metodunda hata oluştu.");
                return false;
            }
        }

        #endregion

        #region Special Execution

        public List<SpecialExecution> GetApprovementForSpecialExecution(int userId)
        {
            var specialExecutionList = new List<SpecialExecution>();
            try
            {
                var command = Db.GetStoredProcCommand("dbo.Usp_GetApprovementForSpecialExecution");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        specialExecutionList.Add(new SpecialExecution()
                        {
                            Id = dr["Id"].Convert<int>(),
                            AccountId = dr["AccountId"].Convert<string>(),
                            FullName = dr["FullName"].Convert<string>(),
                            TotalDebtAmount = dr["TotalDebtAmount"].Convert<string>(),
                            DebtPrinciple = dr["DebtPrinciple"].Convert<string>(),
                            CreditPrinciple = dr["CreditPrinciple"].Convert<string>(),
                            Reason = dr["Reason"].Convert<string>(),
                            CreatedBy = dr["CreatedBy"].Convert<string>(),
                            CreatedDate = dr["CreatedDate"].Convert<string>()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetApprovementForSpecialExecution metodunda hata oluştu.", ex);
            }
            return specialExecutionList;
        }

        public bool ApproveOrDenySpecialExecution(int userId, bool approved, string desc, int id)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_ApproveOrDenySpecialExecution");
                Db.AddInParameter(dbCommand, "@Id", DbType.Int32, id);
                Db.AddInParameter(dbCommand, "@Desc", DbType.String, desc);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Approved", DbType.Boolean, approved);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ApproveOrDenySpecialExecution metodunda hata oluştu.");
                return false;
            }
        }

        #endregion

        #region Land Register

        public List<LandRegister> GetWaitingLandRegister(int userId)
        {
            var landRegisterList = new List<LandRegister>();
            try
            {
                var command = Db.GetStoredProcCommand("dbo.Usp_GetBulkLandRegisterApprovement");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        landRegisterList.Add(new LandRegister()
                        {
                            Id = dr["BulkId"].Convert<string>(),
                            FileName = dr["FileName"].Convert<string>(),
                            FileDate = dr["CreateDate"].Convert<DateTime>(),
                            RecordCount = dr["RecordCount"].Convert<int>(),
                            StarterLoadingId = dr["CreateBy"].Convert<string>(),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetWaitingLandRegister metodunda hata oluştu.", ex);
            }
            return landRegisterList;
        }

        public bool ApproveOrDenyLandRegister(int userId, bool approved, string id)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_SetBulkLandRegisterApprovement");
                Db.AddInParameter(dbCommand, "@BulkId", DbType.String, id);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Approved", DbType.Boolean, approved);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ApproveOrDenyLandRegister metodunda hata oluştu.");
                return false;
            }
        }

        #endregion

        #region Land Register

        public List<SalaryDistraint> GetWaitingSalaryDistraint(int userId)
        {
            var salaryDistraintList = new List<SalaryDistraint>();
            try
            {
                var command = Db.GetStoredProcCommand("dbo.Usp_GetBulkSalaryDistraintApprovement");
                Db.AddInParameter(command, "@UserId", DbType.Int32, userId);
                var dr = Db.ExecuteReader(command);
                using (dr)
                {
                    while (dr.Read())
                    {
                        salaryDistraintList.Add(new SalaryDistraint()
                        {
                            Id = dr["BulkId"].Convert<string>(),
                            FileName = dr["FileName"].Convert<string>(),
                            FileDate = dr["CreateDate"].Convert<DateTime>(),
                            RecordCount = dr["RecordCount"].Convert<int>(),
                            StarterLoadingId = dr["CreateBy"].Convert<string>(),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetWaitingSalaryDistraint metodunda hata oluştu.", ex);
            }
            return salaryDistraintList;
        }

        public bool ApproveOrDenySalaryDistraint(int userId, bool approved, string id)
        {
            try
            {
                var dbCommand = Db.GetStoredProcCommand("dbo.Usp_SetBulkSalaryDistraintApprovement");
                Db.AddInParameter(dbCommand, "@BulkId", DbType.String, id);
                Db.AddInParameter(dbCommand, "@UserId", DbType.Int32, userId);
                Db.AddInParameter(dbCommand, "@Approved", DbType.Boolean, approved);
                return Db.ExecuteNonQuery(dbCommand) > 0;
            }
            catch (Exception ex)
            {
                ex.Log("ApproveOrDenySalaryDistraint metodunda hata oluştu.");
                return false;
            }
        }

        #endregion
    }
}
