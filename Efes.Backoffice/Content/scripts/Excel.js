﻿
var Excel = (function () {
    var type = "";

    function addExcelFile() {
        "use strict";
        $("#fileupload").fileupload({
            url: "FileImport",
            dataType: "json",
            formData: { type: type },
            done: function (e, data) {
                App.unblockUI("#fileUploadDiv");
                if (data.result.Result === "Success") {
                    Message.Show("Dosya Yükleme işlemi başarıyla sonuçlanmıştır.", "Bilgilendirme", Message.ThemeType.Teal);
                } else if (data.result.Result === "Error") {
                    if (data.result.MissingColumnNames) {
                        Message.Show("Dosyada eksik kolonlar bulunmaktadır. Hataları düzelttikten sonra tekrar deneyin.<br/>" + data.result.MissingColumnNames, "", Message.ThemeType.Ruby, true);
                    } else {
                        Message.Show("Dosya yüklenemedi", "Hata", Message.ThemeType.Ruby);
                    }
                }
            },
            progressall: function (e, data) {
                App.blockUI({
                    target: "#fileUploadDiv",
                    boxed: true
                });
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#progress .progress-bar").css(
                    "width",
                    progress + "%"
                );
            },
            error: function (ex) {
                App.unblockUI("#fileUploadDiv");
                console.log(ex);
                alert("Dosya yukleme esnasinda hata ile karsilasildi.!!");
            }
        }).prop("disabled", !$.support.fileInput)
          .parent().addClass($.support.fileInput ? undefined : "disabled");
    }

    function addAddress() {
        type = "Address";
        addExcelFile();
    }

    function addNote() {
        type = "Note";
        addExcelFile();
    }

    function addPhone() {
        type = "Phone";
        addExcelFile();
    }

    function attachBulkFile() {
        type = "BulkFileAttach";
        addExcelFile();
    }

    return {
        AddAddress: addAddress,
        AddNote: addNote,
        AddPhone: addPhone,
        AttachBulkFile: attachBulkFile
    }

})();