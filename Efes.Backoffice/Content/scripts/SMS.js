﻿var SMS = (function () {
    var fileType = {
        "AccountId": 1,
        "TCKN": 2
    }
    var optFileType = 1;
    var impId = "";

    function init() {
        addExcelFile(fileType.AccountId);
        $("#optAccountId").on("change", function () {
            if ($(this).prop("checked")) {
                optFileType = fileType.AccountId;
                addExcelFile();
            }
        });
        $("#optTckn").on("change", function () {
            if ($(this).prop("checked")) {
                optFileType = fileType.TCKN;
                addExcelFile();
            }
        });
        $("#aktifProtocol, #protocolOnay, #infaz, #maas").on("change", function () {
            addExcelFile();
        });
    }

    function addExcelFile() {
        "use strict";

        $("#fileupload").fileupload({
            url: "FileImport",
            dataType: "json",
            formData: {
                type: optFileType,
                activeProtocol: $("#aktifProtocol").prop('checked'),
                protocolApprove: $("#protocolOnay").prop('checked'),
                closeAccount: $("#infaz").prop('checked'),
                salary: $("#maas").prop('checked')
            },
            done: function (e, data) {
                App.unblockUI("#fileUploadDiv");
                if (data.result.Result === "Success") {
                    Message.Show("SMS gönderim işlemi başarıyla sonuçlanmıştır.", "Bilgilendirme", Message.ThemeType.Teal);
                } else if (data.result.Result === "Error") {
                    if (data.result.MissingColumnNames) {
                        Message.Show("Dosyada eksik kolonlar bulunmaktadır. Hataları düzelttikten sonra tekrar deneyin.<br/>" + data.result.MissingColumnNames, "", Message.ThemeType.Ruby, true);
                    } else {
                        $("#infoBoxError").show();
                        $("#spnErrorMessage").html(data.result.Message);
                        Message.Show("Dosya yüklenemedi", "Hata", Message.ThemeType.Ruby);
                    }
                }
            },
            progressall: function (e, data) {
                console.log("start");
                App.blockUI({
                    target: "#fileUploadDiv",
                    boxed: true
                });
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#progress .progress-bar").css(
                    "width",
                    progress + "%"
                );
            },
            error: function (ex) {
                App.unblockUI("#fileUploadDiv");
                console.log(ex);
                alert("Dosya yukleme esnasinda hata ile karsilasildi.!!");
            }
        }).prop("disabled", !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : "disabled");
    }

    function sendSms() {
        if (impId) {
            $("#btnSendSms").prop("disabled", true);
            App.blockUI({
                target: "#fileUploadDiv",
                boxed: true
            });

            Core.AjaxCall({
                url: "/Sms/SendSms",
                data: JSON.stringify({ importId: impId, type: optFileType }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    console.log(data);
                    App.unblockUI("#fileUploadDiv");

                    if (data.Result === "Success") {
                        $("#infoBoxSuccess").html("Mesaj gönderim işlemi tamamlandı.");
                        Message.Show("İşlem başarıyla gerçekleşti.", "Bilgi");
                        console.log(data.Result);
                    } else if (data.Result === "Info") {
                        Message.Show(data.Message, "Bilgi", Message.ThemeType.Lemon);
                    } else if (data.Result === "Error") {
                        Message.Show(data.Message, "Hata", Message.ThemeType.Ruby);
                    }
                }
            });
        } else {
            Message.Show("İşlmede hata oluştu, işleme devam etmeden sistem yöneticine danışın.", "Hata", Message.ThemeType.Ruby);
        }
    }

    return {
        Init: init,
        SendSms: sendSms
    }

})();






