var mobileNavigation = null;

var ScreenInformation = (function () {

    function isPhone() {
        return Modernizr.mq('only all and (max-width: 767px)');
    }

    function isTablet() {
        return Modernizr.mq('only all and (max-width: 995px)and (min-width: 768px)');
    }

    function isDesktop() {
        return Modernizr.mq('only all and (min-width: 996px)');
    }

    function isTouch() {
        return Modernizr.touch || window.navigator.msMaxTouchPoints;
    }

    return {
        IsPhone: isPhone,
        IsTablet: isTablet,
        IsDesktop: isDesktop,
        IsTouch: isTouch
    };
})();

var ResponsiveUI = (function () {

    var ui = {};
    var CurrentScreen = null;
    var WindowHeight = $(window).height();
    var subnavleftoffset = 0;
    var subnavfullwidth = 0;

    /**/
    var inputMaskOptions = {
        cellphone: {
            mask: "0(199) 999 99 99",
            options: {
                translation: {
                    0: {
                        pattern: /[0]/,
                        fallback: '0'
                    },
                    1: {
                        pattern: /[1-9]/,
                    },
                    9: { pattern: /[0-9]/ }
                }
            }
        },
        tcIdentity: {
            mask: "99999999999",
            options: {
                translation: {
                    9: { pattern: /[0-9]/ }
                }
            }
        }
        //... Yeni mask opsiyonlar� buraya eklenebilir...
    };
    /**/

    function SetMobileFooter() {
        if (Modernizr.mq('only all and (max-width: 480px)')) {
            $(".footer-links h4").click(function () {
                $(this).parent().toggleClass('active');
            });
        } else {
            $(".footer-links .links-body").show();
        }
    }

    function SetMobileCarousel() {
        if (Modernizr.mq('only all and (max-width: 480px)')) {
            $('#nsn-carousel').carousel({
                interval: false
            });
        } else {
            $('#nsn-carousel').carousel({
                interval: 3000
            });
        }
    }

    function SetSiteSubNav() {
        if (!ResponsiveUI.ScreenInfo.IsTouch() && Modernizr.mq('only all and (min-width: 1024px) and (max-width: 1149px)')) {
            $('#sub-nav').each(function () {
                if ($(this).find('.item').not('.other-subgames, .other-item').length > 7) {
                    $(this).find('.item').not('.other-subgames').slice(-2).addClass('other-item').hide();
                    $(this).find(".other-item").clone().appendTo($('.other-subgames').find('.sub-dropdown'));
                    $('.sub-dropdown .other-item').find('a').text();
                    $('.sub-dropdown .other-item').each(function () {
                        var $thisText = $(this).attr('data-text');
                        $(this).find('a').html($thisText);
                    });
                    $('.sub-dropdown .other-item').show();
                }
                else {
                    $('.sub-dropdown .other-item').remove();
                }
            });
        }

        $(window).scroll(function () {
            $('#sub-nav li .sub-dropdown').hide();
        })

        $('#sub-nav nav ul li').on('mouseover', function () {
            $(this).find('.sub-dropdown').show();
        })
    }

    function SetCustomUI(scope) {
        scope = scope || document;

        $(scope).find('input[type=checkbox].custom-ui, input[type=radio].custom-ui, .customall-ui input[type=checkbox], .customall-ui input[type=radio]').iCheck({
            checkboxClass: 'icheckbox',
            radioClass: 'iradio',
            inheritClass: true
        }).on('ifChecked', function (event) {
            $(this).parents('.input-wrap').addClass('checked');
        }).on('ifUnchecked', function (event) {
            $(this).parents('.input-wrap').removeClass('checked');
        });

        $(scope).find('input[type=checkbox].custom-ui-sm, input[type=radio].custom-ui-sm').iCheck({
            checkboxClass: 'icheckbox-sm',
            radioClass: 'iradio-sm',
            inheritClass: true
        }).on('ifChecked', function (event) {
            $(this).parents('.input-wrap').addClass('checked');
        }).on('ifUnchecked', function (event) {
            $(this).parents('.input-wrap').removeClass('checked');
        });
    }

    function BindTouchableEvents() {

        $('body').on('touchstart', function (e) {
            var target = $(e.target);
            if (target.parents('#sub-nav').length == 0) {
                $('#sub-nav li ul').css('display', 'none');
            }
        });
        if (ResponsiveUI.ScreenInfo.IsTouch()) {
            $('#sub-nav nav').scroll(function () {
                $('#sub-nav li .sub-dropdown').css('display', 'none');
            });
            if (!ResponsiveUI.ScreenInfo.IsPhone()) {
                $('#sub-nav li > a').not("#sub-nav li ul a").click(function (e) {
                    if ($(this).next('.sub-dropdown').length != 0) {
                        if ($(this).next('.sub-dropdown').is(':visible') == false) {
                            e.preventDefault();
                            var position = getViewportOffset($(this).parent());
                            $(this).parent().find('.sub-dropdown').css('left', position.left);
                            $(this).parent().find('.sub-dropdown').css('top', position.top + $(this).parent().outerHeight(true));
                            $(this).parent().find('.sub-dropdown').show();
                        }
                    }
                });

                $('#sub-nav li .sub-dropdown').not($(this).next('.sub-dropdown')).css('display', 'none');
            }
            /// Masa�st� browser'� 320 boyutunda etkiledi�inden yorum sat�r�na al�nd�.
            //else {
            //    $('#sub-nav li > a').not("#sub-nav li ul a").hover(function () {
            //        document.location = $(this).attr('href');
            //    });
            //}

            $('#sub-nav li .sub-dropdown').css('display', 'none');
        }
    }

    function BindSubNav() {
        $('#sub-nav nav').scroll(function () {
            $('#sub-nav li .sub-dropdown').css('display', 'none');
        });

        $('#sub-nav li > a').not("#sub-nav li ul a").hover(function (e) {
            if ($(this).next('.sub-dropdown').length != 0) {
                e.preventDefault();
                var position = getViewportOffset($(this).parent());
                $(this).parent().find('.sub-dropdown').css('left', position.left);
                $(this).parent().find('.sub-dropdown').css('top', position.top + $(this).parent().outerHeight(true));
                $(this).parent().find('.sub-dropdown').show();
            }
        });

        $(window).resize(function () {
            var fullwidth = sumOuterWidth($('#sub-nav li ').not("#sub-nav li ul li"), true);
            var visiblewidth = $('#sub-nav nav').outerWidth();
            var scrollposition = $('#sub-nav nav').scrollLeft();
            $('#sub-nav').removeAttr('class');
            if (fullwidth > visiblewidth - 2) {
                if (scrollposition < 5) {
                    $('#sub-nav').attr('class', 'scroll-right');
                } else if (scrollposition + visiblewidth > fullwidth + 10) {
                    $('#sub-nav').attr('class', 'scroll-left');
                } else {
                    $('#sub-nav').attr('class', 'scroll-both');
                }
            }

        });

    }

    function getViewportOffset($e) {
        var $window = $(window),
          scrollLeft = $window.scrollLeft(),
          scrollTop = $window.scrollTop(),
          offset = $e.offset(),
          rect1 = { x1: scrollLeft, y1: scrollTop, x2: scrollLeft + $window.width(), y2: scrollTop + $window.height() },
          rect2 = { x1: offset.left, y1: offset.top, x2: offset.left + $e.width(), y2: offset.top + $e.height() };
        return {
            left: offset.left - scrollLeft,
            top: offset.top - scrollTop,
            insideViewport: rect1.x1 < rect2.x2 && rect1.x2 > rect2.x1 && rect1.y1 < rect2.y2 && rect1.y2 > rect2.y1
        };
    }

    function sumOuterWidth($target, useMargins) {
        var sum = 0.0;
        $target.each(function () {
            sum += parseInt($(this).outerWidth(useMargins));
        });
        return sum;
    }

    function BindSubNavMenuScroll() {
        var sumAllOuter = sumOuterWidth($('#sub-nav li').not("#sub-nav li ul li"), true);
        var sumSubnav = $('#sub-nav nav').outerWidth(true);

        if (sumAllOuter > sumSubnav)
            $('#sub-nav').attr('class', 'scroll-right');

        subnavfullwidth = sumOuterWidth($('#sub-nav li ').not("#sub-nav li ul li"), true);

        $('#sub-nav nav').scroll(function (event) {
            var fullwidth = sumOuterWidth($('#sub-nav li ').not("#sub-nav li ul li"), true);
            subnavfullwidth = fullwidth;
            var visiblewidth = $('#sub-nav nav').outerWidth();
            var scrollposition = $('#sub-nav nav').scrollLeft();
            $('#sub-nav').removeAttr('class');
            if (scrollposition < 2) {
                $('#sub-nav').attr('class', 'scroll-right');
            } else if (scrollposition + visiblewidth > fullwidth + 10) {
                $('#sub-nav').attr('class', 'scroll-left');
            } else {
                $('#sub-nav').attr('class', 'scroll-both');
            }

        });
    }

    function SubnavRight() {
        if (!ResponsiveUI.ScreenInfo.IsTouch()) {
            var systemtime = 80;
            if (ResponsiveUI.ScreenInfo.IsPhone()) {
                systemtime = 0;
            }

            if (subnavleftoffset < subnavfullwidth + systemtime) { subnavleftoffset += 55; }
            $('#sub-nav nav').scrollLeft(subnavleftoffset);
        }
    }

    function SubnavLeft() {
        if (!ResponsiveUI.ScreenInfo.IsTouch()) {
            if (subnavleftoffset > 0) { subnavleftoffset -= 55; }
            $('#sub-nav nav').scrollLeft(subnavleftoffset);
        }
    }

    function BindToolTips() {
        var showTimer = 500;
        var hideTimer = 100;

        if (isBulten() && ScreenInformation.IsTablet()) {
            $('[data-toggle=tooltip]').tooltip('destroy');
            return;
        }

        if (!ResponsiveUI.ScreenInfo.IsPhone()) {

            if (isBulten()) {
                showTimer = 50;
                hideTimer = 50;               
            }

            $('[data-toggle=tooltip]').tooltip({
                'container': "body",
                'delay': { show: 500, hide: 100 },
                'html': true
            });

        }
    }

    function BindBootstrapHover() {
        $('.dropdown-hover').hover(function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });
    }

    function BindHomepageSwipeEvents() {
        $("#nsn-carousel").swiperight(function () {
            $(this).carousel('prev');
        });

        $("#nsn-carousel").swipeleft(function () {
            $(this).carousel('next');
        });
    }

    function BindDivLinks() {
        $(".div-link").click(function () {
            var link = $(this).attr("data-divlink");
            if ($(this).attr("data-divlinktarget") != undefined && $(this).attr("data-divlinktarget") == "_blank") {
                window.open(link);
            } else {
                window.location = link;
            }
        });
    }

    function BindSlidingSubMenu() {
        $('[data-toggle=nsn-offcanvas]').click(function () {
            var menuName = $(this).attr("data-target");
            $(menuName).toggle();
        });
    }

    function SetSlidingSubMenu() {
        if (ResponsiveUI.ScreenInfo.IsPhone() || ResponsiveUI.ScreenInfo.IsTablet()) {
            $(".offcanvas-sm").hide(0);
        } else {
            $(".offcanvas-sm").show(0);
        }
    }

    function BindSelectBox() {
        $(".select-tabs").parent().each(function () {
            var el = $(this);
            el.append($("<select />").addClass("selectbox-tabs"));
            el.find(".select-tabs a").each(function () {
                var optionItem = $(this);
                if (optionItem.parent().hasClass("active")) {
                    el.find(".selectbox-tabs").append($("<option />", {
                        "value": optionItem.attr("href"),
                        "text": optionItem.text(),
                        "selected": "selected",
                        "id": 'selectbox' + optionItem.attr("id")
                    }));
                } else {
                    el.find(".selectbox-tabs").append($("<option />", {
                        "value": optionItem.attr("href"),
                        "text": optionItem.text(),
                        "id": 'selectbox' + optionItem.attr("id")
                    }));
                }

            });
        });

        $(".selectbox-tabs").change(function () {
            var uri = $(this).find("option:selected").val();
            if (uri) {
                window.location = uri;
            }
        });

    }

    function BindPlaceholderPolly() {
        if (!Modernizr.input.placeholder) {
            $('input, textarea').placeholder();
        }
    }

    function BindWindowResize() {
        $(window).resize(function () {

            var cs = GetCurrentScreen();

            if (cs != CurrentScreen) {

                $(document).trigger("devicechanged");

                if (cs == "mobile") {

                    $(document).trigger("mobilechanged");

                } else if (cs == "tablet") {

                    $(document).trigger("tabletchanged");

                } else if (cs == "desktop") {

                    $(document).trigger("desktopchanged");
                }

                //mobilden dogrudan tablet veya desktopa
                //yada tam tersi durumda tetiklenir
                var mobileToFromChange = (CurrentScreen == "mobile" && (cs == "tablet" || cs == "desktop")) || (cs == "mobile" && (CurrentScreen == "tablet" || CurrentScreen == "desktop"));

                //tablet yada desktopdan mobile 
                if (mobileToFromChange) {

                    //if (cs == "tablet" || cs == "desktop") {
                    $(document).trigger("tabletordesktopchanged");
                }

                CurrentScreen = cs;
            };

            if (cs == "mobile") { ResponsiveCoupon.SetMobileCouponHeight(); }

            WindowHeight = $(window).height();

            SetMobileFooter();
            //SetMobileHeader();
            SetSlidingSubMenu();
        });
    }

    function isBulten() {
        return $("body").hasClass("bet-in-play");
    }

    function GetCurrentScreen() {
        if (ResponsiveUI.ScreenInfo.IsDesktop()) {
            return "desktop";
        } else if (ResponsiveUI.ScreenInfo.IsPhone()) {
            return "mobile";
        } else if (ResponsiveUI.ScreenInfo.IsTablet()) {
            return "tablet";
        };
    }

    function BindPopover() {

        $('*[data-toggle=popover]').each(function () {

            var popoverTemplate = '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content @class"></div></div>'.replace("@class", $(this).data("style") || "");

            var popoverPlacement = $(this).data("placement") || "top";

            $(this).popover({
                html: true,
                template: popoverTemplate,
                placement: function (context, source) {
                    if (!ResponsiveUI.ScreenInfo.IsDesktop() && $(source).data("mplacement")) {
                        return $(source).data("mplacement");
                    }
                    return popoverPlacement;
                },
                content: function () {
                    var targetId = $(this).attr('data-target');
                    return $(targetId).html();
                }
            });
        });
    }

    function HideAllPopovers() {
        $('*[data-toggle=popover]').popover('hide');
        $("#BetsToolTip").hide();
        $("#divBetListPopover").hide();
        $('[data-toggle=tooltip]').tooltip('hide');
    }

    function MobileNavigationToggle() {
        mobileNavigation.toggle();
        ManageTouchEventForMobileNavigation();

        // Coupon disabler
        if ($("html, body").hasClass("OpenedMobileCoupon")) {
            ResponsiveCoupon.SetOpenedCoupon();
            ResponsiveCoupon.ToggleCoupon();
        };
    }

    function BindMobilNavigationMenu() {
        mobileNavigation = new Slideout({
            'panel': document.getElementById('nsn-offcanvas-content'),
            'menu': document.getElementById('nsn-offcanvas'),
            'padding': 250,
            'tolerance': 70,
            'duration': 0,
            'touch': false
        });

        $(".login-action a.btn-login").click(function () {
            Navigation.MoveMenu = false;
            MobileNavigationToggle();
        });

        $(".mobile-menu-link").click(function () {
            if (!mobileNavigation.isOpen()) {
                MobileNavigationToggle();
            }
        });

        $(document).on("desktopchanged tabletchanged", function () {
            mobileNavigation.close();
            ManageTouchEventForMobileNavigation();
        });
    }

    function ClosePopovers(e) {
        $('[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    }

    function ManageTouchEventForMobileNavigation() {
        if (mobileNavigation.isOpen()) {
            FixBodyScroll("#nsn-offcanvas, .mobile-menu-link");
        } else {
            UnFixBodyScroll();
        }
    }

    function BindBodyActions() {
        $('body').on('touchstart', function (e) {
            ClosePopovers(e);
        });

        $('body').on('click', function (e) {
            ClosePopovers(e);
        });
    }
    //BROWSER HACKS
    function ApplyBrowserHacks() {
        var user = detect.parse(navigator.userAgent);
        // Display some property values in my browser's dev tools console
        if (user.browser.family === "Safari") {

            //$('.bet-container .odds-row.foundBet').width($("section").width());

            if (user.os && user.os.name && user.os.name.indexOf("Windows") > -1) {
                $('.bet-container .odds-row.foundBet').width($("section").width() - 2);
                $('.bet-container .odds-row.bomb-of-day').width($("section").width() - 2);
            }

        }
    }

    function ApplyMaxLengthForAndroid() {
        if (ResponsiveUI.DeviceInfo.IsAndroidPhone) {
            $('input[type=text], input[type=number]').on('keydown keyup', function () {
                var maxLength = $(this).attr('maxlength');
                if (maxLength) {
                    if ($(this).val().length > maxLength) {
                        $(this).val($(this).val().substring(0, maxLength));
                    }
                }
            });
        }
    }

    function FixBodyScroll(exceptSelector) {
        if (!ResponsiveUI.ScreenInfo.IsDesktop()) {
            //$("html, body").addClass("fixBodyScroll").on("touchmove", function (e) { e.preventDefault(); });
            $("body").addClass("fixBodyScroll").on("touchmove", function (e) { e.preventDefault(); });

            if (exceptSelector) {
                $(exceptSelector).on('touchmove', function (e) { e.stopPropagation(); });
            }
        }
    }

    function UnFixBodyScroll() {
        if (!ResponsiveUI.ScreenInfo.IsDesktop()) {
            //$("html, body").removeClass("fixBodyScroll").off("touchmove");
            $("body").removeClass("fixBodyScroll").off("touchmove");
        }
    }

    function BindInputMask() {
        if ($('input[data-inputmask]').inputmask) {
            $('input[data-inputmask]').inputmask();
            //if (ResponsiveUI.DeviceInfo.IsAndroidPhone || ResponsiveUI.DeviceInfo.IsAndroidTablet) { --> A�a��ya al�nd�...
            //    $("input[data-android-type='tel']").attr("type", "tel");
            //}
        }

        // Alternatif mask k�t�phanesi kullanacak input var ve bu library sayfaya dahil edilmi� ise...
        if ($('input[data-inputmask-name]').mask) {
            $("[data-inputmask-name]").each(function (i, v) {
                var opts = inputMaskOptions[$(v).attr("data-inputmask-name")];
                $(v).mask(opts.mask, opts.options);
            });
        }

        // Eski mask k�t�phanesi veya alternatif mask k�t�phanesi sayfaya dahil edilmi� ise...
        if ($('input[data-inputmask]').inputmask || $('input[data-inputmask-name]').mask) {
            if (ResponsiveUI.DeviceInfo.IsAndroidPhone || ResponsiveUI.DeviceInfo.IsAndroidTablet) {
                $("input[data-android-type='tel']").attr("type", "tel");
            }
        }
    }

    //TODO:sadece andorid 4.1'de calisir,  screeninfo'ya tasinmali 
    function ApplyAndroidFixes() {

        var nua = navigator.userAgent
        var isAndroid = (nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1 && nua.indexOf('Chrome') === -1);

        if (isAndroid) {
            $('select.form-control').removeClass('form-control').css('width', '100%')
        }
    }

    function BindFooterList() {

        if (!ResponsiveUI.ScreenInfo.IsDesktop() && !ResponsiveUI.ScreenInfo.IsTablet()) {
            $('#collapseFooter > div > h4').off("click").on("click", function () {
                var self = $(this);
                var parent = self.parent();
                $('.links-body', parent).toggle();
            });
        } else {
            $('#collapseFooter > div > h4').off("click");
        }
    }

    function BindFooterListChange() {
        $(document).on("devicechanged", function () {
            BindFooterList();
        });
    }

    function ResizeContentHeight() {

        var brandHeight = $("#brand").height();
        var offcanvasHeight = $("#nsn-offcanvas-content").height() + brandHeight;
        var contentHeight = $("#content").height();
        var bodyHeight = $("body").outerHeight();
        if (bodyHeight - offcanvasHeight > 0) {
            var x = offcanvasHeight - contentHeight;
            var y = bodyHeight - x;
            $("#content").height(y);
        }
        $(".lt-bet-title").click(function () {
            $("#content").css("height", "auto");
        });
    };

    function BindContentResize() {

        if (ResponsiveUI.ScreenInfo.IsPhone()) {
            $(document).on("bultenloaded", function () {
                ResizeContentHeight();
            })
        }
    }
    function CheckIconTooltip () {
        for (var i in Core.Structures.EventIconType)
        {
            $("." + Core.Structures.EventIconType[i].IconClass).attr("data-toggle", "tooltip");
            $("." + Core.Structures.EventIconType[i].IconClass).attr("data-original-title", Core.Structures.EventIconType[i].IconTitle);
        }
    }

    ui.Init = function (opt) {

        CurrentScreen = GetCurrentScreen();

        ui.DeviceInfo = opt.DeviceInfo;

        CheckIconTooltip();

        SetSiteSubNav();

        BindTouchableEvents();

        SetCustomUI();

        BindToolTips();

        BindPopover();

        BindBootstrapHover();

        SetMobileCarousel();

        BindHomepageSwipeEvents();

        BindDivLinks();

        BindSlidingSubMenu();

        SetMobileFooter();

        BindSelectBox();

        BindWindowResize();

        BindPlaceholderPolly();

        BindMobilNavigationMenu();

        BindBodyActions();

        ApplyMaxLengthForAndroid();

        BindInputMask();

        ApplyAndroidFixes();

        //BindSubNavMenuScroll();

        BindSubNav();

        BindFooterList();

        BindFooterListChange();

        BindContentResize();
    }

    ui.BindSubNavMenuScroll = BindSubNavMenuScroll;
    ui.BindPopover = BindPopover;
    ui.BindToolTips = BindToolTips;
    ui.BindSelectBox = BindSelectBox;
    ui.IsBulten = isBulten;
    ui.SetCustomUI = SetCustomUI;
    ui.ApplyBrowserHacks = ApplyBrowserHacks;
    ui.ApplyMaxLengthForAndroid = ApplyMaxLengthForAndroid;
    ui.FixBodyScroll = FixBodyScroll;
    ui.UnFixBodyScroll = UnFixBodyScroll;
    ui.HideAllPopovers = HideAllPopovers;
    ui.BindInputMask = BindInputMask;
    ui.ScreenInfo = ScreenInformation;
    ui.DeviceInfo = {}; // set at init
    ui.BindDivLinks = BindDivLinks;
    ui.sumOuterWidth = sumOuterWidth;
    ui.SubnavRight = SubnavRight;
    ui.SubnavLeft = SubnavLeft;
    ui.MobileNavigationToggle = MobileNavigationToggle;
    ui.CheckIconTooltip = CheckIconTooltip;

    /**/

    // Programmatically olarak inputmask yapabilmek i�in kullan�l�r.
    // - element = Input elementi (Jquery objesi veya html nesnesi olabilir)
    // - inputmaskName = "ResponsiveUI.js --> inputMaskOptions" alt�nda yer alan opsiyonlardan birisidir.
    // - defaultVal = inputmask i�leminin ard�ndan inputa set edilecek de�er. (opsiyoneldir)
    ui.setInputmask = function (element, inputmaskName, defaultVal) {
        
            //if ($('input[data-inputmask]').inputmask) {
            //    $('input[data-inputmask]').inputmask();
            //    //if (ResponsiveUI.DeviceInfo.IsAndroidPhone || ResponsiveUI.DeviceInfo.IsAndroidTablet) { --> A�a��ya al�nd�...
            //    //    $("input[data-android-type='tel']").attr("type", "tel");
            //    //}
            //}

            // Alternatif mask k�t�phanesi sayfaya dahil edilmi� ise...
            if ($(element).mask) {
                var opts = inputMaskOptions[inputmaskName];
                $(element).mask(opts.mask, opts.options);

                if (defaultVal && defaultVal.length > 0) {
                    $(element).val($(element).masked(defaultVal));
                }
            }

            // Eski mask k�t�phanesi veya alternatif mask k�t�phanesi sayfaya dahil edilmi� ise...
            if ($(element).inputmask || $(element).mask) {
                if (ResponsiveUI.DeviceInfo.IsAndroidPhone || ResponsiveUI.DeviceInfo.IsAndroidTablet) {
                    $("input[data-android-type='tel']").attr("type", "tel");
                }
            }
    }
    /**/

    return ui;

})();

