﻿
var Core = (function () {

    var core = {};

    core.AjaxCall = function (settings) {
        var defaultSettings = {
            url: "",
            method: "post",
            data: null,
            dataType: "json",
            success: null,
            error: null,
            actiontype: null,
            contentType: "application/json; charset=utf-8",
            async: true,
            blockDiv: null,
            loadingMessage: null,
            defaultError: true
        };

        var options = $.extend(defaultSettings, settings);

        core.Block(options.blockDiv, options.loadingMessage);

        try {
            $.ajax({
                cache: false,
                url: options.url,
                type: options.method,
                data: options.data,
                dataType: options.dataType,
                contentType: options.contentType,
                async: options.async,
                success: function (result) {
                    if (result.isFault) {
                        if (result.isAuthorized) {
                            window.location = "/Account/Login?r=" + encodeURIComponent(window.location.href);
                        } else {
                            result.error = "İşlemi yapmak için yetkiniz bulunmamaktadır.";
                            Message.Show("İşlemi yapmak için yetkiniz bulunmamaktadır.", "Hata", Message.ThemeType.Ruby);
                        }
                    }
                    options.success(result);
                    core.UnBlock(options.blockDiv);

                },
                error: function (result) {
                    if (result) {
                        console.log(result);
                        if (result.Redirect != undefined && result.Redirect != '') {
                            location.href = result.Redirect;
                            return;
                        }
                    }
                    if (options.error) {
                        options.error(result);
                    }
                    if (options.blockDiv) {
                        core.UnBlock(options.blockDiv);
                    }
                },
                complete: function (result) {
                    if (options.complete) {
                        options.complete(result);
                    }
                }
            });
        }
        catch (err) {
            //core.Popup.Error("Sistemde Bir hata var daha sonra deneğiniz") popup lar entegre edildiğinde kodlanacal
            core.UnBlock(options.blockDiv);
        }
    };

    core.Block = function (element, message) {
        $(element).block({ message: message });
    };

    core.UnBlock = function (element) {
        $(element).unblock();
    };

    core.Popup = {
        CloseButtonSelector: ".modal-content .btn-close",
        TitleSelector: ".modal-title",
        EmptyContentSelector: false,
        Settings: {},

        //options açıklamaları
        //content : html data (içerik), buraya set edilen value yu olduğu gibi basar , deafult : false
        //onOpen : çalıştırılmadan önce execute edilir, default false
        //onLoad : content yüklenmeden önce execute edilir. default : false
        //onComplete : content gösterildikten sonra çalıştırılır default: false
        //onClose : popup kapatıldıktan sonra çalıştırılır : default : false
        //href : eğer selector ise ("#ducument", ".document") buradaki html i content alanına basar, eğer bir url ise ajax call yaparak dönen içeriği basar. default : false
        //scrolling : scrooll gösterilecek mi, default : false
        //inline : true olduğu durumda href veya content in kendisi modal form olarak işlenir, default : false
        //isOverlayClosed : false olduğu durumda modal mouse click veya escape ile kapatılamaz. default : true
        //title : popup headerında gösterilecek cümle. default: empty
        //modalSelector : modal formun selectoru. default olarak bir modal formu vardır.
        //modalContentSelector : modal formu içindeki content alanı selectoru. default olarak bir boş bir content html i vardır. eğer contentSelector false gonderilirse content modalSelector a basılır.
        Show: function (options) {

            var defaults = {
                content: false,
                onOpen: false,
                onLoad: false,
                onComplete: false,
                onClose: false,
                href: false,
                scrolling: false,
                inline: false,
                isOverlayClosed: true,
                isAddBody: false,
                title: "",
                modalSelector: "#mod-base",
                modalContentSelector: ".modal-body",
                allowCalculateHeight: true,
                hasTitle: true,

            };

            core.Block();

            core.Popup.CloseOldModal();

            core.Popup.Settings = $.extend(defaults, options);

            if (core.Popup.Settings.onOpen) {
                core.Popup.Settings.onOpen();
            }

            if (core.Popup.Settings.inline && core.Popup.Settings.modalContentSelector) {
                core.Popup.Settings.modalContentSelector = core.Popup.EmptyContentSelector;
            }

            if (core.Popup.Settings.href) {
                core.Popup.Settings.content = false;
            }

            //render content and set modal selector inner
            core.Popup.RenderContent();

            if (core.Popup.Settings.onLoad) {
                core.Popup.Settings.onLoad();
            }

            //show modal OK
            var modalOptions = { show: true };

            if (!core.Popup.Settings.isOverlayClosed) {
                modalOptions = { show: true, keyboard: false, backdrop: "static" };
            }

            if (!ResponsiveUI.ScreenInfo.IsDesktop()) {
                modalOptions.backdrop = "static"; //backgrounda basilinca kapanmasini onler
                $(core.Popup.Settings.modalSelector + " .modal-body").addClass("modal-body-touch");
            }

            $(core.Popup.Settings.modalSelector).off('shown.bs.modal').on('shown.bs.modal', function () {
                if (ResponsiveUI.ScreenInfo.IsDesktop()) { //
                    $(core.Popup.Settings.modalSelector).find('input:text:visible:first').focus();
                }
                if (core.Popup.Settings.onComplete) {
                    core.Popup.Settings.onComplete();
                }
            });


            $(core.Popup.Settings.modalSelector).off("hidden.bs.modal").on("hidden.bs.modal", function () {
                if (core.Popup.Settings.onClose) {
                    core.Popup.Settings.onClose();
                }

                ResponsiveUI.UnFixBodyScroll();
            });


            if (mobileNavigation && mobileNavigation.isOpen()) { //mobile menu solda açıksa
                mobileNavigation.close();
                mobileNavigation.on('close', function (e) {
                    mobileNavigation.off("close");
                    $(core.Popup.Settings.modalSelector).modal(modalOptions);
                });
            } else {
                if (core.Popup.Settings.allowCalculateHeight) {
                    //bugnet.nesine.int/Issues/IssueDetail.aspx?id=18860 maddesi için kapandı
                    this.ResizePopup();
                    $(window).on("resize", function () {
                        if (!$(core.Popup.Settings.modalSelector).data("keep-height")) {
                            core.Popup.ResizePopup();
                        }
                    });
                }

                ResponsiveUI.HideAllPopovers();
                ResponsiveUI.FixBodyScroll(core.Popup.Settings.modalSelector);
                $(core.Popup.Settings.modalSelector).modal(modalOptions);
            }

            core.Popup.Settings.open = true;
            core.UnBlock();
        },

        ResizePopup: function () {
            var maxHeight = $(window).height() - ($(window).height() > $(window).width() ? 120 : 160);
            $(".modal-body-touch").css("max-height", maxHeight);
        },

        CloseOldModal: function () {
            if (core.Popup.Settings.modalSelector != 'undefined') {
                $(core.Popup.Settings.modalSelector).removeClass("fade").modal("hide");
                $(core.Popup.Settings.modalSelector).html();
            }
        },

        GetHtml: function (url) {
            var html;

            $.ajax({
                cache: false,
                url: url,
                type: "GET",
                async: false,
                success: function (data) {
                    html = data;
                },
                error: function (data) {
                    html = "";
                }
            });

            return html;
        },

        RenderContent: function () {
            var selector;

            if (core.Popup.Settings.modalContentSelector) {
                selector = $(core.Popup.Settings.modalSelector).find(core.Popup.Settings.modalContentSelector);
            } else {
                selector = $(core.Popup.Settings.modalSelector);
            }

            if (core.Popup.Settings.content) {
                if (core.Popup.Settings.inline) {
                    core.Popup.Settings.modalSelector = core.Popup.Settings.content;
                } else {
                    selector.html(core.Popup.Settings.content);
                }
            } else {
                if (core.Popup.IsSelector(core.Popup.Settings.href)) {
                    if (core.Popup.Settings.inline) {
                        core.Popup.Settings.modalSelector = core.Popup.Settings.href;
                    } else {
                        selector.html($(core.Popup.Settings.href).clone());
                    }
                } else {
                    var html = core.Popup.GetHtml(core.Popup.Settings.href);
                    if (core.Popup.Settings.inline) {
                        core.Popup.Settings.modalSelector = html;
                    } else {
                        selector.html(html);
                    }
                }
            }

            //if (!core.Popup.Settings.inline && $(core.Popup.Settings.modalSelector).find(core.Popup.TitleSelector)) { title atanıp inline true yapıldığında title atanmasına izin vermediğinden dolayı comment'lendi.
            if (core.Popup.Settings.title != "") {
                if ($(core.Popup.Settings.modalSelector).find(core.Popup.TitleSelector)) {
                    $(core.Popup.Settings.modalSelector).find(core.Popup.TitleSelector).html(core.Popup.Settings.title);
                }
            }
            else if (core.Popup.Settings.title == "" && !core.Popup.Settings.hasTitle) {
                if ($(core.Popup.Settings.modalSelector).find("h3") && $(core.Popup.Settings.modalSelector).find(core.Popup.TitleSelector)) {
                    $(core.Popup.Settings.modalSelector).find("h3").attr("style", "color: transparent;");
                    $(core.Popup.Settings.modalSelector).find(core.Popup.TitleSelector).html("Nesine Popup");
                }
            }

            if (core.Popup.Settings.scrolling) {
                selector.css({ overflow: 'auto' });
            }
        },

        Close: function () {
            if (core.Popup.Settings.modalSelector != 'undefined') {
                $(core.Popup.Settings.modalSelector).modal("hide");
            }

            //if (core.Popup.Settings.onClose) {
            //    core.Popup.Settings.onClose();
            //}

            core.Popup.Settings.open = false;
        },

        IsSelector: function (selector) {
            try {
                $(selector);
            } catch (x) {
                return false;
            }
            return true;
        }
    };

    core.MessageType = {
        Error: 0,
        Warning: 1,
        Ok: 2,
        Information: 3
    };

    core.GetMessageClassAndTitle = function (messageType) {


        switch (messageType) {
            case core.MessageType.Error:
                return { ClassName: "alert-danger", Title: "Hata" };
            case core.MessageType.Information:
                return { ClassName: "alert-info", Title: "Bilgi" };
            case core.MessageType.Ok:
                return { ClassName: "alert-success", Title: "Onay" };
            case core.MessageType.Warning:
                return { ClassName: "alert-warning", Title: "Uyarı" };
        }

        return { ClassName: "alert-info", Title: "Bilgi" };

    };

    core.MessageBox = {

        Settings: {},

        //options
        //type :  core.MessageType nesnesini kullanılır. örnk : core.MessageType.Error default değeri core.MessageType.Information
        //content : içerik. html olabilir.
        //title : başlık, eğer herhangi bir value set edilmez ise, core.MessageType na göre default değerler basılır.
        //modalSelector : modal formun selectoru. default olarak bir modal formu vardır.
        //modalContentSelector : modal formu içindeki content alanı selectoru. default olarak bir boş bir content html i vardır.
        //onComplete : content gösterildikten sonra çalıştırılır default: false
        //onClose : popup kapatıldıktan sonra çalıştırılır : default : false
        Show: function (options) {

            var defaults = {
                type: 3,
                content: "",
                title: "",
                modalSelector: "#mod-alert",
                modalContentSelector: ".modal-body",
                onClose: false,
                onComplete: false,
                backdrop: true,
                allowCalculateHeight: true
            };

            core.MessageBox.Settings = $.extend(defaults, options);

            $(core.MessageBox.Settings.modalSelector).find(core.MessageBox.Settings.modalContentSelector).removeClass("alert-success").removeClass("alert-info").removeClass("alert-warning").removeClass("alert-danger");

            var templateAlert = $('<div class="alert" role="alert"></div>');  //$("#modal-template").find(".alert").clone();

            var classTitle = Core.GetMessageClassAndTitle(core.MessageBox.Settings.type);

            templateAlert.addClass(classTitle.ClassName);

            core.MessageBox.Settings.title = classTitle.Title;

            templateAlert.html(core.MessageBox.Settings.content);

            //ResponsiveUI.HideAllPopovers();

            core.Popup.Show({
                title: core.MessageBox.Settings.title, content: templateAlert, modalSelector: core.MessageBox.Settings.modalSelector,
                modalContentSelector: core.MessageBox.Settings.modalContentSelector, onClose: core.MessageBox.Settings.onClose,
                onComplete: core.MessageBox.Settings.onComplete,
                allowCalculateHeight: core.MessageBox.Settings.allowCalculateHeight
            });
        },

        //AjaxResponse nesnesinden otomatik olarak mesajbox gösterir, AjaxResponse nesnesinin kendisi verilir.
        //Birden fazla mesaj olabileceğinden title da en son eklenen mesajın tipini gösterir.
        ShowAjaxResponse: function (ajaxResponse) {
            if (ajaxResponse != null && ajaxResponse.Messsages != null && ajaxResponse.Messsages.length > 0) {

                var defaults = {
                    type: 3,
                    content: "",
                    title: "",
                    modalSelector: "#mod-alert",
                    modalContentSelector: ".modal-body",
                    backdrop: true,
                    allowCalculateHeight: true
                };

                core.MessageBox.Settings = $.extend(defaults, {});
                var content = [];

                $.each(ajaxResponse.Messsages, function (index, value) {

                    var templateAlert = '<div class="alert" role="alert"></div>';

                    $(core.MessageBox.Settings.modalSelector).find(core.MessageBox.Settings.modalContentSelector).removeClass("alert-success").removeClass("alert-info").removeClass("alert-warning").removeClass("alert-danger");

                    switch (value.Type) {
                        case 0:
                            templateAlert.addClass("alert-danger");
                            core.MessageBox.Settings.title = core.MessageBox.Settings.title ? core.MessageBox.Settings.title : "Hata";
                            break;
                        case 1:
                            templateAlert.addClass("alert-warning");
                            core.MessageBox.Settings.title = core.MessageBox.Settings.title ? core.MessageBox.Settings.title : "Uyarı";
                            break;
                        case 2:
                            templateAlert.addClass("alert-success");
                            core.MessageBox.Settings.title = core.MessageBox.Settings.title ? core.MessageBox.Settings.title : "Onay";
                            break;
                        case 3:
                            templateAlert.addClass("alert-info");
                            core.MessageBox.Settings.title = core.MessageBox.Settings.title ? core.MessageBox.Settings.title : "Bilgi";
                            break;
                    }

                    templateAlert.html(value.Message);

                    content.push(templateAlert);
                });
                //if (!ResponsiveUI.ScreenInfo.IsDesktop()) {
                //    core.Popup.Settings.backdrop = "static"; //backgrounda basilinca kapanmasini onler
                //    $(core.Popup.Settings.modalSelector + " .modal-body").addClass("modal-body-touch");
                //}

                core.Popup.Show({
                    title: core.MessageBox.Settings.title,
                    content: content,
                    modalSelector: core.MessageBox.Settings.modalSelector,
                    modalContentSelector: ".modal-body",
                    backdrop: core.Popup.Settings.backdrop
                });
            }
        }
    };

    core.InlineMesageBox = {

        Show: function (element, messageType, message, dissmissEnable) {

            var classTitle = Core.GetMessageClassAndTitle(messageType);

            var messageHTML = TemplateManager.GetGeneratedHTML({
                ClassName: classTitle.ClassName,
                DismissEnable: dissmissEnable,
                Message: message
            }, "InlineMessageTemplate");

            $(element).html(messageHTML);
        }
    };

    core.Global = {
        GlobalAjaxCompleteEvent: function () {
            $(document).ajaxComplete(function (a, b, c, d) {
                Nesine.LoginWrapper.BindLoginReqLinks();
            });
        }
    };

    core.Init = function () {
        core.Global.GlobalAjaxCompleteEvent();
    };

    return core;

})();

var CacheManager = (function () {

    var exports = {};

    var Cache = {};

    exports.Set = function (cacheKey, obj) {
        Cache[cacheKey] = obj;
    };

    exports.Get = function (cacheKey, cbFN) {

        var cachedObject = Cache[cacheKey];

        if (!cachedObject && cbFN) {
            cachedObject = cbFN.apply();
            this.Set(cacheKey, cachedObject);
        }

        return cachedObject;
    };

    return exports;

})();

var Site = (function () {
    function prepareSearchBoxModal() {
        $("#searchObligorModal").on("shown.bs.modal", function () {
            $("#searchObligorModal #searchTerm").focus();
        });
    }

    function createUiSlider() {
        var e = document.getElementById("noUiSliderWitchScoreSearch");
        noUiSlider.create(e, {
            start: [0, 100],
            connect: true,
            step: 1,
            //tooltips: [true, wNumb({ decimals: 1 })],
            format: wNumb({
                decimals: 0
            }),
            range: {
                min: 0,

                max: 100
            }
        });
        for (var n = e.getElementsByClassName("noUi-handle"), t = [], i = 0; i < n.length; i++)
            t[i] = document.createElement("div"),
                n[i].appendChild(t[i]);
        t[1].className += "noUi-tooltip",
            t[1].innerHTML = "<span></span>",
            t[1] = t[1].getElementsByTagName("span")[0],
            t[0].className += "noUi-tooltip",
            t[0].innerHTML = "<span></span>",
            t[0] = t[0].getElementsByTagName("span")[0],
            e.noUiSlider.on("update", function (e, n) {
                t[n].innerHTML = e[n];
                $("#searchObligorModal #searchTerm").val(e);
            });
    }

    function selectCurrentOnSideBar() {
        var pathName = window.location.pathname;
        pathName = changePathName(pathName);
        if (pathName === "/") {
            $(".nav-item.main.search").addClass("active").addClass("open");
            $(".nav-item.main.search").find(".arrow").addClass("open");
        } else {
            $("a[href='" + pathName + "']").closest(".nav-item").addClass("active");
            $(".nav-item.main").removeClass("active").removeClass("open");
            $("span.arrow").removeClass("open");
            var parent = $("a[href='" + pathName + "']").closest(".nav-item.main");
            parent.addClass("active");
            parent.addClass("open");
            parent.find(".arrow").addClass("open");
        }
    };

    function changePathName(pathName) {
        var approvementScreen = [
            "/Approvement/WaitingSalaryDistraint", "/Approvement/ChangeSpecialExecution",
            "/Approvement/ChangePaymentOwner", "/Approvement/WaitingSalaryDistraint"
        ];
        var searchScreen = ["/Obligor/SearchObligorList","/Obligor/Detail" ];
        if (approvementScreen.indexOf(pathName) !== -1) {
            return "/Approvement/MultipleLoadsIndex";
        } else if (searchScreen.indexOf(pathName) !== -1) {
            return "/";
        } else {
            return pathName;
        }
    }

    function init() {
        Notification.GetNotification();
        $(document).ajaxError(function (e, xhr) {
            if (xhr.status === 405) {
                //window.location = "/Account/Login?r=" + encodeURIComponent(window.location.href);
                window.location = "/Account/Login";
            }
        });

        //$(function () {
        //    setInterval(function () {
        //        Notification.GetNotification();
        //    }, 60000);
        //});

        prepareSearchBoxModal();
        $(":input").inputmask();

        //Change if you want, default validate messages
        jQuery.extend(jQuery.validator.messages, {
            required: "Bu alan zorunludur.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
            minlength: jQuery.validator.format("Please enter at least {0} characters."),
            rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
            range: jQuery.validator.format("Please enter a value between {0} and {1}."),
            max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
            min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
        });

        createUiSlider();

        Obligor.Init();

        selectCurrentOnSideBar();
    };

    return {
        Init: init
    }

})();

var ServerClock = (function () {

    var serverTime = null;
    var TimeDiff = null;
    var d = new Date();
    var c = null;
    var serveroffset = null;
    var utcDiff = null;

    function StartDateTime() {
        var cTime = new Date();
        cTime.setSeconds(cTime.getSeconds() + TimeDiff);
        var day = checkTime(cTime.getDate());
        var month = checkTime(cTime.getMonth() + 1);
        var year = cTime.getFullYear();
        var h = checkTime(cTime.getHours());
        var m = checkTime(cTime.getMinutes());
        var s = checkTime(cTime.getSeconds());

        $('.server-date').html(day + "." + month + "." + year);
        $('.server-hour').html(h + ":" + m + ":" + s);
        serverTime = new Date(year, month - 1, day, h, m, s);
        SetTime();
    }
    function SetTime() {
        setTimeout(function () {
            StartDateTime();
        }, 500);
    }
    function checkTime(i) {
        return (i < 10) ? ("0" + i) : i;
    }
    return {
        Init: function (serverEpoch, serverZoneOffset) {

            TimeDiff = (new Date(serverEpoch).getTime() - new Date().getTime()) / 1000;
            d = new Date();
            c = d.getTimezoneOffset() * -1;
            serveroffset = serverZoneOffset;
            utcDiff = c - serveroffset;
            TimeDiff = TimeDiff - (utcDiff * 60);
            StartDateTime();
        },
        GetServerTime: function () {
            return serverTime;
        },
        GetServerEpochDate: function () {
            var c = (new Date()).getTimezoneOffset();
            return (serverTime.getTime() - (c * 60000)) - (serveroffset * 60000);
        }
    };
})();

var AuthUIProcess = (function () {
    var ui = {};
    function BindRemindKeyups() {
        $(".lgpass").keyup(function () {
            $("#" + $(this).data("remind")).toggle(this.value == "");
        });
    };

    function BindLoginBtnClick() {
        //$('.register-loader').show();
        //$('.register-loader').hide();
        $(".lgbtn").click(function () {
            AuthenticateForm(this);
        });
    };

    function BindAuthenticateKeyPress() {
        $(".lgusername,.lgpass").keypress(function (e) {
            if ((e.keyCode && e.keyCode == 13) || (e.which && e.which == 13)) {
                AuthenticateForm(this);
            }
        });
    };

    function AuthenticateForm(sender) {
        var loginForm = $(sender).parents(".lgnform");
        var lgpass = loginForm.find(".lgpass").val();
        var lgusername = loginForm.find(".lgusername").val();
        var lgforgot = loginForm.find(".lgforgot");

        CheckNullOrEmpty(loginForm);

        if (!lgusername || !lgpass) {
            return;
        }

        BlockUI(sender);
        Nesine.Instances.Membership.Authenticate(lgusername, lgpass, "1", sender, function () {
            lgforgot.show();
            if (ResponsiveUI.ScreenInfo.IsDesktop()) {
                $("#brand").focus();
            }
            UnBlockUI(sender);
        });
    };

    function CheckNullOrEmpty(sender) {
        var targetObjLst = $(sender).find(".chckEmpty");
        if (!targetObjLst) {
            return;
        }

        targetObjLst.each(function (index, item) {
            var jObj = $(item);
            if (!jObj.val()) {
                if (jObj.parents().hasClass("input-group")) {
                    if (!jObj.parents(".input-group").hasClass("has-error")) {
                        jObj.parents(".input-group").addClass("has-error");
                    }
                }
                else {
                    if (!jObj.parent().hasClass("has-error")) {
                        jObj.parent().addClass("has-error candelete");
                    }
                }
                jObj.focus();
            }
            else {
                if (jObj.parents().hasClass("input-group")) {
                    jObj.parents(".input-group").removeClass("has-error");
                }
                else {
                    if (jObj.parent().hasClass("candelete")) {
                        jObj.parent().removeClass("has-error candelete");
                    }
                }
            }
        });
    }

    function CheckRemindStatus() {
        if (IsRemember()) {
            $(".lgremind").iCheck("check");

            var value = Nesine.Utilities.GetCookie("NSNLoginInfo");
            if (value !== null) {
                value = decodeURI(value);
                value = unescape(value);
                $(".lgusername").val(value);
            }
            if (!ResponsiveUI.isMobile) {
                $(".lgpass").focus();
            }
        } else {
            $(".lgremind").iCheck("uncheck");
        }
    }

    function ToggleAllRememberCheckboxes() {
        var allReminds = $('.lgremind');
        allReminds.on('ifChecked', function (event) {
            allReminds.iCheck('check');
        }).on('ifUnchecked', function (event) {
            allReminds.iCheck('uncheck');
        });
    };

    function BlockUI(sender) {
        $(sender).parents(".blockElement").block();
    }

    function UnBlockUI(sender) {
        $(sender).parents(".blockElement").unblock();
    }

    function IsRemember() {
        var _cookie = document.cookie;
        return _cookie != null && _cookie.indexOf("NSNRemember") > -1;
    }

    ui.Init = function () {
        BindRemindKeyups();
        BindLoginBtnClick();
        BindAuthenticateKeyPress();
        CheckRemindStatus();
        ToggleAllRememberCheckboxes();
    }

    ui.IsRemember = IsRemember;
    return ui;

})();

var Utility = (function () {
    var dropDownList = (function () {
        function fillItems(jsonDataSource, elementId, value, text, firstRowText, func) {
            if (jsonDataSource) {

                var element = $("#" + elementId);

                element.find("option").remove();

                element.prop("disabled", false);

                if (firstRowText) {
                    element.append($("<option></option>").attr("value", "").text(firstRowText));
                }

                $.each(jsonDataSource, function () {
                    element.append($("<option></option>").attr("value", this[value]).text(this[text]));
                });

                if (func) {
                    func.call();
                }
            }
        }
        return {
            FillItems: fillItems
        };
    })();

    function updateModal(tableId, url, cbFunc) {
        var id = $("#" + tableId + " tbody tr.active").data("id");
        $(".page-content").block();
        Core.AjaxCall({
            url: url,
            data: JSON.stringify({ id: id }),
            dataType: "html",
            success: function (data) {
                $(".page-content").unblock();
                $("#modalTemplate").html(data);
                $('#modalTemplate .modal').modal('show');
                if (cbFunc) {
                    cbFunc.call();
                }
            }
        });
    }

    function insertModal(url, cbFunc) {
        $(".page-content").block();
        Core.AjaxCall({
            url: url,
            dataType: "html",
            success: function (data) {
                $(".page-content").unblock();
                $("#modalTemplate").html(data);
                $('#modalTemplate .modal').modal('show');
                if (cbFunc) {
                    cbFunc.call();
                }
            }
        });
    }

    function deleteModal(tableId, url, cbFunc) {
        var id = $("#" + tableId + " tbody tr.active").data("id");
        $(".page-content").block();
        Core.AjaxCall({
            url: url,
            data: JSON.stringify({ id: id }),
            dataType: "html",
            success: function () {
                $(".page-content").unblock();
                $("#" + tableId + " tr[data-id=" + id + "]").remove();
                $('#modalConfirm .modal').modal('hide');
                if (cbFunc) {
                    cbFunc.call();
                }
            }
        });
    }

    function bindTableRowActiveEvents(tableId) {
        var trList = $("#" + tableId + " tbody tr");
        trList.each(function () {
            $(this).on("click", function () {
                var hasActiveClass = $(this).hasClass("active");
                trList.removeClass("active");
                if (hasActiveClass) {
                    $(this).removeClass("active");
                } else {
                    $(this).addClass("active");
                }
                var hasSelectedRows = $("#" + tableId + "  tbody tr.active").length > 0;
                if (hasSelectedRows) {
                    $("#btnUpdate, #btnDelete").removeAttr("disabled");
                } else {
                    $("#btnUpdate, #btnDelete").attr("disabled", "disabled");
                }
            });

        });
    }

    function confirmBox(tableId, content, url, cbFunc) {
        $("#modalConfirm #content").html(content);
        $('#modalConfirm .modal').modal("show");
        $("#modalConfirm #btnConfirm").on("click", function () {
            deleteModal(tableId, url, cbFunc);
        });
    }

    function formatDate(date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!

        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = "0" + dd;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        return dd + "-" + mm + "-" + yyyy;
    }

    function populateSelect(selector, arr, val, txt) {
        $(selector).find('option').remove();
        $(arr).each(function () {
            $(selector).append('<option value="' + this[val] + '">' + this[txt] + '</option>');
        });
    }

    function getQueryString(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        var hash = window.location.hash;
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent((results[1] + hash).replace(/\+/g, " "));
        }
    }

    function countChar(obj, count) {
        var len = obj.value.length;
        if (len >= count) {
            obj.value = obj.value.substring(0, count);
        }
        //else {
        //    $(obj).text(count - len);
        //}
    }

    function netToJsDate(date){
        var re = /-?\d+/; 
        var m = re.exec(date); 
        return new Date(parseInt(m[0]));
    }

    function roundFloat(num) {
        return Math.round(num * 100) / 100;
    }

    return {
        DropDownList: dropDownList,
        UpdateModal: updateModal,
        InsertModal: insertModal,
        DeleteModal: deleteModal,
        ConfirmBox: confirmBox,
        BindTableRowActiveEvents: bindTableRowActiveEvents,
        FormatDate: formatDate,
        PopulateSelect: populateSelect,
        GetQueryString: getQueryString,
        CountChar: countChar,
        NetToJsDate: netToJsDate,
        RoundFloat: roundFloat,
        Regex: {
            IsNumberic: new RegExp("^\\d+$"),
            IsLetter: new RegExp("^\\s+$"),
            HasNumberAndLetter: new RegExp(/(?=.*\d)(?=.*\D).*/),
            IsEMailValid: new RegExp(/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$/),
        },
        FilterForDecimalInputs: function (event) {
            var keyCode = ("which" in event) ? event.which : event.keyCode;

            var isNumeric = (keyCode >= 48 /* KeyboardEvent.DOM_VK_0 */ && keyCode <= 57 /* KeyboardEvent.DOM_VK_9 */) ||
                //(keyCode >= 96 /* KeyboardEvent.DOM_VK_NUMPAD0 */ && keyCode <= 105 /* KeyboardEvent.DOM_VK_NUMPAD9 */) || //kucuk a harfine basildigin 97 verdiginden dolayi commentlendi.
                (keyCode === 127 /* Delete */) ||
                (keyCode === 8 /* backspace*/) ||
                (keyCode === 44 /* virgul */) ||
                (keyCode === 46 /* nokta */);

            var modifiers = (event.altKey || event.ctrlKey || event.shiftKey);
            return isNumeric || modifiers;
        }
    };
})();

var Form = (function () {
    function post(url) {
        $.ajax({
            cache: false,
            url: url,
            async: false,
            method: "post"
        });
    }
    return {
        Post: post
    };
})();

var Message = (function () {
    var themeType = {
        Teal: "teal",
        Amethyst: "amethyst",
        Ruby: "ruby",
        Tangerine: "tangerine",
        Lemon: "lemon",
        Lime: "lime",
        Ebony: "ebony",
        Smoke: "smoke"

    }
    var heading = "";
    var theme = "";
    var sticky = false;
    var life = 10000;
    function show(text, title, themeType, isSticky) {
        if (themeType) {
            theme = themeType;
        } else {
            theme = "teal";
        }

        if (isSticky) {
            sticky = true;
        } else {
            sticky = false;
        }

        heading = title;
        var settings = {
            theme: theme,
            sticky: sticky,
            horizontalEdge: "top",
            verticalEdge: "right"
        }
        if (heading !== "") {
            settings.heading = heading;
        }
        if (!settings.sticky) {
            settings.life = life;
        }

        $.notific8('zindex', 11500);
        $.notific8(text, settings);
    }

    return {
        Show: show,
        Theme: theme,
        Sticky: sticky,
        ThemeType: themeType,
        Life: life
    };
})();
(function ($) {

    var extensionMethods = {
        SetTemplateHtml: function (data, templateId) {
            //console.log(this[0].id);
            var generatedHtml = TemplateManager.GetGeneratedHTML(data, templateId);

            this[0].innerHTML = generatedHtml; //%50 faster than .html()
        },
        AppendTemplateHtml: function (data, templateId) {
            var generatedHTML = TemplateManager.GetGeneratedHTML(data, templateId);
            $(this[0]).append(generatedHTML);
            //this[0].innerHTML += generatedHTML; //%50 faster than .append()
        },
        ShowElement: function () { //%85 faster than show()/hide() http://jsperf.com/jquery-show-hide-vs-css-display-none-block/27
            this[0].style.display = "block";
        },
        HideElement: function () {
            this[0].style.display = "none";
        },
        ToggleElement: function (status) {
            this[0].style.display = status ? "block" : "none";
        }
    };

    $.fn.extend(extensionMethods);

})(jQuery);

var CommonDataTablesSettings = {
    language: {
        aria: {
            sortAscending: ": Baştan sona sırala",
            sortDescending: ": Sondan başa sırala"
        },
        emptyTable: "Kayıt bulunamadı",
        info: "_TOTAL_ kaydın, _START_ ile _END_ arası gösteriliyor",
        infoEmpty: "Kayıt bulunamadı",
        infoFiltered: "(_MAX_  kayıt içerisinde arandı.)",
        lengthMenu: "_MENU_ kayıt",
        zeroRecords: "Eşleşen kayıt bulunamadı",
        search: "Ara:",
        "language": {
            "decimal": ",",
            "thousands": "."
        }
    },
    buttons: [],
    responsive: !0,
    lengthMenu: [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Hepsi"]],
    pageLength: 10,
    dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

};

$.fn.dataTable.moment("D.MM.YYYY HH:mm:ss"); //tarihe gore datatable js library ile siralama yapilabilmesi icin init edilmesi gerekiyor
$.fn.dataTable.moment("D.MM.YYYY"); //tarihe gore datatable js library ile siralama yapilabilmesi icin init edilmesi gerekiyor.

