﻿//Underscore.js template yapısını kullanmayı kolaylaştıran wrapper module.
//template html'leri performans için cache'lenir, görüntülenmesi istenilen html oluşturularak yollanır.
var TemplateManager = (function () {

    var templateCache = {};

    //CAN'T TOUCH THIS! Proje içerisinde kullanılan library'lerden dolayı Underscore.js üzerinde bazı ayarlar yapıldı. Değiştirmeyiniz.
    function setEngineDefaults() {
        _.templateSettings = {
            interpolate: /\{\{(.+?)\}\}/g, // print value: {{ value_name }}
            evaluate: /\{%([\s\S]+?)%\}/g, // excute code: {% code_to_execute %}
            escape: /\{%-([\s\S]+?)%\}/g
        };
    };

    //Template'leri compile ederek cache'lemek için kullanılır ve compile edilmiş template'i yollar. Parametre olarak templateIds objesinden bir değer alır.
    var getCompiledTemplate = function (templateId) {
        if (templateCache[templateId] === undefined || templateCache[templateId] === null) {
            var tmpl = $("#" + templateId).html();
            var compiledTemplate = _.template(tmpl);
            templateCache[templateId] = compiledTemplate;
        }
        return templateCache[templateId];
    };

    //Verilen data ve templateId üzerinden görüntülenmeye hazır HTML'i döner
    var getGeneratedHTML = function (data, templateId) {
        var compiledTemplate = getCompiledTemplate(templateId);
        return compiledTemplate({ Model: data });
    };

    var isInitialized = false;

    return {
        GetGeneratedHTML: function (data, templateId) {

            if (!isInitialized) {
                isInitialized = true;
                setEngineDefaults();
            }

            return getGeneratedHTML(data, templateId);
        }
    }
})();