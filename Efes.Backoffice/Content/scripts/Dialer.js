var Dialer = (function () {
    var selectedAgents = [];
    var loadedAccounts = [];

    var DialerStatus = {
        Passive: 0,
        Active: 1,
        Paused: 2
    };

    var addExcelFile = function () {
        $("#fileupload").fileupload({
            url: "InsertExcel",
            dataType: "json",
            done: function (e, data) {
                App.unblockUI("#fileUploadDiv");
                if (data.result.Result === "Success") {
                    Dialer.LoadedAccounts = data.result.AccountIdList;
                    $("#lblAccountCount").text(Dialer.LoadedAccounts.length);
                    $(".step1").hide();
                    $(".step2").show();
                } else if (data.result.Result === "Error") {
                    if (data.result.MissingColumnNames) {
                        Message.Show(
                            "Dosyada eksik kolonlar bulunmaktadır. Hataları düzelttikten " +
                            "sonra tekrar deneyin.<br/>" +
                            data.result.MissingColumnNames,
                            "",
                            Message.ThemeType.Ruby,
                            true);
                    } else {
                        Message.Show("Dosya yüklenemedi", "Hata", Message.ThemeType.Ruby);
                    }
                }
            },
            progressall: function (e, data) {
                App.blockUI({
                    target: "#fileUploadDiv",
                    boxed: true
                });
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#progress .progress-bar").css(
                    "width",
                    progress + "%"
                );
            },
            error: function (ex) {
                App.unblockUI("#fileUploadDiv");
                console.log(ex);
                alert("Dosya yükleme esnasında hata ile karşılaşıldı!!");
            }
        }).prop("disabled", !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : "disabled");
    };

    var initMultipleSelect = function () {
        $("#multiSelectRoleRule").multiSelect({

            selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Ara'>",
            selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Ara'> ",
            afterInit: function (ms) {
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                        return true;
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                        return true;
                    });
            },

            afterSelect: function (values) {
                selectedAgents.push(values[0]);
                console.log(selectedAgents);
            },
            afterDeselect: function (values) {
                selectedAgents.splice(selectedAgents[values[0]], 1);
                console.log(selectedAgents);
            }
        });

        $(".ms-selectable").prepend("<div class='bg-blue-chambray bg-font-blue-chambray' style='padding: 5px;'> Bütün Agentlar </div>");
        $(".ms-selection").prepend("<div class='bg-blue-chambray bg-font-blue-chambray' style='padding: 5px;'> Arama Yapacak Agentlar </div>");
    };

    var getCurrentDialerStatus = function (cbFunc) {
        Core.AjaxCall({
            url: "/SmartCalling/GetCurrentDialerStatus",
            blockDiv: "#paymentPromiseModal",
            success: function (response) {
                if (response.Result === DialerStatus.Passive) {
                    cbFunc();
                }
                else if (response.Result === DialerStatus.Active) {
                    alert("Şu an çalışan aktif bir dialer bulunduğundan dolayı işleminiz gerçekleştirilemedi.");
                    location.reload();
                }
                else if (response.Result === DialerStatus.Paused) {
                    alert("Şu an beklemede bir dialer bulunduğundan dolayı işleminiz gerçekleştirilemedi.");
                    location.reload();
                }
            },
        });
    };

    var startDialer = function () {
        if (!selectedAgents || !selectedAgents.length ||
            !loadedAccounts || loadedAccounts.length) {
            alert("Eksik bilgi !!");
            return;
        }

        getCurrentDialerStatus(sendDataForStartingDialer);
    };

    var sendDataForStartingDialer = function () {
        // $(".portlet-body").block({message: ""});
        var max = $("#range_1").val();
        var band = $("#range_2").val();

        Core.AjaxCall({
            url: "/SmartCalling/StartDialer",
            data: JSON.stringify(
                {
                    selectedAgents: selectedAgents,
                    loadedAccounts: Dialer.LoadedAccounts,
                    maxWaitingTime: max,
                    bandwidth: band,
                    useFct: $("#chcUseFct").prop("checked"),
                    dynamicRouting: $("#chcDynamicRouting").prop("checked")
                }),
            dataType: "json",
            blockDiv: ".portlet-body",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                // $(".portlet-body").unblock();
                var result = "";
                var cls = "";
                App.unblockUI(".form-group");
                $(".step2").hide();
                $(".step3").show();
                if (data.Result === "Success") {
                    result = "Dialer başlatıldı.";
                    cls = "alert-success";
                } else if (data.Result === "Error") {
                    result = "Sorun oluştu.";
                    cls = "alert-danger";
                }
                $("#divResult").html(result).addClass(cls);
            }
        });

        setTimeout(function(){
            location.href = "/SmartCalling/DialerList";
        }, 2000);
    };

    var initSliders = function () {
        $("#range_1").ionRangeSlider({
            min: 0,
            max: 120,
            from: 30,
            postfix: " sn."
        });

        $("#range_2").ionRangeSlider({
            min: 0,
            max: 100,
            from: 60,
            postfix: " Mhz."
        });
    };

    var init = function () {
        addExcelFile();
        initMultipleSelect();
        initSliders();
    };

    return {
        SelectedAgents: selectedAgents,
        LoadedAccounts: loadedAccounts,
        Init: init,
        StartDialer: startDialer,
        GetCurrentDialerStatus: getCurrentDialerStatus
    };
})();

var DialerList = (function () {
    var interval = null;
    var intervalValue = 5000;
    var dataList = [];

    var getData = function () {
        $("#dialerCallListTable").block({ message: "" })
        Core.AjaxCall({
            url: "/SmartCalling/GetDialerCallList",
            blockDiv: "#paymentPromiseModal",
            success: function (response) {
                editTableRow(response.Result.DialerCallList);
                editDetail(response.Result.DialerDetail);
                $("#dialerCallListTable").unblock();

                console.log("guncelledim");
            },
        });
    };

    var editTableRow = function (data) {
        $("#tBodyDialerList").SetTemplateHtml(data, "dialerListTemplate");
    };

    var editDetail = function (data) {
        $("#tDialerListDetail").SetTemplateHtml(data, "dialerListDetail");
    };

    var startSync = function () {
        interval = setInterval(getData, intervalValue);
    };

    var changeStatus = function (status, recId) {
        Core.AjaxCall({
            url: "/SmartCalling/ChangeDialerCallStatus",
            data: JSON.stringify(
                {
                    status: status,
                    recId: recId
                }),
            success: function (response) {
                if (response.IsSuccess) {
                    location.reload();
                } else {
                    alert("Dialer durum değişkliği gerçekleştirilemedi !!");
                }
            }
        });
    };

    var init = function () {
        startSync();
    };

    return {
        Init: init,
        ChangeStatus: changeStatus
    };
})();