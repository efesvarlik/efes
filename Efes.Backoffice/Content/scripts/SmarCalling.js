﻿
var SmartCalling = (function () {


    var updateAllCheckBoxes = function (state) {
        $("[data-type='phone']").prop("checked", state);
        $.uniform.update();
    };

    var bindEvents = function () {
        $("#chcSelectAll").on("change", function () {
            updateAllCheckBoxes($(this).is(":checked"));
        });
    };

    var callAll = function () {

        var checked = $("[data-type='phone']:checked");
        if (checked.length > 0) {
            var callList = [];
            checked.each(function () {
                callList.push({ Name: $(this).data("name"), Phone: $(this).data("phone"), Cpn: $("#ddlFakePhoneList").val(), AccountId: $(this).data("accountid") });
            });

            Core.AjaxCall({
                url: "/SmartCalling/CallWithList",
                data: JSON.stringify({ callList: JSON.stringify(callList) }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response.isSuccess) {
                        Message.Show("Aramalar yapılıyor", "Bilgilendirme", Message.ThemeType.Teal);
                    } else {
                        Message.Show("Arama yapılamadı.", "Bilgilendirme", Message.ThemeType.Ruby);
                    }
                }
            });
        } else {
            alert("Aramak için kişi seçmeniz gerekmektedir.");
        }
    };

    var callOne = function (number, fakeNumber, customerName, accountId) {
        var callList = [{ Name: customerName, Phone: number, Cpn: fakeNumber, AccountId: accountId }];
        Core.AjaxCall({
            url: "/SmartCalling/CallWithList",
            data: JSON.stringify({ callList: JSON.stringify(callList) }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response.isSuccess) {
                    Message.Show("Aramalar yapılıyor", "Bilgilendirme", Message.ThemeType.Teal);
                } else {
                    Message.Show("Arama yapılamadı.", "Bilgilendirme", Message.ThemeType.Ruby);
                }
            }
        });
    };

    var init = function () {
        bindEvents();
        $(".tablePager").dataTable($.extend(CommonDataTablesSettings, { "order": [[0, "desc"]] }));
        
    };


    return {
        Init: init,
        CallAll: callAll,
        CallOne: callOne
    }
})();