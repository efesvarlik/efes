﻿var Obligor = (function () {
    var currentSearchType;

    var type = {
        NameAndSurname: 0,
        TCKN: 1,
        PhoneNumber: 2,
        BankAccountNumber: 3,
        ProtocolNumber: 4,
        ExecutionFileNumber: 5,
        CollectionWage: 6,
        AccountId: 7,
        Score: 8
    }

    var typeName = {
        0: "Ad ve/veya Soyad ile arama",
        1: "TC Kimlik No ile arama",
        2: "Telefon Numarası ile arama",
        3: "Banka Müşteri Numarası ile arama",
        4: "Protokol Numarası ile arama",
        5: "İcra Dosya Numarası ile arama",
        6: "Tahsilat Yevmiye ile arama",
        7: "Account Id ile arama",
        8: "Skor ile arama"
    }

    var prepareModal = function (id) {

        var searchTermInput = $("#searchObligorModal #searchTerm");
        var slider = $("#noUiSliderWitchScoreSearch");

        searchTermInput.data("search-type-id", id);

        if (id === 8) {
            var e = document.getElementById("noUiSliderWitchScoreSearch");
            e.noUiSlider.set([0, 100]);

            searchTermInput.hide();
            slider.show();
        } else {
            searchTermInput.val("");
            searchTermInput.show();
            slider.hide();
        }

        var tn = typeName[id];
        $("#searchObligorModal #title").html(tn);
    }

    var searchModal = function (searchTypeId) {
        prepareModal(searchTypeId);
        $("#searchObligorModal").modal("show");
        $("#searchObligorModal #searchTypeId").val(searchTypeId);

        currentSearchType = searchTypeId;
    }

    var getUsersForAccountOwnership = function (cbFunc) {

        Core.AjaxCall({
            url: "/Obligor/GetUsersForAccountOwnership",
            // data: JSON.stringify({ tabId: tabId, obligorId: ObligorId }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (cbFunc) {
                    cbFunc(response);
                }
            }
        });
    };

    var onSuccessFunc = null;
    var changeAccountOwner = function () {
        var data = getChangeOwnerModalValues();
        changeAccountOwnerRequest(data.accountId, data.userId, data.loginname);
    }

    var getChangeOwnerModalValues = function () {
        return {
            accountId: $("#changeAccountOwnerModal #accountId").val(),
            userId: $("#ddlUserId").val(),
            loginname: $("#ddlUserId :selected").data("login-name"),
            owner: $("#ddlUserId :selected").text(),
        }
    }

    var changeAccountOwnerRequest = function (accountId, userId, loginname) {
        Core.AjaxCall({
            url: "/Obligor/ChangeAccountOwner",
            data: JSON.stringify({ accountId: accountId, userId: userId }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response) {
                    if (response.IsSuccess) {
                        if (onSuccessFunc) {
                            onSuccessFunc(accountId, loginname);
                        }
                        Message.Show("Dosya sahibi başarıyla değiştirildi", "Bilgilendirme", Message.ThemeType.Teal);
                        return;
                    }
                }
                Message.Show("Dosya sahibi değiştirilemedi.", "Hata", Message.ThemeType.Ruby);
            },
            complete: function (result) {
                $("#changeAccountOwnerModal").modal("hide");
            }
        });
    }

    var confirmChangePaymentOwner = function (opt) {
        var tr = $("#tblList_11 tr[data-id='" + opt.RecId + "']");
        var data = $.extend(true, {
            AccountId: tr.data("account-id"),
            PaymentDate: tr.data("payment-date"),
            OldOwner: tr.data("file-owner"),
            PaymentValue: tr.data("value"),
        }, opt);

        data.PaymentDate = moment(data.PaymentDate, 'DD/MM/YYYY HH:mm:ss').format("DD.MM.YYYY");

        var content = TemplateManager.GetGeneratedHTML(
            data, "tmpChangeAccountOwnerDesc");
        $("#cnfrmChangeAccountOwner [data-id='content']").html(content);
        $("#cnfrmChangeAccountOwner").modal("show");
        $("#cnfrmChangeAccountOwner [data-id='btnSubmit']")
            .off('click')
            .on('click', function () {
                Core.AjaxCall({
                    url: "/Obligor/ChangePaymentOwner",
                    data: JSON.stringify(
                        {
                            accountId: data.AccountId,
                            newOwnerId: data.NewOwnerId,
                            recId: data.RecId
                        }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        if (response) {
                            if (response.IsSuccess) {
                                if (onSuccessFunc) {
                                    onSuccessFunc(accountId, loginname);
                                }
                                Message.Show("Ödeme sahibi değiştirme isteği başarıyla gönderildi", "Bilgilendirme", Message.ThemeType.Lime);
                                // ObligorDetail.LoadData(ObligorTab.AllPayments, null);
                            } else {
                                Message.Show("Ödeme sahibi değiştirme isteği gönderilemedi.", "Hata", Message.ThemeType.Ruby);
                            }
                        }
                        else {
                            Message.Show("Ödeme sahibi değiştirme isteği gönderilemedi.", "Hata", Message.ThemeType.Ruby);
                        }
                        $("#cnfrmChangeAccountOwner").modal("hide");
                    }
                });
            });
    };

    var openAccountOwnerChangeModal = function (accountId, onSuccess, recId) {
        onSuccessFunc = onSuccess;
        var btnChange = $("#changeAccountOwnerModal [data-id='btnChange']")
        $("#changeAccountOwnerModal #accountId").val(accountId);
        $("#changeAccountOwnerModal").modal("show");

        var callbackFunc = null;

        if (recId) {
            $("#changeAccountOwnerModal").data("row-id", recId);
            $("#changeAccountOwnerModal .modal-title").html("Ödeme Sahibi Değiştirme");
            callbackFunc = function () {
                var opt = {
                    RecId: recId,
                    NewOwner: $("#ddlUserId :selected").text(),
                    NewOwnerLoginName: $("#ddlUserId :selected").data("login-name"),
                    NewOwnerId: $("#ddlUserId :selected").val(),
                };
                $("#changeAccountOwnerModal").modal("hide");
                confirmChangePaymentOwner(opt)
            };
        } else {
            $("#changeAccountOwnerModal .modal-title").html("Dosya Sahibi Değiştirme");
            callbackFunc = Obligor.ChangeAccountOwner;
        }
        btnChange.off("click").on("click", callbackFunc);
    };

    var prepareOwnerModal = function () {
        $("#changeAccountOwnerModal").on("show.bs.modal", function () {
            getUsersForAccountOwnership(function (response) {
                $("#ddlUserId").find("option").remove();

                $.each(response, function (key, value) {
                    $("#ddlUserId").append(
                        "<option value=" + value.Id +
                        " data-login-name='" + value.LoginName +
                        "'>" + value.Name + "</option>");
                });
            });
        });
    }

    var init = function (opt) {
        prepareOwnerModal();
    }

    return {
        //Call: call,
        SearchModal: searchModal,
        Type: type,
        CurrentSearchType: currentSearchType,
        ChangeAccountOwner: changeAccountOwner,
        Init: init,
        OpenAccountOwnerChangeModal: openAccountOwnerChangeModal,
        ConfirmChangePaymentOwner: confirmChangePaymentOwner
    }
})();