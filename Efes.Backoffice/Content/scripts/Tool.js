﻿var Phone = (function () {

    function changePhoneStatus(accountId, phone, status) {
        App.blockUI({
            target: '#blockPlace',
            boxed: true
        });
        Core.AjaxCall({
            url: "/Tool/ChangePhoneStatus",
            data: JSON.stringify({ accountId: accountId, phone: phone, status: status }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                App.unblockUI('#blockPlace');

                if (data.Result === 'Success') {
                    location.reload();
                }
            },
            error: function () {
                App.unblockUI('#blockPlace');
            }
        });
    }

    function passivePhoneList(phoneString, btn) {
        $(btn).button("loading");

        Core.AjaxCall({
            url: "/Tool/PassivePhoneList",
            data: JSON.stringify({ phones: phoneString }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(btn).button("reset");

                if (data.Result === "Success") {
                    Message.Show("İşlem başarıyla gerçekleşti.", "Bilgi");
                    $("#txtPhoneList").val("");
                } else if (data.Result === "Error") {
                    Message.Show("İşlemde hata oluştu.", "Hata", Message.ThemeType.Ruby);
                }
            }
        });
    }

    return {
        ChangePhoneStatus: changePhoneStatus,
        PassivePhoneList: passivePhoneList
    };
})();