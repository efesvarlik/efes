﻿var genaralAlertSentence = "Sistemde sorun oluştu, Lütfen sistem yöneticinizden yardım alınız.";
var V = (function () {

    var validate = function (form, cbFunc, rules) {
        var gv = {
            highlight: function (element) {
                $(element).closest(".form-group").addClass("has-error");
            },
            unhighlight: function (element) {
                $(element).closest(".form-group").removeClass("has-error");
            },
            errorElement: "span",
            errorClass: "help-block",
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: cbFunc
        };

        $("form[name='" + form + "']").validate($.extend(gv, rules));
    };

    return {
        Validate: validate
    };
})();

var ObligorTab = {
    DebitsInAllPortfoy: 2,
    Notlar: 3,
    SmsTarihce: 4,
    CommunicationHistory: 5,
    ProtocolHistory: 9,
    AllPayments: 11,
    Phone: 12,
    Address: 13,
    Debit: 15,
    FileAttachment: 16,
    ObligorProcessHistory: 37
};

var ObligorId = $("#obligorId").val();

var GetCallBackFunctions = function (tabId) {

    if (tabId === ObligorTab.DebitsInAllPortfoy) {
        return DebitsInAllPortfoy.TabCallBackFunc;
    } else if (tabId === ObligorTab.Notlar) {
        return Note.TabCallBackFunc;
    } else if (tabId === ObligorTab.SmsTarihce) {
        return SmsHistory.TabCallBackFunc;
    } else if (tabId === ObligorTab.CommunicationHistory) {
        return CommunicationHistory.TabCallBackFunc;
    } else if (tabId === ObligorTab.AllPayments) {
        return AllPayments.TabCallBackFunc;
    } else if (tabId === ObligorTab.Phone) {
        return Phone.TabCallBackFunc;
    } else if (tabId === ObligorTab.Address) {
        return Address.TabCallBackFunc;
    } else if (tabId === ObligorTab.ProtocolHistory) {
        return ProtocolHistory.TabCallBackFunc;
    } else if (tabId === ObligorTab.Debit) {
        return Debit.TabCallBackFunc;
    } else if (tabId === ObligorTab.FileAttachment) {
        return FileAttachment.TabCallBackFunc;
    }
};

var ObligorDetail = (function () {

    var showHiddenFieldsInCurrentScope = function (sender) {
        var button = $(sender);
        button.hide();
        button.parent(".table-scrollable").find(".canHide").show();
        button.parent(".table-scrollable").find(".hideButton").show();
    };
    var hideShownFieldsInCurrentScope = function (sender) {
        var button = $(sender);
        button.hide();
        button.parent(".table-scrollable").find(".canHide").hide();
        button.parent(".table-scrollable").find(".showButton").show();
    };
    var tabShortcutKeysEventHandler = function (key, cbFunction) {
        $("body").on("keypress", function (e) {
            if ($(".modal.in").length === 0) {
                if (key.indexOf(e.which) !== -1) {
                    cbFunction.call();
                }
            }
        });
    };
    var tabEventManager = function (tabId) {
        $("body").off("keypress");
        switch (tabId) {
            case ObligorTab.Notlar:
                //tabShortcutKeysEventHandler([78, 110], function () { $("#tab3Add").modal(); }); //N Key
                break;
            case ObligorTab.SmsTarihce:
                //tabShortcutKeysEventHandler([83, 115], function () { $("#tab4Add").modal(); }); //S Key
                break;
            case ObligorTab.Phone:
                //tabShortcutKeysEventHandler([84, 116], function () { $("#tab12Add").modal(); }); //T Key
                break;
        }
    };
    var loadData = function (tabId, cbFunc) {
        Core.AjaxCall({
            url: "/Obligor/GetTabContent",
            data: JSON.stringify({ tabId: tabId, obligorId: ObligorId }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response.isSuccess) {
                    $("#tab" + tabId).SetTemplateHtml(response.data, "tmpTab" + tabId);
                    if (cbFunc) {
                        cbFunc();
                    }

                    $(".nav-tabs").tabdrop("layout"); //tablarin yanina refresh butonu geldiginde asagi kayanlar olursa. onlari sagdaki listeye almak icin
                } else {
                    console.log(response.error);
                    $("#tab" + tabId).html(response.error);
                }
            }
        });
    };
    //var removeAccordingRule = function () {
    //    if (UserRules && UserRules.length) {

    //    }
    //};
    var onReadyProcess = function () {
        hideRows();
    };
    var hideRows = function () {
        $(".canHide").hide();
    };
    var bindTabEvents = function () {
        var tabs = $('.nav-tabs [data-toggle="tab"]');
        tabs.each(function () {
            var tab = $(this);
            var refreshButton = tab.find(".fa-refresh");
            var tabId = tab.data("id");

            //General click events for all tabs
            tab.on("click", function () {
                var tabLoaded = tab.data("loaded");
                if (!tabLoaded) {
                    ObligorDetail.LoadData(tabId, function () {

                        tab.data("loaded", true);
                        refreshButton.show();

                        var cb = GetCallBackFunctions(tabId);
                        if (cb) {
                            cb.call();
                        }
                    });
                }

                //bind shortcut key for tabs
                tabEventManager(tabId);
            });

            refreshButton.on("click", function () {
                ObligorDetail.LoadData(tabId, GetCallBackFunctions(tabId));
            });
        });
    }
    var bindWidgetEvents = function () {
    };
    var keypressHandler = function (e) {
        if (e.which === 13) {
            var buttons = $(".modal.in button[type='submit']");
            if (buttons.length > 0) {
                e.preventDefault(); //stops default action: submitting form
                $(this).blur();
                $(buttons[0]).click(); //give your submit an ID
            }
        }
    };
    var modalsOnShowing = function () {
        $("#tab4Add").on("show.bs.modal", function () {
            SmsHistory.Init();
        });
    }
    var bindModalEvents = function () {
        //Bind enter submit event all modal submit keys
        $(".modal").keypress(keypressHandler);

        //Select first editable element on shown modal
        $(".modal").on("shown.bs.modal", function () {
            if ($(".modal.in").find("input, textarea, select").length > 0) {
                $(".modal.in").find("input, textarea, select")[0].focus();
            }
        });

        //Clear all editable elements after modal hiden
        $(".modal").on("hide.bs.modal", function () {
            if ($(this).attr("id") !== "mdlPaymentPlan" &&
                $(this).attr("id") !== "paymentPromiseModal") {
                $(".modal.in").find("input, textarea").each(function () {
                    $(this).val("");
                });
                $(".modal.in").find("select").each(function () {
                    $(this).find("option:first").prop("selected", true);
                });
            }
        });

        modalsOnShowing();
    };
    var prepareVisualSettings = function () {
        $("table").addClass("table-condensed");
        $("table:not(.elleme) tr td:first-child").addClass("bg-blue-soft").addClass("bg-font-blue-soft");
    };
    var selectTabById = function (tabId) {
        if ($("a[href='#tab" + tabId + "']").length > 0) {
            $("a[href='#tab" + tabId + "']").click();
        } else {
            $("ul.nav-tabs [data-toggle='tab']")[0].click();
        }
        //Set first initiliza tab
    };
    var jqueryLowerThanFunc = function (value, element, param) {
        var $element = $(element)
            , $max;

        if (typeof (param) === "string") {
            $max = $(param);
        } else {
            $max = $("#" + $element.data("max"));
        }

        if (this.settings.onkeyup) {
            $max.off(".validate-lowerThan").on("blur.validate-lowerThan", function () {
                $element.valid();
            });
        }
        if (value.trim() === "") {
            value = 0;
        }
        return parseFloat(value) <= parseFloat($max.val());
    };
    function onSuccessForChangingAccountOwner(accountId, loginName) {
        $("#bAccountOwner").html(loginName + "  ");
    };
    var selectPhoneForCallsFromDialer = function () {
        var phoneNumber = Utility.GetQueryString("phoneNumber");
        var exist = $("#ddlPhoneList option[value='" + phoneNumber + "']").length > 0;
        if (phoneNumber && exist) {
            $("#ddlPhoneList").val(phoneNumber);
            $("#txtMainScreenNote").val($("#ddlPhoneList option:selected").text().trim() + " - ");
        }
    }
    var init = function () {
        prepareVisualSettings();
        onReadyProcess();
        bindTabEvents();
        bindWidgetEvents();
        bindModalEvents();
        if (window.selectedTab) {
            selectTabById(window.selectedTab);
        }

        Protocol.Init();

        if (!isLawyer) { //TODO: buralar acil acil denen islerden dolayi yama yapilarak gecildi, duzene sokulmasi gerekiyor.
            $.validator.addMethod("lowerThan", jqueryLowerThanFunc, "Peşinat, anaparadan fazla olamaz.");
            $.validator.addClassRules(
                "lowerThan", { lowerThan: true }
            );
        }

        V.Validate("frmCreateProtocol", null, {
            errorClass: "help-block pull-right"
        });

        Reminder.Init();

        $(".row").show();

        PaymentPromise.OnModalShown();

        selectPhoneForCallsFromDialer();

        Debit.Init();
    };

    return {
        HideShownFieldsInCurrentScope: hideShownFieldsInCurrentScope,
        Init: init,
        LoadData: loadData,
        OnSuccessForChangingAccountOwner: onSuccessForChangingAccountOwner,
        SelectTabById: selectTabById,
        ShowHiddenFieldsInCurrentScope: showHiddenFieldsInCurrentScope,
    };

})();

var Protocol = (function () {

    var savedProtocol = [];

    var save = function () {
        var model = [];
        var inputs = $(".protocolInput");
        $(inputs).each(function () {
            var currentTr = $(this).closest("tr");
            var installment = {}
            installment["Month"] = $(currentTr).data("month");
            installment["Value"] = parseFloat($(this).val() ? $(this).val() : 0);
            installment["PaymentDate"] = $(currentTr).data("payment-date");
            installment["Desc"] = $(currentTr).data("desc");

            model.push(installment);
        });

        Protocol.SavedProtocol = model;
        $("#mdlPaymentPlan").modal("hide");

    };

    var calculate = function () {
        var mainMoney = (parseFloat($("#txtProtocolValue").val())).toFixed(2);
        var advancePaymentValue = parseFloat($("#txtAdvancePayment").val());

        var inputs = $(".protocolInput");
        var total = 0.0;
        $(inputs).each(function () {
            total += parseFloat($(this).val() ? $(this).val() : 0);
        });
        total += advancePaymentValue;
        total = parseFloat(total.toFixed(2));
        mainMoney = parseFloat(mainMoney);

        if (isLawyer) {
            var agencyFee = parseFloat(parseFloat($("#txtAgencyFee").val()).toFixed(2));
            mainMoney += agencyFee;
        }

        var difference = (mainMoney - total).toFixed(2);

        //Aradaki farki text olarak hesapla
        var inf = "";
        if (difference > 0) {
            inf = "(" + difference.toString() + " TL eksik)";
        } else if (difference < 0) {
            inf = "(" + (difference * -1).toString() + " TL fazla)";
        }

        $("#mdlPaymentPlan [type=submit]").prop("disabled", inf !== ""); //eger calculate sonucunda fark varsa kaydet dugmesi calismasin

        $("#prtResult").text("Toplam: " + total + " TL " + inf);

        if (mainMoney < total || mainMoney > total) {
            $("#prtResult").removeClass("label-success").removeClass("label-danger").addClass("label-danger");
            $("#btnArrangeOnLastInst").show();
        } else if (mainMoney === total) {
            $("#prtResult").removeClass("label-success").removeClass("label-danger").addClass("label-success");
            $("#btnArrangeOnLastInst").hide();
        }

        return {
            mainMoney: mainMoney,
            total: total,
            difference: parseFloat(difference)
        };
    };

    var arrange = function () {
        var lastInstInput = $("#mppTable tr").last().find("input.protocolInput");
        var val = parseFloat(lastInstInput.val());
        var calcResult = calculate();
        val += calcResult.difference;
        lastInstInput.val(val.toFixed(2));
        calculateInstallmentTable();
    };

    var prepareProtocolModel = function () {
        var model = [];
        if (Protocol.SavedProtocol.length > 0) {
            return Protocol.SavedProtocol; //we have already predefined protocol 
        }

        var formValues = getFormValuesForProtocolInstallments();
        var protocolValue = formValues.protocolValue;
        var advancePaymentValue = formValues.advancePaymentValue;
        var installmentCount = formValues.installmentCount;
        var agencyFee = formValues.agencyFee;

        //Pesinat degeri veya protocol tutari 0 dan kucukse hata ver
        if (advancePaymentValue < 0 || protocolValue < 0) {
            Message.Show("Peşinat tutarı veya protokol tutarı 0 TL'den küçük olamaz.", "Hata", Message.ThemeType.Ruby);
            return null;
        }

        //Kullanici avukatlik burosundan degilse ve 
        if (advancePaymentValue > protocolValue && !window.isLawyer) {
            Message.Show("Peşinat tutarı, protokol tutarından büyük olamaz.", "Hata", Message.ThemeType.Ruby);
            return null;
        }

        if ((window.isLawyer || protocolValue - advancePaymentValue > 0) && installmentCount !== 0) {
            var totalInstallmentAmount = protocolValue - advancePaymentValue;
            var debit = totalInstallmentAmount;
            if (isLawyer) {
                totalInstallmentAmount = totalInstallmentAmount + agencyFee;
            }
            var avgPaymentPerMonthWithoutFixing = totalInstallmentAmount / installmentCount;
            var avgPaymentPerMonth = avgPaymentPerMonthWithoutFixing.toFixed(2);
            var avgPaymentPerMonthLastMonth = (totalInstallmentAmount - (avgPaymentPerMonth * (installmentCount - 1))).toFixed(2);

            for (var i = 1; i < installmentCount; i++) {
                var installment = {
                    Month: i,
                    Value: avgPaymentPerMonth,
                    PaymentDate: KnowgeDate.GetDatePlusMonth(i)
                };
                model.push(installment);
            }

            //For Last Month
            var lastInstallment = {
                Month: installmentCount,
                Value: avgPaymentPerMonthLastMonth,
                PaymentDate: KnowgeDate.GetDatePlusMonth(installmentCount)
            };

            model.push(lastInstallment);

            Protocol.SavedProtocol = prepareInstallmentDesc(model, debit, agencyFee);
        }

        return model;
    };

    var prepareInstallmentDesc = function (model, ttlInsAmount, agencyFee) {
        var diff = 0;
        for (var i = 1; i < model.length; i++) {
            paymentValue = model[i - 1].Value;
            if (model[i - 1].Value <= ttlInsAmount) {
                model[i - 1].Desc = "Borç";
                ttlInsAmount -= paymentValue;
            } else if (paymentValue > ttlInsAmount) {
                diff = paymentValue - ttlInsAmount;

                if (ttlInsAmount > 0) {
                    model[i - 1].Desc = "Borç (" + ttlInsAmount.toFixed(2) + " TL) + Vekalet Ücreti (" + diff.toFixed(2) + " TL)";
                    ttlInsAmount -= paymentValue;
                }
                else if (ttlInsAmount <= 0) {
                    model[i - 1].Desc = "Vekalet Ücreti";
                }
            }
        }

        //For Last Month
        var lastIndex = model.length - 1;
        if (ttlInsAmount > 0) {
            if (isLawyer && agencyFee > 0) {
                paymentValue = model[lastIndex].Value;
                model[lastIndex].Desc = "Borç (" + ttlInsAmount.toFixed(2) + " TL) + Vekalet Ücreti (" + agencyFee.toFixed(2) + " TL)";
            } else {
                model[lastIndex].Desc = "Borç";
            }
        } else if (isLawyer && agencyFee > 0) {
            //son aydan bir onceki ay, eger vekalet ucretinin bir kismi alindiysa bu diff'te deger olarak
            //gozukur o yuzden vekalet ucretinde bu miktar dusulur.
            if (diff != 0) {
                agencyFee -= diff;
                model[lastIndex].Desc = "Vekalet Ücreti (" + agencyFee.toFixed(2) + " TL)";
            } else {

                model[lastIndex].Desc = "Vekalet Ücreti";
            }
        }

        return model;
    };

    var applyDiscountToProtocolValue = function (discountPercentage) {
        var pv = parseFloat($("#txtProtocolValue").data("original-value"));
        var discountedPv = parseFloat((pv * (100 - discountPercentage)) / 100);
        var agencyFee = parseFloat((discountedPv * 12) / 100);
        $("#txtProtocolValue").val(discountedPv.toFixed(2));
        $("#txtAgencyFee").val(agencyFee.toFixed(2));
        $("#txtProtocolValue").data("min-value", discountedPv.toFixed(2));
    };

    var applyOriginalValueToProtocolValue = function () {
        var protokolValue = $("#txtProtocolValue").data("value");
        $("#txtProtocolValue").val(protokolValue);
        $("#txtAgencyFee").val(((protokolValue * 12) / 100).toFixed(2));
        $("#txtProtocolValue").data("min-value", protokolValue);
    };

    var setMaxDateForAdvancePaymentDate = function () {
        var maxAdvancePaymentDay = parseInt($("#ddlProtocolCampaigns option:selected").data("first-advance-payment-day"));

        //Pesinat tarihi inputuna secilebilir max tarih set edilmesi
        var maxAdvancePaymentDate = new Date();
        maxAdvancePaymentDate.setDate(maxAdvancePaymentDate.getDate() + maxAdvancePaymentDay);
        var maxAdvancePaymentDateStr = window.Utility.FormatDate(maxAdvancePaymentDate);
        $(".txtAdvancePaymentDate").data("date-end-date", maxAdvancePaymentDateStr).datepicker("remove");
        $(".txtAdvancePaymentDate").datepicker();
    };

    var getFormValuesForProtocolInstallments = function () {
        return {
            protocolValue: parseFloat($("#txtProtocolValue").val()),
            advancePaymentValue: parseFloat($("#txtAdvancePayment").val()),
            installmentCount: parseInt($("#ddlInstallment :selected").val()),
            agencyFee: parseFloat($("#txtAgencyFee").val()),
        };
    };

    var getChangedInstValues = function () {
        var inputs = $("#mppTable .protocolInput");
        var installmentValues = [];
        $(inputs).each(function () {
            var id = $(this).parents("tr").data("id");
            installmentValues.push({
                "Month": id,
                "Value": parseFloat($(this).val()),
                "PaymentDate": $("#installmentDate_" + id).val()
            });
        });
        return installmentValues;
    };

    var updatePaymentPlanDesc = function (model) {
        for (var key in model) {
            if (model.hasOwnProperty(key)) {
                var inst = model[key];
                $("tr[data-id='" + inst.Month + "'] td.desc").html(inst.Desc);
            }
        }
    };

    var calculateInstallmentTable = function () {
        calculate();

        var formValues = getFormValuesForProtocolInstallments();

        var model = getChangedInstValues();

        var protocolValue = formValues.protocolValue;
        var advancePaymentValue = formValues.advancePaymentValue;
        var ttlInsAmount = protocolValue - advancePaymentValue;

        var agencyFee = formValues.agencyFee;

        model = prepareInstallmentDesc(model, ttlInsAmount, agencyFee);
        updatePaymentPlanDesc(model);

    };

    var bindEvents = function () {

        //Acilan odeme plani ekraninda ki her text input icin event baglanmasi
        $("#mdlPaymentPlan").on("shown.bs.modal", function () {
            calculate();
            $(".protocolInput").off("keypress keydown keyup").on("keypress, keydown, keyup", function (e) {
                calculateInstallmentTable();
                return true;
            });
        });

        $("#txtAdvancePayment").on("keyup keydown keypress", function () {
            Protocol.SavedProtocol = [];
        });

        $("#txtAgencyFee").on("keyup keydown keypress", function () {
            Protocol.SavedProtocol = [];
        });

        //taksit sayisinda ve secilen kampanya degisikliklerinde degisiklik oldugunda kayitli protokolu sil
        $("#ddlInstallment, #ddlProtocolCampaigns").on("change", function () {
            Protocol.SavedProtocol = [];
        });

        //Kullanici eger ozel yetkisi yoksa yonetici degilse indirimden daha asagi veya toplam risk ten daha fazla deger girerse
        //onaya gonder butonu aktif olur. Protocol olustur butonu pasif olur.
        $("#txtProtocolValue").on("keypress keydown keyup", function () {
            Protocol.SavedProtocol = [];
            var pv = parseFloat($("#txtProtocolValue").val());

            var minpv = parseFloat($("#txtProtocolValue").data("min-value"));
            var maxpv = parseFloat($("#txtProtocolValue").data("max-value"));

            //ignoreProtocolMaxValue
            //Hukuk Dis Burodakiler icin protocol max value'ya takilmamasi icin kural konuldu.
            var ignoreProtocolMaxValue = UserRules.indexOf("IgnoreProtocolMaxValue") !== -1
            if (pv < minpv || (pv > maxpv && !ignoreProtocolMaxValue)) {
                $("#btnApprove").hide();
                $("#btnSendApprovement").show();
            } else {
                $("#btnApprove").show();
                $("#btnSendApprovement").hide();
            }
        });

        $("#ddlProtocolCampaigns").on("change", function () {
            var ddlProtocolCampaigns = $(this).find("option:selected");
            switch ($(this).val()) {
                case "0":
                    applyOriginalValueToProtocolValue();
                    break;
                default:
                    var cashAmount = parseFloat(ddlProtocolCampaigns.data("cash-closing-amount"));
                    if (cashAmount > 0) {
                        $("#txtProtocolValue").val(cashAmount);
                        $("#txtAgencyFee").val(((cashAmount * 12) / 100).toFixed(2));
                        $("#txtProtocolValue").data("value", cashAmount);
                    }
                    var dp = parseFloat(ddlProtocolCampaigns.data("discount-percentage"));
                    applyDiscountToProtocolValue(dp);
            }

            //Taksit sayisi dropdownunu kampanya yetkisi sinirinda tekrar olustur.
            var maxInstallmentCount = parseInt(ddlProtocolCampaigns.data("max-installment-count"));
            $("#ddlInstallment option").remove();
            $("#ddlInstallment").append($("<option></option>").attr("value", 0).text('Taksit Yok'));

            for (var i = 1; i <= maxInstallmentCount; i++) {
                $("#ddlInstallment").append($("<option></option>").attr("value", i).text(i + ' taksit'));
            }

            setMaxDateForAdvancePaymentDate();

            //dropdowndan secildiginde protokol degeri hep original degerine dondugu icin veya orginal deger uzerinden
            //indirim yapildigi icin onayla butonu pasif protokol olustur butonu aktif olacak.
            $("#btnApprove").show();
            $("#btnSendApprovement").hide();
        });

        //Odeme planini goster butonu icin click
        $("#btnShowPaymentPlan").on("click", function () {
            var model = prepareProtocolModel();
            if (model != null) {
                $("#mppTable").SetTemplateHtml(model, "paymentPlan");

                //ilk once acilan popuptaki butun tarih inputlarini initilize et
                var ddlProtocolCampaigns = $("#ddlProtocolCampaigns option:selected");
                var dateinputs = $("#mppTable .date-picker").datepicker({ autoclose: true });

                //sadece ilk inputa enddate sinirlamasi getir protocol kampanyasi ozelliklerine gore
                var maxInstallmentDay = parseInt(ddlProtocolCampaigns.data("first-installment-day"));
                var maxInstallmentDate = new Date();
                maxInstallmentDate.setDate(maxInstallmentDate.getDate() + maxInstallmentDay);
                var maxAInstallmentDateStr = Utility.FormatDate(maxInstallmentDate);
                var firstInstallmentDateDiv = $("#mppTable [data-month='1'] .date-picker");
                firstInstallmentDateDiv.data("date-end-date", maxAInstallmentDateStr).datepicker("remove");
                firstInstallmentDateDiv.datepicker({ autoclose: true }).on('changeDate', function () {
                    if (dateinputs.length > 1) {
                        //inputun tarihini javascript tarihe cevir
                        var strDate = $(this).find("input").val();
                        var dateParts = strDate.split("-");
                        var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

                        var index = 0;
                        for (var i in dateinputs) {
                            if (dateinputs.hasOwnProperty(i)) {
                                if (i != 0) {
                                    index++;
                                    var input = $("#installmentDate_" + i);
                                    var tempDate = new Date(date);
                                    tempDate.setMonth(tempDate.getMonth() + index);
                                    input.val(Utility.FormatDate(tempDate));
                                    input.closest("tr").data("payment-date", Utility.FormatDate(tempDate));
                                }
                            }
                        }
                    }
                });

                //her deger degisimini ilgili parent tr sine yaz
                dateinputs.on('changeDate', function () {
                    $(this).closest("tr").data("payment-date", $(this).find("input").val());
                });

                (model && model.length) ? $("#prtResult").show() : $("#prtResult").hide();
                //model ? $("#prtResult").show() : $("#prtResult").hide();


                $("#mdlPaymentPlan").modal("show");
                $("#mdlPaymentPlan :input").inputmask();
            }
        });

        //Onayla butonu icin click
        $("#btnSendApprovement, #btnApprove").on("click", function () {

            var model = {};
            var ap = parseFloat($("#txtAdvancePayment").val());

            model.Installments = prepareProtocolModel();

            if (model.Installments != null && (model.Installments.length > 0 || ap > 0)) { //taksıt ve pesinat 0 dan buyuk olmali
                var txtProtocolValue = $("#txtProtocolValue");

                var pv = parseFloat(txtProtocolValue.val());

                if (isLawyer) {
                    var agencyFee = parseFloat($("#txtAgencyFee").val());
                    if (agencyFee <= 0) {
                        alert("Vekalet ücreti girmelisiniz !");
                        return;
                    }
                    pv += agencyFee;
                }

                pv = parseFloat(pv.toFixed(2));

                var total = 0.0;
                $(model.Installments).each(function () {
                    total += parseFloat(this.Value);
                });

                if (Utility.RoundFloat(ap + parseFloat(total.toFixed(2))) === pv) {

                    model.TotalDebt = pv.toFixed(2) + " TL";
                    model.AdvancePayment = ap.toFixed(2) + " TL";
                    model.AdvancePaymentDate = $("#txtAdvancePaymentDate").val();
                    model.MainMoney = parseFloat(txtProtocolValue.data("value")).toFixed(2) + " TL";
                    model.TotalRisk = parseFloat(txtProtocolValue.data("max-value")).toFixed(2) + " TL";

                    if (parseFloat($("#txtAdvancePayment").val()) > 0 && model.AdvancePaymentDate === "") {
                        Message.Show("Peşinat tarihini girmelisiniz.", "Hata", Message.ThemeType.Ruby);
                        $("#txtAdvancePaymentDate").click();
                        return;
                    }

                    model.TotalInstallment = total.toFixed(2) + " TL";
                    if ($("#ddlProtocolCampaigns option")) {
                        model.TotalMoneyWithDiscount = (parseFloat(txtProtocolValue.val())).toFixed(2) + " TL";;
                        $(".TotalMoneyWithDiscount").show();
                    }
                    $("#mppTableApprovement").SetTemplateHtml(model, "paymentPlanApprovement");

                    $("#mdlbtnSendApprovement").toggle($("#btnSendApprovement:visible").length > 0);
                    $("#mdlbtnApprove").toggle($("#btnApprove:visible").length > 0);
                    $("#paymentPlanNote").toggle($("#btnSendApprovement:visible").length > 0);

                    $("#mdlPaymentPlanApprovement").modal("show");

                } else {
                    alert("Peşinat ve taksit toplamları protokol değerine eşit olmalı");
                }

                if (model.Installments.length === 0) {
                    $("#installmentContractDiv").hide();
                    $("#sulhContractDiv").show();
                } else {
                    $("#sulhContractDiv").hide();
                    $("#installmentContractDiv").show();
                }
            } else {
                alert("Peşinat ve taksit toplamları protokol değerine eşit olmalı");
            }
        });
    };

    var insertProtocol = function (needApprovement) {
        var mainMoney = parseFloat($("#txtProtocolValue").val());
        if (isLawyer) {
            mainMoney += parseFloat($("#txtAgencyFee").val());
        }
        mainMoney = parseFloat(mainMoney.toFixed(2));
        var advancePaymnent = parseFloat($("#txtAdvancePayment").val() ? $("#txtAdvancePayment").val() : 0);
        var advancePaymnentDate = $("#txtAdvancePaymentDate").val();

        var sendInstallmentContract = $("#sendInstallmentContract").prop("checked");
        var sendSulhContract = $("#sendSulhContract").prop("checked");
        var sendPaymentPlanToObligor = $("#sendPaymentPlanToObligor").prop("checked");
        var smsProtocolReminder = $("#smsProtocolReminder").prop("checked");
        var agencyFee = $("#txtAgencyFee").val() ? $("#txtAgencyFee").val() : 0;

        var note = $("#txtProtocolNote").val();
        var campaignId = 0;
        var roleIdMakingProtocol = 0;
        if ($("#ddlProtocolCampaigns").length > 0 && $("#ddlProtocolCampaigns :selected").length > 0) {
            campaignId = $("#ddlProtocolCampaigns :selected").val();
            roleIdMakingProtocol = $("#ddlProtocolCampaigns :selected").data("role-id");
        }
        Core.AjaxCall({
            url: "/Obligor/InsertProtocol",
            data: JSON.stringify({
                protocol: JSON.stringify(Protocol.SavedProtocol),
                obligorId: ObligorId,
                campaignId: campaignId,
                totalMoney: mainMoney,
                advancePaymnent: advancePaymnent,
                advancePaymnentDate: advancePaymnentDate,
                roleIdMakingProtocol: roleIdMakingProtocol,
                sendInstallmentContract: sendInstallmentContract,
                sendSulhContract: sendSulhContract,
                sendPaymentPlanToObligor: sendPaymentPlanToObligor,
                smsProtocolReminder: smsProtocolReminder,
                agencyFee: agencyFee,
                note: note
            }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#protocolWidget, #mdlbtnSendApprovement, #mdlbtnApprove, .modal-dialog.modal-lg, #mdlbtnApprove, #mdlbtnSendApprovement",
            success: function (response) {
                if (response.isSuccess) {
                    if (needApprovement) {
                        Message.Show("Protokol onaya gönderildi.", "Bilgilendirme", Message.ThemeType.Teal);
                        $("#protocolWidget .portlet-body").html('<div class="alert alert-info"><strong>Protokol onaya gönderildi. Tekrar protokol oluşturmak için sayfayı yenileyin</strong></div><span></span>');

                    } else {
                        Message.Show("Protokol başarıyla oluşturuldu.", "Bilgilendirme", Message.ThemeType.Teal);
                        $("#protocolWidget .portlet-body").html('<div class="alert alert-info"><strong>Borçlunun aktif protokolu var.</strong></div><span></span>');
                    }
                    ObligorDetail.SelectTabById(ObligorTab.ProtocolHistory);
                    ObligorDetail.LoadData(ObligorTab.ProtocolHistory, ProtocolHistory.TabCallBackFunc);

                } else {
                    if (response.message) {
                        Message.Show(response.message, "Bilgilendirme", Message.ThemeType.Ruby);
                    } else {
                        Message.Show("Protokol oluşturma işleminde hata oluştu", "Bilgilendirme", Message.ThemeType.Ruby);
                    }
                }
                $("#mdlPaymentPlanApprovement").modal("hide");
            }
        });
    };

    var changeClosureAmountByScore = function () {
        if (window.userScore && window.userScore >= window.dependentScore) {
            var txtPv = $("#txtProtocolValue");
            var orgValue = txtPv.data("original-value");
            var changedPv = (orgValue * window.scoreMultiplier).toFixed(2);
            txtPv.data("value", changedPv);
            txtPv.data("min-value", changedPv);
            txtPv.val(changedPv);
        }
    };

    var init = function () {
        bindEvents();
        setMaxDateForAdvancePaymentDate();
        changeClosureAmountByScore();
    };

    return {
        Arrange: arrange,
        Init: init,
        Save: save,
        SavedProtocol: savedProtocol,
        InsertProtocol: insertProtocol,
        GetChangedInstValues: getChangedInstValues
    };
})();

var DebitsInAllPortfoy = (function () {

    var tabCallBackFunc = function () {
        $("#tblList_2").dataTable(CommonDataTablesSettings);
    };

    return {
        TabCallBackFunc: tabCallBackFunc
    };

})();

var Note = (function () {
    var pulse = false;

    var bindEvents = function () {
        $("#detail_3").off("keypress keydown keyup").on("keypress keydown keyup", function () {
            $("#btnSaveNote_3").prop("disabled", $("#detail_3").val().trim() === "");
        });

        $("#txtMainScreenNote").off("keypress keydown keyup change").on("keypress keydown keyup change", function () {
            $("#btnMainScreenAddNote").prop("disabled", $("#txtMainScreenNote").val().trim() === "");
        });

        if (pulse) {
            $("#tblList_3 tbody tr:first").pulsate({
                color: "#2aca26",
                repeat: false
            }
            );
            pulse = false;
        }

        $("#tab3Add").on("shown.bs.modal", function () {
            var e = document.getElementById("noUiSliderAddNote");
            if (!e.noUiSlider) {
                noUiSlider.create(e, {
                    start: 5,
                    range: {
                        min: 0,
                        max: 10
                    },
                    pips: {
                        mode: "values",
                        values: [0, 2, 4, 6, 8, 10],
                        density: 4
                    }
                }),
                    e.noUiSlider.on("change", function (n, t) {
                        //n[t] < 20 ? e.noUiSlider.set(20) : n[t] > 80 && e.noUiSlider.set(80)
                    });
            }
        });

        $("#ddlCallResult_12").on("change", function () {
            $("#txtCallResult_12").prop("disabled", $(this).val() != -1);
            $("#btnSaveCallResult_12").prop("disabled", $(this).val() == -1);
        });

        $("#txtCallResult_12").on("keypress keydown keyup", function () {
            $("#btnSaveCallResult_12").prop("disabled", $(this).val() == "");
        });
    }

    var tabCallBackFunc = function () {
        $("#tblList_3").dataTable($.extend(CommonDataTablesSettings, { "order": [[0, "desc"]] }));
        bindEvents();
    }

    var getCriticNotes = function () {
        Core.AjaxCall({
            url: "/Obligor/GetCriticNotes",
            data: JSON.stringify({ obligorId: ObligorId }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_3",
            success: function (response) {
                if (response.isSuccess) {
                    var div = $("#divLast5CriticNote");
                    var html = "";
                    for (var i in response.Notes) {
                        if (response.Notes.hasOwnProperty(i)) {
                            var note = response.Notes[i];
                            if (note.Type == 0) {
                                html += "<tr><td title=" + note.Text + "><span class='label label label-danger'>" + note.ShortenedDetail + "</span></td></tr>";
                            } else if (note.Type == 1) {
                                html += "<tr><td title=" + note.Text + "><span class='label label label-info'>" + note.ShortenedDetail + "</span></td></tr>";
                            } else if (note.Type == 2) {
                                html += "<tr " + (i % 2 == 0 ? "class=warning" : "") + "><td title=" + note.Text + ">" + note.ShortenedDetail + "</td></tr>";
                            }
                        }
                    }
                    div.html(html);
                } else {
                    console.log(response.error);
                }
            },
            complete: function () {
                $("#tab3Add").modal("hide");
            }
        });
    }

    var addNote = function () {
        var detail = $("#detail_3").val().trim();
        var type = $("#ddlType_3").val();
        var e = document.getElementById("noUiSliderAddNote");
        var rate = e.noUiSlider.get().replace(".00", "");

        var data = {
            Detay: detail,
            Tip: type
        }

        Core.AjaxCall({
            url: "/Obligor/AddNote",
            data: JSON.stringify({ note: data, obligorId: ObligorId, rate: rate }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_3",
            success: function (response) {
                if (response.isSuccess) {
                    pulse = true;
                    ObligorDetail.LoadData(ObligorTab.Notlar, tabCallBackFunc);
                    if (type == 1) {
                        getCriticNotes();
                    }
                    Message.Show("Not başarıyla eklenmiştir.", "Bilgilendirme", Message.ThemeType.Teal);
                } else {
                    console.log(response.error);
                }
            },
            complete: function () {
                $("#tab3Add").modal("hide");
            }
        });
    };

    var addMainScreenNote = function () {
        var detail = $("#txtMainScreenNote").val().trim();
        var type = $("#ddlMainScreenNoteType").val();
        var rate = $("#ddlMainScreenNoteRate").val();

        var data = {
            Detay: detail,
            Tip: type
        }

        Core.AjaxCall({
            url: "/Obligor/AddNote",
            data: JSON.stringify({ note: data, obligorId: ObligorId, rate: rate }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_3",
            success: function (response) {
                if (response.isSuccess) {
                    pulse = true;
                    ObligorDetail.LoadData(ObligorTab.Notlar, tabCallBackFunc);
                    if (type == 1) {
                        getCriticNotes();
                    }
                    Message.Show("Not başarıyla eklenmiştir.", "Bilgilendirme", Message.ThemeType.Teal);
                    $("#txtMainScreenNote").val("");
                    $("#ddlMainScreenNoteType").val(0);
                    $("#ddlMainScreenNoteRate").val(-1);
                    $("#btnMainScreenAddNote").prop("disabled", true);
                } else {
                    console.log(response.error);
                }
            },
            complete: function () {
                $("#tab3Add").modal("hide");
            }
        });
    }

    var shortAdd = function () {
        bindEvents();
        $('#tab3Add').modal('show');
    };

    var deleteNote = function (id) {
        Core.AjaxCall({
            url: "/Obligor/DeleteNote",
            data: JSON.stringify({ id: id }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_3",
            success: function (response) {
                if (response.isSuccess) {
                    ObligorDetail.LoadData(ObligorTab.Notlar, tabCallBackFunc);
                    Message.Show(response.message, "Bilgilendirme", Message.ThemeType.Teal);
                } else {
                    Message.Show(response.message, "Bilgilendirme", Message.ThemeType.Ruby);
                }
            }
        });
    }

    var showCallResultScreen = function () {
        var phone = $("#ddlPhoneList :selected").val();
        if (phone) {
            $("#txtCallResult_12").val("");
            $("#txtCallResult_12").prop("disabled", true);

            $("#ddlCallResult_12").find('option').remove();
            $("#ddlCallResult_12").append('<option value="0">Lütfen sonuç seçiniz!</option>');
            $.each(callResults, function (key, value) {
                $("#ddlCallResult_12").append('<option value=' + value.Id + '>' + value.Value + '</option>');
            });
            $("#tab12AddCallResult #headerCallResult_12").html('<b>"' + phone + '" için sonuç gir</b>');
            $("#tab12AddCallResult").modal("show");
        }
    }

    var addCallResult = function () {
        var phone = $("#ddlPhoneList :selected").val();
        var id = $("#ddlCallResult_12 :selected").val();
        var text = "";
        text = id == -1 ? $("#txtCallResult_12").val() : $("#ddlCallResult_12 :selected").text();

        Core.AjaxCall({
            url: "/Obligor/AddCallResult",
            data: JSON.stringify({ resultId: id, resultText: text, obligorId: ObligorId, phone: phone }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_3",
            success: function (response) {
                if (response.isSuccess) {
                    Message.Show("Arama sonucu başarıyla eklenmiştir.", "Bilgilendirme", Message.ThemeType.Teal);
                } else {
                    Message.Show("Arama sonucu eklemede sorun oluşmuştur.", "Hata", Message.ThemeType.Ruby);
                }
            },
            complete: function () {
                $("#tab12AddCallResult").modal("hide");
            }
        });
    }

    return {
        TabCallBackFunc: tabCallBackFunc,
        AddNote: addNote,
        AddCallResult: addCallResult,
        ShortAdd: shortAdd,
        AddMainScreenNote: addMainScreenNote,
        DeleteNote: deleteNote,
        ShowCallResultScreen: showCallResultScreen
    }
})();

var SmsHistory = (function () {

    var bannedWords = [];

    var bindEventsForShortSms = function() {
        $("#txtSms_4").off("keypress keydown keyup").on("keypress keydown keyup", function() {
            var smsText = $(this).val().toLowerCase();
            var smsWords = smsText.replace(/[^\w\s]|_/g, function($1) { return ' ' + $1 + ' '; }).replace(/[ ]+/g, ' ').split(' ');
            var result = _.intersection(bannedWords, smsWords);

            $("#btnSendSms_4").prop("disabled", smsText.trim() === "" || result.length !== 0);
        });
    };

    var tabCallBackFunc = function() {
        $("#tblList_4").dataTable($.extend(CommonDataTablesSettings, { "order": [[0, "desc"]] }));

        bindEventsForShortSms();
    };

    var preview = function() {
        var txtSms4 = $("#txtSms_4");
        var lasttext = txtSms4.val();
        if (window.smsTemplateVariables) {
            $.each(window.smsTemplateVariables, function(key, val) {
                lasttext = lasttext.replace(key, val);
            });
            txtSms4.val(lasttext);
        }
    };

    var sendSms = function () {
        preview();
        var number = $("#ddlMobilePhone_4 option:selected").text();
        var tempId = $("#ddlSmsTemplate_4 option:selected").val();
        var smsText = $("#txtSms_4").val();
        Core.AjaxCall({
            url: "/Obligor/InsertSms",
            data: JSON.stringify({ obligorId: ObligorId, number: number, tempId: tempId, smsText: smsText }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response.isSuccess) {
                    ObligorDetail.LoadData(ObligorTab.SmsTarihce, tabCallBackFunc);
                } else {
                    console.log(response.isSuccess);
                }
            }
        });
        $("#tab4Add").modal("hide");
    }

    var getGsmNumbers = function () {
        Core.AjaxCall({
            url: "/Obligor/GetGsmNumbers",
            data: JSON.stringify({ obligorId: ObligorId }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                $("#ddlMobilePhone_4").find('option').remove();
                $.each(response, function (key, value) {
                    $("#ddlMobilePhone_4").append('<option value=' + value.Id + '>' + value.Number + '</option>');
                });
            }
        });
    }

    var getTemplates = function () {
        Core.AjaxCall({
            url: "/Sms/GetSmsTemplates",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                $("#ddlSmsTemplate_4").find('option').remove();
                //$("#ddlSmsTemplate_4").append('<option data-text="" value="-1">Bos Sablon</option>');
                $.each(response, function (key, value) {
                    $("#ddlSmsTemplate_4").append('<option data-text="' + value.TemplateText + '" value=' + value.Id + '>' + value.Name + '</option>');
                });
                var txtSms4 = $("#txtSms_4");
                var selectedOption = $("#ddlSmsTemplate_4 option:selected")[0];
                var smsTemp = $(selectedOption).data("text");
                txtSms4.val(smsTemp);

            }
        });
    }

    var getBannedWords = function () {
        if (bannedWords.length === 0) {
            Core.AjaxCall({
                url: "/Sms/GetBannedWords",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    bannedWords = _.map(response.bannedWords, function (s) {
                        return _.isString(s) ? s.toLowerCase() : s;
                    });;
                }
            });
        }
    }

    var bindEvents = function () {

        // Sablon Ddl icin
        var ddlSmsTemplate4 = $("#ddlSmsTemplate_4");
        var txtSms4 = $("#txtSms_4");
        ddlSmsTemplate4.on("change", function () {
            var selectedOption = $("#ddlSmsTemplate_4 option:selected")[0];
            var smsTemp = $(selectedOption).data("text");
            txtSms4.val(smsTemp);
            //$("#btnSendSms_4").prop("disabled", (smsTemp.indexOf("<") !== -1 && smsTemp.indexOf(">") !== -1));
            //txtSms4.prop("disabled", $(selectedOption).val() !== "-1");
            //$("#btnSendSms_4").prop("disabled", txtSms4.val() === "");
        });
    }

    var shortSms = function () {
        bindEventsForShortSms();

        $('#tab4Add').modal('show');
    };

    var init = function () {
        getTemplates();
        getGsmNumbers();
        getBannedWords();
        bindEvents();
    }

    return {
        SendSms: sendSms,
        Init: init,
        Preview: preview,
        TabCallBackFunc: tabCallBackFunc,
        ShortSms: shortSms
    }
})();

var CommunicationHistory = (function () {

    var tabCallBackFunc = function () {
        $("#tblList_5").dataTable(CommonDataTablesSettings);

        var elem = $("*[data-rating]");
        if (elem.length > 0) {
            elem.each(function () {
                var id = $(this).data("id");
                noUiSlider.create(this, { start: [0], step: 1, range: { 'min': 0, 'max': 100 } });
                var btn = document.getElementById('rangeButton_' + id);
                this.noUiSlider.on('update', function (values, handle) {
                    var score = parseInt(values[handle]).toFixed(0);
                    $(btn).data("score", score);
                    $(btn).text(score + "/100 | Onay");
                });
            });
        } else {
            $(".rtgbtn").hide();
        }
    }

    var setRating = function (chi) {
        var score = $("#rangeButton_" + chi).data("score");
        Core.AjaxCall({
            url: "/Obligor/SetRating",
            data: JSON.stringify({ obligorId: ObligorId, communicationHistoryId: chi, score: score }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response.isSuccess) {
                    ObligorDetail.LoadData(ObligorTab.CommunicationHistory, tabCallBackFunc);
                } else {
                    console.log(response.isSuccess);
                }
            }
        });
    }

    var init = function () {
    }

    return {
        Init: init,
        TabCallBackFunc: tabCallBackFunc,
        SetRating: setRating
    }
})();

var AllPayments = (function () {

    var tabCallBackFunc = function () {
        $("#tblList_11").dataTable(CommonDataTablesSettings);
    }

    var getAndShowDataOnModal = function (id) {
        var tr = $("#tblList_11 tr[data-id='" + id + "']");
        $("#tab11TahTar").html(tr.data("payment-date"));
        $("#tab11Tutar").html(tr.data("value"));
        $("#tab11Banka").html(tr.data("bank"));
        $("#tab11CID").html(tr.data("cid"));
        $("#tab11Status").html(tr.data("status"));
        $("#tab11StatusDesc").html(tr.data("status-text"));
        $("#tab11TahYevNo").html(tr.data("cjn"));
        $("#tab11MuhFisNo").html(tr.data("avn"));

        $("#tab11TahAcik").html(tr.data("payment-desc"));
        $("#tab11EfsTahSat").html(tr.data("efs"));
        $("#tab11Mesaj").html(tr.data("message-text"));
        $("#tab11BorAdSoyad").html(tr.data("name"));
        $("#tab11TahAnDosSah").html(tr.data("tads"));
        //$("#tblList_11 tr[data-payment-date='" + tr.data("payment-date") + "']");
    };

    var openAttachProtocolModal = function (id, hasProtocoled) {

        getAndShowDataOnModal(id);

        var planTypeDdl = $("#ddlPlanType_11");
        var subPlanTypeDdl = $("#ddlPlanSubType_11");
        var lblPlanType = $("#lblPlanType_11");
        var lblPlanSubType = $("#lblPlanSubType_11");
        var attachPrt = $("#tab11AttachP");
        var deAttachPrt = $("#tab11DeAttachP");
        var deAttachObl = $("#chcDeAttachObligor");
        var deAttachOblLbl = $("#lblDeAttachObligor");

        deAttachObl.prop("checked", false).uniform();

        if (hasProtocoled) {
            attachPrt.hide();
            deAttachPrt.show();
            deAttachOblLbl.show();
            planTypeDdl.hide();
            lblPlanType.hide();
            subPlanTypeDdl.hide();
            lblPlanSubType.hide();
        } else {
            attachPrt.show();
            deAttachPrt.hide();
            deAttachOblLbl.hide();
            planTypeDdl.show();
            lblPlanType.show();
            subPlanTypeDdl.show();
            lblPlanSubType.show();

            $("#ddlPlanType_11").off("change").on("change", function () {
                var key = $(this).val();
                fillSubPlanTypes(key);
            });
            Utility.PopulateSelect("#ddlPlanType_11", window.paymentPlanTypes, "Id", "Value");
            fillSubPlanTypes(window.paymentPlanTypes[0].Id);
        }

        $("#tab11AttachProtocol").modal("show");
        $("#tab11AttachProtocol").data("rec-id", id);

    }

    var fillSubPlanTypes = function (id) {
        var obj = JSON.stringify({ paymentPlanId: id });

        Core.AjaxCall({
            url: "/Obligor/GetPaymentSubPlanType",
            data: obj,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "ddlPlanType_11",
            success: function (response) {
                if (response.isSuccess) {
                    Utility.PopulateSelect("#ddlPlanSubType_11", response.paymentPlanSubType, "Id", "Value");
                }
            },

        });
    }

    var attachToProtocol = function () {
        var paymentPlanId = $("#ddlPlanType_11").val();
        var paymentSubPlanId = $("#ddlPlanSubType_11").val();
        var recId = $("#tab11AttachProtocol").data("rec-id");
        var obj = JSON.stringify({ paymentPlanId: paymentPlanId, paymentSubPlanId: paymentSubPlanId, recId: recId });

        window.Core.AjaxCall({
            url: "/Obligor/AttachToProtocol",
            data: obj,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "tab11AttachProtocol",
            success: function (response) {
                if (response.isSuccess) {
                }
            },
            complete: function () {
                $("#tab11AttachProtocol").modal("hide");
                location.reload();
            }
        });

    }

    var deAttachFromProtocol = function () {
        var recId = $("#tab11AttachProtocol").data("rec-id");
        var deAttachObligor = $("#chcDeAttachObligor").prop("checked");
        var obj = JSON.stringify({ recId: recId, deAttachObligor: deAttachObligor });

        window.Core.AjaxCall({
            url: "/Obligor/DeattachToProtocol",
            data: obj,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "tab11AttachProtocol",
            success: function (response) {
                if (response.isSuccess) {
                }
            },
            complete: function () {
                $("#tab11AttachProtocol").modal("hide");
                location.reload();
            }
        });
    }

    var init = function () {
    }

    return {
        Init: init,
        TabCallBackFunc: tabCallBackFunc,
        OpenAttachProtocolModal: openAttachProtocolModal,
        AttachToProtocol: attachToProtocol,
        DeAttachFromProtocol: deAttachFromProtocol,
    }
})();

var Phone = (function () {
    var pulse = false;

    var validationRules = {
        rules: {
            txtCalledPerson_12: "required",
            txtRelatedPerson_12: "required",
            txtPhone_12: "required",
            //txtPhoneExtension_12: "required",
            txtInformationSource_12: "required"
        },
        messages: {
            txtCalledPerson_12: "",
            txtRelatedPerson_12: "",
            txtPhone_12: "",
            //txtPhoneExtension_12: "",
            txtInformationSource_12: ""
        }
    }

    var pulseAfterAdd = function () {
        if (pulse) {
            $("#tblList_12 tbody tr:first").pulsate({
                color: "#2aca26",
                repeat: false
            });
            pulse = false;
        }
    };

    var tabCallBackFunc = function () {
        $("#tblList_12").dataTable($.extend(CommonDataTablesSettings, { "order": [[9, "desc"]] }));
        pulseAfterAdd();
    };

    var add = function () {
        var data = {
            Status: $("#ddlStatus_12").val(),
            Type: $("#ddlType_12").val(),
            RelatedPerson: $("#txtRelatedPerson_12").val(),
            CalledPerson: $("#txtCalledPerson_12").val(),
            PhoneNumber: $("#txtPhone_12").val(),
            Extension: $("#txtPhoneExtension_12").val(),
            InformationSource: $("#txtInformationSource_12").val()
        }

        var obj = JSON.stringify({ phone: data, obligorId: ObligorId });
        Core.AjaxCall({
            url: "/Obligor/AddPhone",
            data: obj,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "tblList_12",
            success: function (response) {
                if (response.isSuccess) {
                    pulse = true;
                    ObligorDetail.LoadData(ObligorTab.Phone, tabCallBackFunc);
                    Message.Show("Telefon başarıyla eklendi", "Bilgilendirme", Message.ThemeType.Teal);

                } else {
                    console.log("Add Phone modali acilmasi esnasinda sorun olustu.");
                }
            },
            complete: function () {
                $("#tab12Add").modal("hide");
            }
        });
    };

    var vAdd = function () {
        V.Validate("frmPhoneAdd", add, validationRules);
    }

    var getDetails = function (id) {

        Core.AjaxCall({
            url: "/Obligor/GetPhone",
            data: JSON.stringify({ id: id }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_12",
            success: function (response) {
                if (response.isSuccess) {
                    if (response.details) {
                        var d = response.details;
                        $("#idU_12").val(id);
                        $("#ddlUStatus_12").val(d.Status);
                        $("#ddlUType_12").val(d.Type);
                        $("#txtURelatedPerson_12").val(d.RelatedPerson);
                        $("#txtUCalledPerson_12").val(d.CalledPerson);
                        $("#txtUPhone_12").val(d.PhoneNumber);
                        $("#txtUPhoneExtension_12").val(d.Extension);
                        $("#txtUInformationSource_12").val(d.InformationSource);
                        $("#tab12Update").modal("show");
                    }
                } else {
                    alert(genaralAlertSentence);
                    console.log("Update Phone modali acilmasi esnasinda sorun olustu.");
                }
            }
        });
    };

    var update = function () {
        var data = {
            Id: $("#idU_12").val(),
            Status: $("#ddlUStatus_12").val(),
            Type: $("#ddlUType_12").val(),
            RelatedPerson: $("#txtURelatedPerson_12").val(),
            CalledPerson: $("#txtUCalledPerson_12").val(),
            PhoneNumber: $("#txtUPhone_12").val(),
            Extension: $("#txtUPhoneExtension_12").val(),
            InformationSource: $("#txtUInformationSource_12").val()
        }

        var obj = JSON.stringify({ phone: data });

        Core.AjaxCall({
            url: "/Obligor/UpdatePhone",
            data: obj,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tab12Update",
            success: function (response) {
                if (response.isSuccess) {
                    Message.Show("Telefon numarası başarıyla güncellendi", "Bilgilendirme", Message.ThemeType.Teal);
                    ObligorDetail.LoadData(ObligorTab.Phone, tabCallBackFunc);
                } else {
                    alert(genaralAlertSentence);
                    console.log("Update Phone islemi gerceklestirelemedi.");
                }
            },
            complete: function () {
                $("#tab12Update").modal("hide");
            }
        });
    };

    var vUpdate = function () {
        V.Validate("frmPhoneUpdate", update, validationRules);
    }

    var init = function () {
        $("#tab12Add").validate();
        alert(3);
    };

    var call = function () {
        var ddlPhoneSelOpt = $("#ddlPhoneList option:selected");
        var number = ddlPhoneSelOpt.val();
        var fakenumber = $("#ddlFakePhoneList option:selected").val();
        var customerName = $("#customerName").val();
        var accountId = $("#accountId").val();
        if (number.length === 10 || number.length === 11) {
            Core.AjaxCall({
                url: "/Obligor/Call",
                data: JSON.stringify({ number: number, preferredCpn: fakenumber, customerName: customerName, accountId: accountId }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                blockDiv: "#mainCallButton",
                success: function (response) {
                    if (response.isSuccess) {
                        Message.Show("\"" + number + "\" aranıyor...", "Bilgilendirme", Message.ThemeType.Teal);
                    } else {
                        Message.Show("Arama yapılamadı.", "Bilgilendirme", Message.ThemeType.Ruby);
                    }
                },
                complete: function () {

                }
            });
        }

        var txtMainScreenNote = $("#txtMainScreenNote");
        if (txtMainScreenNote.val().trim() === "") {
            txtMainScreenNote.val(ddlPhoneSelOpt.text().trim() + " - ");
        } else {
            txtMainScreenNote.val(txtMainScreenNote.val().trim() + " / " + ddlPhoneSelOpt.text().trim() + " - ");
        }
    };

    return {
        Init: init,
        Add: vAdd,
        Call: call,
        Update: vUpdate,
        GetDetails: getDetails,
        TabCallBackFunc: tabCallBackFunc
    }
})();

var ProtocolHistory = (function () {
    var tabCallBackFunc = function () {
        $("#tblList_9").dataTable($.extend(CommonDataTablesSettings, { "order": [[0, "desc"]] }));
    };

    var openDetail = function (recId) {

    };

    var confirmForDelete = function (recId, id) {
        var modal = $("#confirmationProtocolCancelModal");
        modal.data("rec-id", recId);
        modal.data("id", id);
        var protocolCancelModalOk = $("#protocolCancelModalOk");
        $("#protocolCancelReason").off("change").on("change", function () {
            protocolCancelModalOk.prop("disabled", $(this).val() == -1);
        });
        $("#confirmationProtocolCancelModal").modal("show");
    };

    var deleteProtocol = function () {

        var recId = $("#confirmationProtocolCancelModal").data("rec-id");
        var id = $("#confirmationProtocolCancelModal").data("id");
        var reason = $("#protocolCancelReason option:selected").val();

        var obj = JSON.stringify({ recId: recId, reason: reason, id: id });

        Core.AjaxCall({
            url: "/Obligor/DeleteProtocol",
            data: obj,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_9",
            success: function (response) {
                if (response.isSuccess) {
                    location.reload();
                    //ObligorDetail.LoadData(ObligorTab.ProtocolHistory, tabCallBackFunc);
                } else {
                    alert("Protokol silinemedi");
                }
            },
            complete: function () {
                $("#confirmationProtocolCancelModal").modal("hide");
            }
        });
    };

    var showDetail = function (protocolId) {
        Core.AjaxCall({
            url: "/Obligor/GetProtocolInstalmentsById",
            data: JSON.stringify({ protocolId: protocolId }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tab10",
            success: function (response) {
                if (response.isSuccess) {
                    console.log(response.data);
                    $("#tbl9HistoryDetail").SetTemplateHtml(response.data, "protocolHistoryDetail");
                    $("#tab9HistoryDetail").modal("show");
                    //Message.Show("Not başaıyla eklenmiştir.", "Bilgilendirme", Message.ThemeType.Teal);
                } else {
                    console.log(response.data);
                    //console.log(response.error);
                }
            },
            complete: function () {
                //$("#tab12AddCallResult").modal("hide");
            }
        });


        //var phone = $("#ddlPhoneList :selected").val();
        //if (phone) {
        //    $("#txtCallResult_12").val("");
        //    $("#txtCallResult_12").prop("disabled", true);

        //    $("#ddlCallResult_12").find('option').remove();
        //    $("#ddlCallResult_12").append('<option value="0">Lütfen sonuç seçiniz!</option>');
        //    $.each(callResults, function (key, value) {
        //        $("#ddlCallResult_12").append('<option value=' + value.Id + '>' + value.Value + '</option>');
        //    });
        //    $("#tab12AddCallResult #headerCallResult_12").html('<b>"' + phone + '" için sonuç gir</b>');
        //    $("#tab12AddCallResult").modal("show");


        //    var phone = $("#ddlPhoneList :selected").val();
        //    var id = $("#ddlCallResult_12 :selected").val();
        //    var text = "";
        //    text = id == -1 ? $("#txtCallResult_12").val() : $("#ddlCallResult_12 :selected").text();



        //    $("#mppTable").SetTemplateHtml(model, "paymentPlan");
        //}
    }

    return {
        //Init: init,
        TabCallBackFunc: tabCallBackFunc,
        ConfirmForDelete: confirmForDelete,
        OpenDetail: openDetail,
        DeleteProtocol: deleteProtocol,
        ShowDetail: showDetail
    }
})();

var Address = (function () {
    var cities = [];
    var counties = [];

    var validationRules = {
        rules: {
            ddlAddressType_13: "required",
            ddlCity_13: "required",
            ddlCounty_13: "required",
            //txtPostalCode_13: "required",
            txtAddress_13: "required"
        },
        messages: {
            ddlAddressType_13: "",
            ddlCity_13: "",
            ddlCounty_13: "",
            //txtPostalCode_13: "",
            txtAddress_13: ""
        }
    }

    var bindEvents = function () {
        $("#ddlCity_13").off("change").on("change", function () {
            fillCounties($(this).val());
        });
    }

    var getCities = function () {
        if (cities.length === 0) {
            Core.AjaxCall({
                url: "/Obligor/GetCities",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                blockDiv: "tblList_13",
                success: function (response) {
                    if (response.isSuccess) {
                        cities = response.cities;
                    }
                }
            });
        }
    }

    var tabCallBackFunc = function () {
        $("#tblList_13").dataTable($.extend(CommonDataTablesSettings, { "order": [[0, "desc"]] }));
        bindEvents();
        getCities();
    };

    var getCounties = function (id, cbFunc) {
        Core.AjaxCall({
            url: "/Obligor/GetCounties",
            data: JSON.stringify({ cityId: id }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "ddlCounty_13",
            success: function (response) {
                if (response.isSuccess) {
                    counties = response.counties;
                    cbFunc();
                }
            }
        });
    }

    var add = function () {
        var data = {
            AddressType: $("#ddlAddressType_13").val(),
            CityId: $("#ddlCity_13").val(),
            CountyId: $("#ddlCounty_13").val(),
            PostaKodu: $("#txtPostalCode_13").val(),
            Address: $("#txtAddress_13").val()
        }

        var obj = JSON.stringify({ address: data, obligorId: ObligorId });
        Core.AjaxCall({
            url: "/Obligor/AddAddress",
            data: obj,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "tblList_13",
            success: function (response) {
                if (response.isSuccess) {
                    ObligorDetail.LoadData(ObligorTab.Address, tabCallBackFunc);
                } else {
                }
            },
            complete: function () {
                $("#tab13Add").modal("hide");
            }
        });
    };

    var fillCounties = function (cityId) {
        getCounties(cityId, function () {
            Utility.PopulateSelect("#ddlCounty_13", counties, "Id", "Name");
        });
    }

    var showModal = function () {
        if (cities.length > 0) {

            Utility.PopulateSelect("#ddlCity_13", cities, "Id", "Name");
            var cityId = cities[0].Id;
            if (cityId) {
                fillCounties(cityId);
                $("#tab13Add").modal("show");
                return;
            }
        }
        alert("Adres oluşturmada hata oluştu. Lütfen sistem yöneticisine danışın.(Cities is null)");


    }

    var init = function () {
    };

    var vAdd = function () {
        V.Validate("frmAddressAdd", add, validationRules);
    }

    return {
        Init: init,
        TabCallBackFunc: tabCallBackFunc,
        ShowModal: showModal,
        Add: add
    }
})();

var Debit = (function () {

    var bindEvents = function () {

    };

    var tabCallBackFunc = function () {
        $("#tblList_15").dataTable($.extend(CommonDataTablesSettings, { "order": [[0, "desc"]] }));
        bindEvents();
    };

    var showExecutionConfirmation = function (recId) {
        var modal = $("#debitExecutionModal");
        modal.data("rec-id", recId);
        $("#debitExecutionModal").modal("show");
    };

    var doExecution = function () {
        var recId = $("#debitExecutionModal").data("rec-id");

        var obj = JSON.stringify({ recId: recId });

        Core.AjaxCall({
            url: "/Obligor/DoExecution",
            data: obj,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_15",
            success: function (response) {
                if (response.isSuccess) {
                    ObligorDetail.LoadData(ObligorTab.Debit, tabCallBackFunc);
                } else {
                    alert("İnfaz gerçekleştirilemedi.");
                }
            },
            complete: function () {
                $("#debitExecutionModal").modal("hide");
            }
        });
    };

    //2. Yapilan exection, protokolun alt tarafinda ki buton
    var showExecutionModal = function () {
        $("#mdlSpecialExecution").modal("show");
    };

    var prepareExecutionModal = function () {

        var allRadios = $("#mdlSpecialExecution input[type='radio']").prop("checked", false);
        $("#spe1").prop("checked", true);
        allRadios.uniform();
    };

    var bindEvents = function () {
        $("#mdlSpecialExecution input[type='radio']").off("change").on("change", function () {
            if (this.id === "spe4" && this.checked === true) {
                $("[data-id='txtOtherReasonForExecution']").show();
            } else {
                $("[data-id='txtOtherReasonForExecution']").hide();
            }
        });
    };

    var checkSpecialExecution = function () {
        var spe4Checked = $("#spe4").prop("checked");
        var reason = "";
        if (spe4Checked) {
            var textbox = $("[data-id='txtOtherReasonForExecution']");
            var length = textbox.val().trim().length;
            if (length < 15 || length > 100) {
                Message.Show("Açıklama en az 15 en çok 100 karakter olmalıdır.", "Hata", Message.ThemeType.Ruby);
                $("[data-id='txtOtherReasonForExecution']").focus();
                return;
            }
            reason = textbox.val().trim();
        } else {
            var selectedId = $("#mdlSpecialExecution input[type='radio']:checked").attr('id');
            reason = $("label[for='" + selectedId + "']").text()
        }

        setApproveSpecialExecution(reason);
    };

    var setApproveSpecialExecution = function (reason) {
        Core.AjaxCall({
            url: "/Obligor/SetApproveSpecialExecution",
            data: JSON.stringify({ obligorId: ObligorId, reason: reason }),
            blockDiv: "#mdlSpecialExecution",
            success: function (response) {
                if (response.isSuccess) {
                    $("#mdlSpecialExecution").modal("hide");
                } else {
                    alert("Dosya infaz edilemedi.");
                    console.log(response.error);
                }
            },
        });
    };

    var init = function () {
        bindEvents();
        prepareExecutionModal();
    };

    return {
        Init: init,
        TabCallBackFunc: tabCallBackFunc,
        ShowExecutionConfirmation: showExecutionConfirmation,
        DoExecution: doExecution,
        ShowExecutionModal: showExecutionModal,
        CheckSpecialExecution: checkSpecialExecution
    }
})();

var Reminder = (function () {

    var bindEvents = function () {
        $("#mainScreenReminderDesc").off("keypress keydown keyup").on("keypress keydown keyup", function () {
            $("#btnMainScreenAddReminder").prop("disabled", $("#mainScreenReminderDesc").val().trim() === "");
        });

        $("#tabReminder").on('shown.bs.modal', function () {
            $('#mainScreenReminderDate').datetimepicker("remove");
            $('#mainScreenReminderDate').data("date", "");
            $('#mainScreenReminderDate').datetimepicker({
                format: 'dd-mm-yyyy hh:ii'
            });

            var reminderOptionCount = $("#mainScreenReminderType option").length;
            if (reminderOptionCount === 0) {
                for (var i in window.reminderTypes) {
                    if (window.reminderTypes.hasOwnProperty(i)) {
                        var reminderType = window.reminderTypes[i];
                        $("#mainScreenReminderType")
                            .append($("<option></option>")
                                .attr("value", reminderType.Id)
                                .text(reminderType.Value));
                    }
                }
            }
        });
        $("#tabReminder").on('hidden.bs.modal', function () {
            $('#mainScreenReminderType option:first-child').attr("selected", "selected");
            $('#mainScreenReminderDesc').val("");
            $('#mainScreenReminderDate').datetimepicker({
                format: 'dd-mm-yyyy hh:ii'
            });
        });
    }

    var addReminder = function () {
        var detail = $("#mainScreenReminderDesc").val().trim();
        var type = $("#mainScreenReminderType").val();
        var date = $("#mainScreenReminderDate").data("date");
        if (date === "") {
            alert("Lütfen tarihi tam olarak seçiniz");
            return;
        }

        Core.AjaxCall({
            url: "/Obligor/InsertReminder",
            data: JSON.stringify({ typeId: type, reminderDate: date, detail: detail, obligorId: ObligorId }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            //blockDiv: "#tblList_3",
            success: function (response) {
                if (response.isSuccess) {
                    Message.Show("Hatırlatıcı başarıyla eklenmiştir.", "Bilgilendirme", Message.ThemeType.Teal);
                    $("#tabReminder").modal("hide");
                } else {
                    console.log(response.error);
                }
            },
            complete: function () {

            },
            error: function () {
                Message.Show("Hatırlatıcı eklenememiştir.", "Bilgilendirme", Message.ThemeType.Ruby);
                $("#tabReminder").modal("hide");
            }
        });
    };

    var getDate = function () {
        var minutes = $($(".datetimepicker-minutes .active")[0]).text();
        var years = $($(".datetimepicker-years .active")[0]).text();
        var months = $($(".datetimepicker-months .active")[0]).text();
        var days = $($(".datetimepicker-days .active")[0]).text();

        var date = days + "-" + months + "-" + years + " " + minutes;
    };

    var showModal = function () {
        $("#tabReminder").modal("show");
    }

    var init = function () {
        bindEvents();
    }
    return {
        AddReminder: addReminder,
        ShowModal: showModal,
        GetDate: getDate,
        Init: init
    }
})();

var FileAttachment = (function () {

    var tabCallBackFunc = function () {
        $("#tblList_16").dataTable($.extend(CommonDataTablesSettings, { "order": [[2, "desc"]] }));
        //bindEvents();
    };

    var openModal = function () {
        $("#tmp16IsGkf").prop("checked", false);
        $("#tmp16ObligorId").val($("#obligorId").val());
        $("#tmp16FileUpload").modal("show");
    }

    var fileUpload = function () {
        var files = $("#tmp16File")[0].files;
        //var myID = 3; //uncomment this to make sure the ajax URL works
        var desc = $("#tmp16FileDesc").val().trim();
        if (desc) {
            if (files.length > 0) {
                if (window.FormData !== undefined) {
                    var data = new FormData();
                    for (var x = 0; x < files.length; x++) {
                        data.append("file" + x, files[x]);
                    }

                    Core.Block("#fileUploadModal .modal-content", null);

                    $.ajax({
                        type: "POST",
                        url: '/Obligor/UploadFile?fileDesc=' + $("#tmp16FileDesc").val() + "&obligorId=" + $("#obligorId").val() + "&isGkf=" + $("#tmp16IsGkf").prop("checked"),
                        contentType: false,
                        processData: false,
                        data: data,
                        //blockDiv: "#fileUploadModal",
                        success: function (result) {
                            if (result.isSuccess) {
                                ObligorDetail.LoadData(ObligorTab.FileAttachment, tabCallBackFunc);
                                Message.Show("Dosya başarıyla yüklendi", "Bilgilendirme", Message.ThemeType.Teal);
                                $("#tmp16FileUpload").modal("hide");
                            } else {
                                alert(result.error);
                            }
                        },
                        error: function (xhr, status, p3, p4) {
                            var err = "Error " + " " + status + " " + p3 + " " + p4;
                            if (xhr.responseText && xhr.responseText[0] == "{")
                                err = JSON.parse(xhr.responseText).Message;
                            console.log(err);
                            $("#tmp16FileUpload").modal("hide");
                        },
                        complete: function () {
                            Core.UnBlock("#fileUploadModal .modal-content", null);
                        }
                    });
                } else {
                    alert("This browser doesn't support HTML5 file uploads!");
                }
            }
        } else {
            alert("Lütfen dosya için açıklama girin");
        }
    }

    var removeFile = function (recId) {
        Core.AjaxCall({
            url: "/Obligor/RemoveFile",
            data: JSON.stringify({ recId: recId }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            blockDiv: "#tblList_15",
            success: function (response) {
                if (response.isSuccess) {
                    ObligorDetail.LoadData(ObligorTab.FileAttachment, tabCallBackFunc);
                } else {
                    alert("Dosya silinemedi.");
                }
            },
            complete: function () {

            }
        });
    }

    var init = function () {

    }

    return {
        Init: init,
        OpenModal: openModal,
        RemoveFile: removeFile,
        FileUpload: fileUpload,
        TabCallBackFunc: tabCallBackFunc,
    }
})();

var File = (function () {

    var init = function () {

    };

    var sendExecutionRequest = function (reason) {
        Core.AjaxCall({
            url: "/Obligor/CloseAccountByObligorId",
            data: JSON.stringify({ obligorId: ObligorId, reason: reason }),
            blockDiv: "#btnMainScreenCloseAccount",
            success: function (response) {
                if (response.isSuccess) {
                    location.reload();
                } else {
                    alert("Dosya infaz edilemedi.");
                    console.log(response.error);
                }
            },
        });
    }

    var closeAccount = function () {
        var result = confirm("Bu dosya infaz edilecek. İnfaz etmek istediğinize emin misiniz?");
        if (result) {
            sendExecutionRequest("");
        }
    };
    return {
        Init: init,
        CloseAccount: closeAccount,
    };
})();

var PostDelivery = (function () {

    var init = function () {
    };

    var sendToOperation = function (id) {
        Core.AjaxCall({
            url: "/Obligor/SendPostaGonderiToOperation",
            data: JSON.stringify({ id: id }),
            blockDiv: "#tab34",
            success: function (response) {
                if (response.isSuccess) {
                    $("#sendOperation_" + id).hide();
                    Message.Show("Posta Gönderisi operasyon birimine gönderildi", "Bilgilendirme", Message.ThemeType.Teal);
                    //location.reload();
                } else {
                    alert("İşlemde hata oluştu.");
                    console.log(response.error);
                }
            },
        });
    };

    return {
        Init: init,
        SendToOperation: sendToOperation
    };
})();

var PaymentPromise = (function () {
    var paymentPromise = function () {
        return {
            PlanAmount: 0.0,
            AdvancePayment: 0.0,
            InstallmentCount: 0,
            Date: '',
            ObligorId: ''
        }
    };

    var init = function () {
    };

    var onModalShown = function () {
        $("#paymentPromiseModal").on("shown.bs.modal", function () {
            $("#txtPaymentPromiseAdvancePayment").focus();
        });
    };

    var showDialog = function (kalanAnapara) {
        resetDialogElements();
        $("#txtPaymentPromisePlanAmount").val(kalanAnapara);
        $("#paymentPromiseModal").modal("show");
        // onModalShown();
    };

    var resetDialogElements = function () {
        $("#txtPaymentPromisePlanAmount").val('');
        $("#txtPaymentPromiseInstall").val('');
        $("#txtPaymentPromiseAdvancePayment").val('');
    };

    var checkValues = function () {
        var date = $("#txtPaymentPromiseDate").val().trim();
        if (date === "") {
            alert("Ödeme Tarihini boş bırakamazsınız!");
            return false;
        }

        return true;
    };

    var getFilledObject = function () {
        var obj = new paymentPromise();
        obj.PlanAmount = $("#txtPaymentPromisePlanAmount").val().trim() ?
            $("#txtPaymentPromisePlanAmount").val().trim() : 0.00;

        obj.AdvancePayment = $("#txtPaymentPromiseAdvancePayment").val().trim() ?
            $("#txtPaymentPromiseAdvancePayment").val().trim() : 0.00;

        obj.InstallmentCount = $("#txtPaymentPromiseInstall").val().trim() ?
            $("#txtPaymentPromiseInstall").val().trim() : 0;

        obj.Date = $("#txtPaymentPromiseDate").val();
        obj.ObligorId = ObligorId;

        return obj;
    };

    var sendToServer = function (model) {
        Core.AjaxCall({
            url: "/Protocol/InsertPaymentPromise",
            data: JSON.stringify(model),
            blockDiv: "#paymentPromiseModal",
            success: function (response) {
                if (response.isSuccess) {
                    $("#paymentPromiseModal").modal("hide");
                    $("#btnPaymentPromise").hide();
                    Message.Show("Ödeme sözü başarıyla kaydedildi.", "Bilgilendirme", Message.ThemeType.Teal);
                } else {
                    alert("İşlemde hata oluştu.");
                    console.log(response.error);
                }
            },
        });
    };

    var submit = function () {
        var checkOk = checkValues();
        if (checkOk) {
            var payProm = getFilledObject();
            sendToServer(payProm);
        }
    };

    return {
        Init: init,
        ShowDialog: showDialog,
        Submit: submit,
        OnModalShown: onModalShown
    };
})();
