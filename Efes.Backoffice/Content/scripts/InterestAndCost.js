﻿var InterestAndCost = (function () {
    var addExcelFile = function () {
        $("#fileupload").fileupload({
            url: "PostInterestAndCostUpload",
            dataType: "json",
            done: function (e, data) {
                if (data.result.Result === "Success") {
                    Message.Show("Dosya Yükleme işlemi başarıyla sonuçlanmıştır.", "Bilgilendirme", Message.ThemeType.Teal);
                } else if (data.result.Result === "Error") {
                    if (data.result.MissingColumnNames) {
                        Message.Show("Dosyada eksik kolonlar bulunmaktadır. Hataları düzelttikten sonra tekrar deneyin.<br/>" + data.result.MissingColumnNames, "", Message.ThemeType.Ruby, true);
                    } else {
                        Message.Show("Dosya yüklenemedi", "Hata", Message.ThemeType.Ruby);
                    }
                }
            },
            progressall: function (e, data) {
                App.blockUI({
                    target: "#fileUploadDiv",
                    boxed: true
                });
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#progress .progress-bar").css(
                    "width",
                    progress + "%"
                );
            },
            error: function (ex) {
                App.unblockUI("#fileUploadDiv");
                console.log(ex);
                alert("Dosya yükleme esnasında hata ile karşılaşıldı!!");
            }
        }).prop("disabled", !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : "disabled");
    };

    var init = function () {
        addExcelFile();
    }
    return {
        AddExcelFile: addExcelFile,
        Init: init
    }
})();