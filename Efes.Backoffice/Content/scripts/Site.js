﻿

var City = (function () {
    function init() {

    };

    function getCitiesByCountryId(id, cbFunc, blockDiv) {
        Core.AjaxCall({
            url: "/Home/GetAllCitiesByCountryId",
            data: JSON.stringify({ countryId: id }),
            dataType: "json",
            blockDiv: "#" + blockDiv,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                cbFunc(data);
            }
        });
    }

    return {
        Init: init,
        GetCitiesByCountryId: getCitiesByCountryId
    }
})();

var County = (function () {
    function init() {

    };

    function getCountiesByCityId(id, cbFunc, blockDiv) {
        Core.AjaxCall({
            url: "/Home/GetAllCountiesByCityId",
            data: JSON.stringify({ cityId: id }),
            dataType: "json",
            blockDiv: "#" + blockDiv,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                cbFunc(data);
            }
        });
    }

    return {
        Init: init,
        GetCountiesByCityId: getCountiesByCityId
    }
})();

var Notification = (function () {
    var Type = {
        Information: 1,
        Warning: 2,
        Error: 3,
        Verification: 4,
        General: 5
    }
    var getNotification = function () {
        Core.AjaxCall({
            url: "/Notification/GetNotification",
            dataType: "json",
            blockDiv: "#header_notification_bar",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data && data.length > 0) {
                    $("#notificationCount").text(data.length);

                    var noteTemp = '<li>' +
                            '<a data-id="[id]" data-from="[from]" data-title="[datatitle]" data-body="[body]" data-function="[function]" href="#">' +
                                '<span class="time">[TimeText]</span>' +
                                '<span class="details" >' +
                                '<span class="label label-sm label-icon label-[IconBgType]">' +
                                '<i class="fa fa-[IconType]"></i></span>[Title]</span>' +
                            '</a>' +
                        '</li>';
                    $("#notificationList").text("");


                    for (var j = 0; j < data.length; j++) {

                        var note = noteTemp.replace("[TimeText]", data[j].TimeText)
                                           .replace("[Title]", data[j].Title)
                                           .replace("[IconType]", data[j].IconType)
                                           .replace("[datatitle]", data[j].Title)
                                           .replace("[from]", data[j].From)
                                           .replace("[body]", data[j].Body)
                                           .replace("[id]", data[j].Id)
                                           .replace("[IconBgType]", data[j].IconBgType);

                        if (data[j].Type === Type.Verification) {
                            note = note.replace("[function]", "Confirmation");
                        } else {
                            note = note.replace("[function]", "Information");
                        }

                        $("#notificationList").append(note);
                        
                    }
                    bindEvents();
                }
                //cbFunc(data);
            }
        });
    };

    function bindEvents() {
        $("#notificationList li a").on("click", function() {
            var element = $(this);
            Notification.CallFunction(element);
        });
    }

    function callFunction(element) {
        var id = element.data("id");
        var from = element.data("from");
        var title = element.data("title");
        var body = element.data("body");
        var func = element.data("function");
        openModal(id, from, title, body, func);
    }

    function openModal(id, from, title, body, func) {
        if (func === "Information") {
            toggleInformation(id, from, title, body);
        } else if (func === "Confirmation") {
            toggleConfirmation(id, from, title, body);
        }
    }

    function toggleConfirmation(id, from, title, body) {
        var modalTemp = $('#confirmationModal');
        var html = $(modalTemp).html();
        $(modalTemp).html(html.replace("{Id}", id).replace("{Title}", title).replace("{From}", from).replace("{Body}", body));
        modalTemp.modal('toggle');

        var confirmButton = $('#confirmationModal #btnConfirm');
        confirmButton.off('click').on('click', function () {
            Core.AjaxCall({
                url: "/Notification/Confirm",
                dataType: "json",
                data: JSON.stringify({ id: id }),
                contentType: "application/json; charset=utf-8",
                success: function () {
                    Notification.GetNotification();
                }
            });
        });

    }

    function toggleInformation(id, from, title, body) {
        var modalTemp = $('#informationModal');
        var html = $(modalTemp).html();
        $(modalTemp).html(html.replace("{Id}", id).replace("{Title}", title).replace("{From}", from).replace("{Body}", body));
        modalTemp.modal('toggle');

        modalTemp.off('hidden.bs.modal').on('hidden.bs.modal', function () {
            Core.AjaxCall({
                url: "/Notification/Read",
                dataType: "json",
                data: JSON.stringify({ id: id }),
                contentType: "application/json; charset=utf-8",
                success: function () {
                    Notification.GetNotification();
                }
            });
        });
    }

    return {
        GetNotification: getNotification,
        ToggleConfirmation: toggleConfirmation,
        ToggleInformation: toggleInformation,
        CallFunction: callFunction
    };

})();

var Feedback = (function () {
    function insertFeedback() {
        var textArea = $('#txtFeedback');
        var feedback = textArea.val();
        Core.AjaxCall({
            url: "/Site/InsertFeedback",
            dataType: "json",
            data: JSON.stringify({ feedback: feedback }),
            contentType: "application/json; charset=utf-8",
            success: function () {
                textArea.val("");
                $("#feedbackModal").modal("hide");
                Message.Show("Sorun başarıyla kayıt altına alınmıstır.", "Bilgilendirme", Message.ThemeType.Teal);
            }
        });
    }

    return {
        InsertFeedback: insertFeedback
    }
})();

var KnowgeDate = (function() { //js date ile karisiyordu :))
    

    var init = function() {
        
    };

    var getToday = function() {
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();

       return (day < 10 ? "0" : "") + day + "-" +
            (month < 10 ? "0" : "") + month + "-" +
            d.getFullYear();
    };
    
    var getDatePlusMonth = function(m) {
        var d = new Date();
        var pYear = 0;

        var month = d.getMonth() + 1 + m;
        while (month > 12) {
            pYear++;
            month = month - 12;
        }
        var day = d.getDate();

        if (month == 2 && day > 28) {
            if ((parseInt(d.getFullYear()) + pYear) % 4 === 0 && day > 29) {
                day = 29;
            } else {
                day = 28;
            }
        }
        if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30) {
            day = 30;
        }

        return (day < 10 ? "0" : "") + day + "-" +
             (month < 10 ? "0" : "") + month + "-" +
             (parseInt(d.getFullYear()) + pYear);
    }

    return {
        Init: init,
        GetToday: getToday,
        GetDatePlusMonth: getDatePlusMonth
    };

})();