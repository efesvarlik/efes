﻿var Authorization = (function () {
    function prepareForUpdateRule(id) {
        var currentTr = $("table").find("[data-id='" + id + "']");
        var ruleName = $(currentTr).find('[data-column="name"]').text().trim();
        var isActive = $(currentTr).find('[data-column="isActive"]').data("value") === "True";

        var txtRule = $("#txtValue");
        var divCb = $("#divCb");
        var ruleId = $("#id");
        var cbIsActive = $("#cbIsActive");
        var btnUpdate = $("#btnUpdate");
        var btnSave = $("#btnSave");
        var btnNew = $("#btnNew");
        var activeDiv = $("#activeDiv");

        btnUpdate.show();
        btnNew.show();
        btnSave.hide();

        divCb.show();
        activeDiv.html(isActive ? "Aktif" : "Pasif");
        $('#cbIsActive').on("change",
            function () {
                activeDiv.html($(this).prop("checked") ? "Aktif" : "Pasif");
            });

        $(ruleId).val(id);
        $(txtRule).val(ruleName);
        $(cbIsActive).prop("checked", isActive);
        location.href = "#txtValue";
        window.scrollTo(0, 0);
    }

    function prepareForUpdateRole(id) {
        var currentTr = $("table").find("[data-id='" + id + "']");
        var roleName = $(currentTr).find('[data-column="name"]').text().trim();
        var closureRate = $(currentTr).find('[data-column="closureRate"]').text().trim();
        var isActive = $(currentTr).find('[data-column="isActive"]').data("value") === "True";

        var txtRole = $("#txtValue");
        var txtClosureRate = $("#txtClosureRate");
        var divCb = $("#divCb");
        var ruleId = $("#id");
        var cbIsActive = $("#cbIsActive");
        var btnUpdate = $("#btnUpdate");
        var btnSave = $("#btnSave");
        var btnNew = $("#btnNew");
        var activeDiv = $("#activeDiv");

        btnUpdate.show();
        btnNew.show();
        btnSave.hide();

        divCb.show();
        activeDiv.html(isActive ? "Aktif" : "Pasif");
        $('#cbIsActive').on("change",
            function () {
                activeDiv.html($(this).prop("checked") ? "Aktif" : "Pasif");
            });

        $(ruleId).val(id);
        $(txtRole).val(roleName);
        $(txtClosureRate).val(closureRate);
        $(cbIsActive).prop("checked", isActive);
        location.href = "#txtValue";
        window.scrollTo(0, 0);
    }

    var initForRule = function () {
        $("#txtValue")[0].focus();
        $("#tblRule").dataTable($.extend(CommonDataTablesSettings, { "pageLength": 20 }));
    }

    var initForRole = function () {
        $("#txtValue")[0].focus();
        $("#tblRole").dataTable($.extend(CommonDataTablesSettings, { "pageLength": 20 }));
    }

    return {
        InitForRule: initForRule,
        InitForRole: initForRole,
        PrepareForUpdateRule: prepareForUpdateRule,
        PrepareForUpdateRole: prepareForUpdateRole
    }
})();