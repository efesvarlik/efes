﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Efes.Entities.User;

namespace Efes.Backoffice.Models
{
    public class UserRoleModel
    {
        public User User { get; set; }
        public List<Role> AllRoles { get; set; }
        public List<Role> UserRoles { get; set; }
        public List<Role> RolesUserDoesNotHave { get; set; }
    }
}