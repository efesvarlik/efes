﻿using System.Collections.Generic;
using Efes.Entities.User;

namespace Efes.Backoffice.Models
{
    public class RoleRuleModel
    {
        public Role Role { get; set; }
        public List<Rule> AllRules { get; set; }
        public List<Rule> RoleRules { get; set; }
        public List<Rule> RulesRoleDoesNotHave { get; set; }
    }
}