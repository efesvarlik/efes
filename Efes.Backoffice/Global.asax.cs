﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using Efes.UIPC;
using Efes.Utility;
using log4net.Repository.Hierarchy;
using log4net;
using log4net.Appender;


namespace Efes.Backoffice
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            log4net.Config.XmlConfigurator.Configure();
            Utilities.SetAdoNetAppenderConnectionStrings();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var cookie = HttpContext.Current.Request.Cookies["boid"];
            if (cookie == null)
            {
                cookie = new HttpCookie("boid", new SessionIDManager().CreateSessionID(HttpContext.Current))
                {
                    HttpOnly = true
                };
                HttpContext.Current.Response.Cookies.Set(cookie);
            }
        }
    }
}
