﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Efes.Entities.Obligor;
using Efes.Entities.Protocol;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember(Rules ="AddProtocolCampaign|ViewProtocolCampaign")]
    public class ProtocolCampaignController : BaseController
    {
        [AuthorizeMember(Rules ="ViewProtocolCampaign")]
        public ActionResult Index()
        {
            var protocolCampaigns = new ProtocolUipc().GetProtocolCampaignModels();
            return View(protocolCampaigns);
        }

        [AuthorizeMember(Rules ="AddProtocolCampaign")]
        public ActionResult Create()
        {
            var model = new ProtocolUipc().GetProtocolCampaignModel();
            return View(model);
        }

        [HttpPost]
        [AuthorizeMember(Rules ="AddProtocolCampaign")]
        public ActionResult Create(ProtocolCampaign protocolCampaignViewModel)
        {
            try
            {
                var roles = new UserUipc().GetRolesHasRightToMakeCampaign();
                var selectedCampaignType = Request.Params["selectedCampaignType"].Convert<int>();
                protocolCampaignViewModel.ProtocolCampaignType = (ProtocolCampaignType) selectedCampaignType;

                foreach (var r in roles)
                {
                    protocolCampaignViewModel.Details.Add(new ProtocolCampaignDetails()
                    {
                        RoleId = r.Id,
                        DiscountPercentage = Request.Params["o_discountPercentage" + r.Id].Replace("% ", "").Convert<int>(),
                        MaxInstallmentCount = Request.Params["o_maxInstallmentCount" + r.Id].Convert<int>(),
                        ObligorType = ObligorType.Obligor
                    });

                    protocolCampaignViewModel.Details.Add(new ProtocolCampaignDetails()
                    {
                        RoleId = r.Id,
                        DiscountPercentage = Request.Params["p_discountPercentage" + r.Id].Replace("% ", "").Convert<int>(),
                        MaxInstallmentCount = Request.Params["p_maxInstallmentCount" + r.Id].Convert<int>(),
                        ObligorType = ObligorType.Guarantor
                    });
                }
                List<string> obligorList =new List<string>();
                if (protocolCampaignViewModel.ProtocolCampaignType == ProtocolCampaignType.ObligorList)
                {
                    var stringSeparators = new string[] { "\r\n" };
                    obligorList = string.IsNullOrWhiteSpace(protocolCampaignViewModel.ObligorList) ? new List<string>() : protocolCampaignViewModel.ObligorList.Split(stringSeparators, StringSplitOptions.None).ToList().Where(c => c.Trim() != "").Select(s => s.Trim()).ToList();
                    protocolCampaignViewModel.PortfolioId = -1;
                    protocolCampaignViewModel.Age = -1;
                }
                
                var success = new ProtocolUipc().CreateProtocolCampaign(protocolCampaignViewModel, obligorList, Authentication.UserInfo.Id);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                Logger.Error("ProtocolCampaignController-Create",ex);
                return null;
            }
        }
    }
}
