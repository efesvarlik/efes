﻿using System;
using System.Configuration;
using System.Net;
using System.Web.Mvc;
using Efes.Models.Obligor.Tabs;
using Efes.UIPC;
using Efes.Utility;

namespace Efes.Backoffice.Controllers
{
    public class ApiController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var authHeader = filterContext.HttpContext.Request.Headers["Authorization"];
            if (authHeader == null)
            {
                HandleUnauthorized(filterContext);
                return;
            }


            if (authHeader.StartsWith("basic", StringComparison.OrdinalIgnoreCase) == false)
            {
                HandleUnauthorized(filterContext);
                return;
            }

            var key = authHeader.Substring("Basic ".Length).Trim();
            var muhasebeKey = ConfigurationManager.AppSettings["muhasebeKey"];

            if (!key.Equals(muhasebeKey))
            {
                HandleUnauthorized(filterContext);
            }
        }


        private void HandleUnauthorized(ActionExecutingContext actionContext)
        {
            actionContext.Result = new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public JsonResult GetObligorInfo(string accountId)
        {
            var muhasebeUserId = ConfigurationManager.AppSettings["muhasebeUserId"];
            var obligorDetail = new ObligorUipc().GetObligorByAccountId(accountId, muhasebeUserId.Convert<int>());
            if (obligorDetail == null)
            {
                return Json(new
                {
                    Result = "Account Detail couldn't find"
                });
            }

            return Json(new
            {
                Bakiye = obligorDetail.BToplam,
                Anapara = obligorDetail.BAnapara,
                Faiz = obligorDetail.BFaiz,
                Masraf = obligorDetail.BMasraf
            });
        }

        [HttpPost]
        public JsonResult InsertNote(string note, string type, string rate, string accountId)
        {
            int sType;
            int sRate;
            if (string.IsNullOrWhiteSpace(note))
            {
                return Json(new
                {
                    Result = "Note detail is empty"
                });
            }

            try
            {
                sType = Convert.ToInt32(type);
                if (0 > sType || sType > 1)
                {
                    return Json(new
                    {
                        Result = "Type should take 0 (normal) or 1 (critical)"
                    });
                }
            }
            catch
            {
                return Json(new
                {
                    Result = "Type should take 0 (normal) or 1 (critical)"
                });
            }

            try
            {
                sRate = Convert.ToInt32(rate);
                if (0 > sRate || sRate > 10)
                {
                    return Json(new
                    {
                        Result = "Rate value should be a value between 0 (lowest) and 10 (highest)"
                    });
                }
            }
            catch
            {
                return Json(new
                {
                    Result = "Rate value should be a value between 0 (lowest) and 10 (highest)"
                });
            }

            var muhasebeUserId = ConfigurationManager.AppSettings["muhasebeUserId"];
            var obligorDetail = new ObligorUipc().GetObligorByAccountId(accountId, muhasebeUserId.Convert<int>());

            if (obligorDetail == null)
            {
                return Json(new
                {
                    Result = "Account couldn't find"
                });
            }

            var sNote = new Note()
            {
                Detay = note,
                Tip = sType
            };

            var result = new ObligorUipc().AddNote(sNote, obligorDetail.Id, sRate.ToString());

            return Json(new { Result = result ? "Succeeded" : "Failed" });
        }
    }
}