﻿using Efes.UIPC;
using Efes.WC;
using System.Linq;
using System.Web.Mvc;
using Efes.Utility;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class ApprovementController : Controller
    {
        private readonly ApprovementUipc _approvementUipc;
        public ApprovementController()
        {
            _approvementUipc = new ApprovementUipc();
        }

        #region Upload Approvement

        [AuthorizeMember(Rules = "ViewMultipleLoads", RedirectController = "Approvement", RedirectAction = "ChangePaymentOwner")]
        public ActionResult MultipleLoadsIndex()
        {
            var model = _approvementUipc.GetUploadApprovementList();
            return View(model);
        }

        [AuthorizeMember(Rules = "ApproveMultipleLoads")]
        public ActionResult ApproveMultipleLoads(string id)
        {
            var model = _approvementUipc.GetUploadApprovementList();
            var type = model.First(c => c.BulkId == id).Type;
            var isSuccess = _approvementUipc.ApproveLoadList(id, Authentication.UserInfo.Id, type);
            model = _approvementUipc.GetUploadApprovementList();
            return View("MultipleLoadsIndex", model);
        }

        #endregion

        #region Change Payment Owner

        [HttpGet]
        [AuthorizeMember(Rules = "ViewChangePaymentOwner", RedirectController = "Approvement", RedirectAction = "ChangeSpecialExecution")]
        public ActionResult ChangePaymentOwner()
        {
            var model = _approvementUipc.GetChangePaymentOwnerList(Authentication.UserInfo.Id);
            return View(model);
        }

        [AuthorizeMember(Rules = "ApproveChangePaymentOwner")]
        public ActionResult AprroveChangePaymentOwner(string id, string recId)
        {
            _approvementUipc.ApproveOrDenyChangePaymentOwner(recId, Authentication.UserInfo.Id, true, "", id.Convert<int>());
            return RedirectToAction("ChangePaymentOwner");
        }

        [HttpPost]
        [AuthorizeMember(Rules = "DenyChangePaymentOwner")]
        public ActionResult DenyChangePaymentOwner(string recId, string id, string desc)
        {
            var result = _approvementUipc.ApproveOrDenyChangePaymentOwner(recId, Authentication.UserInfo.Id, false, desc, id.Convert<int>());
            return Json(new { IsSuccess = result });
        }

        #endregion

        #region Special Execution

        [HttpGet]
        [AuthorizeMember(Rules = "ViewSpecialExecution", RedirectController = "Approvement", RedirectAction = "ChangePaymentOwner")]
        public ActionResult ChangeSpecialExecution()
        {
            var model = _approvementUipc.GetApprovementForSpecialExecution(Authentication.UserInfo.Id);
            return View(model);
        }

        [AuthorizeMember(Rules = "ApproveSpecialExecution")]
        public ActionResult AprroveSpecialExecution(string id, string accountId)
        {
            var result = _approvementUipc.ApproveOrDenySpecialExecution(Authentication.UserInfo.Id, true, "", id.Convert<int>());

            if (result)
            {
                new ExternalMuhasebeUipc().CollactionAlacakGuncelle(accountId, Authentication.UserInfo.Name, Authentication.UserInfo.Id.ToString());
            }

            return RedirectToAction("ChangeSpecialExecution");
        }

        [HttpPost]
        [AuthorizeMember(Rules = "DenySpecialExecution")]
        public ActionResult DenySpecialExecution(string id, string desc)
        {
            var result = _approvementUipc.ApproveOrDenySpecialExecution(Authentication.UserInfo.Id, false, desc, id.Convert<int>());
            return Json(new { IsSuccess = result });
        }

        #endregion

        #region LandRegister

        [HttpGet]
        [AuthorizeMember(Rules = "ViewWaitingLandRegister", RedirectController = "Approvement", RedirectAction = "ChangePaymentOwner")]
        public ActionResult WaitingLandRegister()
        {
            var model = _approvementUipc.GetWaitingLandRegister(Authentication.UserInfo.Id);
            return View(model);
        }

        [AuthorizeMember(Rules = "ApproveLandRegister")]
        public ActionResult AprroveLandRegister(string id)
        {
            _approvementUipc.ApproveOrDenyLandRegister(Authentication.UserInfo.Id, true, id);
            return RedirectToAction("WaitingLandRegister");
        }

        [AuthorizeMember(Rules = "DenyLandRegister")]
        public ActionResult DenyLandRegister(string id)
        {
            _approvementUipc.ApproveOrDenyLandRegister(Authentication.UserInfo.Id, false, id);
            return RedirectToAction("WaitingLandRegister");

        }

        #endregion

        #region SalaryDistraint

        [HttpGet]
        [AuthorizeMember(Rules = "ViewWaitingSalaryDistraint", RedirectController = "Approvement", RedirectAction = "ChangePaymentOwner")]
        public ActionResult WaitingSalaryDistraint()
        {
            var model = _approvementUipc.GetWaitingSalaryDistraint(Authentication.UserInfo.Id);
            return View(model);
        }

        [AuthorizeMember(Rules = "ApproveSalaryDistraint")]
        public ActionResult AprroveSalaryDistraint(string id)
        {
            _approvementUipc.ApproveOrDenySalaryDistraint(Authentication.UserInfo.Id, true, id);
            return RedirectToAction("WaitingSalaryDistraint");
        }

        [AuthorizeMember(Rules = "DenySalaryDistraint")]
        public ActionResult DenySalaryDistraint(string id)
        {
            _approvementUipc.ApproveOrDenySalaryDistraint(Authentication.UserInfo.Id, false, id);
            return RedirectToAction("WaitingSalaryDistraint");

        }

        #endregion
    }
}