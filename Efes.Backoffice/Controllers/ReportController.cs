﻿using Efes.Entities.Report;
using Efes.Models;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Efes.Backoffice.Controllers
{
    public class ReportController : BaseController
    {
        private readonly ReportUipc _reportUipc;
        public ReportController()
        {
            _reportUipc = new ReportUipc();
        }

        [AuthorizeMember(Rules = "AgentKarnesi")]
        public ActionResult AgentKarnesi()
        {
            var model = new AgentKarnesiModel()
            {
                Years = _reportUipc.GetAgentListesiYearlist(),
                UserList = _reportUipc.GetAgentListesiAgentList(Authentication.UserInfo.Id),
                BeklentiTahsilatList = new List<BeklentiTahsilat>(),
                CikarilanGKF = null,
                TahsilatList = new List<Tahsilat>(),
            };
            return View(model);
        }

        [AuthorizeMember(Rules = "AgentKarnesi")]
        [HttpPost]
        public ActionResult AgentKarnesi(string year, string month, string userId)
        {
            var iUserId = userId.Convert<int>();
            var iYear = year.Convert<int>();
            var iMonth = month.Convert<int>();
            var model = new AgentKarnesiModel()
            {
                Years = _reportUipc.GetAgentListesiYearlist(),
                UserList = _reportUipc.GetAgentListesiAgentList(Authentication.UserInfo.Id),
                SelectedAgent = iUserId,
                SelectedYear = iYear,
                SelectedMonth = iMonth,
                BeklentiTahsilatList = _reportUipc.GetBeklentiTahsilat(iUserId, iMonth, iYear),
                CikarilanGKF = _reportUipc.GetCikarilanGkf(iUserId, iMonth, iYear),
                TahsilatList = _reportUipc.GetTahsilatList(iUserId, iMonth, iYear)
            };
            return View(model);
        }
    }
}