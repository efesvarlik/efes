﻿using Efes.Models.Law;
using Efes.UIPC;
using Efes.WC;
using System.Web.Mvc;

namespace Efes.Backoffice.Controllers
{
    public class LawOfficeController : BaseController
    {
        private readonly LawOfficeUipc _lawOfficeUipc;
        private readonly LocationUipc _locationUipc;
        public LawOfficeController()
        {
            _lawOfficeUipc = new LawOfficeUipc();
            _locationUipc = new LocationUipc();
        }
        // GET: LawOffice
        [AuthorizeMember(Rules = "ViewOfficeList")]
        public ActionResult Index()
        {
            var model = _lawOfficeUipc.GetLawOfficeModelList(Authentication.UserInfo.Id);
            return View(model);
        }

        // GET: LawOffice/Details/5
        [AuthorizeMember(Rules = "ViewOfficeDetails")]
        public ActionResult Details(int id)
        {
            var model = _lawOfficeUipc.GetLawOfficeById(id);
            return View(model);
        }

        // GET: LawOffice/Create
        [AuthorizeMember(Rules = "CreateOffice")]
        public ActionResult Create()
        {
            var model = new OfficeModel();
            model.CityList = _locationUipc.GetCities();
            return View(model);
        }

        // POST: LawOffice/Create
        [HttpPost]
        [AuthorizeMember(Rules = "CreateOffice")]
        public ActionResult Create(OfficeModel officeModel)
        {
            try
            {
                var success = _lawOfficeUipc.CreateLawOffice(officeModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LawOffice/Edit/5
        [AuthorizeMember(Rules = "EditOffice")]
        public ActionResult Edit(int id)
        {
            var model = _lawOfficeUipc.GetLawOfficeById(id);
            model.CityList = _locationUipc.GetCities();
            return View(model);
        }

        // POST: LawOffice/Edit/5
        [HttpPost]
        [AuthorizeMember(Rules = "EditOffice")]
        public ActionResult Edit(int id, OfficeModel officeModel)
        {
            try
            {
                var success = _lawOfficeUipc.UpdateLawOffice(officeModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LawOffice/Delete/5
        [AuthorizeMember(Rules = "DeleteOffice")]
        public ActionResult Delete(int id)
        {
            var model = _lawOfficeUipc.GetLawOfficeById(id);
            return View(model);
        }

        // POST: LawOffice/Delete/5
        [HttpPost]
        [AuthorizeMember(Rules = "DeleteOffice")]
        public ActionResult Delete(int id, OfficeModel officeModel)
        {
            try
            {
                officeModel.IsActive = false;
                var success = _lawOfficeUipc.UpdateLawOffice(officeModel);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        
    }
}
