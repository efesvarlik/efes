﻿using Efes.Entities.LawOffice;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;
using System.Web.Mvc;

namespace Efes.Backoffice.Controllers
{
    public class LawOfficeUserController : BaseController
    {
        private readonly LawOfficeUserUipc _lawOfficeUserUipc;
        public LawOfficeUserController()
        {
            _lawOfficeUserUipc = new LawOfficeUserUipc();
        }

        [HttpGet]
        [AuthorizeMember(Rules = "ViewLawOfficeUser")]
        public ActionResult Index(int id)
        {
            var model = _lawOfficeUserUipc.GetOfficeUserAssignmentModel(id);
            return View(model);
        }

        [AuthorizeMember(Rules = "UpdateLawOfficeUser")]
        public JsonResult InsertOrUpdateLawOfficeUser(string userId, string lawOfficeId, bool assigned)
        {
            var officeUser = new OfficeUser()
            {
                UserId = userId.Convert<int>(),
                LawOfficeId = lawOfficeId.Convert<int>(),
                IsActive = true
            };
            var result = _lawOfficeUserUipc.InsertOrUpdateLawOfficeUser(officeUser, Authentication.UserInfo.Id, assigned);
            return Json(new { Result = result ? "Success" : "Error" });
        }

    }
}