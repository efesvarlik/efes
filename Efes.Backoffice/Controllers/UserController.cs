﻿using System;
using System.Net;
using System.Web.Mvc;
using Efes.Entities.User;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;
using System.Collections.Generic;
using Efes.Models;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class UserController : BaseController
    {
        private readonly UserUipc _userUipc;

        public UserController()
        {
            _userUipc = new UserUipc();
        }

        #region Results
        [HttpGet]
        [AuthorizeMember(Rules = "ViewUsers")]
        public ActionResult Index()
        {
            var model = _userUipc.GetAllUsers();
            return View(model);
        }

        [HttpGet]
        [AuthorizeMember(Rules = "ViewUserDetails")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = _userUipc.GetUser(id.Convert<int>());
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpGet]
        [AuthorizeMember(Rules = "CreateUser")]
        public ActionResult Create()
        {
            var takimLideriList = _userUipc.GetUsersByRole(Roles.TahsilatTakimLideri);
            var hukukTakimLideriList = _userUipc.GetUsersByRole(Roles.HukukDisBuroTakimLideri);
            CreateUserModel model = new CreateUserModel();
            model.TeamLeaderList.AddRange(takimLideriList);
            model.TeamLeaderList.AddRange(hukukTakimLideriList);
            return View(model);
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeMember(Rules = "CreateUser")]
        public ActionResult Create(CreateUserModel createUserModel)
        {
            if (ModelState.IsValid)
            {
                createUserModel.User.CreatedBy = Authentication.UserInfo.LoginName;
                createUserModel.User.Description = string.Format("{0} tarafından oluşturuldu.", Authentication.UserInfo.Name);
                var password = Guid.NewGuid().ToString().Substring(0, 8);
                createUserModel.User.Password = password.Base64Encode();
                var success = _userUipc.InserUser(createUserModel.User, Authentication.UserInfo.Id);
                if (success)
                {
                    if (!string.IsNullOrWhiteSpace(createUserModel.User.Email))
                    {
                        MailManager.Send("Efes Kullanıcı Bilgileriniz", createUserModel.User.Email, "Kullanıcı Adınız : " +
                            createUserModel.User.LoginName + "<br/> Şifreniz : " + password);
                    }
                }

                return RedirectToAction("Index");
            }
            else
            {
                var takimLideriList = _userUipc.GetUsersByRole(Roles.TahsilatTakimLideri);
                var hukukTakimLideriList = _userUipc.GetUsersByRole(Roles.HukukDisBuroTakimLideri);
                createUserModel.TeamLeaderList.AddRange(takimLideriList);
                createUserModel.TeamLeaderList.AddRange(hukukTakimLideriList);
            }

            return View(createUserModel);
        }

        [HttpGet]
        [AuthorizeMember(Rules = "EditUser")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = _userUipc.GetUser(id.Convert<int>());
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeMember(Rules = "EditUser")]
        public ActionResult Edit([Bind(Include = "Name,ImagePath,LoginName,Email,IsActive")] User user, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (ModelState.IsValid)
            {
                user.Id = id.Convert<int>();
                user.UpdatedBy = Authentication.UserInfo.LoginName;
                var success = _userUipc.UpdateUser(user, user.Id);
                if (success)
                {
                    var userReal = _userUipc.GetUser(user.Id);
                    if (userReal.Email != user.Email)
                    {
                        MailManager.Send("Efes Kullanıcı Email Degişikliği", userReal.Email, "Email adresiniz '" + user.Email + "' olarak değiştirilmiştir.");
                    }
                }
                
                return RedirectToAction("Index");
            }
            return View(user);
        }

        #endregion

    }
}
