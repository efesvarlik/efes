﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web.Mvc;
using Efes.Entities.Excel;
using Efes.Entities.User;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;
using Exception = System.Exception;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class ExcelController : BaseController
    {
        #region Private Instances

        private DateTime CurrentDate { get; set; }
        private readonly UserUipc _userUipc;

        #endregion

        #region Contructor

        public ExcelController()
        {
            _userUipc = new UserUipc();
        }

        #endregion

        #region Result

        [AuthorizeMember(Rules = "AddAddressExcel")]
        public ActionResult AddAddressExcel()
        {
            ViewBag.Caption = "Adres Ekleme";
            ViewBag.Type = "Address";
            return View();
        }

        [AuthorizeMember(Rules = "AddNoteExcel")]
        public ActionResult AddNoteExcel()
        {
            ViewBag.Caption = "Kritik Not Ekleme";
            ViewBag.Type = "Note";
            return View();
        }

        [AuthorizeMember(Rules = "AddPhoneExcel")]
        public ActionResult AddPhoneExcel()
        {
            ViewBag.Caption = "Telefon Ekleme";
            ViewBag.Type = "Phone";
            return View();
        }

        [AuthorizeMember(Rules = "AttachBulkFile")]
        public ActionResult AttachBulkFile()
        {
            ViewBag.Caption = "Toplu Dosya Atama";
            ViewBag.Type = "BulkFileAttach";
            return View();
        }

        [AuthorizeMember(Rules = "AddLandRegister")]
        public ActionResult LandRegister()
        {
            ViewBag.Caption = "Tapu Kaydı Ekleme";
            ViewBag.Type = "LandRegister";
            return View();
        }

        [HttpPost]
        [AuthorizeMember(Rules = "AddLandRegister")]
        public ActionResult PostLandRegister()
        {
            const string cacheName = "PostLandRegister";
            //Eger dosya yoksa hata don
            if (Request.Files.Count == 0)
            {
                return Json(new
                {
                    Result = "Error",
                    Message = "File is missing."
                }, JsonRequestBehavior.AllowGet);
            }

            var fileName = Request.Files[0].FileName;

            var necessaryColumnList = new List<string>() { "TCKN" };

            var resultList = ExcelHelper.ExcelImport(
                necessaryColumnList,
                Request,
                Authentication.UserInfo.Id,
                cacheName);

            if (resultList.Any())
            {
                return Json(new
                {
                    Result = "Error",
                    MissingColumnNames = string.Join(",", resultList)
                });
            }

            var dt = CacheManager.Default.Get(cacheName + Request.BoUniqueId()) as DataTable;
            if (dt.HasData())
            {
                if (dt != null)
                {
                    var tempList = (dt.Columns.Cast<DataColumn>()
                        .Where(column => !necessaryColumnList.Contains(column.ColumnName.Trim()))
                        .Select(column => column.ColumnName)).ToList();

                    foreach (var columnname in tempList)
                    {
                        dt.Columns.Remove(columnname);
                    }
                }

                var result = new FileManagerUipc().LandRegister(dt, Authentication.UserInfo.Id, fileName);

                return Json(new { Result = result ? "Success" : "Error" });
            }

            CacheManager.Default.Remove(cacheName + Request.BoUniqueId());
            return Json(new { Result = "Success" });
        }

        [AuthorizeMember(Rules = "AddSalaryDistraint")]
        public ActionResult SalaryDistraint()
        {
            ViewBag.Caption = "Maaş Haczi Ekleme";
            ViewBag.Type = "SalaryDistraint";
            return View();
        }

        [HttpPost]
        [AuthorizeMember(Rules = "AddSalaryDistraint")]
        public ActionResult PostSalaryDistraint()
        {
            const string cacheName = "PostLandRegister";
            //Eger dosya yoksa hata don
            if (Request.Files.Count == 0)
            {
                return Json(new
                {
                    Result = "Error",
                    Message = "File is missing."
                }, JsonRequestBehavior.AllowGet);
            }

            var necessaryColumnList = new List<string>() { "TCKN" };

            var resultList = ExcelHelper.ExcelImport(
                necessaryColumnList,
                Request,
                Authentication.UserInfo.Id,
                cacheName);

            if (resultList.Any())
            {
                return Json(new
                {
                    Result = "Error",
                    MissingColumnNames = string.Join(",", resultList)
                });
            }

            var fileName = Request.Files[0].FileName;

            var dt = CacheManager.Default.Get(cacheName + Request.BoUniqueId()) as DataTable;
            if (dt.HasData())
            {
                if (dt != null)
                {
                    var tempList = (dt.Columns.Cast<DataColumn>()
                        .Where(column => !necessaryColumnList.Contains(column.ColumnName.Trim()))
                        .Select(column => column.ColumnName)).ToList();

                    foreach (var columnname in tempList)
                    {
                        dt.Columns.Remove(columnname);
                    }
                }

                var result = new FileManagerUipc().SalaryDistraint(dt, Authentication.UserInfo.Id, fileName);

                return Json(new { Result = result ? "Success" : "Error" });
            }

            CacheManager.Default.Remove(cacheName + Request.BoUniqueId());
            return Json(new { Result = "Success" });
        }

        [HttpPost]
        [AuthorizeMember(Rules = "AddAddressExcel|AddNoteExcel|AddPhoneExcel")]
        public ActionResult FileImport(string type)
        {
            if (Request.Files.Count == 0)
            {
                return Json(new { Result = "Error", Message = "File is missing." }, JsonRequestBehavior.AllowGet);
            }

            switch (type)
            {
                case "Address":
                    return Import(DataImportType.Address);
                case "Note":
                    return Import(DataImportType.Note);
                case "Phone":
                    return Import(DataImportType.Phone);
                case "BulkFileAttach":
                    return Import(DataImportType.AttachBulkFile);
            }

            return Json(new { Result = "Error", Message = "File Type is not recognized or declared." }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeMember(Rules = "InterestAndCostUpload")]
        public ActionResult InterestAndCostUpload()
        {
            ViewBag.Caption = "Faiz Ve Masraf Yükleme";
            return View();
        }

        [HttpPost]
        [AuthorizeMember(Rules = "InterestAndCostUpload")]
        public ActionResult PostInterestAndCostUpload()
        {
            const string cacheName = "InterestAndCostUpload";
            //Eger dosya yoksa hata don
            if (Request.Files.Count == 0)
            {
                return Json(new { Result = "Error", Message = "File is missing." }, JsonRequestBehavior.AllowGet);
            }

            var necessaryColumnList = new List<string>() { "AccountId", "Faiz", "Masraf" };

            var resultList = ExcelHelper.ExcelImport(necessaryColumnList, Request, Authentication.UserInfo.Id, cacheName);
            if (resultList.Any())
            {
                return Json(new { Result = "Error", MissingColumnNames = string.Join(",", resultList) });
            }

            var dt = CacheManager.Default.Get(cacheName + Request.BoUniqueId()) as DataTable;
            if (dt.HasData())
            {
                if (dt != null)
                {
                    var tempList = (dt.Columns.Cast<DataColumn>()
                        .Where(column => !necessaryColumnList.Contains(column.ColumnName.Trim()))
                        .Select(column => column.ColumnName)).ToList();

                    foreach (var columnname in tempList)
                    {
                        dt.Columns.Remove(columnname);
                    }
                }

                var result = new FileManagerUipc().InterestAndCostUpload(dt, Authentication.UserInfo.Id);

                return Json(new { Result = result ? "Success" : "Error" });
            }

            CacheManager.Default.Remove(cacheName + Request.BoUniqueId());
            return Json(new { Result = "Success" });
        }

        #endregion

        #region Private Methods

        private List<User> GetResponsibleEmailList()
        {
            return _userUipc.GetUserListHasApproveRight();
        }
        private void SendApprovementEmails()
        {
            var memberList = GetResponsibleEmailList();
            if (memberList.Any())
            {
                foreach (var member in memberList)
                {
                    var hashString = string.Format("{0}_{1}_{2}", CurrentDate, Authentication.UserInfo.Id, member.Email);
                    var aTemp = string.Format("<a href={0}/Notification/ConfirmByEmail/{1}>Evet</a>", Request.ApplicationPath, Cryptography.Encrypt(hashString));
                    MailManager.Send("Excel adres yuklemesi yapildi - " + Authentication.UserInfo.Name, member.Email, "Onayliyor musunuz?" + aTemp);
                }
            }
        }
        private bool InsertToDb(DataImportType dataImportType, DataTable dataTable)
        {
            try
            {
                return new FileManagerUipc().FileUpload(dataImportType, dataTable, Authentication.UserInfo.Id);
            }
            catch (Exception ex)
            {
                Logger.Error("Dosya yukleme islemi esnasinda hata olusmustur.", ex);
                return false;
            }
        }
        private List<string> GetNecessaryColumnList(DataImportType dataImportType)
        {
            List<string> list = new List<string>();
            switch (dataImportType)
            {
                case DataImportType.Address:
                    list.Add("ACCOUNTID");
                    list.Add("TCKN");
                    list.Add("ADRES");
                    list.Add("IL");
                    list.Add("ILCE");
                    break;
                case DataImportType.Note:
                    list.Add("ACCOUNTID");
                    list.Add("TCKN");
                    list.Add("NOT");
                    break;
                case DataImportType.Phone:
                    list.Add("ACCOUNTID");
                    list.Add("TCKN");
                    list.Add("TELEFON");
                    break;
                case DataImportType.Portfolio:
                    break;
                case DataImportType.AttachBulkFile:
                    list.Add("ACCOUNTID");
                    list.Add("AGENT");
                    break;
            }
            return list;
        }
        private List<string> ExcelImport(List<string> necessaryColumnList)
        {

            DataSet ds = new DataSet();
            var httpPostedFileBase = Request.Files[0];
            if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
            {
                string fileExtension = System.IO.Path.GetExtension(Request.Files[0].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    var mapPath = Server.MapPath("~/ImportedExcelFiles/");
                    bool exists = System.IO.Directory.Exists(mapPath);
                    if (!exists)
                    {
                        System.IO.Directory.CreateDirectory(mapPath);
                    }
                    string fileLocation = string.Format("{0}{1}_{2}_{3}", mapPath,
                        Request.Files[0].FileName, Authentication.UserInfo.Id, DateTime.Now.ToLongDateString());
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files[0].SaveAs(fileLocation);

                    string excelConnectionString = string.Empty;
                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        switch (fileExtension)
                        {
                            case ".xls":
                                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                                                        fileLocation +
                                                        ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                                break;
                            case ".xlsx":
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                                        fileLocation +
                                                        ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                break;
                        }
                    }
                    else
                    {
                        return null;
                    }

                    var excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();

                    var dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    var excelSheets = new String[dt.Rows.Count];

                    int t = 0;
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    var excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("SELECT * FROM [{0}]", excelSheets[0]);
                    using (var dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }


                    CacheManager.Default.Add("ImportedExcelFile" + Request.BoUniqueId(), ds.Tables[0]);
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        necessaryColumnList.Remove(column.ColumnName);
                    }

                    excelConnection.Close();
                    excelConnection.Dispose();

                }
            }
            return necessaryColumnList;
        }
        private ActionResult Import(DataImportType dataImportType)
        {
            var necessaryColumnList = GetNecessaryColumnList(dataImportType);

            var resultList = ExcelImport(necessaryColumnList);
            if (resultList.Any())
            {
                return Json(new { Result = "Error", MissingColumnNames = string.Join(",", resultList) });
            }

            var dt = CacheManager.Default.Get("ImportedExcelFile" + Request.BoUniqueId()) as DataTable;
            if (dt.HasData())
            {
                necessaryColumnList = GetNecessaryColumnList(dataImportType);
                var tempList = (from DataColumn column in dt.Columns where !necessaryColumnList.Contains(column.ColumnName.Trim()) select column.ColumnName).ToList();

                foreach (var columnname in tempList)
                {
                    dt.Columns.Remove(columnname);
                }

                var result = InsertToDb(dataImportType, dt);
                CacheManager.Default.Remove("ImportedExcelFile" + Request.BoUniqueId());

                return Json(result ? new { Result = "Success" } : new { Result = "Error", });
            }
            CacheManager.Default.Remove("ImportedExcelFile" + Request.BoUniqueId());
            return null;

        }

        #endregion
    }
}