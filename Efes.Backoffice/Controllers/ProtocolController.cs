﻿using System;
using System.Web.Mvc;
using Efes.Entities.Protocol;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class ProtocolController : BaseController
    {
        private readonly ProtocolUipc _protocolUipc;
        public ProtocolController()
        {
            _protocolUipc = new ProtocolUipc();
        }

        [AuthorizeMember(Rules ="ViewProtocolViewList")]
        public ActionResult Index()
        {
            var model = _protocolUipc.GetProtocolApprovements(Authentication.UserInfo.Id);
            return View(model);
        }

        [AuthorizeMember(Rules ="ApproveWaitingProtocol")]
        public ActionResult Approve(string protocolId)
        {
            var result = _protocolUipc.ApproveProtocol(protocolId.Convert<int>(), Authentication.UserInfo.Id);
            if (result)
            {
                this.ViewBag.Message = "Kayıt başarıyla onaylandı.";
                this.ViewBag.MessageType = "success";
            }
            else
            {
                this.ViewBag.Message = "Kayıt onaylanirken hata oluştu.";
                this.ViewBag.MessageType = "success";
            }
            ModelState.AddModelError("", "Already outbid");
            return RedirectToAction("Index");
        }

        [AuthorizeMember(Rules ="CancelWaitingProtocol")]
        public ActionResult Cancel(string protocolId)
        {
            var result = _protocolUipc.CancelProtocol(protocolId.Convert<int>(), Authentication.UserInfo.Id);
            if (result)
            {
                this.ViewBag.Message = "Kayıt başarıyla iptal edildi.";
                this.ViewBag.MessageType = "success";
            }
            else
            {
                this.ViewBag.Message = "Kayıt iptal edilirken hata oluştu.";
                this.ViewBag.MessageType = "success";
            }
            return RedirectToAction("Index");
        }

        [AuthorizeMember(Rules = "DailyProtocolList")]
        public ActionResult DailyProtocolList()
        {
            var model = _protocolUipc.DailyProtocolList(Authentication.UserInfo.Id);
            return View(model);
        }

        [AuthorizeMember(Rules = "InsertPaymentPromise")]
        public ActionResult InsertPaymentPromise(PaymentPromise paymentPromise)
        {
            paymentPromise.UserId = Authentication.UserInfo.Id;
            var result = _protocolUipc.InsertPaymentPromise(paymentPromise);
            return Json(new { isSuccess = result });
        }

        [AuthorizeMember(Rules = "CancelPaymentPromise|CancelOwnPaymentPromise")]
        public ActionResult CancelPaymentPromise(string promiseId)
        {
            var result = _protocolUipc.CancelPaymentPromise(Authentication.UserInfo.Id, promiseId.Convert<int>());
            return Json(new { isSuccess = result });
        }

        [AuthorizeMember(Rules = "ViewPaymentPromiseList")]
        public ActionResult PaymentPromiseList(string startDate, string endDate)
        {
            DateTime sDate;
            DateTime eDate;
            if (string.IsNullOrWhiteSpace(startDate) || string.IsNullOrWhiteSpace(endDate))
            {
                sDate = DateTime.Now.AddDays(-7);
                eDate = DateTime.Now;
            }
            else
            {
                sDate = startDate.Convert<DateTime>();
                eDate = endDate.Convert<DateTime>();

                if (sDate == default(DateTime) || eDate == default(DateTime))
                {
                    sDate = DateTime.Now.AddDays(-7);
                    eDate = DateTime.Now;
                }
            }

            ViewBag.SDate = sDate.ToString("dd-MM-yyyy");
            ViewBag.EDate = eDate.ToString("dd-MM-yyyy");

            var model = _protocolUipc.GetPaymentPromiseReport(Authentication.UserInfo.Id, sDate, eDate);
            return View(model);
        }
    }
}