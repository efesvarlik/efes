﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Efes.Entities.Fgs;
using Efes.Entities.Obligor;
using Efes.Entities.Protocol;
using Efes.Models;
using Efes.Models.Obligor;
using Efes.Models.Obligor.Tabs;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Phone = Efes.Models.Obligor.Tabs.Phone;

namespace Efes.Backoffice.Controllers
{
    [EfesAuthorize]
    [AuthorizeMember]
    public class ObligorController : BaseController
    {
        private readonly DebitUipc _debitUipc;
        private readonly ProtocolUipc _protocolUipc;
        private readonly LocationUipc _locationUipc;
        private readonly FgsUipc _fgsUipc;
        private readonly ReminderUipc _reminderIUipc;
        private readonly ObligorUipc _obligorUipc;
        private readonly GkfUipc _gkfUipc;

        private const decimal DependentScore = 1;
        private const decimal ScoreMultiplier = 1.15M;

        public ObligorController()
        {
            _debitUipc = new DebitUipc();
            _protocolUipc = new ProtocolUipc();
            _locationUipc = new LocationUipc();
            _fgsUipc = new FgsUipc();
            _reminderIUipc = new ReminderUipc();
            _obligorUipc = new ObligorUipc();
            _gkfUipc = new GkfUipc();
        }

        #region General Methods

        [AuthorizeMember(Rules = "ViewObligorDetail")]
        public ActionResult Detail(string accountId, string contactId)
        {
            var obligorModel = _obligorUipc.GetObligorByAccountId(accountId, contactId, Authentication.UserInfo.Id, Authentication.UserInfo.Roles);
            if (obligorModel != null)
            {
                obligorModel.SmsTemplateDictionary = new Dictionary<string, string>
                {
                    {"<TKDISPLAYNAME>", (obligorModel.Name + " " + obligorModel.Surname).Trim()},
                    {"<TKAGENTTELNO>", (String.IsNullOrWhiteSpace(Authentication.UserInfo.PhoneNumber) ? "02165384600" : "0" + Authentication.UserInfo.PhoneNumber)},
                    {"<TKSOURCETEXT>", obligorModel.Portfoy},
                    {"<TKACCOUNTID>", obligorModel.AccountId},
                };
            }
            else
            {
                return Redirect(HttpUtility.HtmlDecode("/"));
            }

            return View(obligorModel);
        }

        [AuthorizeMember(Rules = "SearchObligorList")]
        public ActionResult SearchObligorList(string searchTerm, string searchType)
        {
            var obligorSearchType = (ObligorSearchType)searchType.Convert<int>();
            var userId = Authentication.UserInfo.Id;
            var model = new ObligorUipc().GetObligorList(searchTerm, obligorSearchType, userId);
            return View(model);
        }

        [AuthorizeMember(Rules = "TabNote|TabAllPortfolioDebt|TabSmsHistory|TabCommunicationHistory|" +
                                 "TabProtocolHistory|TabActiveProtocolInstallments|TabObligorAllPayments|TabPhone|" +
                                 "TabAddress|TabObligorDebts|TabFileAttachments|TabAllPeopleRelatedWithDebt|TabFileOwnershipHistory|TabMailShipments|" +
                                 "TabTeminat|TabHacizliMal|TabKkbDetail")]
        public ActionResult GetTabContent(int? tabId, string obligorId)
        {
            try
            {
                var model = new Object();
                var obligorUipc = new ObligorUipc();
                var userId = Authentication.UserInfo.Id;
                switch (tabId)
                {
                    case 1:
                        model = obligorUipc.GetYapilacakIsler(userId, obligorId);
                        break;
                    case 2:
                        model = obligorUipc.GetTumPortfoydekiBorclari(userId, obligorId);
                        break;
                    case 3:
                        model = obligorUipc.GetNot(userId, obligorId);
                        break;
                    case 4:
                        model = obligorUipc.GetSmsHistory(userId, obligorId);
                        break;
                    case 5:
                        model = obligorUipc.GetIletisimTarihce(userId, obligorId);
                        break;
                    case 6:
                        model = obligorUipc.GetHpTeminat(userId, obligorId);
                        break;
                    case 7:
                        model = obligorUipc.GetHpHacizliMal(userId, obligorId);
                        break;
                    case 9:
                        model = obligorUipc.GetProtokolTarihce(userId, obligorId);
                        break;
                    case 10:
                        model = obligorUipc.GetAktifProtokolTaksit(userId, obligorId);
                        break;
                    case 11:
                        model = obligorUipc.GetAllPayments(userId, obligorId);
                        break;
                    case 12:
                        model = obligorUipc.GetPhones(userId, obligorId);
                        break;
                    case 13:
                        model = obligorUipc.GetAdres(userId, obligorId);
                        break;
                    case 14:
                        model = obligorUipc.GetKisiBilgi(userId, obligorId);
                        break;
                    case 15:
                        model = obligorUipc.GetBorc(userId, obligorId);
                        break;
                    case 16:
                        model = obligorUipc.GetDosyaEk(userId, obligorId);
                        break;
                    case 17:
                        model = obligorUipc.GetIslemTarihce(userId, obligorId);
                        break;
                    case 21:
                        model = obligorUipc.GetYapilacakIsAdmin(userId, obligorId);
                        break;
                    case 29:
                        model = obligorUipc.GetKisininIliskideOlduguTumBorc(userId, obligorId);
                        break;
                    case 30:
                        model = obligorUipc.GetBorcunIliskideOlduguTumKisi(userId, obligorId);
                        break;
                    case 33:
                        model = obligorUipc.GetDosyaSahiplikTarihce(userId, obligorId);
                        break;
                    case 34:
                        model = obligorUipc.GetPostaGonderi(userId, obligorId);
                        break;
                    case 35:
                        model = obligorUipc.GetPriorityGroupTracking(userId, obligorId);
                        break;
                    case 36:
                        model = obligorUipc.GetKkbScoreList(userId, obligorId);
                        break;
                    case 37:
                        model = obligorUipc.GetObligorProcessHistoryList(userId, obligorId);
                        break;

                }
                return Json(new
                {
                    data = model,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                ex.Log(string.Format("TabId: {0}, ObligorId: {1}", tabId, obligorId));
                return Json(new
                {
                    error = ex,
                    isSuccess = false
                });
            }
        }

        [AuthorizeMember(Rules = "CloseAccount")]
        public ActionResult CloseAccountByObligorId(string obligorId)
        {
            var result = _obligorUipc.CloseAccountByObligorId(obligorId, Authentication.UserInfo.Id, 
                Authentication.UserInfo.Name);
            return Json(new { isSuccess = result });
        }

        [AuthorizeMember(Rules = "ViewSpecialExecution")]
        public ActionResult SetApproveSpecialExecution(string obligorId, string reason)
        {
            var result = _obligorUipc.SetApproveSpecialExecution(obligorId, Authentication.UserInfo.Id, reason);
            return Json(new { isSuccess = result });
        }

        public ActionResult SendPostaGonderiToOperation(string id)
        {
            var result = _obligorUipc.SendPostaGonderiToOperation(Authentication.UserInfo.Id, id);
            return Json(new { isSuccess = result });
        }

        #endregion

        #region Note

        [AuthorizeMember(Rules = "AddNote")]
        public ActionResult AddNote(Note note, string obligorId, string rate)
        {
            if (!string.IsNullOrWhiteSpace(note.Detay))
            {
                note.UserId = Authentication.UserInfo.Id;
                return Json(new { isSuccess = _obligorUipc.AddNote(note, obligorId, rate) });
            }
            return Json(new { isSuccess = false, message = "EmptyDetail" });
        }

        [AuthorizeMember(Rules = "DeleteNote")]
        public ActionResult DeleteNote(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var userId = Authentication.UserInfo.Id;
                var deleteNoteResult = _obligorUipc.DeleteNote(id, userId);
                if (deleteNoteResult != null)
                {
                    return Json(new { isSuccess = deleteNoteResult.IsDeleted, message = deleteNoteResult.Message });
                }
                else
                {
                    return Json(new { isSuccess = false, message = "Not silinemedi." });
                }
            }
            return Json(new { isSuccess = false, message = "Not silinemedi." });
        }

        public ActionResult AddCallResult(int resultId, string resultText, string phone, string obligorId)
        {
            var result = _obligorUipc.AddCallResult(Authentication.UserInfo.Id, phone, obligorId, resultId, resultText);
            return Json(new { isSuccess = result });
        }

        #endregion

        #region Sms

        [AuthorizeMember(Rules = "SendSms")]
        public ActionResult InsertSms(string obligorId, string number, string tempId, string smsText)
        {
            var isSuccess = new ObligorUipc().InsertSms(Authentication.UserInfo.Id, number, smsText, obligorId);
            return Json(new { isSuccess = isSuccess });
        }

        public ActionResult GetGsmNumbers(string obligorId)
        {
            return Json(new ObligorUipc().GetGsmNumbers(Authentication.UserInfo.Id, obligorId));
        }

        #endregion

        #region CommunicationHistory

        [AuthorizeMember(Rules = "ViewCommunicationHistory")]
        public ActionResult SetRating(string obligorId, string communicationHistoryId, string score)
        {
            var isSuccess = new ObligorUipc().SetRating(Authentication.UserInfo.Id, obligorId, communicationHistoryId, score.Convert<int>());
            return Json(new { isSuccess = isSuccess });
        }

        #endregion

        #region Phone

        [AuthorizeMember(Rules = "AddPhone")]
        public ActionResult AddPhone(Phone phone, string obligorId)
        {
            phone.Creator = Authentication.UserInfo.LoginName;
            return Json(new { isSuccess = new ObligorUipc().AddPhone(phone, obligorId) });
        }

        [AuthorizeMember(Rules = "EditPhone")]
        public ActionResult UpdatePhone(Phone phone)
        {
            phone.Updater = Authentication.UserInfo.LoginName;
            return Json(new { isSuccess = new ObligorUipc().UpdatePhone(phone) });
        }

        [AuthorizeMember(Rules = "EditPhone")]
        public ActionResult GetPhone(string id)
        {
            var phoneDetails = new ObligorUipc().GetPhone(id);
            return Json(new
            {
                isSuccess = true,
                details = phoneDetails
            });
        }

        #endregion

        #region Protocol

        [AuthorizeMember(Rules = "InsertProtocol")]
        public ActionResult InsertProtocol(string protocol, string obligorId, string campaignId, string totalMoney, string advancePaymnent,
            string advancePaymnentDate, string roleIdMakingProtocol, bool sendInstallmentContract, bool sendSulhContract,
            bool sendPaymentPlanToObligor, bool smsProtocolReminder, string agencyFee, string note = "")
        {
            var accountId = _obligorUipc.GetAccountIdByObligorId(obligorId);
            //Protocol kefile degil account'a yapilir diye contactId bos gonderiliyor.
            var obligor = _obligorUipc.GetObligorByAccountId(accountId, null, Authentication.UserInfo.Id, Authentication.UserInfo.Roles);
            var protocolDetails = JsonConvert.DeserializeObject<ProtocolDetail[]>(protocol, new IsoDateTimeConverter { DateTimeFormat = "dd-MM-yyyy" });

            var clientProtocolAmount = totalMoney.Convert<decimal>();
            var clientInstallmentTotal = protocolDetails.Sum(pro => pro.Value);
            var clientAdvancePayment = advancePaymnent.Convert<decimal>();
            var needApprovement = true;

            if (clientProtocolAmount == (clientInstallmentTotal + clientAdvancePayment))
            {
                //Kampanyali tutar.
                var cId = campaignId.Convert<int>();
                //Hukuk Dis Burodakiler icin protocol max value'ya takilmamasi icin kural konuldu.
                var ignoreProtocolMaxValue = Authentication.UserRules.Contains("IgnoreProtocolMaxValue");
                if (cId > 0)
                {
                    var campaign = obligor.ProtocolCampaignModelList.FirstOrDefault(pcm => pcm.Id == cId);
                    if (campaign != null)
                    {
                        var minCompMoney = campaign.CashClosingAmount > 0 ? campaign.CashClosingAmount : obligor.BAnapara.Convert<decimal>();
                        var maxCompMoney = obligor.BToplam.Convert<decimal>();//TODO: max tutari onay mekanizmasindan gecirmiyoruz, client ta pesinat tutari anapara dan fazla olunmasina izin verilmiyor.
                        var ratio = (100 - campaign.DiscountPercentage) / 100;
                        if (minCompMoney * ratio <= clientProtocolAmount && (clientProtocolAmount <= maxCompMoney || ignoreProtocolMaxValue))
                        {
                            needApprovement = false;
                        }
                    }
                }
                else if (obligor.BAnapara.Convert<decimal>() <= clientProtocolAmount && (clientProtocolAmount <= obligor.BToplam.Convert<decimal>() || ignoreProtocolMaxValue))
                {
                    if (obligor.Score >= DependentScore)
                    {
                        var changedPv = obligor.BAnapara.Convert<decimal>() * ScoreMultiplier;
                        if (changedPv <= clientProtocolAmount && (clientProtocolAmount <= changedPv || ignoreProtocolMaxValue))
                        {
                            needApprovement = false;
                        }
                    }
                    else
                    {
                        needApprovement = false;
                    }
                }

                var dt = Utilities.GetNewdatatable(
                    new Dictionary<string, string>
                    {
                        { "Month", "System.Int32" },
                        { "PaymentDate", "System.DateTime" },
                        { "Value", "System.Decimal" },
                        { "Desc", "System.String" }
                    });

                foreach (var pDetail in protocolDetails)
                {
                    var dr = dt.NewRow();
                    dr["Month"] = pDetail.Month;
                    dr["PaymentDate"] = pDetail.PaymentDate;
                    dr["Value"] = pDetail.Value;
                    dr["Desc"] = pDetail.Desc;
                    dt.Rows.Add(dr);
                }
                var p = new ProtocolModel
                {
                    ObligorId = obligorId,
                    InstallmentCount = protocolDetails.Length,
                    TotalPaymentValue = totalMoney.Convert<decimal>(),
                    CampaignId = campaignId.Convert<int>(),
                    AdvancePaymentValue = advancePaymnent.Convert<decimal>(),
                    AdvancePaymentDate = null,
                    UserId = Authentication.UserInfo.Id,
                    Status = needApprovement ? 2 : 1,
                    RoleIdMakingProtocol = roleIdMakingProtocol.Convert<int>(),
                    Note = note.Trim().Length > 499 ? note.Trim().Substring(0, 499) : note.Trim(),
                    SendInstallmentContract = sendInstallmentContract,
                    SendSulhContract = sendSulhContract,
                    SendPaymentPlanToObligor = sendPaymentPlanToObligor,
                    SmsProtocolReminder = smsProtocolReminder,
                    AgencyFee = agencyFee.Replace(".", ",").Convert<decimal>()
                };

                if (p.AdvancePaymentValue > 0 && !string.IsNullOrWhiteSpace(advancePaymnentDate))
                {
                    p.AdvancePaymentDate = Convert.ToDateTime(advancePaymnentDate);
                }

                var result = _protocolUipc.InsertProtocolAndDetail(p, dt);
                var emailResult = result && SendEmailPaymentPlanAsPdf(p, dt);
                var remindSmsResult = result && InsertRemindSms(p, dt);
                
                if (result)
                {
                    SendProtocolToMuhasebe(accountId, p.TotalPaymentValue, p.InstallmentCount);
                }

                return Json(new { isSuccess = result, emailResult = emailResult, remindSmsResult = remindSmsResult });
            }
            return Json(new { isSuccess = false, message = "Peşinat ve taksit toplamları protokol değerine eşit olmalı" });
        }

        private void SendProtocolToMuhasebe(string accountId, decimal pTotalPaymentValue, int pInstallmentCount)
        {
            new ExternalMuhasebeUipc().CollactionOdemeplaniEkle(accountId, DateTime.Now, pTotalPaymentValue,
                pInstallmentCount);
        }

        private bool SendEmailPaymentPlanAsPdf(Protocol p, DataTable dt)
        {
            return true;
        }

        private bool InsertRemindSms(Protocol p, DataTable dt)
        {
            return true;
        }

        [AuthorizeMember(Rules = "DeleteProtocol")]
        public ActionResult DeleteProtocol(string recId, string reason, string id)
        {
            var userId = Authentication.UserInfo.Id;
            reason = reason.Length > 199 ? reason.Substring(0, 199) : reason;
            var result = _protocolUipc.DeleteProtocol(userId, recId, reason);

            if (result)
            {
                SendDeletedProtocolToMuhasebe(id);
            }

            return Json(new
            {
                isSuccess = result
            });
        }

        private void SendDeletedProtocolToMuhasebe(string id)
        {
            new ExternalMuhasebeUipc().CollactionOdemeplaniIptal(id);
        }

        public ActionResult GetProtocolInstalmentsById(string protocolId)
        {
            try
            {
                var model = _obligorUipc.GetProtocolInstalmentsById(Authentication.UserInfo.Id, protocolId);
                return Json(new
                {
                    isSuccess = true,
                    data = model
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    data = ex
                });
            }
        }

        #endregion

        #region Address

        public ActionResult GetCities()
        {
            return Json(new { isSuccess = true, cities = _locationUipc.GetCities() });
        }

        public ActionResult GetCounties(string cityId)
        {
            return Json(new { isSuccess = true, counties = _locationUipc.GetCountiesByCityId(cityId.Convert<int>()) });
        }

        public ActionResult AddAddress(AddressModel address, string obligorId)
        {
            return Json(new { isSuccess = _obligorUipc.AddAddress(address, obligorId, Authentication.UserInfo.Id) });
        }

        #endregion

        #region Execution (Infaz)
        [AuthorizeMember(Rules = "DoExecution")]
        public ActionResult DoExecution(string recId)
        {
            var userId = Authentication.UserInfo.Id;
            var result = _debitUipc.DoExecution(userId, recId);

            return Json(new
            {
                isSuccess = result
            });
        }
        #endregion

        #region Call

        [AuthorizeMember(Rules = "MakeCall")]
        public ActionResult Call(string number, string preferredCpn = "", string customerName = "", string companyName = "", string accountId = "")
        {
            var call = new Call
            {
                TelNo = number,
                Cpn = string.IsNullOrWhiteSpace(preferredCpn.Trim()) ? Authentication.UserInfo.PhoneNumber : preferredCpn,
                //AgentName = "agent1",
                AgentName = Authentication.UserInfo.LoginName,
                QueueNo = 0,
                RecordType = Entities.Fgs.Call.RecordTypeEnum.Agent,
                NextTryDate = DateTime.Now,
                CustomerName = customerName,
                CompanyName = companyName,
                AccountId = accountId
            };
            //call.AgentName = Authentication.UserInfo.LoginName;

            var resultId = _fgsUipc.Call(call);
            return Json(new { isSuccess = resultId > 0, resultId = resultId });
        }

        [AuthorizeMember(Rules = "MakeCall")]
        public ActionResult CallList(List<Call> callList)
        {
            foreach (var call in callList)
            {
                _fgsUipc.Call(call);
            }

            //call.AgentName = Authentication.UserInfo.LoginName;
            return Json(new { isSuccess = true });
        }

        public ActionResult InsertReminder(string typeId, string reminderDate, string detail, string obligorId)
        {
            var reminder = new Reminder
            {
                TypeId = typeId.Convert<int>(),
                RemindDate = reminderDate.Convert<DateTime>(),
                Detail = detail,
                UserId = Authentication.UserInfo.Id,
                ObligorId = obligorId
            };
            var success = _reminderIUipc.InsertReminder(reminder);
            return Json(new { isSuccess = success });
        }

        public ActionResult GetCriticNotes(string obligorId)
        {
            return Json(new { isSuccess = true, Notes = _obligorUipc.GetLast5ImportantNotes(Authentication.UserInfo.Id, obligorId) });
        }

        [HttpPost]
        public ActionResult UploadFile(string fileDesc, string obligorId, bool isGkf)
        {
            foreach (string item in Request.Files)
            {
                var file = Request.Files[item] as HttpPostedFileBase;
                if (file != null)
                {
                    var ext = Path.GetExtension(file.FileName);
                    if (ext != null)
                    {
                        ext = ext.ToLower();
                        if (ext.Contains("png") || ext.Contains("pdf") || ext.Contains("jpg") || ext.Contains("jpeg") || ext.Contains("doc")
                        || ext.Contains("docx") || ext.Contains("xls") || ext.Contains("xlsx"))
                        {
                            var fileName = Guid.NewGuid() + ext;
                            var uploadPath = System.Configuration.ConfigurationManager.AppSettings["FileUploadPath"];

                            if (file.ContentLength == 0)
                                continue;
                            if (file.ContentLength > 0)
                            {
                                var path = Path.Combine(uploadPath, fileName);

                                try
                                {
                                    file.SaveAs(path);
                                    _obligorUipc.AddAttachment(obligorId, fileDesc, Authentication.UserInfo.Id, uploadPath, fileName, isGkf);
                                    return Json(new { isSuccess = true, pathName = fileName });
                                }
                                catch (Exception ex)
                                {
                                    return Json(new { isSuccess = false, error = ex.Message });
                                }
                            }
                        }
                    }
                    else
                    {
                        return Json(new { isSuccess = false, error = "Lütfen izin verilen dosya formatlarında dosya yükleyiniz" });
                    }
                }
                return Json(new { isSuccess = false, error = "Yüklenecek dosya bulunamadı." });
            }

            return Json(new { isSuccess = false, error = "İşlemde hata oluştu" });
        }

        [HttpGet]
        public ActionResult Download(string fileName)
        {
            string path = Path.Combine(fileName);
            string ext = fileName.Split('.')[1];
            if (ext == "png")
            {
                return File(path, "image/png");
            }
            else if (ext == "jpg")
            {
                return File(path, "image/jpg");
            }
            else if (ext == "jpeg")
            {
                return File(path, "image/jpeg");
            }
            else if (ext == "tiff")
            {
                return File(path, "image/tiff");
            }
            else if (ext == "pdf")
            {
                return File(path, "application/pdf");
            }
            return null;
        }

        #endregion

        #region GKF

        [HttpGet]
        public ActionResult GkfList(string dateTime, string type = "SULHIBRASOZLESMESI")
        {
            var today = DateTime.Now;
            var date = !string.IsNullOrWhiteSpace(dateTime) ? dateTime.Convert<DateTime>() : new DateTime(today.Year, today.Month, today.Day);
            var model = _gkfUipc.GetGkfList(Authentication.UserInfo.Id, date, type);
            ViewBag.Date = date.ToString("dd-MM-yyyy");
            return View(model);
        }

        #endregion

        #region Account Owner

        [AuthorizeMember(Rules = "ChangeAccountOwner")]
        public JsonResult GetUsersForAccountOwnership()
        {
            return Json(_obligorUipc.GetUsersForAccountOwnership(Authentication.UserInfo.Id));
        }

        [AuthorizeMember(Rules = "ChangeAccountOwner")]
        public JsonResult ChangeAccountOwner(string accountId, string userId)
        {
            var result = _obligorUipc.ChangeAccountOwner(userId.Convert<int>(), Authentication.UserInfo.Id, accountId);
            return Json(new { IsSuccess = result });
        }

        [AuthorizeMember(Rules = "ChangePaymentOwner")]
        public JsonResult ChangePaymentOwner(string accountId, string newOwnerId, string recId)
        {
            var result = _obligorUipc.ChangePaymentOwner(newOwnerId.Convert<int>(), Authentication.UserInfo.Id, accountId, recId);
            return Json(new { IsSuccess = result });
        }

        #endregion

        #region FileAttachment

        //[AuthorizeMember(Rules = "ChangeAccountOwner")]
        public JsonResult RemoveFile(string recId)
        {
            var result = _obligorUipc.RemoveFile(recId, Authentication.UserInfo.Id);
            return Json(new { isSuccess = result });
        }

        #endregion

        public ActionResult GetPaymentSubPlanType(string paymentPlanId)
        {
            return Json(new { isSuccess = true, paymentPlanSubType = new PaymentPlanUipc().GetPaymentPlanSubTypes(paymentPlanId.Convert<int>()) });
        }

        public ActionResult AttachToProtocol(string paymentPlanId, string paymentSubPlanId, string recId)
        {
            return Json(new
            {
                isSuccess = true,
                ret = _protocolUipc.AttachToProtocol(recId, paymentPlanId.Convert<int>(), paymentSubPlanId.Convert<int>(), Authentication.UserInfo.Id)
            });
        }

        public ActionResult DeattachToProtocol(string recId, string deAttachObligor)
        {
            var deAttachObl = deAttachObligor == "True";
            return Json(new
            {
                isSuccess = true,
                ret = _protocolUipc.DeAttachToProtocol(recId, deAttachObl, Authentication.UserInfo.Id)
            });
        }
    }
}