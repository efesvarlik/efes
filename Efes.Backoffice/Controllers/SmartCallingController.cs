﻿using System;
using Efes.WC;
using Efes.UIPC;
using Efes.Models;
using System.Data;
using System.Linq;
using Efes.Utility;
using System.Web.Mvc;
using Newtonsoft.Json;
using Efes.Entities.Fgs;
using Efes.Entities.User;
using Efes.Entities.SmartCalling;
using System.Collections.Generic;
using Efes.Models.SmartCalling;

namespace Efes.Backoffice.Controllers
{
    //[AuthorizeMember(Rules = "SmartCalling")]
    public class SmartCallingController : Controller
    {
        private readonly SmartCallingUipc _smartCallingUipc;
        private readonly FgsUipc _fgsUipc;
        private readonly ReminderUipc _reminderUipc;
        private readonly UserUipc _userUipc;

        public SmartCallingController()
        {
            _smartCallingUipc = new SmartCallingUipc();
            _fgsUipc = new FgsUipc();
            _reminderUipc = new ReminderUipc();
            _userUipc = new UserUipc();
        }

        public ActionResult TodayPayments()
        {
            var model = _smartCallingUipc.GetTodayPaymentsList(Authentication.UserInfo.Id);
            return View(model);
        }

        public ActionResult NewAppointed(string type)
        {
            var model = _smartCallingUipc.GetNewAppointedList(Authentication.UserInfo.Id, type.Convert<int>());
            return View(model);
        }

        public ActionResult CancelProtocol()
        {
            var model = _smartCallingUipc.GetCancelProtocolList(Authentication.UserInfo.Id);
            return View(model);
        }

        public ActionResult ByCapitalGroup(int debtGroupType = 0, int portfolioId = 0)
        {
            var debtGroups = _smartCallingUipc.GetDebtGroup(Authentication.UserInfo.Id);
            int firstDebtGroup = debtGroups.Any() ? debtGroups.First().Key.Convert<int>() : 0;
            if (debtGroupType == 0 && firstDebtGroup != 0)
            {
                debtGroupType = firstDebtGroup;
            }

            var model = _smartCallingUipc.GetByCapitalGroupList(Authentication.UserInfo.Id, debtGroupType, portfolioId);
            model.SelectedDebtGroup = debtGroupType;
            model.SelectedPortfolio = portfolioId;

            return View(model);
        }

        public ActionResult CollectionWithoutProtocol()
        {
            var model = _smartCallingUipc.GetCollectionWithoutProtocolList(Authentication.UserInfo.Id);
            return View(model);
        }

        public ActionResult Expired()
        {
            var model = _smartCallingUipc.GetExpiredList(Authentication.UserInfo.Id);
            return View(model);
        }

        [HttpPost]
        [AuthorizeMember(Rules = "MakeCall")]
        public ActionResult CallWithList(string callList)
        {
            var calls = JsonConvert.DeserializeObject<List<ClientCallDetail>>(callList);
            var serverList = new List<Call>();

            if (calls.Any())
            {
                serverList.AddRange(calls.Select(call => new Call
                {
                    TelNo = call.Phone,
                    Cpn = string.IsNullOrWhiteSpace(call.Cpn) ? Authentication.UserInfo.PhoneNumber : call.Cpn,
                    AgentName = Authentication.UserInfo.LoginName,
                    QueueNo = 0,
                    RecordType = Call.RecordTypeEnum.Pool,
                    NextTryDate = new DateTime(2001, 1, 1),
                    CustomerName = call.Name,
                    CompanyName = "",
                    AccountId = call.AccountId
                }));

                if (serverList.Any())
                {
                    foreach (var call in serverList)
                    {
                        _fgsUipc.Call(call);
                    }
                    return Json(new { isSuccess = true });
                }
            }

            return Json(new { isSuccess = false });
        }

        public ActionResult CallOne(string number, string preferredCpn = "", string customerName = "", string companyName = "", string accountId = "")
        {
            var call = new Call
            {
                TelNo = number,
                Cpn = string.IsNullOrWhiteSpace(preferredCpn.Trim()) ? Authentication.UserInfo.PhoneNumber : preferredCpn,
                //AgentName = "agent1",
                AgentName = Authentication.UserInfo.LoginName,
                QueueNo = 0,
                RecordType = Call.RecordTypeEnum.Agent,
                NextTryDate = new DateTime(2001, 1, 1),
                CustomerName = customerName,
                CompanyName = companyName,
                AccountId = accountId
            };
            //call.AgentName = Authentication.UserInfo.LoginName;

            var resultId = _fgsUipc.Call(call);
            return Json(new { isSuccess = resultId > 0, resultId = resultId });
        }

        public ActionResult Reminder(string date = "")
        {
            DateTime tDate = string.IsNullOrEmpty(date) ? DateTime.Now : date.Convert<DateTime>();

            var model = _reminderUipc.GetReminderByUserId(Authentication.UserInfo.Id, tDate);
            ViewBag.Date = tDate.ToString("dd-MM-yyyy");
            return View(model);
        }

        [HttpGet]
        [AuthorizeMember(Rules = "CreateDialer")]
        public ActionResult CreateDialer()
        {
            var model = new CreateDialerModel { DialerStatus = _smartCallingUipc.GetCurrentDialerStatus() };

            //Sadece sayet Dialer kullanimda yada duraklatilmis degilse 
            //Yani dialer olusturmaya uygunsa agent listesi cagrilmistir.
            if (model.DialerStatus == DialerStatus.Passive)
            {
                model.UnselectedUsers = _userUipc.GetUsersHasOnlyOneRoleByRoleRoleId(Roles.TahsilatYetkilisi);
            }

            return View(model);
        }

        [HttpPost]
        [AuthorizeMember(Rules = "CreateDialer")]
        public ActionResult InsertExcel()
        {
            const string cacheName = "Dialer";
            //Eger dosya yoksa hata don
            if (Request.Files.Count == 0)
            {
                return Json(new { Result = "Error", Message = "File is missing." }, JsonRequestBehavior.AllowGet);
            }

            var necessaryColumnList = new List<string>() { "AccountId", "PhoneNumber" };

            var resultList = ExcelHelper.ExcelImport(necessaryColumnList, Request, Authentication.UserInfo.Id, cacheName);
            if (resultList.Any())
            {
                return Json(new { Result = "Error", MissingColumnNames = string.Join(",", resultList) });
            }

            var dt = CacheManager.Default.Get(cacheName + Request.BoUniqueId()) as DataTable;
            if (dt.HasData())
            {
                if (dt != null)
                {
                    var tempList = (dt.Columns.Cast<DataColumn>()
                        .Where(column => !necessaryColumnList.Contains(column.ColumnName.Trim()))
                        .Select(column => column.ColumnName)).ToList();

                    foreach (var columnname in tempList)
                    {
                        dt.Columns.Remove(columnname);
                    }
                }

                return Json(new { Result = "Success", AccountIdList = (dt.ToStringListTwoColumns("AccountId", "PhoneNumber", "|")) });
            }

            CacheManager.Default.Remove(cacheName + Request.BoUniqueId());
            return Json(true ? new { Result = "Success" } : new { Result = "Error", });
        }

        [HttpPost]
        [AuthorizeMember(Rules = "CreateDialer")]
        public ActionResult StartDialer(List<string> selectedAgents, 
                                        List<string> loadedAccounts, 
                                        string bandwidth, 
                                        string maxWaitingTime,
                                        string useFct)
        {
            var dtAgents = Utilities.GetNewdatatable(
                new Dictionary<string, string>
                {
                    { "Agent", "System.String" }
                });

            foreach (var pDetail in selectedAgents)
            {
                var dr = dtAgents.NewRow();
                dr["Agent"] = pDetail;
                dtAgents.Rows.Add(dr);
            }

            var dtAccounts = Utilities.GetNewdatatable(
                new Dictionary<string, string>
                {
                    { "AccountId", "System.String" },
                    { "PhoneNumber", "System.String" }
                });

            foreach (var pDetail in loadedAccounts)
            {
                var dr = dtAccounts.NewRow();
                dr["AccountId"] = pDetail.Split('|')[0];
                dr["PhoneNumber"] = pDetail.Split('|')[1];
                dtAccounts.Rows.Add(dr);
            }

            var result = _smartCallingUipc.StartDialer(
                dtAgents, 
                dtAccounts, 
                Authentication.UserInfo.Id, 
                bandwidth, 
                maxWaitingTime.Convert<int>(),
                useFct.ToLower() == "true");

            return Json(new { Result = result ? "Success" : "Error" });
        }

        [HttpPost]
        [AuthorizeMember(Rules = "CreateDialer")]
        public ActionResult GetCurrentDialerStatus()
        {
            return Json(new { Result = _smartCallingUipc.GetCurrentDialerStatus() });
        }

        [HttpGet]
        [AuthorizeMember(Rules = "ViewDialerAgentList")]
        public ActionResult DialerAgentList()
        {
            var model = _smartCallingUipc.GetDialerAgentList();
            return View(model);
        }

        [AuthorizeMember(Rules = "InsertDialerAgentList")]
        public ActionResult AddDialerAgentList(string agentName)
        {
            _smartCallingUipc.RemoveAgentFromDialerList(agentName, Authentication.UserInfo.Id);
            return RedirectToAction("DialerAgentList");
        }

        [AuthorizeMember(Rules = "ViewDialerList")]
        public ActionResult DialerList()
        {
            var dialerCallList = _smartCallingUipc.GetDialerCallList();
            var dialerStatus = _smartCallingUipc.GetCurrentDialerStatus();
            var dialerDetail = _smartCallingUipc.GetCurrentDialerDetail();

            var model = new DialerCallModel
            {
                DialerStatus = dialerStatus,
                DialerCallList = dialerCallList,
                DialerDetail = dialerDetail
            };
            return View(model);
        }

        [AuthorizeMember(Rules = "ViewDialerList")]
        public ActionResult GetDialerCallList()
        {
            var dialerCallList = _smartCallingUipc.GetDialerCallList();
            var dialerStatus = _smartCallingUipc.GetCurrentDialerStatus();
            var dialerDetail = _smartCallingUipc.GetCurrentDialerDetail();

            var model = new DialerCallModel
            {
                DialerStatus = dialerStatus,
                DialerCallList = dialerCallList,
                DialerDetail = dialerDetail
            };

            return Json(new { Result = model });
        }

        [AuthorizeMember(Rules = "ChangeDialerList")]
        public ActionResult ChangeDialerCallStatus(string status, string recId)
        {
            var result = _smartCallingUipc.ChangeDialerCallStatus(
                Authentication.UserInfo.Id, 
                status.Convert<int>(), 
                recId);
            return Json(new { IsSuccess = result });
        }

        private class ClientCallDetail
        {
            public string Name { get; set; }
            public string Phone { get; set; }
            public string Cpn { get; set; }
            public string AccountId { get; set; }
        }
    }
}