﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;

namespace Efes.Backoffice.Controllers
{
    public class SocketController : Controller
    {
        //
        // GET: /Socket/
        public void SendMessage()
        {
            var client = new RestClient("http://localhost:3000");
            var request = new RestRequest("api/setmessage/", Method.POST) { RequestFormat = DataFormat.Json };
            request.AddBody(
                new { ItemName = "Package", Price = 19.99 });
            client.Execute(request);
            
        }
    }
}                   