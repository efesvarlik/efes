﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Efes.UIPC;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    public class LoosePaymentController : Controller
    {
        // GET: LoosePayment
        [AuthorizeMember(Rules = "LoosePaymentViewList")]
        public ActionResult Index()
        {
            var model = new LoosePaymentUipc().GetLoosePaymentModel();
            return View(model);
        }

        public JsonResult UpdateProccessedPayment(string val, string recId)
        {
            var result = new LoosePaymentUipc().UpdateProccessedPayment(Authentication.UserInfo.Id, recId, val);
            return Json(new {isSucceded = result});
        }

        // GET: LoosePayment/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LoosePayment/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LoosePayment/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LoosePayment/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LoosePayment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LoosePayment/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LoosePayment/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
