﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    public class PortfolioController : Controller
    {

        private readonly PortfolioStageUipc _portfolioStageUipc;

        public PortfolioController()
        {
            _portfolioStageUipc = new PortfolioStageUipc();
        }

        [AuthorizeMember(Rules = "PortfolioCreate")]
        // GET: Portfolio
        public ActionResult Index()
        {
            var portfolio = new PortfolioStageUipc().GetWaitingPortfolio();
            return View(portfolio);
        }

        [HttpPost]
        public ActionResult WorkTables(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.GetMaxId(portfolioName);
            if (!result)
            {
                return Json(new { IsSuccess = false });
            }
            var result1 = _portfolioStageUipc.WorkTables(bankName, portfolioName);
            return Json(new { IsSuccess = result1 });
        }

        [HttpPost]
        public ActionResult SourceAndSourceset(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.SourceAndSourceset(bankName, portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult ContactUpdate(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.ContactUpdate(bankName, portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult UpdateAccount(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.UpdateAccount(bankName, portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult PhoneTable(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.PhoneTable(bankName, portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult AddressTable(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.AddressTable(bankName, portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult NormalizePhoneNumbers()
        {
            var result = _portfolioStageUipc.NormalizePhoneNumbers();
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult DebtDetailTable(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.DebtDetailTable(bankName, portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult MoneyDebtDetailsToAccount()
        {
            var result = _portfolioStageUipc.MoneyDebtDetailsToAccount();
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult ContactRelationsTable(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.ContactRelationsTable(bankName, portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult DataToTable(string portfolioName)
        {
            var result = _portfolioStageUipc.DataToTable(portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult ReleaseData(string portfolioName)
        {
            var result = _portfolioStageUipc.ReleaseData(portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult AddressActive(string bankName, string portfolioName)
        {
            var result = _portfolioStageUipc.SourceAndSourceset(bankName, portfolioName);
            return Json(new { IsSuccess = result });
        }

        [HttpPost]
        public ActionResult CreateBop(string portfolioName)
        {
            var result = _portfolioStageUipc.CreateBop(portfolioName);
            return Json(new { IsSuccess = result });
        }
    }
}