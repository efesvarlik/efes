﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using Efes.Backoffice.Models;
using Efes.Entities.User;
using Efes.Models;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class AuthorizationController : BaseController
    {
        private readonly UserUipc _userUipc;
        public AuthorizationController()
        {
            _userUipc = new UserUipc();
        }
        [HttpGet]
        [AuthorizeMember(Rules = "ViewRules")]
        public ActionResult Rule()
        {
            var model = _userUipc.GetAllRules();
            return View(model);
        }

        [HttpGet]
        [AuthorizeMember(Rules = "ViewRoles")]
        public ActionResult Role()
        {
            var model = _userUipc.GetAllRoles();
            return View(model);
        }

        [HttpGet]
        [AuthorizeMember(Rules = "ViewUserRoles")]
        public ActionResult UserRoles(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var allRoles = _userUipc.GetAllRoles().Where(r => r.IsActive).ToList();
            var userRoles = _userUipc.GetUserRolesById(id.Convert<int>()).Where(r => r.IsActive).ToList();
            var userRoleModel = new UserRoleModel
            {
                AllRoles = allRoles,
                UserRoles = userRoles,
                RolesUserDoesNotHave = allRoles.Except(userRoles).ToList(),
                User = _userUipc.GetUser(id.Convert<int>())
            };
            return View(userRoleModel);
        }

        [HttpGet]
        [AuthorizeMember(Rules = "ViewRoleRules")]
        public ActionResult RoleRules(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var allRules = _userUipc.GetAllRules().Where(r => r.IsActive).ToList();
            var rolesRules = _userUipc.GetRoleRulesById(id.Convert<int>()).Where(r => r.IsActive).ToList();
            RoleRuleModel userRoleModel = new RoleRuleModel
            {
                AllRules = allRules,
                RoleRules = rolesRules,
                RulesRoleDoesNotHave = allRules.Except(rolesRules).ToList(),
                Role = _userUipc.GetRole(id.Convert<int>())
            };
            return View(userRoleModel);
        }

        [HttpPost]
        [AuthorizeMember(Rules = "CreateUserRole")]
        public ActionResult InsertUserRole(int roleId, int userId)
        {
            var result = _userUipc.InsertUserRole(userId, roleId, Authentication.UserInfo.Id);
            if (result)
            {

            }
            return Json(new { Result = result ? "Success" : "Error" });
        }

        [HttpPost]
        [AuthorizeMember(Rules = "DeleteUserRole")]
        public ActionResult DeleteUserRole(int roleId, int userId)
        {
            var result = _userUipc.DeleteUserRole(userId, roleId, Authentication.UserInfo.Id);
            return Json(new { Result = result ? "Success" : "Error" });
        }

        [HttpPost]
        [AuthorizeMember(Rules = "CreateRoleRule")]
        public ActionResult InsertRoleRule(int roleId, int ruleId)
        {
            var result = _userUipc.InsertRoleRule(roleId, ruleId, Authentication.UserInfo.Id);
            return Json(new { Result = result ? "Success" : "Error" });
        }

        [HttpPost]
        [AuthorizeMember(Rules = "DeleteRoleRule")]
        public ActionResult DeleteRoleRule(int roleId, int ruleId)
        {
            var result = _userUipc.DeleteRoleRule(roleId, ruleId, Authentication.UserInfo.Id);
            return Json(new { Result = result ? "Success" : "Error" });
        }

        [HttpPost]
        [AuthorizeMember(Rules = "SaveOrUpdateRule")]
        public ActionResult SaveOrUpdateRule(Rule rule)
        {
            if (rule.Id > 0)
            {
                _userUipc.UpdateRule(rule.Id, rule.Value, rule.IsActive);
            }
            else
            {
                _userUipc.InsertRule(rule.Value);
            }
            return RedirectToAction("Rule");
        }

        [HttpPost]
        [AuthorizeMember(Rules = "SaveOrUpdateRole")]
        public ActionResult SaveOrUpdateRole(Role role)
        {
            if (role.Id > 0)
            {
                _userUipc.UpdateRole(role);
            }
            else
            {
                _userUipc.InsertRole(role);
            }
            return RedirectToAction("Role");
        }

        [HttpGet]
        [AuthorizeMember(Rules = "ViewUserLoginHistoryList")]
        public ActionResult LoginHistory()
        {
            var model = new UserHistoryLoginModel
            {
                UserList = new ObligorUipc().GetUsersForAccountOwnership(Authentication.UserInfo.Id)
            };
            return View(model);
        }

        [AuthorizeMember(Rules = "ViewUserLoginHistoryList")]
        [HttpPost]
        public ActionResult LoginHistory(string userId)
        {
            var userList = new ObligorUipc().GetUsersForAccountOwnership(Authentication.UserInfo.Id);
            var selectedUser = userList.FirstOrDefault(c => c.Id == userId.Convert<int>());
            var userName = selectedUser == null ? "" : selectedUser.LoginName;
            var model = new UserHistoryLoginModel()
            {
                UserList = new ObligorUipc().GetUsersForAccountOwnership(Authentication.UserInfo.Id),
                SelectedAgent = userName,
                LoginHistoryList = new UserUipc().GetLoginHistory(Authentication.UserInfo.Id, userName)
            };
            return View(model);
        }

    }
}