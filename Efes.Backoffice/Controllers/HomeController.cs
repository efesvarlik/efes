﻿using System.Web.Mvc;
using System.Web.UI;
using Efes.UIPC;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            ViewBag.Title = string.Empty;
            var model = new HomeUipc().GetDashBoardModel(Authentication.UserInfo.Id);
            return View(model);
        }
    }
}