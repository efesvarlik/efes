﻿using Efes.Entities.Law;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Efes.Backoffice.Controllers
{
    public class LawCollectionController : BaseController
    {
        private readonly LawOfficeUipc _lawOfficeUipc;
        public LawCollectionController()
        {
            _lawOfficeUipc = new LawOfficeUipc();
        }
        [AuthorizeMember(Rules = "ViewLawOfficeCollectionList")]
        public ActionResult Index(string date = "", string endDate = "")
        {
            DateTime tDate = string.IsNullOrEmpty(date) ? DateTime.Now : date.Convert<DateTime>();
            DateTime eDate = string.IsNullOrEmpty(endDate) ? DateTime.Now.AddDays(1) : endDate.Convert<DateTime>();
            
            ViewBag.Date = tDate.ToString("dd-MM-yyyy");
            ViewBag.EDate = eDate.ToString("dd-MM-yyyy");
            var collections = _lawOfficeUipc.GetCollectionByLawOfficeId(Authentication.UserInfo.Id, tDate, eDate);
            return View(collections);
        }
    }
}
