﻿using Efes.WC;
using System.Web.Mvc;

namespace Efes.Backoffice.Controllers
{
    public class ErrorController : BaseController
    {
        //
        // GET: /Error/
        public ActionResult BadRequest()
        {
            return View();
        }
	}
}