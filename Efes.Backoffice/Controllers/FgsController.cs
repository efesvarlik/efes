﻿using System.Web.Mvc;
using Efes.UIPC;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class FgsController : Controller
    {
        private readonly FgsUipc _fgsUipc;
        public FgsController()
        {
            _fgsUipc = new FgsUipc();
        }

        public ActionResult MissedCalls()
        {
            var model = _fgsUipc.GetMissedCall(Authentication.UserInfo.Id);
            return View(model);
        }
	}
}