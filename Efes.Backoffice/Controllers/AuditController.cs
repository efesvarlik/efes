﻿using System.Web.Mvc;
using Efes.UIPC;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class AuditController : BaseController
    {
        [AuthorizeMember(Rules = "ViewAuditLogs")] 
        public ActionResult Index()
        {
            var model = new AuditUipc().GetAuditModelListLast30Minutes();
            return View(model);
        }
    }
}