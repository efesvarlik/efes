﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Efes.UIPC;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class NotificationController : BaseController
    {
        //
        // GET: /Notification/
        [AuthorizeMember]
        public JsonResult GetNotification()
        {
            return Json(new NotificationUipc().GetNotifications(Authentication.UserInfo.Id));
        }

        public ActionResult Read(int id)
        {
            new NotificationUipc().Read(id);
            return Json( new { Result = "Success" });
        }

        public ActionResult Confirm(int id)
        {
            new NotificationUipc().Confirm(id, Authentication.UserInfo.Id);
            return Json(new { Result = "Success" });
        }
	}
}