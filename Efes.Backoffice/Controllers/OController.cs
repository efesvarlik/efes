﻿using System.Web;
using System.Web.Mvc;

namespace Efes.Backoffice.Controllers
{
    public class OController : Controller
    {
        public ActionResult D(string a)
        {
            if (a.Contains("b") && a.Split('b').Length > 1)
            {
                var accountId = a.Split('b')[1];
                var phoneNumber = "";
                if (a.Split('b').Length > 2)
                {
                    phoneNumber = a.Split('b')[2];
                }
                return RedirectToAction("Detail", "Obligor", new { accountId, phoneNumber });
            }
            return Redirect(HttpUtility.HtmlDecode("/"));
        }
    }
}