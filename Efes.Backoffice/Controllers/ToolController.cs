﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Efes.Entities;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class ToolController : BaseController
    {
        #region ActionResults

        [AuthorizeMember(Rules = "SinglePhoneChangeStatus")]
        public ActionResult PhoneActivePassive()
        {
            return View();
        }

        [AuthorizeMember(Rules = "MultiplePhonePassive")]
        public ActionResult PassivePhoneList()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeMember(Rules = "SinglePhoneChangeStatus")]
        public ActionResult PhoneActivePassive(string searchTerm)
        {
            ViewBag.Title = "Telefon Aktif/Pasif";

            var toolUipc = new ToolUipc();
            var lispPhones = toolUipc.GetPhoneNumbers(searchTerm);

            return View(lispPhones);
        }

        [HttpPost]
        [AuthorizeMember(Rules = "MultiplePhonePassive")]
        public ActionResult PassivePhoneList(string phones)
        {
            bool result = false;
            if (!string.IsNullOrWhiteSpace(phones) && phones.Split(',').Any())
            {
                List<string> phoneList = phones.Split(',').Select(s => s.Trim()).Where(s => s != String.Empty).ToList();

                DataTable dt = ListToDataTable(phoneList);
                if (dt.HasData())
                {
                    result = new ToolUipc().PassivePhoneList(dt, Authentication.UserInfo.Id);
                }
            }

            return Json(result ? new { Result = "Success" } : new { Result = "Error" });
        }

        [HttpPost]
        [AuthorizeMember(Rules = "SinglePhoneChangeStatus")]
        public ActionResult ChangePhoneStatus(string accountId, string phone, bool status)
        {
            ViewBag.Title = "Telefon Aktif/Pasif";

            var toolUipc = new ToolUipc();
            var result = toolUipc.ChangePhoneStatus(accountId, phone, status);

            return Json(result ? new { Result = "Success" } : new { Result = "Error" });
        }

        public ActionResult TopTenWords()
        {
            var topTenWords = new ToolUipc().GetTopTenWords();

            return View(topTenWords);
        }


        #endregion


        private DataTable ListToDataTable(List<string> stringList)
        {
            DataTable dt = new DataTable("tempTable");
            dt.Columns.Add("tempColumn");

            foreach (var str in stringList)
            {
                dt.Rows.Add(str);
            }

            return dt;
        }
    }
}