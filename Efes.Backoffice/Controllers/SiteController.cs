﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Efes.UIPC;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class SiteController : BaseController
    {
        //
        // GET: /Site/
        public ActionResult InsertFeedback(string feedback)
        {
            try
            {
                new ToolUipc().InsertFeedback(feedback, Authentication.UserInfo.Id);
                return Json(new { Result = "Success" });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return Json(new { Result = "Error" });
            }
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}