﻿using System;
using System.Web;
using System.Web.Mvc;
using Efes.Backoffice.Models;
using Efes.Entities.User;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;
using Microsoft.AspNet.Identity;

namespace Efes.Backoffice.Controllers
{

    public class AccountController : BaseController
    {
        private readonly UserUipc _userUipc;
        public AccountController()
        {
            _userUipc = new UserUipc();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string r)
        {
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }

            if (Authentication.UserInfo == null) return View();

            if (Authentication.UserInfo.IsLockedOut)
            {
                ModelState.AddModelError("", "Kullanici kilitlendi.");
                return View();
            }

            if (!Authentication.UserInfo.IsSecurePassword)
            {
                return RedirectToAction("ChangePassword", new { userName = Authentication.UserInfo.LoginName });
            }

            //Redirection veya normal giris
            if (AuthManager.IsAuthenticated)
            {
                if (!string.IsNullOrWhiteSpace(Request.QueryString["r"]))
                {
                    return Redirect(HttpUtility.HtmlDecode(Request.QueryString["r"]));
                }
                return RedirectToAction("Dashboard", "Home");
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model, string r)
        {
            try
            {
                var userInfo = Authentication.AuthenticateUser(model.UserName,
                    model.Password, System.Web.HttpContext.Current.Request.BoUniqueId());

                //Kullanici var
                if (userInfo != null)
                {
                    //Locked Out
                    if (userInfo.IsLockedOut)
                    {
                        ModelState.AddModelError("", "Kullanici kilitlendi.");
                        return View(model);
                    }

                    //Secure Password
                    if (!userInfo.IsSecurePassword)
                    {
                        return RedirectToAction("ChangePassword", new { userName = userInfo.LoginName });
                    }

                    //Redirection veya normal giris
                    if (AuthManager.IsAuthenticated)
                    {
                        if (!string.IsNullOrWhiteSpace(Request.QueryString["r"]))
                        {
                            return Redirect(HttpUtility.HtmlDecode(Request.QueryString["r"]));
                        }
                        return RedirectToAction("Dashboard", "Home");
                    }
                }

                //var ret = LoginProcess();
                //if (LoginProcess() != null)
                //{
                //    return ret;
                //}
                ModelState.AddModelError("", "Yanlış kullanıcı adı veya şifre");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Hatalı Giriş");
                Logger.Error("Login isleminde hata olusmustur.", ex);
            }

            return View(model);

        }

        //private ActionResult LoginProcess()
        //{
        //    try
        //    {
        //        if (AuthManager.IsAuthenticated)
        //        {
        //            if (Authentication.UserInfo != null && !Authentication.UserInfo.IsSecurePassword)
        //            {
        //                return RedirectToAction("ChangePassword");
        //            }

        //            if (!string.IsNullOrWhiteSpace(Request.QueryString["r"]))
        //            {
        //                return Redirect(HttpUtility.HtmlDecode(Request.QueryString["r"]));
        //            }
        //            return RedirectToAction("Dashboard", "Home");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex);

        //    }
        //    return null;
        //}

        public ActionResult Logout()
        {
            try
            {
                if (Authentication.UserInfo != null)
                {
                    _userUipc.InsertMemberLog(Authentication.UserInfo.Id, UserLogType.Logout);
                }
                AuthManager.LogoutMember();
                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return null;
        }

        [HttpGet]
        public ActionResult ChangePassword(string userName = "")
        {
            ViewBag.UserName = userName;
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(LoginViewModel model)
        {
            try
            {
                if (model.Password == model.NewPassword)
                {
                    ModelState.AddModelError("", "Yeni şifreniz eski şifreniz ile aynı olamaz");
                    return View();
                }

                if (model.NewPassword != model.RePassword)
                {
                    ModelState.AddModelError("", "Şifreler birbirleriyle uyuşmamaktadır.");
                    return View();
                }

                if (!PasswordCheck.IsValidPassword(model.NewPassword))
                {
                    ViewBag.IsValidPassword = false;
                    return View();
                }

                //if (Authentication.UserInfo != null && Authentication.UserInfo.Id > 0)
                //{
                var ret = _userUipc.ChangePassword(model.UserName, model.Password, model.NewPassword);

                if (string.IsNullOrWhiteSpace(ret))
                {
                    var userInfo = Authentication.AuthenticateUser(model.UserName,
                        model.NewPassword, System.Web.HttpContext.Current.Request.BoUniqueId());

                    //Kullanici var
                    if (userInfo == null) return RedirectToAction("Login");

                    //Locked Out
                    if (userInfo.IsLockedOut)
                    {
                        ModelState.AddModelError("", "Kullanici kilitlendi.");
                        return View(model);
                    }

                    //Secure Password
                    if (!userInfo.IsSecurePassword)
                    {
                        return RedirectToAction("ChangePassword", new { userName = userInfo.LoginName });
                    }

                    //Redirection veya normal giris
                    if (AuthManager.IsAuthenticated)
                    {
                        if (!string.IsNullOrWhiteSpace(Request.QueryString["r"]))
                        {
                            return Redirect(HttpUtility.HtmlDecode(Request.QueryString["r"]));
                        }
                        return RedirectToAction("Dashboard", "Home");
                    }
                    return RedirectToAction("Login");
                }

                ModelState.AddModelError("", ret);
                //AuthManager.LogoutMember();
                //model.UserName = Authentication.UserInfo.LoginName;
                //return Login(model, "");

                //}
                //else
                //{
                //    return RedirectToAction("Logout");
                //}
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(string email)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(email))
                {
                    ModelState.AddModelError("", "Email adresini girmelisiniz");
                }
                else
                {
                    var emailUser = _userUipc.GetUser(email.Trim());
                    if (emailUser != null)
                    {
                        var tempPass = Guid.NewGuid().ToString().Substring(0, 8);
                        emailUser.Password = tempPass.Base64Encode();
                        var result = _userUipc.ResetPassword(emailUser, emailUser.Id, "Kullanıcının kendisi - Şifre Yenileme");
                        if (result)
                        {
                            MailManager.Send("Efes Kullanıcı Bilgileriniz", email.Trim(),
                                "Kullanıcı Adınız : " + emailUser.LoginName + "<br/> Geçici Şifreniz : " + tempPass);
                        }
                        else
                        {
                            ModelState.AddModelError("", "Şifre yenileme esnasında hata oluştu.");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Girdiğiniz email adresi sistemde kayıtlı değildir.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            if (!ModelState.IsValid)
            {
                TempData["ViewData"] = ViewData;
            }

            return RedirectToAction("Login");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}