﻿using System.Web.Mvc;
using Efes.Entities.Law;
using Efes.UIPC;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    public class LawOfficeAccountController : BaseController
    {
        private readonly LawOfficeUipc _lawOfficeUipc;
        public LawOfficeAccountController()
        {
            _lawOfficeUipc = new LawOfficeUipc();
        }
        // GET: LawOfficeFile
        [AuthorizeMember(Rules = "ViewOfficeAccountList")]
        public ActionResult Index(int id)
        {
            var model = _lawOfficeUipc.GetLawOfficeAccountListModelByOfficeId(id);
            return View(model);
        }

        [AuthorizeMember(Rules = "ViewOfficeAccountList")]
        public ActionResult Index2()
        {
            var model = _lawOfficeUipc.GetLawOfficeAccountListModelByUserId(Authentication.UserInfo.Id);
            return View("Index", model);
        }

        // GET: LawOfficeFile/Details/5
        [AuthorizeMember(Rules = "ViewOfficeAccountDetails")]
        public ActionResult Details(int id)
        {
            var model = _lawOfficeUipc.GetLawOfficeAccountByOfficeId(id);
            return View(model);
        }

        // GET: LawOfficeFile/Create
        [AuthorizeMember(Rules = "CreateOfficeAccount")]
        public ActionResult Create(int id)
        {
            var officeAccount = new OfficeAccount();
            officeAccount.LawOfficeId = id;
            return View(officeAccount);
        }

        // POST: LawOfficeFile/Create
        [HttpPost]
        [AuthorizeMember(Rules = "CreateOfficeAccount")]
        public ActionResult Create(OfficeAccount officeAccount)
        {
            try
            {
                officeAccount.CreatedBy = Authentication.UserInfo.Id;
                var success = _lawOfficeUipc.AddAccountToLawOffice(officeAccount);

                return RedirectToAction("Index", new { id = officeAccount.LawOfficeId });
            }
            catch
            {
                return View(officeAccount);
            }
        }

        // GET: LawOfficeFile/Edit/5
        [AuthorizeMember(Rules = "EditOfficeAccount")]
        public ActionResult Edit(int id)
        {
            var model = _lawOfficeUipc.GetLawOfficeAccountByOfficeId(id);
            return View(model);
        }

        // POST: LawOfficeFile/Edit/5
        [HttpPost]
        [AuthorizeMember(Rules = "EditOfficeAccount")]
        public ActionResult Edit(int id, OfficeAccount officeAccount)
        {
            try
            {
                officeAccount.UpdatedBy = Authentication.UserInfo.Id;
                var success = _lawOfficeUipc.UpdateLawOfficeAccount(officeAccount);
                return RedirectToAction("Index", new { id = officeAccount.LawOfficeId});
            }
            catch
            {
                return View();
            }
        }

        // GET: LawOfficeFile/Delete/5
        [AuthorizeMember(Rules = "DeleteOfficeAccount")]
        public ActionResult Delete(int id)
        {
            var model = _lawOfficeUipc.GetLawOfficeAccountByOfficeId(id);
            return View(model);
        }

        // POST: LawOfficeFile/Delete/5
        [HttpPost]
        [AuthorizeMember(Rules = "DeleteOfficeAccount")]
        public ActionResult Delete(int id, OfficeAccount officeAccount)
        {
            try
            {
                // TODO: Add delete logic here
                officeAccount.UpdatedBy = Authentication.UserInfo.Id;
                officeAccount.IsActive = false;
                var success = _lawOfficeUipc.UpdateLawOfficeAccount(officeAccount);
                return RedirectToAction("Index", new { id = officeAccount.LawOfficeId });
            }
            catch
            {
                return View();
            }
        }
    }
}
