﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web.Mvc;
using Efes.Entities.Excel;
using Efes.Entities.Sms;
using Efes.Models;
using Efes.UIPC;
using Efes.Utility;
using Efes.WC;

namespace Efes.Backoffice.Controllers
{
    [AuthorizeMember]
    public class SmsController : BaseController
    {
        //
        // GET: /Sms/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FileImport(string type, bool? activeProtocol, bool? protocolApprove, bool? closeAccount, bool? salary)
        {
            if (Request.Files.Count == 0)
            {
                return Json(new { Result = "Error", Message = "File is missing." }, JsonRequestBehavior.AllowGet);
            }

            var importType = (SmsImportType)type.Convert<int>();
            return importType != SmsImportType.None ?
                Import(importType,
                activeProtocol.Convert<bool>(),
                protocolApprove.Convert<bool>(),
                closeAccount.Convert<bool>(),
                salary.Convert<bool>()) :
                Json(new
                {
                    Result = "Error",
                    Message = "File Type is not recognized or declared."
                }, JsonRequestBehavior.AllowGet);
        }

        private ActionResult Import(SmsImportType smsImportType, bool activeProtocol, bool protocolApprove, bool closeAccount, bool salary)
        {
            try
            {
                var necessaryColumnList = GetNecessaryColumnList(smsImportType);

                var resultList = ExcelImport(necessaryColumnList);
                if (resultList.Any())
                {
                    return Json(new { Result = "Error", MissingColumnNames = string.Join(",", resultList) });
                }

                var dt = CacheManager.Default.Get("ImportedSmsFile" + Request.BoUniqueId()) as DataTable;
                if (dt.HasData())
                {
                    necessaryColumnList = GetNecessaryColumnList(smsImportType);
                    var tempList = (dt.Columns.Cast<DataColumn>()
                        .Where(column => !necessaryColumnList.Contains(column.ColumnName.Trim()))
                        .Select(column => column.ColumnName)).ToList();

                    foreach (var columnname in tempList)
                    {
                        dt.Columns.Remove(columnname);
                    }

                    var result = InsertToDb(smsImportType, dt, activeProtocol, protocolApprove, closeAccount, salary);
                    CacheManager.Default.Remove("ImportedSmsFile" + Request.BoUniqueId());


                    return Json(new
                    {
                        Result = result ? "Success" : "Error",
                        Message = result ? "" : "Kayıtlar veritabanına alınamadı."
                    });
                }
                CacheManager.Default.Remove("ImportedSmsFile" + Request.BoUniqueId());
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("BulkSmsImport error", ex);
                return Json(new { Result = "Error", Message = "Kayıtlar içeri alınırken hata ile karşılaşıldı." });
            }
        }

        private bool InsertToDb(SmsImportType smsImportType, DataTable dataTable, bool activeProtocol, bool protocolApprove, bool closeAccount, bool salary)
        {
            return new FileManagerUipc().FileImportForBulkSms(smsImportType, dataTable, Authentication.UserInfo.Id, activeProtocol, protocolApprove, closeAccount, salary);
        }

        private List<string> GetNecessaryColumnList(SmsImportType smsImportType)
        {
            List<string> list = new List<string>();
            switch (smsImportType)
            {
                case SmsImportType.AccountId:
                    list.Add("AccountId");
                    list.Add("Message");
                    break;
                case SmsImportType.Tckn:
                    list.Add("TCKN");
                    list.Add("Message");
                    break;
            }
            return list;
        }

        private List<string> ExcelImport(List<string> necessaryColumnList)
        {

            DataSet ds = new DataSet();
            var httpPostedFileBase = Request.Files[0];
            if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0)
            {
                string fileExtension = System.IO.Path.GetExtension(Request.Files[0].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    var mapPath = Server.MapPath("~/ImportedExcelFiles/Sms/");
                    bool exists = System.IO.Directory.Exists(mapPath);
                    if (!exists)
                    {
                        System.IO.Directory.CreateDirectory(mapPath);
                    }
                    string fileLocation = string.Format("{0}{1}_{2}_{3}", mapPath,
                        Request.Files[0].FileName, Authentication.UserInfo.Id, DateTime.Now.ToLongDateString());
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    else
                    {

                    }
                    Request.Files[0].SaveAs(fileLocation);

                    string excelConnectionString = string.Empty;
                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        switch (fileExtension)
                        {
                            case ".xls":
                                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                                                        fileLocation +
                                                        ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                                break;
                            case ".xlsx":
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                                        fileLocation +
                                                        ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                break;
                        }
                    }
                    else
                    {
                        return null;
                    }

                    var excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();

                    var dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    var excelSheets = new String[dt.Rows.Count];

                    int t = 0;
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    var excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("SELECT * FROM [{0}]", excelSheets[0]);
                    using (var dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }

                    CacheManager.Default.Add("ImportedSmsFile" + Request.BoUniqueId(), ds.Tables[0]);
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        necessaryColumnList.Remove(column.ColumnName);
                    }

                    excelConnection.Close();
                    excelConnection.Dispose();

                }
            }
            return necessaryColumnList;
        }

        public ActionResult SendSms(string importId, string type)
        {
            try
            {
                var id = Cryptography.Decrypt(importId).Convert<int>();
                var smsImportType = (SmsImportType)type.Convert<int>();

                if (id > 0 && (smsImportType == SmsImportType.AccountId || smsImportType == SmsImportType.Tckn))
                {
                    var smsContactModelList = new List<SmsContactModel>();

                    switch (smsImportType)
                    {
                        case SmsImportType.AccountId:
                            smsContactModelList = new ContactUipc().GetContactDataWithImportIdByAccountId(id);
                            break;
                        case SmsImportType.Tckn:
                            smsContactModelList = new ContactUipc().GetContactDataWithImportIdByTckn(id);
                            break;
                    }

                    if (smsContactModelList.Any() && (smsContactModelList.Any(s => !string.IsNullOrWhiteSpace(s.Gsm)) || smsContactModelList.Any(s => !string.IsNullOrWhiteSpace(s.Tckn))))
                    {
                        var smsMessenger = new SmsMessenger();
                        var resultList = new List<string>();

                        switch (smsImportType)
                        {
                            case SmsImportType.AccountId:
                                var gsmMessageList = smsContactModelList.Select(smsContactmodel => new GsmMessage()
                                {
                                    Content = smsContactmodel.Sms,
                                    GsmNumber = smsContactmodel.Gsm,
                                }).ToList();
                                resultList = smsMessenger.SendSmsWithGsmNo(gsmMessageList);
                                break;
                            case SmsImportType.Tckn:
                                return Json(new { Result = "Error", ResultList = resultList });
                        }

                        return Json(new { Result = "Success", ResultList = resultList });
                    }
                    Logger.Error("Veritabanina ilk asamada alinan logid ile gonderme asamasinda cekilen kayitlarda sms gonderilecek kayit bulunamadi. Sms gonderim tipi: " + smsImportType);
                    return Json(new { Result = "Info", Message = "Gonderilecek kayit bulunmadi." });
                }

                Logger.Error("Import Id veya  sms gonderim tipi client'tan dogru gelmedi.", new Exception());
                return Json(new { Result = "Error", Message = "Import Id veya  sms gonderim tipi client'tan dogru gelmedi." });
            }
            catch (Exception ex)
            {
                Logger.Error("Sms gönderilirken hata ile karşılaşıldı.", ex);
                return Json(new { Result = "Error", Message = "Sms gönderilirken hata ile karşılaşıldı.", Exc = ex });
            }
        }

        public ActionResult GetSmsTemplates()
        {
            return Json(new SmsUipc().GetSmsTemplates());
        }

        public ActionResult GetBannedWords()
        {
            return Json(new { bannedWords = new SmsUipc().GetBannedWords() });
        }
    }
}