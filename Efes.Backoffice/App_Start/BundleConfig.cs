﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Optimization;
using Efes.Utility;

namespace Efes.Backoffice
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Javascript

            var bundle = new Bundle("~/bundles/layout") { Orderer = new AsIsBundleOrderer() };
            bundle
                .Include("~/assets/global/plugins/jquery.min.js")
                .Include("~/assets/global/plugins/bootstrap/js/bootstrap.min.js")
                .Include("~/assets/global/plugins/js.cookie.min.js")
                .Include("~/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js")
                .Include("~/assets/global/plugins/jquery.blockui.min.js")
                .Include("~/assets/global/plugins/uniform/jquery.uniform.min.js")
                .Include("~/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js")
                .Include("~/assets/global/plugins/jquery-notific8/jquery.notific8.min.js")
                .Include("~/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")
                .Include("~/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js")
                //.Include("~/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.tr.min.js")
                .Include("~/assets/global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.tr.js")
                .Include("~/assets/global/scripts/app.min.js")
                .Include("~/assets/layouts/scripts/layout.min.js")
                .Include("~/assets/layouts/scripts/demo.min.js")
                .Include("~/assets/layouts/global/scripts/quick-sidebar.min.js")
                .Include("~/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js")
                .Include("~/assets/pages/scripts/ui-notific8.min.js")
                .Include("~/assets/pages/scripts/ui-modals.min.js")
                .Include("~/assets/global/plugins/moment.min.js")
                .Include("~/assets/pages/scripts/components-date-time-pickers.min.js")
                .Include("~/assets/pages/scripts/components-date-time-pickers-tr.js")
                .Include("~/Scripts/underscore.min.js")
                .Include("~/Scripts/jquery.validate.js")
                .Include("~/assets/global/plugins/datatables/datatables.min.js")
                .Include("~/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js")
                .Include("~/assets/global/plugins/datatables/plugins/datatables.moment.js")
                .Include("~/assets/global/plugins/jquery.pulsate.min.js")
                .Include("~/assets/global/plugins/nouislider/nouislider.min.js")
                .Include("~/assets/global/plugins/nouislider/wNumb.min.js")
                .Include("~/Scripts/Inputmask/inputmask.min.js")
                .Include("~/Scripts/Inputmask/jquery.inputmask.min.js")
                .Include("~/Scripts/Inputmask/inputmask.extensions.min.js")
                .Include("~/Scripts/Inputmask/inputmask.date.extensions.min.js")
                .Include("~/Scripts/Inputmask/inputmask.regex.extensions.min.js")
                .Include("~/Scripts/Inputmask/inputmask.numeric.extensions.min.js")
                .Include("~/Scripts/quicksearch.js")
                .Include("~/content/scripts/Site.js")
                .Include("~/content/scripts/Obligor.js")
                .Include("~/content/scripts/TemplateManager.js")
                .Include("~/content/scripts/Core.js")
                .Include("~/content/scripts/ResponsiveUI.js");
            bundles.Add(bundle);

            bundles.Add(new ScriptBundle("~/bundles/fileupload")
                .Include("~/assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js")
                .Include("~/assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js")
                .Include("~/assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js")
                .Include("~/content/scripts/Excel.js"));

            bundles.Add(new ScriptBundle("~/bundles/Tool")
                .Include("~/content/scripts/Tool.js"));

            bundles.Add(new ScriptBundle("~/bundles/Authorization")
                .Include("~/content/scripts/Authorization.js"));

            bundles.Add(new ScriptBundle("~/bundles/dashboard")
                .Include("~/assets/global/plugins/moment.min.js")
                .Include("~/assets/global/plugins/morris/raphael-min.js")
                .Include("~/assets/global/plugins/counterup/jquery.waypoints.min.js")
                .Include("~/assets/global/plugins/counterup/jquery.counterup.min.js")
                .Include("~/assets/global/plugins/flot/jquery.flot.min.js")
                .Include("~/assets/global/plugins/flot/jquery.flot.resize.min.js")
                .Include("~/assets/global/plugins/flot/jquery.flot.categories.min.js")
                .Include("~/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js")
                .Include("~/assets/global/plugins/jquery.sparkline.min.js")
                .Include("~/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js")
                .Include("~/assets/pages/scripts/dashboard.js")
                .Include("~/assets/global/plugins/morris/morris.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/ltie9")
                .Include("~/assets/global/plugins/respond.min.js")
                .Include("~/assets/global/plugins/excanvas.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/multi-select")
                .Include("~/assets/pages/scripts/components-multi-select.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/select2")
                .Include("~/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js")
                .Include("~/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js")
                .Include("~/assets/global/plugins/select2/js/select2.full.min.js"));

            #endregion

            #region CSS

            //bundles.Add(new StyleBundle("~/Content/bundle_css")
            //    .Include("~/assets/global/plugins/font-awesome/css/font-awesome.min.css")
            //    .Include("~/assets/global/plugins/simple-line-icons/simple-line-icons.min.css")
            //    .Include("~/assets/global/plugins/bootstrap/css/bootstrap.min.css")
            //    .Include("~/assets/global/plugins/uniform/css/uniform.default.css")
            //    .Include("~/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css")
            //    .Include("~/assets/global/plugins/jquery-notific8/jquery.notific8.min.css")
            //    .Include("~/assets/global/css/components.min.css")
            //    .Include("~/assets/global/css/plugins.min.css")
            //    .Include("~/assets/layouts/css/layout.min.css")
            //    .Include("~/assets/layouts/css/themes/light.min.css")
            //    .Include("~/assets/layouts/css/custom.min.css")
            //    .Include("~/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css")
            //    .Include("~/assets/global/plugins/nouislider/nouislider.min.css")
            //    .Include("~/assets/global/plugins/nouislider/nouislider.pips.css")
            //    .Include("~/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css")
            //    .Include("~/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css")
            //    .Include("~/Content/css/google.open.sans.css"));


            bundles.Add(new StyleBundle("~/Content/fileUploadCss")
                .Include("~/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css")
                .Include("~/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css"));

            bundles.Add(new StyleBundle("~/Content/dashboardCss")
                .Include("~/assets/global/plugins/morris/morris.css")
                .Include("~/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"));

            bundles.Add(new StyleBundle("~/Content/select2")
                .Include("~/assets/global/plugins/select2/css/select2.min.css")
                .Include("~/assets/global/plugins/select2/css/select2-bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/obligor_css")
                .Include("~/assets/pages/css/obligor.css")
                .Include("~/assets/global/plugins/bootstrap-tabdrop/css/tabdrop.css"));


            #endregion
        }
    }
}
