﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    public class Word
    {
        [DisplayName("Kelime")]
        public string WordText { get; set; }
        [DisplayName("Kelime Sayısı")]
        public int WordCount { get; set; }
    }
}
