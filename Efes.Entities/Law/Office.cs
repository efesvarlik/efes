﻿using System.ComponentModel.DataAnnotations;

namespace Efes.Entities
{
    public class Office
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        [Display(Name = "Hukuk Bürosu")]
        public string Name { get; set; }
        [Display(Name = "Şehir")]
        public int CityId { get; set; }
        [Display(Name = "Aktif mi?")]
        public bool IsActive { get; set; }
        [Display(Name = "Telefon Numarası")]
        public string PhoneNumber { get; set; }
    }
}
