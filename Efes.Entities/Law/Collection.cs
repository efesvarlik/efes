﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Law
{
    public class Collection
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string PaymentBank { get; set; }
        public string PaymentDesc { get; set; }
        public string StatusText { get; set; }
        public string EmployeeLoginID { get; set; }
        public string LawOffice { get; set; }
    }
}
