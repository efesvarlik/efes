﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Efes.Entities.LawOffice
{
    public class OfficeUser
    {
        [Display(Name = "Hukuk Bürosu")]
        public int LawOfficeId { get; set; }
        [Display(Name = "Kullanıcı")]
        public int UserId { get; set; }
        [Display(Name = "Aktif mi?")]
        public bool IsActive { get; set; }
        [Display(Name = "Oluşturulma Tarihi")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Oluşturan")]
        public int CreatedBy { get; set; }
        [Display(Name = "Değiştirilme Tarihi")]
        public DateTime UpdatedDate { get; set; }
        [Display(Name = "Değiştiren")]
        public int UpdatedBy { get; set; }
    }
}
