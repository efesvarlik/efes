﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Efes.Entities.Law
{
    public class OfficeAccount
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        [Display(Name = "Account Id")]
        public string AccountId { get; set; }
        [Display(Name = "Hukuk Bürosu Id")]
        public int LawOfficeId { get; set; }
        [Display(Name = "Aktif mi?")]
        public bool IsActive { get; set; }
        [Display(Name = "Oluşturulma Tarihi")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Oluşturan")]
        public int CreatedBy { get; set; }
        [Display(Name = "Değiştirilme Tarihi")]
        public DateTime UpdatedDate { get; set; }
        [Display(Name = "Değiştiren")]
        public int UpdatedBy { get; set; }
        [Display(Name = "Dosya Sahibi")]
        public string AccountOwner { get; set; }
    }
}
