﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Fgs
{
    public class MissedCall
    {
        public DateTime CallTime { get; set; }
        public string CallId { get; set; }
        public string CallingNumber { get; set; }
        public string CalledNumber { get; set; }
        public string ClosingReason { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string Number { get; set; }
    }
}
