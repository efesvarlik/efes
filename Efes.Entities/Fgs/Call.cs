﻿using System;

namespace Efes.Entities.Fgs
{
    public class Call
    {
        public string TelNo { get; set; }
        public string Cpn { get; set; }
        public string AgentName { get; set; }
        public int QueueNo { get; set; }
        public RecordTypeEnum RecordType { get; set; }
        public DateTime NextTryDate { get; set; }
        public string CustomerName { get; set; }
        public string CompanyName { get; set; }
        public string AccountId { get; set; }

        public enum RecordTypeEnum
        {
            Agent = 0,
            Pool = 1,
        }

        public Call()
        {
            
        }

        public Call(string telNo, string cpn, string agentName, int queueNo, RecordTypeEnum recordType, DateTime nextTryDate, string customerName, string companyName)
        {
            TelNo = telNo;
            Cpn = cpn;
            AgentName = agentName;
            QueueNo = queueNo;
            RecordType = recordType;
            NextTryDate = nextTryDate;
            CustomerName = customerName;
            CompanyName = companyName;
        }

    }
}
