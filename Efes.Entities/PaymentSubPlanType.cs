﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    public class PaymentSubPlanType
    {
        public int Id { get; set; }
        public int PaymentPlanId { get; set; }
        public string Value { get; set; }
    }
}
