﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    [Serializable]
    public class Notification
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string Title  { get; set; }
        public string From { get; set; }
        public string Body { get; set; }
        public string TimeText { get; set; }
        public bool IsRead { get; set; }
        public NotificationType NotificationType { get; set; }
    }

    public enum NotificationType
    {
        Information = 1,
        Warning = 2,
        Error = 3,
        Verification = 4,
        General = 5
    }
}
