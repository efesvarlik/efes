﻿using System.ComponentModel;

namespace Efes.Entities.Portfolio
{
    public class Portfolio
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("Banka İsmi")]
        public string BankName { get; set; }

        [DisplayName("Portföy İsmi")]
        public string Name { get; set; }

        [DisplayName("Borçlu Sayısı")]
        public string BorcluSayisi { get; set; }

        [DisplayName("Kefil Sayısı")]
        public string KefilSayisi { get; set; }

        [DisplayName("Borç Sayısı")]
        public string BorcSayisi { get; set; }

        [DisplayName("Adres Sayısı")]
        public string AdresSayisi { get; set; }

        [DisplayName("Telefon Sayısı")]
        public string TelefonSayisi { get; set; }

        public bool IsActive { get; set; }
    }
}
