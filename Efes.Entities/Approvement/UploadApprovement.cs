﻿using System;
using System.ComponentModel;

namespace Efes.Entities.Approvement
{
    [Serializable]
    public class UploadApprovement
    {
        [DisplayName("Bulk Id")]
        public string BulkId { get; set; }
        [DisplayName("Yükleme Tipi")]
        public string Type { get; set; }
        [DisplayName("Kayıt Sayısı")]
        public int TotalCount { get; set; }
        [DisplayName("Kullanıcı")]
        public string UserName { get; set; }
        [DisplayName("Açıklama")]
        public string Desc { get; set; }
    }
}
