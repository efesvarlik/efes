﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Approvement
{
    [Serializable]
    public class ChangePaymentOwner
    {
        [DisplayName("Id")]
        public int Id { get; set; }
        [DisplayName("Rec Id")]
        public string RecId { get; set; }
        [DisplayName("Dosya ID")]
        public string FileId { get; set; }
        [DisplayName("Ödeme Tarihi")]
        public string PaymentDate { get; set; }
        [DisplayName("Ödeme Tutarı")]
        public string PaymentAmount { get; set; }
        [DisplayName("Eski Ödeme Sahibi")]
        public string OldOwner { get; set; }
        [DisplayName("Yeni Ödeme Sahibi")]
        public string NewOwner { get; set; }
        [DisplayName("Değiştiren")]
        public string Changer { get; set; }
        [DisplayName("Değişiklik Tarihi")]
        public string ChangeDate { get; set; }
    }
}
