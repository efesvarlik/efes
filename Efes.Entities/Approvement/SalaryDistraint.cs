﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Approvement
{
    [Serializable]
    public class SalaryDistraint
    {
        [DisplayName("Id")]
        public string Id { get; set; }
        [DisplayName("Dosya Adı")]
        public string FileName { get; set; }
        [DisplayName("Dosya Tarihi")]
        public DateTime FileDate { get; set; }
        [DisplayName("Kayıt Sayısı")]
        public int RecordCount { get; set; }
        [DisplayName("Yüklemeyi Başlatan ID")]
        public string StarterLoadingId { get; set; }
    }
}
