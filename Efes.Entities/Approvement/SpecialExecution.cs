﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Approvement
{
    [Serializable]
    public class SpecialExecution
    {
        // DosyaID, BorçluAdSoyad, DevirAlınanToplam, DevirAlınanAnapara, TahsilEdilenToplam,  DeğiştirenID, DeğişiklikTarihi 
        //Id,AccountId,FullName,TotalDebtAmount,DebtPrinciple,CreditPrinciple,Reason, CreatedBy, CreatedDate

        [DisplayName("Id")]
        public int Id { get; set; }
        [DisplayName("DosyaID")]
        public string AccountId { get; set; }
        [DisplayName("Borçlu Ad Soyad")]
        public string FullName { get; set; }
        [DisplayName("Devir Alınan Toplam")]
        public string TotalDebtAmount { get; set; }
        [DisplayName("Devir Alınan Anapara")]
        public string DebtPrinciple { get; set; }
        [DisplayName("Tahsil Edilen Toplam")]
        public string CreditPrinciple { get; set; }
        [DisplayName("Sebep")]
        public string Reason { get; set; }
        [DisplayName("Değiştiren")]
        public string CreatedBy { get; set; }
        [DisplayName("Değişiklik Tarihi")]
        public string CreatedDate { get; set; }
    }
}
