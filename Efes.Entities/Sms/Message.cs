﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Sms
{
    [Serializable]
    public class Message
    {
        public string GsmNumber { get; set; }
        public string Content { get; set; }
        public int LogId { get; set; }
        public string TcNo { get; set; }

    }

    [Serializable]
    public class MessageContent
    {
        public string Content { get; set; }
        public string RecId { get; set; }
    }

    [Serializable]
    public class GsmMessage: MessageContent
    {
        public string GsmNumber { get; set; }
    }

    [Serializable]
    public class TcknMessage : MessageContent
    {
        public string TcNo { get; set; }
    }
}
