﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Sms
{
    [Serializable]
    public class Template
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string TemplateText { get; set; }
    }
}
