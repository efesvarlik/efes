﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Protocol
{
    [Serializable]
    public class ProtocolCampaign
    {
        [Description("Id")]
        public int Id { get; set; }
        [Description("Açıklama")]
        public string Description { get; set; }
        [Description("Baslangıç Tarihi")]
        public DateTime StartDate { get; set; }
        [Description("Bitiş Tarihi")]
        public DateTime EndDate { get; set; }
        [Description("Geçerli mi?")]
        public bool IsValid { get; set; }
        [Description("Oluşturan")]
        public int CreatorId { get; set; }
        [Description("Değiştiren")]
        public int UpdaterId { get; set; }
        [Description("Oluşturulma Tarihi")]
        public DateTime CreatedDate { get; set; }
        [Description("Güncelleme Tarihi")]
        public DateTime UpdatedDate { get; set; }
        [Description("İlk Peşinat Tarihi")]
        public int MaxAdvancePaymentDay { get; set; }
        [Description("İlk Taksit Tarihi")]
        public int MaxInstallmentDay { get; set; }
        [Description("Minumum Tutar")]
        public decimal MinAmount { get; set; }
        [Description("Maksimum Tutar")]
        public decimal MaxAmount { get; set; }
        [Description("Yaş")]
        public int Age { get; set; }
        [Description("Portföy")]
        public int PortfolioId { get; set; }
        [Description("Borçlu Listesi")]
        public string ObligorList { get; set; }
        [Description("Kampanya Başarı Oranı")]
        public int CampaignSuccessRate { get; set; }
        [Description("Peşin Kapama Tutarı")]
        public decimal CashClosingAmount { get; set; }
        [Description("Detaylar")]
        public List<ProtocolCampaignDetails> Details { get; set; }
        [Description("Protokol Kampanya Tipi")]
        public ProtocolCampaignType ProtocolCampaignType { get; set; }

        public ProtocolCampaign()
        {
            Details = new List<ProtocolCampaignDetails>();
        }

    }

    public enum ProtocolCampaignType
    {
        ObligorList = 0,
        AgePortfolio = 1
    }
}
