﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Entities.Obligor;

namespace Efes.Entities.Protocol
{
    [Serializable]
    public class ProtocolCampaignDetails
    {
        [DisplayName("Id")]
        public int Id { get; set; }
        [DisplayName("Kampanya Id")]
        public int ProtocolCampaignId { get; set; }
        [DisplayName("Maksimum Taksit Miktarı")]
        public int MaxInstallmentCount { get; set; }
        [DisplayName("İndirim Yüzdesi")]
        public int DiscountPercentage { get; set; }
        [DisplayName("Rol")]
        public int RoleId { get; set; }
        [DisplayName("Rol İsmi")]
        public string RoleName { get; set; }
        [DisplayName("Borçlu Tipi")]
        public ObligorType ObligorType { get; set; }
    }
}
