﻿using System;
using System.ComponentModel;

namespace Efes.Entities.Protocol
{
    [Serializable]
    public class ProtocolDetail
    {
        [Description("Id")]
        public int Id { get; set; }
        [Description("Protokol Id")]
        public int ProtocolId { get; set; }
        [Description("Ay")]
        public int Month { get; set; }
        [Description("Ödeme Tarihi")]
        public DateTime PaymentDate { get; set; }
        [Description("Ödendi mi?")]
        public bool HasPayed { get; set; }
        [Description("Tutar")]
        public decimal Value { get; set; }
        [Description("Açıklama")]
        public string Desc { get; set; }
    }
}

