﻿using System;

namespace Efes.Entities.Protocol
{
    [Serializable]
    public class Protocol
    {
        public int Id { get; set; }
        public string ObligorId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ConfirmedId { get; set; }
        public int CampaignId { get; set; }
        public int? SenderId { get; set; }
        public decimal AdvancePaymentValue { get; set; }
        public DateTime? AdvancePaymentDate { get; set; }
        public decimal TotalPaymentValue { get; set; }
        public int InstallmentCount { get; set; }
        public bool HasCompleted { get; set; }
        public bool IsValid { get; set; }
        public string Note { get; set; }
        public decimal AgencyFee { get; set; }
    }
}
