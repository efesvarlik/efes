﻿using System;

namespace Efes.Entities.Protocol
{
    [Serializable]
    public class ProtocolApprovement
    {
        public string Id { get; set; }
        public int ProtocolId { get; set; }
        public string NameSurname { get; set; }
        public string Relation { get; set; }
        public string CreatedBy { get; set; }
        public decimal DebtPrinciple { get; set; }
        public decimal TotalDebtAmount { get; set; }
        public decimal TotalCreditAmount { get; set; }
        public decimal PlanAmount { get; set; }
        public int InstallmentCount { get; set; }
        public int ObligorAge { get; set; }
        public int ClosureRate { get; set; }
        public string GkfUrl { get; set; }
        public DateTime CreateDate { get; set; }
    }

    
}