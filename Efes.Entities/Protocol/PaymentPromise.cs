﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Protocol
{
    public class PaymentPromise
    {
        public int Id { get; set; }
        public decimal PlanAmount { get; set; }
        public decimal AdvancePayment { get; set; }
        public int InstallmentCount { get; set; }
        public DateTime Date { get; set; }
        public DateTime CreateDate { get; set; }
        public string ObligorId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string AccountId { get; set; }
    }
}
