﻿using System;
namespace Efes.Entities.Obligor
{
    public class KkbScore
    {
        public string TckNo { get; set; }
        public decimal KkbSkoru { get; set; }
        public DateTime KkbSorguTarihi { get; set; }
        public decimal ToplamHesapLimit { get; set; }
        public decimal ToplamHesapBakiye { get; set; }
        public DateTime KkbSonKanuniTakipTarihi { get; set; }
        public decimal ToplamKanuniTakipBakiye { get; set; }
        public decimal KanunTakipLimiti { get; set; }
        public decimal ToplamTakipBakiyesi { get; set; }
        public DateTime SonKrediVerilisTarihi { get; set; }

    }
}
