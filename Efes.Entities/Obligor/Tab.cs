﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Obligor
{
    [Serializable]
    public class Tab
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public string RuleName { get; set; }
    }
}
