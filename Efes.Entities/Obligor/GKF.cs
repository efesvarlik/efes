﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Obligor
{
    public class Gkf
    {
        public string AccountId { get; set; }
        public string SourceSet { get; set; }
        public string DisplayName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDateTime { get; set; }
        public string SendType { get; set; }
        public string ReportPage { get; set; }
    }
}
