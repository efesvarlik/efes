﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Obligor
{
    [Serializable]
    public class CallResult
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
