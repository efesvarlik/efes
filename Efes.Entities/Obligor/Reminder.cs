﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Obligor
{
    public class Reminder
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public DateTime RemindDate { get; set; }
        public string Detail { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string ObligorId { get; set; }
        public string AccountId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
