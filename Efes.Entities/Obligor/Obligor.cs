﻿using System;

namespace Efes.Entities.Obligor
{
    [Serializable]
    public class Obligor
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TcNo { get; set; }
        public string AccountId { get; set; }
        public string Gsm { get; set; }
        public string Portfoy { get; set; }
        public string RelationAboutDept { get; set; }
        public string ExecutionFile { get; set; }
        public string ExecutionDetail { get; set; }
        public string TotalPrincipal { get; set; }
        public string TotalRisk { get; set; }
        public string Bank { get; set; }
        public string Owner { get; set; }
        public bool IsBiggest{ get; set; }
        public string ContactRecId { get; set; }
    }

    public enum ObligorType
    {
        //Borclu
        Obligor = 1,
        //Kefil
        Guarantor = 2
    }
}
