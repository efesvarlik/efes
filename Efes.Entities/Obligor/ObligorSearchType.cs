namespace Efes.Entities.Obligor
{
    public enum ObligorSearchType
    {
        NameAndSurname = 0,
        Tckn = 1,
        PhoneNumber = 2,
        BankAccountNumber = 3,
        ProtocolNumber = 4,
        ExecutionFileNumber = 5,
        CollectionWage = 6,
        AccountId = 7,
        Score = 8
    }
}