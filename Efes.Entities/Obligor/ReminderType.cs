﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Obligor
{
    public class ReminderType
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
