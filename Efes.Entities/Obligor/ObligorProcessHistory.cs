﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Obligor
{
    [Serializable]
    public class ObligorProcessHistory
    {
        //Sıra, Oluşturan, Oluşturma Tarihi, İşlem, İşlem Alt kodu, Açıklama
        public string Id { get; set; }
        public int Order { get; set; }
        public string Creator { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string Process { get; set; }
        public string ProcessSubCode { get; set; }
        public string Desc { get; set; }
    }
}
