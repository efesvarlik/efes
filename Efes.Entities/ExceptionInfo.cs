﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    [Serializable]
    public class ExceptionInfo
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string UserMessage { get; set; }
        public string TransactionId { get; set; }
        public string AdditionalInformation { get; set; }
    }
}
