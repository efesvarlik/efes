﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    [Serializable]
    public class LoginHistory
    {
        public string Date { get; set; }
        public string Status { get; set; }
    }
}
