﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Utility;

namespace Efes.Entities.Krm
{
    public class Footer
    {
        private string _footerIdentifier;
        private string _versionNumber;
        private string _memberCode;
        private string _reserveField1;
        private string _totalCountOfCustomerRecords;
        private string _reserveField2;

        public string FooterIdentifier
        {
            get { return _footerIdentifier.ExpandStringPadRight(5); }
            set { _footerIdentifier = value; }
        }

        public string VersionNumber
        {
            get { return _versionNumber.ExpandStringPadRight(2); }
            set { _versionNumber = value; }
        }

        public string MemberCode
        {
            get { return _memberCode.ExpandStringPadLeft(5, '0'); }
            set { _memberCode = value; }
        }
        public string ReserveField1
        {
            get
            {
                if (_reserveField1 == null)
                {
                    _reserveField1 = string.Empty;
                }
                return _reserveField1.ExpandStringPadRight(65);
            }
            set { _reserveField1 = value; }
        }

        public string TotalCountOfCustomerRecords
        {
            get { return _totalCountOfCustomerRecords.ExpandStringPadLeft(7, '0'); }
            set { _totalCountOfCustomerRecords = value; }
        }

        public string ReserveField2
        {
            get
            {
                if (_reserveField2 == null)
                {
                    _reserveField2 = string.Empty;
                }
                return _reserveField2.ExpandStringPadRight(616);
            }
            set { _reserveField2 = value; }
        }
     
    }
}
