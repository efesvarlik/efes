﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Utility;

namespace Efes.Entities.Krm
{
    public class CSVYD
    {
        private string _recordType;
        private string _versionNumber;
        private string _memberCode;
        private string _identityType;
        private string _currencyCode;
        private string _identityNumber;
        private string _reserveField;
        private string _accountNumber;
        private string _transferDate;
        private string _transferMemberCode;
        private string _principalBalanceTransferedIn;
        private string _accountOpeningDate;

        public string RecordType
        {
            get { return _recordType.ExpandStringPadRight(5); }
            set { _recordType = value; }
        }

        public string VersionNumber
        {
            get { return _versionNumber.ExpandStringPadRight(2); }
            set { _versionNumber = value; }
        }

        public string MemberCode
        {
            get { return _memberCode.ExpandStringPadLeft(5, '0'); }
            set { _memberCode = value; }
        }

        public string IdentityType
        {
            get { return _identityType.ExpandStringPadRight(1); }
            set { _identityType = value; }
        }

        public string IdentityNumber
        {
            get { return _identityNumber.ExpandStringPadRight(11); }
            set { _identityNumber = value; }
        }

        public string AccountNumber
        {
            get { return _accountNumber.ExpandStringPadRight(20); }
            set { _accountNumber = value; }
        }

        public string TransferDate
        {
            get { return _transferDate.ExpandStringPadRight(8); }
            set { _transferDate = value; }
        }

        public DateTime TransferDateForOrder { get; set; }

        public string TransferMemberCode
        {
            get { return _transferMemberCode.ExpandStringPadLeft(5, '0'); }
            set { _transferMemberCode = value; }
        }

        public string PrincipalBalanceTransferedIn
        {
            get { return _principalBalanceTransferedIn.ExpandStringPadLeft(15, '0'); }
            set { _principalBalanceTransferedIn = value; }
        }

        public string CurrencyCode
        {
            get { return _currencyCode.ExpandStringPadLeft(3, '0'); }
            set { _currencyCode = value; }
        }

        public string AccountOpeningDate
        {
            get { return _accountOpeningDate.ExpandStringPadRight(8); }
            set { _accountOpeningDate = value; }
        }

        public string ReserveField
        {
            get
            {
                if (_reserveField == null)
                {
                    _reserveField = string.Empty;
                }
                return _reserveField.ExpandStringPadRight(617);
            }
            set { _reserveField = value; }
        }

        public string RecId { get; set; }
        
    }
}
