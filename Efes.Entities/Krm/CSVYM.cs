﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Utility;

namespace Efes.Entities.Krm
{
    public class CSVYM
    {
        private string _recordType;
        private string _versionNumber;
        private string _memberCode;
        private string _identityType;
        private string _identityNumber;
        private string _name;
        private string _secondaryName;
        private string _surname;
        private string _address1;
        private string _address2;
        private string _phoneNumber1;
        private string _phoneNumber2;
        private string _phoneNumber3;
        private string _email1;
        private string _email2;
        private string _paymentPlanDate;
        private string _paymentPlanInstallmentCount;
        private string _payedInstallmentCount;
        private string _payedDebt;
        private string _remainingDebt;
        private string _currencyCode;
        private string _revisedPaymentPlanDate;
        private string _revisedPaymentPlanInstallmentCount;
        private string _revisedPayedInstallmentCount;
        private string _revisedRemainingDebt;
        private string _firstExecutiveDate;
        private string _lastExecutiveDate;
        private string _executiveAmount;
        private string _deptStatus;
        private string _deptStatusAlterationStatus;
        private string _closureDate;
        private string _reserveField;
        public List<CSVYD> CSVYDList { get; set; }
        public string RecordType
        {
            get { return _recordType.ExpandStringPadRight(5); }
            set { _recordType = value; }
        }

        public string VersionNumber
        {
            get { return _versionNumber.ExpandStringPadRight(2); }
            set { _versionNumber = value; }
        }

        public string MemberCode
        {
            get { return _memberCode.ExpandStringPadLeft(5, '0'); }
            set { _memberCode = value; }
        }

        public string IdentityType
        {
            get { return _identityType.ExpandStringPadRight(1); }
            set { _identityType = value; }
        }

        public string IdentityNumber
        {
            get { return _identityNumber.ExpandStringPadRight(11); }
            set { _identityNumber = value; }
        }

        public string Name
        {
            get { return _name.ExpandStringPadRight(15); }
            set { _name = value; }
        }

        public string SecondaryName
        {
            get { return _secondaryName.ExpandStringPadRight(15); }
            set { _secondaryName = value; }
        }

        public string Surname
        {
            get { return _surname.ExpandStringPadRight(30); }
            set { _surname = value; }
        }

        public string Address1
        {
            get { return _address1.ExpandStringPadRight(120); }
            set { _address1 = value; }
        }

        public string Address2
        {
            get { return _address2.ExpandStringPadRight(120); }
            set { _address2 = value; }
        }

        public string PhoneNumber1
        {
            get { return _phoneNumber1.ExpandStringPadRight(16); }
            set { _phoneNumber1 = value; }
        }

        public string PhoneNumber2
        {
            get { return _phoneNumber2.ExpandStringPadRight(16); }
            set { _phoneNumber2 = value; }
        }

        public string PhoneNumber3
        {
            get { return _phoneNumber3.ExpandStringPadRight(16); }
            set { _phoneNumber3 = value; }
        }

        public string Email1
        {
            get { return _email1.ExpandStringPadRight(50); }
            set { _email1 = value; }
        }

        public string Email2
        {
            get { return _email2.ExpandStringPadRight(50); }
            set { _email2 = value; }
        }

        public string PaymentPlanDate
        {
            get { return _paymentPlanDate.ExpandStringPadRight(8); }
            set { _paymentPlanDate = value; }
        }

        public string PaymentPlanInstallmentCount
        {
            get { return _paymentPlanInstallmentCount.ExpandStringPadLeft(4, '0'); }
            set { _paymentPlanInstallmentCount = value; }
        }

        public string PayedInstallmentCount
        {
            get { return _payedInstallmentCount.ExpandStringPadLeft(4, '0'); }
            set { _payedInstallmentCount = value; }
        }

        public string PayedDebt
        {
            get { return _payedDebt.ExpandStringPadLeft(15, '0'); }
            set { _payedDebt = value; }
        }

        public string RemainingDebt
        {
            get { return _remainingDebt.ExpandStringPadLeft(15, '0'); }
            set { _remainingDebt = value; }
        }

        public string CurrencyCode
        {
            get { return _currencyCode.ExpandStringPadRight(3); }
            set { _currencyCode = value; }
        }

        public string RevisedPaymentPlanDate
        {
            get { return _revisedPaymentPlanDate.ExpandStringPadRight(8); }
            set { _revisedPaymentPlanDate = value; }
        }

        public string RevisedPaymentPlanInstallmentCount
        {
            get { return _revisedPaymentPlanInstallmentCount.ExpandStringPadLeft(4, '0'); }
            set { _revisedPaymentPlanInstallmentCount = value; }
        }

        public string RevisedPayedInstallmentCount
        {
            get { return _revisedPayedInstallmentCount.ExpandStringPadLeft(4, '0'); }
            set { _revisedPayedInstallmentCount = value; }
        }

        public string RevisedRemainingDebt
        {
            get { return _revisedRemainingDebt.ExpandStringPadLeft(15, '0'); }
            set { _revisedRemainingDebt = value; }
        }

        public string FirstExecutiveDate
        {
            get { return _firstExecutiveDate.ExpandStringPadRight(8); }
            set { _firstExecutiveDate = value; }
        }

        public string LastExecutiveDate
        {
            get { return _lastExecutiveDate.ExpandStringPadRight(8); }
            set { _lastExecutiveDate = value; }
        }

        public string ExecutiveAmount
        {
            get { return _executiveAmount.ExpandStringPadLeft(15, '0'); }
            set { _executiveAmount = value; }
        }

        public string DebtStatus
        {
            get { return _deptStatus.ExpandStringPadRight(1); }
            set { _deptStatus = value; }
        }

        public string DebtStatusAlterationStatus
        {
            get { return _deptStatusAlterationStatus.ExpandStringPadRight(1); }
            set { _deptStatusAlterationStatus = value; }
        }

        public string ClosureDate
        {
            get { return _closureDate.ExpandStringPadRight(8); }
            set { _closureDate = value; }
        }

        public string ReserveField
        {
            get
            {
                if (_reserveField == null)
                {
                    _reserveField = string.Empty;
                }
                return _reserveField.ExpandStringPadRight(107);
            }
            set { _reserveField = value; }
        }

        public string RecId { get; set; }

        public CSVYM()
        {
            CSVYDList = new List<CSVYD>();
        }
    }
}
