﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Efes.Utility;

namespace Efes.Entities.Krm
{
    public class Header
    {
        private string _fileHeaderRecord;
        private string _versionNumber;
        private string _memberCode;
        private string _reserveField1;
        private string _memberName;
        private string _creationDate;
        private string _declarationDate;
        private string _reserveField2;

        public string FileHeaderRecord
        {
            get { return _fileHeaderRecord.ExpandStringPadRight(5); }
            set { _fileHeaderRecord = value; }
        }

        public string VersionNumber
        {
            get { return _versionNumber.ExpandStringPadRight(2); }
            set { _versionNumber = value; }
        }

        public string MemberCode
        {
            get { return _memberCode.ExpandStringPadLeft(5, '0'); }
            set { _memberCode = value; }
        }

        public string ReserveField1
        {
            get
            {
                if (_reserveField1 == null)
                {
                    _reserveField1 = string.Empty;
                }
                return _reserveField1.ExpandStringPadRight(65);
            }
            set { _reserveField1 = value; }
        }

        public string MemberName
        {
            get { return _memberName.ExpandStringPadRight(30); }
            set { _memberName = value; }
        }

        public string CreationDate
        {
            get { return _creationDate.ExpandStringPadRight(8); }
            set { _creationDate = value; }
        }

        public string DeclarationDate
        {
            get { return _declarationDate.ExpandStringPadRight(8); }
            set { _declarationDate = value; }
        }

        public string ReserveField2
        {
            get
            {
                if (_reserveField2 == null)
                {
                    _reserveField2 = string.Empty;
                }
                return _reserveField2.ExpandStringPadRight(577);
            }
            set { _reserveField2 = value; }
        }
    }
}
