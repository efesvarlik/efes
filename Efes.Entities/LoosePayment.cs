﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    public class LoosePayment
    {
        public string RecId { get; set; }
        public string ContactRecId { get; set; }
        public bool IsRelated { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentDateStr {
            get { return PaymentDate.ToShortDateString(); }
        }
        public decimal Amount { get; set; }
        public string PaymentBank { get; set; }
        public string PaymentDesc { get; set; }
        public string DbsLine { get; set; }
        public string DisplayName { get; set; }
        public string Tckn { get; set; }
        public string EmployeeLoginId { get; set; }
        public string StatusText { get; set; }
        public string MessageText { get; set; }
        public string TahsilatYevmiyeNo { get; set; }
        public string MuhasebeFisNo { get; set; }
    }
}
