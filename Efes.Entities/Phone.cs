﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    public class Phone
    {
        public string AccountId { get; set; }
        public string Number { get; set; }
        public string NumberText { get; set; }
        public string Fct { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
    }
}
