﻿namespace Efes.Entities
{
    public class County
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
    }
}