﻿using System;

namespace Efes.Entities.Excel
{
    public enum DataImportType
    {
        Address = 1,
        Note = 2,
        Phone = 3,
        Portfolio = 4,
        AttachBulkFile = 5
    }

    public enum SmsImportType
    {
        None = 0,
        AccountId = 1,
        Tckn = 2
    }

    [Serializable]
    public class AccountIdSms
    {
        public string AccountId { get; set; }
        public string Sms { get; set; }
    }
}
