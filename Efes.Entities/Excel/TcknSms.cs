﻿using System;

namespace Efes.Entities.Excel
{
    [Serializable]
    public class TcknSms
    {
        public string Tckn { get; set; }
        public string Sms { get; set; }
    }
}