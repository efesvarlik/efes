﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    [Serializable]
    public class AuthToken
    {
        public int Id { get; set; }// User Id
        public string UserName { get; set; }// User Name
        public DateTime Exp { get; set; } //Expire Date
    }
}
