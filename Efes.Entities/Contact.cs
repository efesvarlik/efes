﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities
{
    [Serializable]
    public class Contact
    {
        public int Id { get; set; }
        public string AccountId { get; set; }
        public string Tckn { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Gsm { get; set; }
    }
}
