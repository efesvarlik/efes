using System;

namespace Efes.Entities.User
{
    [Serializable]
    public enum RoleTypes
    {
        Admin = 1,
        FinanceAdmin = 2,
        ItUser = 3,
        Client = 4,
        BoAdmin = 5,
        FinanceUser = 6,
        ItAdmin = 7
    }
}