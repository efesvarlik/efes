using System;

namespace Efes.Entities.User
{
    [Serializable]
    public class Rule
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public bool IsActive { get; set; }
    }
}