using System;

namespace Efes.Entities.User
{
    [Serializable]
    public enum RuleTypes
    {
        UserCreate = 1,
        EditUser = 2,
        ViewAuditLogs = 3,
        AddExcelFile = 4,
        MultiplePhonePassive = 5,
        SinglePhonePassive = 6,
        ViewFinancialRecord = 7,
        CreateFinancialRecord = 8,
        EditFinancialRecord = 9
    }
}