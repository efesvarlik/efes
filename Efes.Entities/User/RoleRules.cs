using System;

namespace Efes.Entities.User
{
    [Serializable]
    public class RoleRules
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int RuleId { get; set; }
        public bool IsActive { get; set; }
    }
}