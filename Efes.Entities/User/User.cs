﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Efes.Entities.User
{
    [Serializable]
    public class User
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        [Display(Name = "İsim")]
        [Required]
        public string Name { get; set; }
        [Display(Name = "Avatar")]
        public string ImagePath { get; set; }
        [Display(Name = "Şifre")]
        public string Password { get; set; }
        [Display(Name = "Login İsmi")]
        [Required]
        public string LoginName { get; set; }
        [Display(Name = "Açıklama")]
        public string Description { get; set; }
        [Display(Name = "Oluşturan")]
        public string CreatedBy { get; set; }
        [Display(Name = "Oluşturan Id")]
        public int CreatedById { get; set; }
        [Display(Name = "Son Güncelleyen")]
        public string UpdatedBy { get; set; }
        [Display(Name = "Son Güncelleyen Id")]
        public string UpdatedById { get; set; }
        [Display(Name = "Email")]
        [Required]
        public string Email { get; set; }
        [Display(Name = "Oluşturulma Tarihi")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Son Güncellenme Tarihi")]
        public DateTime UpdatedDate { get; set; }
        [Display(Name = "Aktif")]
        public bool IsActive { get; set; }
        [Display(Name = "Admin")]
        public bool IsAdmin { get; set; }
        [Display(Name = "Roller")]
        public List<Role> Roles { get; set; }
        [Display(Name = "Kurallar")]
        public List<Rule> Rules { get; set; }
        [Display(Name = "Güvenli Şifre mi?")]
        public bool IsSecurePassword { get; set; }
        [Display(Name = "Hesap Kilitlendi mi?")]
        public bool IsLockedOut { get; set; }
        [Display(Name = "Telefon Numarası")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Takım Lideri")]
        [Required]
        public int TeamLeaderId { get; set; }
        public ExceptionInfo ExceptionInfo { get; set; }
    }

    public enum UserLogType
    {
        Login = 1,
        Logout = 2,
        SessionTimeoutLogout = 3
    }


}
