using System;

namespace Efes.Entities.User
{
    [Serializable]
    public class Role
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsActive { get; set; }
        public decimal ClosureRate { get; set; }
    }

    public enum Roles
    {
        Admin = 1,
        TahsilatYetkilisi = 2,
        Hukuk = 3,
        Operasyon = 4,
        Kobi = 5,
        TahsilatTakimLideri = 6,
        Muhasebe = 7,
        Yonetim = 8,
        SistemAdmin = 9,
        GenelMudurYardimci = 10,
        Bt = 11,
        HukukDisBuro = 12,
        HukukDisBuroTakimLideri = 13,
    }
}