﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Report
{
    public class CikarilanGKF
    {
        [DisplayName("Adet")]
        public int Adet { get; set; }
        [DisplayName("AnaparaToplami")]
        public decimal AnaparaToplami { get; set; }
        [DisplayName("KapamaBakiyesi")]
        public decimal KapamaBakiyesi { get; set; }
        [DisplayName("Yuzdesi")]
        public string Yuzdesi { get; set; }
    }
}
