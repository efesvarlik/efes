﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Report
{
    public class BeklentiTahsilat
    {
        [DisplayName("Tarih")]
        public DateTime Date { get; set; }
        [DisplayName("Beklenti")]
        public decimal Beklenti  { get; set; }
        [DisplayName("Tahsilat")]
        public decimal Tahsilat { get; set; }
        [DisplayName("Çağrı Sayısı")]
        public int CagriSayisi { get; set; }
        [DisplayName("Ulaşılan")]
        public int Ulasilan { get; set; }
        [DisplayName("Protokol")]
        public int Protokol { get; set; }
        [DisplayName("Hatta Kalma")]
        public int HattaKalma { get; set; }
    }
}
