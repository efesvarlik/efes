﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Report
{
    public class Tahsilat
    {
        [DisplayName("AnaparaGrubu")]
        public string AnaparaGrubu { get; set; }
        [DisplayName("TahsilatAdedi")]
        public int TahsilatAdedi { get; set; }
        [DisplayName("Tahsilat")]
        public decimal TahsilatTutar { get; set; }
        [DisplayName("Anapara")]
        public decimal Anapara { get; set; }
    }
}
