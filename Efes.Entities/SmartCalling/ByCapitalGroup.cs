﻿namespace Efes.Entities.SmartCalling
{
    public class ByCapitalGroup
    {
        public string AccountId { get; set; }
        public string Name { get; set; }
        public string Portfolio { get; set; }
        public decimal DebtPrinciple { get; set; }
        public decimal TotalDebtAmount { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsCalledToday { get; set; }
    }
}