﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.SmartCalling
{
    public class DialerCall
    {
        public string Agent { get; set; }
        public string AccountId { get; set; }
        public string PhoneNumber { get; set; }
        public int CallDuration { get; set; }
        public string CallStatus { get; set; }
    }
}
