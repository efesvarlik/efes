﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Efes.Entities.SmartCalling
{
    public class CollectionWithoutProtocol
    {
        public string AccountId { get; set; }
        public string AdSoyad { get; set; }
        public string Banka { get; set; }
        public decimal ToplamRisk { get; set; }
        public decimal AnaPara { get; set; }
        public decimal OdenenTutar { get; set; }
        public string Son3KritikNot { get; set; }
        public List<string> Son3KritikNotSplitter
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(this.Son3KritikNot))
                {
                    return this.Son3KritikNot.Split(new string[] {"Tarih:"}, StringSplitOptions.RemoveEmptyEntries).ToList();
                }
                else
                {
                    return new List<string>();
                }
            }
        }
        public DateTime SonOdemeTarihi { get; set; }
        public string SonOdemeTarihiStr {
            get { return SonOdemeTarihi.ToShortDateString(); }
        }
        public bool IsCalledToday { get; set; }
        public string Phone { get; set; }
    }
}