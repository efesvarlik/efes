﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.SmartCalling
{
    public class TodayPayments
    {
        public string AccountId { get; set; }
        public string AdSoyad { get; set; }
        public int TaksitNumarasi { get; set; }
        public DateTime OdemeTarihi { get; set; }
        public string OdemeTarihiStr
        {
            get { return OdemeTarihi.ToShortDateString(); }
        }
        public decimal ToplamProtokolTutari { get; set; }
        public decimal OdenecekTutar { get; set; }
        public decimal OdenenTutar { get; set; }
        public decimal KalanTutar { get; set; }
        public string OdemeDurumu { get; set; }
        public DateTime ProtokolTarihi { get; set; }
        public string ProtokolTarihiStr
        {
            get { return ProtokolTarihi.ToShortDateString(); }
        }
        public string ProtokolOlusturan { get; set; }
        public int GecikmadekiTaksitAdedi { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsCalledToday { get; set; }
        public bool PaymnetStatus { get; set; }

    }
}
