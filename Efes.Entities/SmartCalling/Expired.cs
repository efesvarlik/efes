﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.SmartCalling
{
    public class Expired
    {
        public string AccountId { get; set; }
        public string Name { get; set; }
        public string Portfolio { get; set; }
        public decimal DebtPrinciple { get; set; }
        public decimal ExpiredAmount { get; set; }
        public int InstallmentCount { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsCalledToday { get; set; }

    }
}
