﻿using System;

namespace Efes.Entities.SmartCalling
{
    public class NewAppointed
    {
        public string AccountId { get; set; }
        public string AdSoyad { get; set; }
        public DateTime AtamaTarihi { get; set; }
        public string DosyaSahibi { get; set; }
        public string Portfoy { get; set; }
        public string Phone { get; set; }
        public decimal ToplamRisk { get; set; }
        public decimal Anapara { get; set; }
        public bool IsCalledToday { get; set; }
    }
}