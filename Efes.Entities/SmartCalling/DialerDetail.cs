﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.SmartCalling
{
    public class DialerDetail
    {
        public int Total { get; set; }
        public int Finished { get; set; }
        public int Remain { get; set; }
        public DateTime StartDate { get; set; }
        public string DialerRecId { get; set; }
    }
}
