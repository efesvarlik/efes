﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.SmartCalling
{
    public enum DialerStatus
    {
        Passive = 0,
        Active = 1,
        Paused = 2
    }
}
