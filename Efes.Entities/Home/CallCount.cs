﻿using System;

namespace Efes.Entities.Home
{
    [Serializable]
    public class CallCount
    {
        public string Key { get; set; }
        public int Value { get; set; }
    }
}