﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efes.Entities.Home
{
    [Serializable]
    public class DashboardSummary
    {
        public int Id { get; set; }
        public string TextKey { get; set; }
        public string TextValue { get; set; }

        public enum TypeEnum //todo: hazirlandi fakat kullanilmadi. Kullanici haklari ve kullanici tercihleri oluşturulana kadar beklenecek
        {
            [Description("Günlük Tahsilat")]
            MoneyCollection = 1,
            [Description("Arama Başarı Oranı")]
            CallSuccessRate = 2,
            [Description("Aktif Protokol Sayısı")]
            ActiveProtocolCount = 3,
            [Description("Hatta Kalma Süresi")]
            TimeSpentOnHold = 4
        }
    }
}
