﻿using System;
using System.ComponentModel;

namespace Efes.Entities.Home
{
    [Serializable]
    public class TargetAndActualCollection
    {
        public enum TargetAndActualCollectionTypeEnum
        {
            [Description("Günlük")]
            Day = 1,
            [Description("Aylık")]
            Month = 2,
            [Description("Yıllık")]
            Year = 3
        }

        public TargetAndActualCollectionTypeEnum TargetAndActualCollectionType { get; set; }
        public int Target { get; set; }
        public int Actual { get; set; }
    }
}