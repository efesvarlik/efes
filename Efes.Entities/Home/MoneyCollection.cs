﻿using System;
using System.Collections.Generic;

namespace Efes.Entities.Home
{
    [Serializable]
    public class MoneyCollection
    {
        public Dictionary<string, int> ValueList { get; set; }
    }
}