﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.KVClient
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using DesktopControlAPI.Private;
using System;
using System.Threading;
using System.Windows.Threading;

namespace DesktopControlAPI
{
  /// <summary>
  /// Parser and formatter of messages for communicating with the agent server
  /// 
  /// </summary>
  /// 
  /// <summary>
  /// Parser and formatter of messages for communicating with the agent server
  /// 
  /// </summary>
  internal class KVClient
  {
    /// <summary>
    /// Limit an individual message to 20 kilobytes. When a message length greater than the limit is received from the socket,
    ///             the badDataLength event is emitted. User then may act upon the event in any way, including ignoring, in which case
    ///             a long message will keep being receved.
    /// 
    /// </summary>
    private const uint MAX_MESSAGE_LENGTH = 20480U;
    private readonly RawTransport m_transport;
    private readonly DataWriter m_receiver;
    private uint m_messageLength;
    private readonly ReaderWriterLockSlim m_lockEvents;

    public RawTransport rawTransport
    {
      get
      {
        return this.m_transport;
      }
    }

    public event KVClient.MessageReceived messageReceived
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_messageReceived += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_messageReceived -= value;
      }
    }

    public event KVClient.BadData badData
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_badData += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_badData -= value;
      }
    }

    public event KVClient.BadDataLength badDataLength
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_badDataLength += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_badDataLength -= value;
      }
    }

    private event KVClient.MessageReceived m_messageReceived;

    private event KVClient.BadData m_badData;

    private event KVClient.BadDataLength m_badDataLength;

    public KVClient()
    {
      this.m_lockEvents = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
      this.m_receiver = new DataWriter(512U, 2U);
      this.m_transport = new RawTransport();
      this.m_transport.dataReceived += new RawTransport.onData(this.onDataReceived);
      this.m_transport.disconnected += new RawTransport.onFailure(this.onDisconnected);
      this.m_messageLength = 0U;
    }

    /// <summary>
    /// Release all resources and references to other objects.
    ///             The object cannot be used after the method had been called.
    /// 
    /// </summary>
    public void close()
    {
      if (this.m_transport == null)
        return;
      this.m_transport.dataReceived -= new RawTransport.onData(this.onDataReceived);
      this.m_transport.disconnected -= new RawTransport.onFailure(this.onDisconnected);
      this.m_receiver.clear();
      this.m_messageLength = 0U;
      this.m_transport.close();
    }

    /// <summary>
    /// Send an encoded KV-list to the message server. Caller is responsible for monitoring
    ///             state of the connection and knowing when sending data is possible.
    /// 
    /// </summary>
    /// <param name="_message">Key-value list for sending.</param>
    public void send(KVList _message)
    {
      Logger.Log(0, "Sent: " + _message.toJson());
      this.m_transport.write(_message.pack());
    }

    /// <summary>
    /// Send an encoded KV-list to the message server and wait until all data has been sent.
    ///             Caller is responsible for monitoring state of the connection and knowing when sending data is possible.
    /// 
    /// </summary>
    /// <param name="_message">Key-value list for sending.</param>
    public void sendNow(KVList _message)
    {
      this.m_transport.writeNow(_message.pack());
    }

    private void onDisconnected(object _sender, RawTransport.FailureArgs _args)
    {
      this.m_receiver.remove(this.m_receiver.length);
    }

    private void onDataReceived(object _sender, RawTransport.DataArgs _args)
    {
      this.m_receiver.append(_args.data);
      while (this.messageIsWaiting())
        this.processMessage();
      if (this.m_messageLength <= 20480U)
        return;
      this.emitBadDataLength(new KVClient.BadDataLengthArgs(this.m_messageLength));
    }

    private bool messageIsWaiting()
    {
      bool flag = false;
      if ((int) this.m_messageLength == 0 && this.m_receiver.length >= 4U)
        this.m_messageLength = this.retrieveMessageLength();
      if ((int) this.m_messageLength != 0 && this.m_messageLength <= this.m_receiver.length)
        flag = true;
      return flag;
    }

    /// <summary>
    /// Parse the 4-byte message length and remove it from the buffer
    /// 
    /// </summary>
    /// 
    /// <returns/>
    private uint retrieveMessageLength()
    {
      uint num = (uint) this.m_receiver.data[0] | (uint) this.m_receiver.data[1] << 8 | (uint) this.m_receiver.data[2] << 16 | (uint) this.m_receiver.data[3] << 24;
      this.m_receiver.remove(4U);
      return num;
    }

    private void processMessage()
    {
      KVList _message1 = this.parseMessage();
      if (_message1 != null)
      {
        if (_message1.ContainsKey("__ping__"))
        {
          KVList _message2 = new KVList();
          _message2.addString("__pong__", _message1.getStringValue("__ping__"));
          this.send(_message2);
        }
        else
          this.emitMessageReceived(new KVClient.MessageArgs(_message1));
      }
      else
        this.emitBadData();
      this.m_receiver.remove(this.m_messageLength);
      this.m_messageLength = 0U;
    }

    private KVList parseMessage()
    {
      using (DataReader _data = new DataReader(this.m_receiver.data, this.m_messageLength))
      {
        KVList kvList = new KVList();
        kvList.parse(_data);
        return kvList;
      }
    }

    private void emitMessageReceived(KVClient.MessageArgs _args)
    {
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_messageReceived == null)
          return;
        this.m_messageReceived((object) this, _args);
      }
    }

    private void emitBadData()
    {
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_badData == null)
          return;
        this.m_badData((object) this, EventArgs.Empty);
      }
    }

    private void emitBadDataLength(KVClient.BadDataLengthArgs _args)
    {
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_badDataLength == null)
          return;
        this.m_badDataLength((object) this, _args);
      }
    }

    public class EventDispatcher
    {
      private readonly KVClient m_source;
      private readonly Dispatcher m_dispatcher;
      private readonly ReaderWriterLockSlim m_lockEvents;

      public KVClient source
      {
        get
        {
          return this.m_source;
        }
      }

      public event KVClient.MessageReceived messageReceived
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_messageReceived == null)
              this.m_source.messageReceived += new KVClient.MessageReceived(this.agsrvMessageReceived);
            this.m_messageReceived += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_messageReceived -= value;
            if (this.m_messageReceived != null)
              return;
            this.m_source.messageReceived -= new KVClient.MessageReceived(this.agsrvMessageReceived);
          }
        }
      }

      public event KVClient.BadData badData
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_badData == null)
              this.m_source.badData += new KVClient.BadData(this.agsrvBadData);
            this.m_badData += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_badData -= value;
            if (this.m_badData != null)
              return;
            this.m_source.badData -= new KVClient.BadData(this.agsrvBadData);
          }
        }
      }

      public event KVClient.BadDataLength badDataLength
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_badDataLength == null)
              this.m_source.badDataLength += new KVClient.BadDataLength(this.agsrvBadDataLength);
            this.m_badDataLength += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_badDataLength -= value;
            if (this.m_badDataLength != null)
              return;
            this.m_source.badDataLength -= new KVClient.BadDataLength(this.agsrvBadDataLength);
          }
        }
      }

      private event KVClient.MessageReceived m_messageReceived;

      private event KVClient.BadData m_badData;

      private event KVClient.BadDataLength m_badDataLength;

      public EventDispatcher(KVClient _source)
      {
        this.m_dispatcher = Dispatcher.CurrentDispatcher;
        this.m_lockEvents = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        this.m_source = _source;
      }

      private void agsrvMessageReceived(object _sender, KVClient.MessageArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitMessageReceived(_sender, _args);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new KVClient.MessageReceived(this.emitMessageReceived), new object[2]
          {
            _sender,
            (object) _args
          });
      }

      private void agsrvBadData(object _sender, EventArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitBadData(_sender, _args);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new KVClient.BadData(this.emitBadData), new object[2]
          {
            _sender,
            (object) _args
          });
      }

      private void agsrvBadDataLength(object _sender, KVClient.BadDataLengthArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitBadDataLength(_sender, _args);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new KVClient.BadDataLength(this.emitBadDataLength), new object[2]
          {
            _sender,
            (object) _args
          });
      }

      private void emitMessageReceived(object _sender, KVClient.MessageArgs _args)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_messageReceived == null)
            return;
          this.m_messageReceived(_sender, _args);
        }
      }

      private void emitBadData(object _sender, EventArgs _args)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_badData == null)
            return;
          this.m_badData(_sender, _args);
        }
      }

      private void emitBadDataLength(object _sender, KVClient.BadDataLengthArgs _args)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_badDataLength == null)
            return;
          this.m_badDataLength(_sender, _args);
        }
      }
    }

    public class MessageArgs : EventArgs
    {
      private readonly KVList m_message;

      public KVList message
      {
        get
        {
          return this.m_message;
        }
      }

      internal MessageArgs(KVList _message)
      {
        this.m_message = _message;
      }
    }

    public class BadDataLengthArgs : EventArgs
    {
      private readonly uint m_length;

      public uint length
      {
        get
        {
          return this.m_length;
        }
      }

      internal BadDataLengthArgs(uint _length)
      {
        this.m_length = _length;
      }
    }

    public delegate void MessageReceived(object _sender, KVClient.MessageArgs _args);

    public delegate void BadData(object _sender, EventArgs _args);

    public delegate void BadDataLength(object _sender, KVClient.BadDataLengthArgs _args);
  }
}
