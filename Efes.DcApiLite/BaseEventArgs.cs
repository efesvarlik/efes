﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.BaseEventArgs
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;

namespace DesktopControlAPI
{
  /// <summary>
  /// Base event arguments class.
  /// 
  /// </summary>
  public class BaseEventArgs : EventArgs
  {
    private readonly ulong m_reqId;

    /// <summary>
    /// Request ID.
    /// 
    /// </summary>
    public ulong reqId
    {
      get
      {
        return this.m_reqId;
      }
    }

    internal BaseEventArgs(KVList _message)
    {
      this.m_reqId = _message.getUInt64Value("request_id");
    }
  }
}
