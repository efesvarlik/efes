﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Call
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

namespace DesktopControlAPI
{
  /// <summary>
  /// Class describes phone call
  /// 
  /// </summary>
  public class Call
  {
    /// <summary>
    /// Call ID.
    /// 
    /// </summary>
    public string m_id;
    /// <summary>
    /// Call direction.
    /// 
    /// </summary>
    public CallDirection m_direction;
    /// <summary>
    /// ANI
    /// 
    /// </summary>
    public string m_ANI;
    /// <summary>
    /// DNIS
    /// 
    /// </summary>
    public string m_DNIS;
    /// <summary>
    /// Caller name
    /// 
    /// </summary>
    public string m_callerName;
    /// <summary>
    /// Service name
    /// 
    /// </summary>
    public string m_serviceName;
    /// <summary>
    /// Local agent ID
    /// 
    /// </summary>
    public string m_agentId;
    /// <summary>
    /// Local agent phone number
    /// 
    /// </summary>
    public string m_agentPhone;
    /// <summary>
    /// Optinal atatched data, use scenario's Attach Data block to initialize
    /// 
    /// </summary>
    public KVList m_attachedData;

    internal Call()
    {
      this.m_direction = CallDirection.Unknown;
    }

    /// <summary/>
    public override string ToString()
    {
      return string.Format("{0}: {1} -> {2}", (object) this.m_direction, (object) this.m_ANI, (object) this.m_DNIS);
    }
  }
}
