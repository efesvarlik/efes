﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.KVList
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using DesktopControlAPI.Private;
using Procurios.Public;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DesktopControlAPI
{
  /// <summary>
  /// Key-Value list.
  /// 
  /// </summary>
  public class KVList : Dictionary<string, object>
  {
    private static string QUOTE = "\"";
    private static string COLON = ":";
    private static string COMMA = ",";

    /// <summary>
    /// Creates new ordered KVList from array.
    /// 
    /// </summary>
    public static KVList fromArray(object[] _value)
    {
      KVList kvList = new KVList();
      for (int index = 0; index < _value.Length; ++index)
        kvList.addNext(_value[index]);
      return kvList;
    }

    /// <summary>
    /// Add new value using integer ID as key.
    /// 
    /// </summary>
    public bool addNext(object _value)
    {
      bool flag = null != _value;
      if (flag)
      {
        try
        {
          this.Add(this.Count.ToString(), _value);
        }
        catch
        {
          flag = false;
        }
      }
      return flag;
    }

    /// <summary>
    /// Add new value of String type. If key already exists the value will be replaced.
    /// 
    /// </summary>
    public bool addString(string _key, string _value)
    {
      bool flag = _key != null && null != _value;
      if (flag)
      {
        try
        {
          this.Add(_key, (object) _value);
        }
        catch
        {
          flag = false;
        }
      }
      return flag;
    }

    /// <summary>
    /// Add new value of KVList type. If key already exists the value will be replaced.
    /// 
    /// </summary>
    public bool addKVList(string _key, KVList _value)
    {
      bool flag = true;
      try
      {
        this.Add(_key, (object) _value);
      }
      catch
      {
        flag = false;
      }
      return flag;
    }

    /// <summary>
    /// Get value as String. If key does not exist or value cannot be represented as string the method returns null.
    /// 
    /// </summary>
    public string getStringValue(string _key)
    {
      string str = (string) null;
      try
      {
        if (this.ContainsKey(_key))
        {
          str = this[_key] as string;
          if (string.IsNullOrEmpty(str))
            str = (string) null;
        }
      }
      catch
      {
      }
      return str;
    }

    /// <summary>
    /// Get value as KVList. If key does not exist or value is not of KVList type the method returns null.
    /// 
    /// </summary>
    public KVList getKVListValue(string _key)
    {
      KVList kvList = (KVList) null;
      try
      {
        kvList = this[_key] as KVList;
      }
      catch
      {
      }
      return kvList;
    }

    /// <summary>
    /// Get value as boolean. If key does not exist or if value cannot be interpreted as boolean the method returns _default argument value.
    /// 
    /// </summary>
    public bool getBooleanValue(string _key, bool _default)
    {
      bool flag = _default;
      try
      {
        if (this.ContainsKey(_key))
        {
          string str = this[_key] as string;
          flag = !string.IsNullOrEmpty(str) ? "1" == str.ToLower() || "true" == str.ToLower() : _default;
        }
      }
      catch
      {
      }
      return flag;
    }

    /// <summary>
    /// Get value as UInt64. If key does not exist or if value cannot be interpreted as UInt64 the method returns 0.
    /// 
    /// </summary>
    public ulong getUInt64Value(string _key)
    {
      ulong num = 0UL;
      try
      {
        if (this.ContainsKey(_key))
          num = ulong.Parse(this[_key] as string);
      }
      catch
      {
      }
      return num;
    }

    /// <summary>
    /// Get value as UInt32. If key does not exist or if value cannot be interpreted as UInt32 the method returns 0.
    /// 
    /// </summary>
    public uint getUInt32Value(string _key)
    {
      uint num = 0U;
      try
      {
        if (this.ContainsKey(_key))
          num = uint.Parse(this[_key] as string);
      }
      catch
      {
      }
      return num;
    }

    /// <summary>
    /// Get value as UInt16. If key does not exist or if value cannot be interpreted as UInt16 the method returns 0.
    /// 
    /// </summary>
    public ushort getUInt16Value(string _key)
    {
      ushort num = (ushort) 0;
      try
      {
        if (this.ContainsKey(_key))
          num = ushort.Parse(this[_key] as string);
      }
      catch
      {
      }
      return num;
    }

    /// <summary>
    /// Get value as Int64. If key does not exist or if value cannot be interpreted as UInt64 the method returns 0.
    /// 
    /// </summary>
    public long getInt64Value(string _key)
    {
      long num = 0L;
      try
      {
        if (this.ContainsKey(_key))
          num = long.Parse(this[_key] as string);
      }
      catch
      {
      }
      return num;
    }

    /// <summary>
    /// Get value as Int32. If key does not exist or if value cannot be interpreted as UInt32 the method returns 0.
    /// 
    /// </summary>
    public int getInt32Value(string _key)
    {
      int num = 0;
      try
      {
        if (this.ContainsKey(_key))
          num = int.Parse(this[_key] as string);
      }
      catch
      {
      }
      return num;
    }

    /// <summary>
    /// Get value as Int16. If key does not exist or if value cannot be interpreted as UInt16 the method returns 0.
    /// 
    /// </summary>
    public short getInt16Value(string _key)
    {
      short num = (short) 0;
      try
      {
        if (this.ContainsKey(_key))
          num = short.Parse(this[_key] as string);
      }
      catch
      {
      }
      return num;
    }

    /// <summary>
    /// Get value as double. If key does not exist or value cannot be represented as string the method returns 0.
    /// 
    /// </summary>
    public double getDoubleValue(string _key)
    {
      double num = 0.0;
      try
      {
        if (this.ContainsKey(_key))
          num = double.Parse(this[_key] as string);
      }
      catch
      {
      }
      return num;
    }

    /// <summary/>
    public override string ToString()
    {
      bool flag = true;
      string str = base.ToString() + "{";
      foreach (KeyValuePair<string, object> keyValuePair in (Dictionary<string, object>) this)
      {
        if (flag)
          flag = false;
        else
          str += ", ";
        str += keyValuePair.Key;
        str += (string) (object) '=';
        str += keyValuePair.Value.ToString();
      }
      return str + "}";
    }

    /// <summary>
    /// Fills in the KVList object from JSON formatted string
    /// 
    /// </summary>
    public bool fromJson(string _json)
    {
      Hashtable hashtable = (Hashtable) JSON.JsonDecode(_json);
      if (hashtable != null)
      {
        this.Clear();
        this.AddHashtable(this, hashtable);
      }
      return true;
    }

    /// <summary>
    /// Serializes the object into JSON formatted string
    /// 
    /// </summary>
    public string toJson()
    {
      string str1 = "";
      foreach (KeyValuePair<string, object> keyValuePair in (Dictionary<string, object>) this)
      {
        string str2 = KVList.QUOTE + keyValuePair.Key + KVList.QUOTE + KVList.COLON;
        string str3 = keyValuePair.Value.GetType() != typeof (KVList) ? str2 + KVList.QUOTE + keyValuePair.Value.ToString() + KVList.QUOTE : str2 + ((KVList) keyValuePair.Value).toJson();
        if (str1.Length > 0)
          str1 += KVList.COMMA;
        str1 += str3;
      }
      return "{" + str1 + "}";
    }

    internal void AddHashtable(KVList _parent, Hashtable _obj)
    {
      foreach (DictionaryEntry dictionaryEntry in _obj)
      {
        if (dictionaryEntry.Value.GetType() == typeof (Hashtable))
        {
          KVList _parent1 = new KVList();
          this.AddHashtable(_parent1, (Hashtable) dictionaryEntry.Value);
          _parent.addKVList(dictionaryEntry.Key.ToString(), _parent1);
        }
        else
          _parent.addString(dictionaryEntry.Key.ToString(), dictionaryEntry.Value.ToString());
      }
    }

    internal void parse(DataReader _data)
    {
      bool flag = false;
      do
      {
        if (_data.atEnd())
        {
          flag = true;
        }
        else
        {
          switch (_data.readTag())
          {
            case DataReader.TAG.LIST:
              string _key = _data.readString();
              KVList kvList = new KVList();
              kvList.parse(_data);
              this.addKVList(_key, kvList);
              break;
            case DataReader.TAG.STRING:
              this.addString(_data.readString(), _data.readString());
              break;
            case DataReader.TAG.EOL:
              flag = true;
              break;
          }
        }
      }
      while (!flag);
    }

    internal byte[] pack()
    {
      DataWriter _packed = new DataWriter(256U, 2U);
      byte[] numArray = (byte[]) null;
      this.packKVList(_packed);
      if ((int) _packed.length != 0)
      {
          numArray = new byte[(int)(_packed.length + 4U)];
        numArray[0] = (byte) (_packed.length & (uint) byte.MaxValue);
        numArray[1] = (byte) (_packed.length >> 8 & (uint) byte.MaxValue);
        numArray[2] = (byte) (_packed.length >> 16 & (uint) byte.MaxValue);
        numArray[3] = (byte) (_packed.length >> 24 & (uint) byte.MaxValue);
        Array.Copy((Array) _packed.data, 0L, (Array) numArray, 4L, (long) _packed.length);
      }
      return numArray;
    }

    private void packKVList(DataWriter _packed)
    {
      foreach (KeyValuePair<string, object> keyValuePair in (Dictionary<string, object>) this)
      {
        string _key;
        if ((_key = keyValuePair.Value as string) != null)
        {
          this.packTag('s', _packed);
          this.packString(keyValuePair.Key, _packed);
          this.packString(_key, _packed);
        }
        else
        {
          KVList kvList;
          if ((kvList = keyValuePair.Value as KVList) != null)
          {
            this.packTag('l', _packed);
            this.packString(keyValuePair.Key, _packed);
            kvList.packKVList(_packed);
          }
        }
      }
      this.packTag('x', _packed);
    }

    private void packString(string _key, DataWriter _packed)
    {
      byte[] bytes = Encoding.UTF8.GetBytes(_key);
      byte[] _data = new byte[4];
      uint num = (uint) bytes.Length;
      _data[0] = (byte) (num & (uint) byte.MaxValue);
      _data[1] = (byte) (num >> 8 & (uint) byte.MaxValue);
      _data[2] = (byte) (num >> 16 & (uint) byte.MaxValue);
      _data[3] = (byte) (num >> 24 & (uint) byte.MaxValue);
      _packed.append(_data);
      _packed.append(bytes);
    }

    private void packTag(char _tag, DataWriter _packed)
    {
      byte[] _data = new byte[1]
      {
        (byte) _tag
      };
      _packed.append(_data);
    }

    private object get(string _key)
    {
      object obj = (object) null;
      try
      {
        obj = this[_key];
      }
      catch
      {
      }
      return obj;
    }
  }
}
