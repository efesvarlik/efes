﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: AssemblyCopyright("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyTitle("dcapilite")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("d6468f48-7979-4b38-96c0-fa53457b2ea1")]
[assembly: AssemblyFileVersion("3.6.2.20830")]
[assembly: AssemblyVersion("3.6.2.20830")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
