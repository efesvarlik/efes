﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.IORDNS
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.Net;

namespace DesktopControlAPI.Private
{
  internal class IORDNS : IOResults.RQ
  {
    public IORDNS()
      : base(IOResults.RQTYPE.DNS)
    {
    }

    public override void terminate(IAsyncResult _result)
    {
      try
      {
        Dns.EndGetHostAddresses(_result);
      }
      catch (Exception ex)
      {
      }
    }
  }
}
