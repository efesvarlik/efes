﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.IORWrite
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.IO;

namespace DesktopControlAPI.Private
{
  internal class IORWrite : IOResults.RQ
  {
    private readonly Stream m_stream;

    public IORWrite(Stream _stream)
      : base(IOResults.RQTYPE.WRITE)
    {
      this.m_stream = _stream;
    }

    public override void terminate(IAsyncResult _result)
    {
      try
      {
        this.m_stream.EndWrite(_result);
      }
      catch
      {
      }
    }
  }
}
