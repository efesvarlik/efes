﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.DataWriter
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;

namespace DesktopControlAPI.Private
{
  internal class DataWriter
  {
    private byte[] m_data;
    private uint m_length;
    private uint m_growth;

    public byte[] data
    {
      get
      {
        return this.m_data;
      }
    }

    public uint length
    {
      get
      {
        return this.m_length;
      }
    }

    /// <summary>
    /// Constructor of the data receiver object.
    /// 
    /// </summary>
    /// <param name="_growth">Size of the allocation chunk of the buffer. Must be greater than zero.</param><param name="_initialChunks">Number of chunks allocated upon creation of the object.</param>
    public DataWriter(uint _growth, uint _initialChunks)
    {
      uint num = _growth * _initialChunks;
      this.m_data = (int) num == 0 ? (byte[]) null : new byte[(int) num];
      this.m_length = 0U;
      this.m_growth = _growth;
    }

    /// <summary>
    /// Add new data to the end of the buffer. If necessary, the buffer is re-allocated.
    ///             An exception may be thrown if re-allocation has failed.
    /// 
    /// </summary>
    /// <param name="_data">data to add to the buffer</param>
    public void append(byte[] _data)
    {
      uint num1 = this.m_length + (uint) _data.Length;
      if ((long) num1 > (long) this.m_data.Length)
      {
        uint num2 = (uint) ((int) num1 + (int) this.m_growth - 1) / this.m_growth;
        if (this.m_data == null)
          this.m_data = new byte[(int) (num2 * this.m_growth)];
        else
          Array.Resize<byte>(ref this.m_data, (int) num2 * (int) this.m_growth);
      }
      Array.Copy((Array) _data, 0L, (Array) this.m_data, (long) this.m_length, (long) _data.Length);
      this.m_length += (uint) _data.Length;
    }

    /// <summary>
    /// Remove data from the front end of the buffer.
    /// 
    /// </summary>
    /// <param name="_length">number of bytes to remove</param>
    public void remove(uint _length)
    {
      Array.Copy((Array) this.m_data, (long) _length, (Array) this.m_data, 0L, (long) (this.m_length - _length));
      this.m_length -= _length;
    }

    /// <summary>
    /// Remove all data from the buffer and release the allocated memory.
    /// 
    /// </summary>
    public void clear()
    {
      this.m_length = 0U;
      this.m_data = (byte[]) null;
    }
  }
}
