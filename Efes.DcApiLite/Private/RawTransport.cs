﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.RawTransport
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using DesktopControlAPI;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using System.Windows.Threading;

namespace DesktopControlAPI.Private
{
  internal class RawTransport
  {
    private const string shName = "Local\\~~~bphone_api~~~";
    private const int RECONNECT_TIMEOUT = 3000;
    private const int READ_BUFFER_SIZE = 2048;
    private readonly ReaderWriterLockSlim m_lockResults;
    private readonly ReaderWriterLockSlim m_lockEvents;
    private NetworkLocation m_location;
    private readonly IOResults m_asyncResults;
    private TcpClient m_client;
    private System.Timers.Timer m_reconnectTimer;
    private byte[] m_buffer;
    private Stream m_stream;
    private SslStream m_sslStream;
    private bool m_Running;

    public event RawTransport.onConnecting connecting
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_connecting += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_connecting -= value;
      }
    }

    public event RawTransport.onFailure connectFailed
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_connectFailed += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_connectFailed -= value;
      }
    }

    public event RawTransport.onEvent reconnecting
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_reconnecting += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_reconnecting -= value;
      }
    }

    public event RawTransport.onEvent connected
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_connected += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_connected -= value;
      }
    }

    public event RawTransport.onEvent authenticating
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_authenticating += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_authenticating -= value;
      }
    }

    public event RawTransport.onEvent ready
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_ready += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_ready -= value;
      }
    }

    public event RawTransport.onData dataReceived
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_dataReceived += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_dataReceived -= value;
      }
    }

    public event RawTransport.onFailure writeFailed
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_writeFailed += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_writeFailed -= value;
      }
    }

    public event RawTransport.onFailure disconnected
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_disconnected += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_disconnected -= value;
      }
    }

    public event RawTransport.onFailure closed
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_closed += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_closed -= value;
      }
    }

    private event RawTransport.onConnecting m_connecting;

    private event RawTransport.onFailure m_connectFailed;

    private event RawTransport.onFailure m_disconnected;

    private event RawTransport.onFailure m_closed;

    private event RawTransport.onFailure m_writeFailed;

    private event RawTransport.onEvent m_reconnecting;

    private event RawTransport.onEvent m_connected;

    private event RawTransport.onEvent m_authenticating;

    private event RawTransport.onEvent m_ready;

    private event RawTransport.onData m_dataReceived;

    public RawTransport()
    {
      this.m_lockResults = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
      this.m_lockEvents = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
      this.m_asyncResults = new IOResults(this.m_lockResults);
      this.m_buffer = new byte[2048];
      this.m_location = (NetworkLocation) null;
      this.m_client = (TcpClient) null;
      this.m_reconnectTimer = (System.Timers.Timer) null;
      this.m_stream = (Stream) null;
      this.m_sslStream = (SslStream) null;
      this.m_Running = false;
    }

    private void emitConnecting(IPAddress[] _addresses)
    {
      Logger.Log(0, "Emitting Connecting event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_connecting == null)
          return;
        this.m_connecting((object) this, new RawTransport.ConnectingArgs(_addresses));
      }
    }

    private void emitConnectFailed(SystemException _ex)
    {
      Logger.Log(0, "Emitting ConnectFailed event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_connectFailed == null)
          return;
        this.m_connectFailed((object) this, new RawTransport.FailureArgs(_ex));
      }
    }

    private void emitReconnecting()
    {
      Logger.Log(0, "Emitting Reconnecting event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_reconnecting == null)
          return;
        this.m_reconnecting((object) this, EventArgs.Empty);
      }
    }

    private void emitConnected()
    {
      Logger.Log(0, "Emitting Connected event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_connected == null)
          return;
        this.m_connected((object) this, EventArgs.Empty);
      }
    }

    private void emitAuthenticating()
    {
      Logger.Log(0, "Emitting Authenticating event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_authenticating == null)
          return;
        this.m_authenticating((object) this, EventArgs.Empty);
      }
    }

    private void emitReady()
    {
      Logger.Log(0, "Emitting Ready event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_ready == null)
          return;
        this.m_ready((object) this, EventArgs.Empty);
      }
    }

    private void emitDataReceived(byte[] _data)
    {
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_dataReceived == null)
          return;
        this.m_dataReceived((object) this, new RawTransport.DataArgs(_data));
      }
    }

    private void emitWriteFailed(SystemException _ex)
    {
      Logger.Log(0, "Emitting WriteFailed event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_writeFailed == null)
          return;
        this.m_writeFailed((object) this, new RawTransport.FailureArgs(_ex));
      }
    }

    private void emitDisconnected(SystemException _ex)
    {
      Logger.Log(0, "Emitting Disconnected event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_disconnected == null)
          return;
        this.m_disconnected((object) this, new RawTransport.FailureArgs(_ex));
      }
    }

    private void emitClosed(SystemException _ex)
    {
      Logger.Log(0, "Emitting Closed event");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_client == null || this.m_closed == null)
          return;
        this.m_closed((object) this, new RawTransport.FailureArgs(_ex));
      }
    }

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern IntPtr CreateFileMapping(IntPtr hFile, int lpAttributes, RawTransport.FileProtection flProtect, uint dwMaximumSizeHigh, uint dwMaximumSizeLow, string lpName);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern IntPtr OpenFileMapping(RawTransport.FileRights dwDesiredAccess, bool bInheritHandle, string lpName);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject, RawTransport.FileRights dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow, uint dwNumberOfBytesToMap);

    [DllImport("Kernel32.dll")]
    private static extern bool UnmapViewOfFile(IntPtr map);

    [DllImport("kernel32.dll")]
    private static extern int CloseHandle(IntPtr hObject);

    private unsafe ushort getApiPort()
    {
      ushort num1 = (ushort) 0;
      IntPtr num2 = RawTransport.OpenFileMapping(RawTransport.FileRights.Read, false, "10.104.1.12\\~~~bphone_api~~~");
      if (num2 != IntPtr.Zero)
      {
        IntPtr map = RawTransport.MapViewOfFile(num2, RawTransport.FileRights.Read, 0U, 0U, 0U);
        if (map != IntPtr.Zero)
        {
          num1 = (ushort) *(int*) map.ToPointer();
          RawTransport.UnmapViewOfFile(map);
        }
        RawTransport.CloseHandle(num2);
      }
      Logger.Log(0, "Published API port: " + (object) num1);
      return num1;
    }

    private bool initAddressResolution()
    {
      bool flag = false;
      Logger.Log(0, "initAddressResolution: " + this.m_location.networkName);
      using (RWLock._write(this.m_lockResults))
      {
        try
        {
          this.m_asyncResults.add(Dns.BeginGetHostAddresses(this.m_location.networkName, new AsyncCallback(this.getHostAddressesCallback), (object) this), (IOResults.RQ) new IORDNS());
          flag = true;
        }
        catch (Exception ex)
        {
          Logger.Log(1, "Exception: unable to initialize DNS resolution: " + ex.Message);
        }
      }
      if (!flag)
        this.setReconnectTimer(3000.0);
      return flag;
    }

    private void getHostAddressesCallback(IAsyncResult _ar)
    {
      Logger.Log(0, "getHostAddresses callback");
      bool flag = false;
      if (IOResults.RQTYPE.DNS == this.m_asyncResults.remove(_ar, IOResults.RQTYPE.DNS))
      {
        IPAddress[] _addresses = (IPAddress[]) null;
        SocketException socketException1 = (SocketException) null;
        try
        {
          _addresses = Dns.EndGetHostAddresses(_ar);
        }
        catch (SocketException ex)
        {
          Logger.Log(1, "Exception: unable to obtain DNS resolution results: " + ex.Message);
          socketException1 = ex;
        }
        if (_addresses != null && _addresses.Length > 0)
        {
          SocketException socketException2;
          if ((socketException2 = this.initConnect(_addresses)) != null)
            this.emitClosed((SystemException) socketException2);
          else
            flag = true;
        }
        else
          this.emitClosed((SystemException) socketException1);
      }
      if (flag)
        return;
      this.setReconnectTimer(3000.0);
    }

    private SocketException initConnect(IPAddress[] _addresses)
    {
      SocketException socketException = (SocketException) null;
      Logger.Log(0, "initConnect");
      this.emitConnecting(_addresses);
      using (RWLock._write(this.m_lockResults))
      {
        try
        {
          this.m_client = new TcpClient();
          if ((int) this.m_location.portNumber == 0)
            this.m_location.portNumber = this.getApiPort();
          Logger.Log(0, string.Format("Connecting to {0}:{1}", (object) _addresses[0].ToString(), (object) this.m_location.portNumber));
          this.m_asyncResults.add(this.m_client.BeginConnect(_addresses, (int) this.m_location.portNumber, new AsyncCallback(this.connectCallback), (object) this), (IOResults.RQ) new IORConnect(this.m_client));
        }
        catch (SocketException ex)
        {
          Logger.Log(1, "Exception: unable to initialize TCP connection to Agent Desktop: " + ex.Message);
          socketException = ex;
          this.m_client.Close();
          this.m_client = (TcpClient) null;
        }
      }
      return socketException;
    }

    private void connectCallback(IAsyncResult _ar)
    {
      Logger.Log(0, "Connect callback");
      bool flag = false;
      if (IOResults.RQTYPE.CONNECT == this.m_asyncResults.remove(_ar, IOResults.RQTYPE.CONNECT))
      {
        try
        {
          this.m_client.EndConnect(_ar);
        }
        catch (SocketException ex)
        {
          Logger.Log(1, "Exception: unable complete connect: " + ex.Message);
          this.m_client.Close();
          this.m_client = (TcpClient) null;
          this.emitConnectFailed((SystemException) ex);
        }
        if (this.m_client != null)
        {
          SystemException _ex1 = (SystemException) null;
          this.emitConnected();
          if (this.m_location.secureSocket)
          {
            this.m_sslStream = new SslStream((Stream) this.m_client.GetStream(), false);
            using (RWLock._write(this.m_lockResults))
            {
              try
              {
                this.m_asyncResults.add(this.m_sslStream.BeginAuthenticateAsClient(this.m_location.networkName, new AsyncCallback(this.authenticateCallback), (object) this), (IOResults.RQ) new IORAuthenticate(this.m_sslStream));
              }
              catch (SystemException ex)
              {
                _ex1 = ex;
              }
            }
            if (_ex1 == null)
            {
              this.emitAuthenticating();
              flag = true;
            }
            else
            {
              this.emitDisconnected(_ex1);
              this.m_sslStream.Close();
              this.m_sslStream = (SslStream) null;
              this.m_client.Close();
              this.emitClosed(_ex1);
              this.m_client = (TcpClient) null;
            }
          }
          else
          {
            this.m_stream = (Stream) this.m_client.GetStream();
            SystemException _ex2;
            if ((_ex2 = this.initRead()) == null)
            {
              this.emitReady();
              flag = true;
            }
            else
            {
              if (this.m_stream != null)
              {
                this.m_stream.Close();
                this.m_stream = (Stream) null;
              }
              this.emitDisconnected(_ex2);
              this.m_client.Close();
              this.emitClosed(_ex2);
              this.m_client = (TcpClient) null;
            }
          }
        }
      }
      if (flag)
        return;
      this.setReconnectTimer(3000.0);
    }

    private SystemException initRead()
    {
      SystemException systemException = (SystemException) null;
      using (RWLock._write(this.m_lockResults))
      {
        try
        {
          this.m_asyncResults.add(this.m_stream.BeginRead(this.m_buffer, 0, this.m_buffer.Length, new AsyncCallback(this.readCallback), (object) this), (IOResults.RQ) new IORRead(this.m_stream));
        }
        catch (SystemException ex)
        {
          systemException = ex;
        }
      }
      return systemException;
    }

    private void setReconnectTimer(double _timeout)
    {
      if (!this.m_Running)
        return;
      Logger.Log(0, "Setting reconnect timer " + _timeout.ToString());
      using (RWLock._write(this.m_lockResults))
      {
        if (this.m_client != null)
          Logger.Log(1, "Setting reconnect timer: client still exists");
        else if (this.m_reconnectTimer == null)
        {
          this.m_reconnectTimer = new System.Timers.Timer(_timeout);
          this.m_reconnectTimer.AutoReset = false;
          this.m_reconnectTimer.Elapsed += new ElapsedEventHandler(this.reconnectCallback);
          this.m_reconnectTimer.Start();
        }
        else
          Logger.Log(0, "Setting reconnect timer: reconnect timer already set");
      }
    }

    private void cancelReconnectTimer()
    {
      Logger.Log(0, "Cancelling reconnect timer");
      using (RWLock._write(this.m_lockResults))
      {
        if (this.m_reconnectTimer == null)
          return;
        this.m_reconnectTimer.Stop();
        this.m_reconnectTimer.Elapsed -= new ElapsedEventHandler(this.reconnectCallback);
        this.m_reconnectTimer = (System.Timers.Timer) null;
      }
    }

    private void reconnectCallback(object _sender, ElapsedEventArgs _args)
    {
      Logger.Log(0, "Reconnect callback");
      this.m_reconnectTimer.Stop();
      this.m_reconnectTimer = (System.Timers.Timer) null;
      this.emitReconnecting();
      if (this.initAddressResolution())
        return;
      this.emitClosed((SystemException) null);
    }

    private void readCallback(IAsyncResult _ar)
    {
      if (IOResults.RQTYPE.READ != this.m_asyncResults.remove(_ar, IOResults.RQTYPE.READ))
        return;
      try
      {
        int length = this.m_stream.EndRead(_ar);
        if (length == 0)
        {
          if (this.m_stream != null)
          {
            this.m_stream.Close();
            this.m_stream = (Stream) null;
          }
          this.emitDisconnected((SystemException) null);
          this.m_client.Close();
          this.emitClosed((SystemException) null);
          this.m_client = (TcpClient) null;
          this.setReconnectTimer(3000.0);
        }
        else
        {
          byte[] _data = new byte[length];
          Array.Copy((Array) this.m_buffer, (Array) _data, length);
          this.emitDataReceived(_data);
          SystemException systemException;
          if ((systemException = this.initRead()) != null)
            throw systemException;
        }
      }
      catch (SystemException ex)
      {
        if (this.m_stream != null)
        {
          this.m_stream.Close();
          this.m_stream = (Stream) null;
        }
        this.emitDisconnected(ex);
        if (this.m_client != null)
        {
          this.m_client.Close();
          this.emitClosed(ex);
        }
        this.m_client = (TcpClient) null;
        this.setReconnectTimer(3000.0);
      }
    }

    private void writeCallback(IAsyncResult _ar)
    {
      if (IOResults.RQTYPE.WRITE != this.m_asyncResults.remove(_ar, IOResults.RQTYPE.WRITE))
        return;
      try
      {
        this.m_stream.EndWrite(_ar);
      }
      catch (SystemException ex)
      {
        this.emitWriteFailed(ex);
      }
    }

    private void authenticateCallback(IAsyncResult _ar)
    {
      Logger.Log(0, "Authenticate callback");
      if (IOResults.RQTYPE.AUTHENTICATE != this.m_asyncResults.remove(_ar, IOResults.RQTYPE.AUTHENTICATE))
        return;
      SystemException _ex;
      try
      {
        this.m_sslStream.EndAuthenticateAsClient(_ar);
        this.m_stream = (Stream) this.m_sslStream;
        this.m_sslStream = (SslStream) null;
        _ex = this.initRead();
      }
      catch (SystemException ex)
      {
        _ex = ex;
      }
      if (_ex == null)
      {
        this.emitReady();
      }
      else
      {
        if (this.m_sslStream != null)
        {
          this.m_sslStream.Close();
          this.m_sslStream = (SslStream) null;
        }
        if (this.m_stream != null)
        {
          this.m_stream.Close();
          this.m_stream = (Stream) null;
        }
        this.emitDisconnected(_ex);
        this.m_client.Close();
        this.emitClosed(_ex);
        this.m_client = (TcpClient) null;
      }
    }

    public bool open(NetworkLocation _location)
    {
      Logger.Log(0, "RawTransport::open()");
      bool flag = false;
      using (RWLock._write(this.m_lockResults))
      {
        if (this.m_location == null)
        {
          this.m_location = _location;
          flag = this.initAddressResolution();
        }
      }
      this.m_Running = flag;
      return flag;
    }

    public void close()
    {
      Logger.Log(0, "RawTransport::close()");
      this.m_Running = false;
      this.cancelReconnectTimer();
      this.emitDisconnected((SystemException) null);
      SslStream sslStream = this.m_sslStream;
      Stream stream = this.m_stream;
      TcpClient tcpClient = this.m_client;
      this.m_sslStream = (SslStream) null;
      this.m_stream = (Stream) null;
      this.m_client = (TcpClient) null;
      this.m_location = (NetworkLocation) null;
      try
      {
        if (sslStream != null)
          sslStream.Close();
        if (stream != null)
          stream.Close();
      }
      catch (Exception ex)
      {
      }
      this.m_asyncResults.terminate();
      if (tcpClient == null)
        return;
      try
      {
        tcpClient.Close();
      }
      catch (Exception ex)
      {
      }
    }

    public bool write(byte[] _data)
    {
      bool flag = false;
      using (RWLock._write(this.m_lockResults))
      {
        if (this.m_client != null)
        {
          if (this.m_stream != null)
          {
            try
            {
              this.m_asyncResults.add(this.m_stream.BeginWrite(_data, 0, _data.Length, new AsyncCallback(this.writeCallback), (object) this), (IOResults.RQ) new IORWrite(this.m_stream));
              flag = true;
            }
            catch (Exception ex)
            {
              Logger.Log(0, "RawTransport::write exception: " + ex.Message);
            }
          }
        }
      }
      return flag;
    }

    public bool writeNow(byte[] _data)
    {
      bool flag = false;
      Logger.Log(0, "RawTransport::writeNow()");
      using (RWLock._write(this.m_lockResults))
      {
        if (this.m_client != null)
        {
          if (this.m_stream != null)
          {
            try
            {
              this.m_stream.Write(_data, 0, _data.Length);
              this.m_stream.Flush();
              flag = true;
            }
            catch (Exception ex)
            {
              Logger.Log(0, "RawTransport::writeNow exception: " + ex.Message);
            }
          }
        }
      }
      return flag;
    }

    public class ConnectingArgs : EventArgs
    {
      private readonly IPAddress[] m_addresses;

      public IPAddress[] addresses
      {
        get
        {
          return this.m_addresses;
        }
      }

      internal ConnectingArgs(IPAddress[] _addresses)
      {
        this.m_addresses = _addresses;
      }
    }

    public class FailureArgs : EventArgs
    {
      private readonly SystemException m_exception;

      public SystemException exception
      {
        get
        {
          return this.m_exception;
        }
      }

      internal FailureArgs(SystemException _exception)
      {
        this.m_exception = _exception;
      }
    }

    public class DataArgs : EventArgs
    {
      private readonly byte[] m_data;

      public byte[] data
      {
        get
        {
          return this.m_data;
        }
      }

      internal DataArgs(byte[] _data)
      {
        this.m_data = _data;
      }
    }

    public delegate void onConnecting(object _sender, RawTransport.ConnectingArgs _args);

    public delegate void onFailure(object _sender, RawTransport.FailureArgs _args);

    public delegate void onEvent(object _sender, EventArgs _args);

    public delegate void onData(object _sender, RawTransport.DataArgs _args);

    private enum FileProtection : uint
    {
      ReadOnly = 2U,
      ReadWrite = 4U,
    }

    private enum FileRights : uint
    {
      Write = 2U,
      Read = 4U,
      ReadWrite = 6U,
    }

    /// <summary>
    /// Proxy object that dispatches events emitted by the RawTransport class to the thread that has created the object.
    ///             RawTransport object emits all events on I/O threads maintained by the platform. Clients interested in receiving
    ///             events on the UI thread must create the EventDispatcher for the RawTransport object and add event handlers to the
    ///             dispatcher object.
    /// 
    /// </summary>
    public class EventDispatcher
    {
      private readonly RawTransport m_transport;
      private readonly Dispatcher m_dispatcher;
      private readonly ReaderWriterLockSlim m_lockEvents;
      private EventArgs m_emptyEventArgs;

      public RawTransport source
      {
        get
        {
          return this.m_transport;
        }
      }

      public event RawTransport.onConnecting connecting
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_connecting == null)
              this.m_transport.connecting += new RawTransport.onConnecting(this.transportConnecting);
            this.m_connecting += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_connecting -= value;
            if (this.m_connecting != null)
              return;
            this.m_transport.connecting -= new RawTransport.onConnecting(this.transportConnecting);
          }
        }
      }

      public event RawTransport.onFailure connectFailed
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_connectFailed == null)
              this.m_transport.connectFailed += new RawTransport.onFailure(this.transportConnectFailed);
            this.m_connectFailed += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_connectFailed -= value;
            if (this.m_connectFailed != null)
              return;
            this.m_transport.connectFailed -= new RawTransport.onFailure(this.transportConnectFailed);
          }
        }
      }

      public event RawTransport.onEvent reconnecting
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_reconnecting == null)
              this.m_transport.reconnecting += new RawTransport.onEvent(this.transportReconnecting);
            this.m_reconnecting += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_reconnecting -= value;
            if (this.m_reconnecting != null)
              return;
            this.m_transport.reconnecting -= new RawTransport.onEvent(this.transportReconnecting);
          }
        }
      }

      public event RawTransport.onEvent connected
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_connected == null)
              this.m_transport.connected += new RawTransport.onEvent(this.transportConnected);
            this.m_connected += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_connected -= value;
            if (this.m_connected != null)
              return;
            this.m_transport.connected -= new RawTransport.onEvent(this.transportConnected);
          }
        }
      }

      public event RawTransport.onEvent authenticating
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_authenticating == null)
              this.m_transport.authenticating += new RawTransport.onEvent(this.transportAuthenticating);
            this.m_authenticating += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_authenticating -= value;
            if (this.m_authenticating != null)
              return;
            this.m_transport.authenticating -= new RawTransport.onEvent(this.transportAuthenticating);
          }
        }
      }

      public event RawTransport.onEvent ready
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_ready == null)
              this.m_transport.ready += new RawTransport.onEvent(this.transportReady);
            this.m_ready += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_ready -= value;
            if (this.m_ready != null)
              return;
            this.m_transport.ready -= new RawTransport.onEvent(this.transportReady);
          }
        }
      }

      public event RawTransport.onData dataReceived
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_dataReceived == null)
              this.m_transport.dataReceived += new RawTransport.onData(this.transportDataReceived);
            this.m_dataReceived += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_dataReceived -= value;
            if (this.m_dataReceived != null)
              return;
            this.m_transport.dataReceived -= new RawTransport.onData(this.transportDataReceived);
          }
        }
      }

      public event RawTransport.onFailure writeFailed
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_writeFailed == null)
              this.m_transport.writeFailed += new RawTransport.onFailure(this.transportWriteFailed);
            this.m_writeFailed += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_writeFailed -= value;
            if (this.m_writeFailed != null)
              return;
            this.m_transport.writeFailed -= new RawTransport.onFailure(this.transportWriteFailed);
          }
        }
      }

      public event RawTransport.onFailure disconnected
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_disconnected == null)
              this.m_transport.disconnected += new RawTransport.onFailure(this.transportDisconnected);
            this.m_disconnected += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_disconnected -= value;
            if (this.m_disconnected != null)
              return;
            this.m_transport.disconnected -= new RawTransport.onFailure(this.transportDisconnected);
          }
        }
      }

      public event RawTransport.onFailure closed
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_closed == null)
              this.m_transport.closed += new RawTransport.onFailure(this.transportClosed);
            this.m_closed += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_closed -= value;
            if (this.m_closed != null)
              return;
            this.m_transport.closed -= new RawTransport.onFailure(this.transportClosed);
          }
        }
      }

      private event RawTransport.onConnecting m_connecting;

      private event RawTransport.onFailure m_connectFailed;

      private event RawTransport.onFailure m_disconnected;

      private event RawTransport.onFailure m_closed;

      private event RawTransport.onFailure m_writeFailed;

      private event RawTransport.onEvent m_reconnecting;

      private event RawTransport.onEvent m_connected;

      private event RawTransport.onEvent m_authenticating;

      private event RawTransport.onEvent m_ready;

      private event RawTransport.onData m_dataReceived;

      public EventDispatcher(RawTransport _transport)
      {
        this.m_dispatcher = Dispatcher.CurrentDispatcher;
        this.m_lockEvents = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        this.m_emptyEventArgs = (EventArgs) null;
        this.m_transport = _transport;
      }

      private EventArgs getEmptyEventArgs()
      {
        if (this.m_emptyEventArgs == null)
          this.m_emptyEventArgs = new EventArgs();
        return this.m_emptyEventArgs;
      }

      private void emitConnecting(object _sender, IPAddress[] _addresses)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_connecting == null)
            return;
          this.m_connecting(_sender, new RawTransport.ConnectingArgs(_addresses));
        }
      }

      private void emitConnectFailed(object _sender, SystemException _ex)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_connectFailed == null)
            return;
          this.m_connectFailed(_sender, new RawTransport.FailureArgs(_ex));
        }
      }

      private void emitReconnecting(object _sender)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_reconnecting == null)
            return;
          this.m_reconnecting(_sender, this.getEmptyEventArgs());
        }
      }

      private void emitConnected(object _sender)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_connected == null)
            return;
          this.m_connected(_sender, this.getEmptyEventArgs());
        }
      }

      private void emitAuthenticating(object _sender)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_authenticating == null)
            return;
          this.m_authenticating(_sender, this.getEmptyEventArgs());
        }
      }

      private void emitReady(object _sender)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_ready == null)
            return;
          this.m_ready(_sender, this.getEmptyEventArgs());
        }
      }

      private void emitDataReceived(object _sender, byte[] _data)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_dataReceived == null)
            return;
          this.m_dataReceived(_sender, new RawTransport.DataArgs(_data));
        }
      }

      private void emitWriteFailed(object _sender, SystemException _ex)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_writeFailed == null)
            return;
          this.m_writeFailed(_sender, new RawTransport.FailureArgs(_ex));
        }
      }

      private void emitDisconnected(object _sender, SystemException _ex)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_disconnected == null)
            return;
          this.m_disconnected(_sender, new RawTransport.FailureArgs(_ex));
        }
      }

      private void emitClosed(object _sender, SystemException _ex)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_closed == null)
            return;
          this.m_closed(_sender, new RawTransport.FailureArgs(_ex));
        }
      }

      private void transportConnecting(object _sender, RawTransport.ConnectingArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitConnecting(_sender, _args.addresses);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitConnecting(this.emitConnecting), new object[2]
          {
            _sender,
            (object) _args.addresses
          });
      }

      private void transportConnectFailed(object _sender, RawTransport.FailureArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitConnectFailed(_sender, _args.exception);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitFailure(this.emitConnectFailed), new object[2]
          {
            _sender,
            (object) _args.exception
          });
      }

      private void transportReconnecting(object _sender, EventArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitReconnecting(_sender);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitEvent(this.emitReconnecting), new object[1]
          {
            _sender
          });
      }

      private void transportAuthenticating(object _sender, EventArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitAuthenticating(_sender);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitEvent(this.emitAuthenticating), new object[1]
          {
            _sender
          });
      }

      private void transportConnected(object _sender, EventArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitConnected(_sender);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitEvent(this.emitConnected), new object[1]
          {
            _sender
          });
      }

      private void transportReady(object _sender, EventArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitReady(_sender);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitEvent(this.emitReady), new object[1]
          {
            _sender
          });
      }

      private void transportDisconnected(object _sender, RawTransport.FailureArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitDisconnected(_sender, _args.exception);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitFailure(this.emitDisconnected), new object[2]
          {
            _sender,
            (object) _args.exception
          });
      }

      private void transportClosed(object _sender, RawTransport.FailureArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitClosed(_sender, _args.exception);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitFailure(this.emitClosed), new object[2]
          {
            _sender,
            (object) _args.exception
          });
      }

      private void transportWriteFailed(object _sender, RawTransport.FailureArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitWriteFailed(_sender, _args.exception);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitFailure(this.emitWriteFailed), new object[2]
          {
            _sender,
            (object) _args.exception
          });
      }

      private void transportDataReceived(object _sender, RawTransport.DataArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitDataReceived(_sender, _args.data);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new RawTransport.EventDispatcher.EmitDataReceived(this.emitDataReceived), new object[2]
          {
            _sender,
            (object) _args.data
          });
      }

      private delegate void EmitConnecting(object _sender, IPAddress[] _addresses);

      private delegate void EmitFailure(object _sender, SystemException _exception);

      private delegate void EmitEvent(object _sender);

      private delegate void EmitDataReceived(object _sender, byte[] _data);
    }
  }
}
