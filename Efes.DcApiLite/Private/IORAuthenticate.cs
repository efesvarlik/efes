﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.IORAuthenticate
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.Net.Security;

namespace DesktopControlAPI.Private
{
  internal class IORAuthenticate : IOResults.RQ
  {
    private readonly SslStream m_stream;

    public IORAuthenticate(SslStream _stream)
      : base(IOResults.RQTYPE.AUTHENTICATE)
    {
      this.m_stream = _stream;
    }

    public override void terminate(IAsyncResult _result)
    {
      try
      {
        this.m_stream.EndAuthenticateAsClient(_result);
      }
      catch
      {
      }
    }
  }
}
