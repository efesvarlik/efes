﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.DataReader
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.Text;

namespace DesktopControlAPI.Private
{
  internal class DataReader : IDisposable
  {
    private byte[] m_data;
    private uint m_position;
    private uint m_remainingData;

    public DataReader(byte[] _data, uint _length)
    {
      this.m_data = _data;
      this.m_remainingData = _length;
      this.m_position = 0U;
    }

    ~DataReader()
    {
      this.dispose(false);
    }

    private void dispose(bool _disposing)
    {
      if (this.m_data == null)
        return;
      this.m_data = (byte[]) null;
      this.m_position = 0U;
      this.m_remainingData = 0U;
      GC.SuppressFinalize((object) this);
    }

    public void Dispose()
    {
      this.dispose(true);
    }

    public bool atEnd()
    {
      return 0 == (int) this.m_remainingData;
    }

    public DataReader.TAG readTag()
    {
      byte num = (byte) 0;
      if (this.m_remainingData < 1U)
        DataReader.BadData._throw("Not enough data to read a 1-byte tag", this.m_data, this.m_position);
      else if (!DataReader._isValidTag(num = this.m_data[(int) this.m_position]))
      {
        DataReader.BadData._throw("Invalid tag byte", this.m_data, this.m_position);
      }
      else
      {
        ++this.m_position;
        --this.m_remainingData;
      }
      return (DataReader.TAG) num;
    }

    public uint readLength()
    {
      uint num = 0U;
      if (this.m_remainingData < 4U)
      {
        DataReader.BadData._throw("Not enough data to read a 4-byte length", this.m_data, this.m_position);
      }
      else
      {
          num = (uint)byte.MaxValue & (uint)this.m_data[(int)this.m_position] | (uint)(((int)byte.MaxValue & (int)this.m_data[(int)(this.m_position + 1U)]) << 8) | (uint)(((int)byte.MaxValue & (int)this.m_data[(int)(this.m_position + 2U)]) << 16) | (uint)(((int)byte.MaxValue & (int)this.m_data[(int)(this.m_position + 3U)]) << 24);
        this.m_position += 4U;
        this.m_remainingData -= 4U;
      }
      return num;
    }

    public string readString()
    {
      string str = (string) null;
      uint num = this.readLength();
      if (num > this.m_remainingData)
        DataReader.BadData._throw(string.Format("Not enough data to read {0} bytes of string UTF8", (object) num), this.m_data, this.m_position);
      else if (num > 0U)
      {
        str = Encoding.UTF8.GetString(this.m_data, (int) this.m_position, (int) num);
        this.m_position += num;
        this.m_remainingData -= num;
      }
      return str;
    }

    private static bool _isValidTag(byte _tag)
    {
      if (115 != (int) _tag && 108 != (int) _tag)
        return 120 == (int) _tag;
      return true;
    }

    public enum TAG : byte
    {
      LIST = (byte) 108,
      STRING = (byte) 115,
      EOL = (byte) 120,
    }

    internal class BadData : Exception
    {
      private string m_description;
      private byte[] m_data;
      private uint m_position;

      private BadData(string _description, byte[] _data, uint _position)
      {
        this.m_description = _description;
        this.m_data = _data;
        this.m_position = _position;
      }

      public static void _throw(string _description, byte[] _data, uint _position)
      {
        //throw new DataReader.BadData(_description, _data, _position);
      }
    }
  }
}
