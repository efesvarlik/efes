﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.IORConnect
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.Net.Sockets;

namespace DesktopControlAPI.Private
{
  internal class IORConnect : IOResults.RQ
  {
    private readonly TcpClient m_client;

    public IORConnect(TcpClient _client)
      : base(IOResults.RQTYPE.CONNECT)
    {
      this.m_client = _client;
    }

    public override void terminate(IAsyncResult _result)
    {
      try
      {
        this.m_client.Close();
      }
      catch
      {
      }
      try
      {
        this.m_client.EndConnect(_result);
      }
      catch
      {
      }
    }
  }
}
