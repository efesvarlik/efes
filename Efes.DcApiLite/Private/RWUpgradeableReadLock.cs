﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.RWUpgradeableReadLock
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System.Threading;

namespace DesktopControlAPI.Private
{
  internal class RWUpgradeableReadLock : RWLock
  {
    public RWUpgradeableReadLock(ReaderWriterLockSlim _lock)
      : base(_lock)
    {
    }

    protected override void enter(ReaderWriterLockSlim _lock)
    {
      _lock.EnterUpgradeableReadLock();
    }

    protected override void leave(ReaderWriterLockSlim _lock)
    {
      _lock.ExitUpgradeableReadLock();
    }
  }
}
