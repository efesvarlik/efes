﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.Logger
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.Diagnostics;
using System.IO;

namespace DesktopControlAPI.Private
{
  internal class Logger
  {
    private static StreamWriter mFile;

    public static void Init()
    {
      if (Logger.mFile != null)
        return;
      using (Logger.mFile = new StreamWriter("dcapi.log"))
      {
          
      }
    }

    public static void Log(int _level, string _msg)
    {
      Debugger.Log(_level, "Debug: ", DateTime.Now.ToLongTimeString() + ": " + _msg + "\n");
      if (Logger.mFile == null)
        return;
      Logger.mFile.WriteLine("{0}: {1}: {2}", (object) _level, (object) DateTime.Now.ToLongTimeString(), (object) _msg);
      Logger.mFile.Flush();
    }
  }
}
