﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.RWLock
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.Threading;

namespace DesktopControlAPI.Private
{
  internal abstract class RWLock : IDisposable
  {
    private bool m_disposed;
    private ReaderWriterLockSlim m_lock;

    protected RWLock(ReaderWriterLockSlim _lock)
    {
      this.m_disposed = false;
      this.enter(_lock);
      this.m_lock = _lock;
    }

    ~RWLock()
    {
      this.Dispose(false);
    }

    public static IDisposable _read(ReaderWriterLockSlim _lock)
    {
      return (IDisposable) new RWReadLock(_lock);
    }

    public static IDisposable _readNew()
    {
      return RWLock._readNew(true);
    }

    public static IDisposable _readNew(bool _recursive)
    {
      return (IDisposable) new RWReadLock(RWLock._newLock(_recursive));
    }

    public static IDisposable _upgradeableRead(ReaderWriterLockSlim _lock)
    {
      return (IDisposable) new RWUpgradeableReadLock(_lock);
    }

    public static IDisposable _upgradeableReadNew()
    {
      return RWLock._upgradeableReadNew(true);
    }

    public static IDisposable _upgradeableReadNew(bool _recursive)
    {
      return (IDisposable) new RWUpgradeableReadLock(RWLock._newLock(_recursive));
    }

    public static IDisposable _write(ReaderWriterLockSlim _lock)
    {
      return (IDisposable) new RWWriteLock(_lock);
    }

    public static IDisposable _writeNew()
    {
      return RWLock._writeNew(true);
    }

    public static IDisposable _writeNew(bool _recursive)
    {
      return (IDisposable) new RWWriteLock(RWLock._newLock(_recursive));
    }

    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize((object) this);
    }

    private void Dispose(bool _disposing)
    {
      if (this.m_disposed)
        return;
      this.m_disposed = true;
      ReaderWriterLockSlim _lock = this.m_lock;
      if (_lock == null)
        return;
      this.m_lock = (ReaderWriterLockSlim) null;
      this.leave(_lock);
    }

    protected abstract void enter(ReaderWriterLockSlim _lock);

    protected abstract void leave(ReaderWriterLockSlim _lock);

    private static ReaderWriterLockSlim _newLock(bool _recursive)
    {
      return new ReaderWriterLockSlim(_recursive ? LockRecursionPolicy.SupportsRecursion : LockRecursionPolicy.NoRecursion);
    }
  }
}
