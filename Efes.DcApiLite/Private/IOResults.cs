﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.IOResults
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.Collections.Generic;
using System.Threading;

namespace DesktopControlAPI.Private
{
  internal class IOResults
  {
    private readonly ReaderWriterLockSlim m_lock;
    private readonly Dictionary<IAsyncResult, IOResults.RQ> m_results;

    public IOResults(ReaderWriterLockSlim _lock)
    {
      this.m_lock = _lock;
      this.m_results = new Dictionary<IAsyncResult, IOResults.RQ>(3);
    }

    public void add(IAsyncResult _result, IOResults.RQ _type)
    {
      using (RWLock._write(this.m_lock))
      {
        if (_result.CompletedSynchronously)
          return;
        this.m_results.Add(_result, _type);
      }
    }

    public IOResults.RQTYPE remove(IAsyncResult _result, IOResults.RQTYPE _defaultType)
    {
      IOResults.RQTYPE rqtype = IOResults.RQTYPE.UNDEFINED;
      if (_result.CompletedSynchronously)
      {
        rqtype = _defaultType;
      }
      else
      {
        using (RWLock._write(this.m_lock))
        {
          if (this.m_results.ContainsKey(_result))
          {
            rqtype = this.m_results[_result].type;
            this.m_results.Remove(_result);
          }
        }
      }
      return rqtype;
    }

    public void terminate()
    {
      using (RWLock._write(this.m_lock))
      {
        foreach (KeyValuePair<IAsyncResult, IOResults.RQ> keyValuePair in this.m_results)
        {
          if (!keyValuePair.Key.IsCompleted)
            keyValuePair.Value.terminate(keyValuePair.Key);
        }
        this.m_results.Clear();
      }
    }

    public enum RQTYPE
    {
      UNDEFINED,
      DNS,
      CONNECT,
      AUTHENTICATE,
      READ,
      WRITE,
    }

    public abstract class RQ
    {
      private readonly IOResults.RQTYPE m_type;

      public IOResults.RQTYPE type
      {
        get
        {
          return this.m_type;
        }
      }

      protected RQ(IOResults.RQTYPE _type)
      {
        this.m_type = _type;
      }

      public abstract void terminate(IAsyncResult _result);
    }
  }
}
