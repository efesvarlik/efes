﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.IORRead
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.IO;

namespace DesktopControlAPI.Private
{
  internal class IORRead : IOResults.RQ
  {
    private readonly Stream m_stream;

    public IORRead(Stream _stream)
      : base(IOResults.RQTYPE.READ)
    {
      this.m_stream = _stream;
    }

    public override void terminate(IAsyncResult _result)
    {
      try
      {
        this.m_stream.EndRead(_result);
      }
      catch
      {
      }
    }
  }
}
