﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.Private.RWReadLock
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System.Threading;

namespace DesktopControlAPI.Private
{
  internal class RWReadLock : RWLock
  {
    public RWReadLock(ReaderWriterLockSlim _lock)
      : base(_lock)
    {
    }

    protected override void enter(ReaderWriterLockSlim _lock)
    {
      _lock.EnterReadLock();
    }

    protected override void leave(ReaderWriterLockSlim _lock)
    {
      _lock.ExitReadLock();
    }
  }
}
