﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.CallArgs
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;

namespace DesktopControlAPI
{
  /// <summary>
  /// Arguments for generic evtCallXXX events.
  /// 
  /// </summary>
  public class CallArgs : EventArgs
  {
    private readonly Call m_call;
    private readonly ulong m_requestId;

    /// <summary>
    /// Call ID.
    /// 
    /// </summary>
    public string callId
    {
      get
      {
        return this.m_call.m_id;
      }
    }

    /// <summary>
    /// Call ANI.
    /// 
    /// </summary>
    public string ANI
    {
      get
      {
        return this.m_call.m_ANI;
      }
    }

    /// <summary>
    /// Call DNIS.
    /// 
    /// </summary>
    public string DNIS
    {
      get
      {
        return this.m_call.m_DNIS;
      }
    }

    /// <summary>
    /// Caller name.
    /// 
    /// </summary>
    public string callerName
    {
      get
      {
        return this.m_call.m_callerName;
      }
    }

    /// <summary>
    /// Call service name.
    /// 
    /// </summary>
    public string serviceName
    {
      get
      {
        return this.m_call.m_serviceName;
      }
    }

    /// <summary>
    /// Call attached data.
    /// 
    /// </summary>
    public KVList attachedData
    {
      get
      {
        return this.m_call.m_attachedData;
      }
    }

    /// <summary>
    /// Optional request ID of the method which the event responds to.
    /// 
    /// </summary>
    public ulong requestId
    {
      get
      {
        return this.m_requestId;
      }
    }

    internal CallArgs(Call _call, ulong _requestId)
    {
      this.m_call = _call;
      this.m_requestId = _requestId;
    }
  }
}
