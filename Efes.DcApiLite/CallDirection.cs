﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.CallDirection
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

namespace DesktopControlAPI
{
  /// <summary>
  /// Phone call direction enumeration.
  /// 
  /// </summary>
  public enum CallDirection
  {
    Unknown,
    Inbound,
    Outbound,
  }
}
