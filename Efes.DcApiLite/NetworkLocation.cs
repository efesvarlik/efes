﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.NetworkLocation
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

namespace DesktopControlAPI
{
    /// <summary>
    /// Class describes the server network location. SSL is not suppoerted at this time.
    /// 
    /// </summary>
    public class NetworkLocation
    {
        private string m_networkName;
        private ushort m_portNumber;
        private bool m_secureSocket;

        /// <summary>
        /// Computer name or IP address of the server.
        /// 
        /// </summary>
        public string networkName
        {
            get
            {
                return this.m_networkName;
            }
        }

        /// <summary>
        /// TCP port of the server.
        /// 
        /// </summary>
        public ushort portNumber
        {
            get
            {
                return this.m_portNumber;
            }
            set
            {
                this.m_portNumber = value;
            }
        }

        /// <summary>
        /// Specified if SSL should be used. Not supported in this version.
        /// 
        /// </summary>
        public bool secureSocket
        {
            get
            {
                return this.m_secureSocket;
            }
        }

        /// <summary>
        /// Class constructor.
        /// 
        /// </summary>
        public NetworkLocation(string _networkName, ushort _portNumber, bool _secureSocket)
        {
            this.m_networkName = _networkName;
            this.m_portNumber = _portNumber;
            this.m_secureSocket = _secureSocket;
        }

        /// <summary/>
        public override string ToString()
        {
            return base.ToString() + string.Format("{{{0}:{1} bilge:{2}}}", (object)this.m_networkName, (object)this.m_portNumber, (object)(bool)(this.m_secureSocket ? true : false));
        }
    }
}
