﻿// Decompiled with JetBrains decompiler
// Type: Procurios.Public.JSON
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using System;
using System.Collections;
using System.Globalization;
using System.Text;

namespace Procurios.Public
{
    /// <summary>
    /// This class encodes and decodes JSON strings.
    ///             Spec. details, see http://www.json.org/
    /// 
    ///             JSON uses Arrays and Objects. These correspond here to the datatypes ArrayList and Hashtable.
    ///             All numbers are parsed to doubles.
    /// 
    /// </summary>
    internal class JSON
    {
        public const int TOKEN_NONE = 0;
        public const int TOKEN_CURLY_OPEN = 1;
        public const int TOKEN_CURLY_CLOSE = 2;
        public const int TOKEN_SQUARED_OPEN = 3;
        public const int TOKEN_SQUARED_CLOSE = 4;
        public const int TOKEN_COLON = 5;
        public const int TOKEN_COMMA = 6;
        public const int TOKEN_STRING = 7;
        public const int TOKEN_NUMBER = 8;
        public const int TOKEN_TRUE = 9;
        public const int TOKEN_FALSE = 10;
        public const int TOKEN_NULL = 11;
        private const int BUILDER_CAPACITY = 2000;

        /// <summary>
        /// Parses the string json into a value
        /// 
        /// </summary>
        /// <param name="json">A JSON string.</param>
        /// <returns>
        /// An ArrayList, a Hashtable, a double, a string, null, true, or false
        /// </returns>
        public static object JsonDecode(string json)
        {
            bool success = true;
            return JSON.JsonDecode(json, ref success);
        }

        /// <summary>
        /// Parses the string json into a value; and fills 'success' with the successfullness of the parse.
        /// 
        /// </summary>
        /// <param name="json">A JSON string.</param><param name="success">Successful parse?</param>
        /// <returns>
        /// An ArrayList, a Hashtable, a double, a string, null, true, or false
        /// </returns>
        public static object JsonDecode(string json, ref bool success)
        {
            success = true;
            if (json == null)
                return (object)null;
            char[] json1 = json.ToCharArray();
            int index = 0;
            return JSON.ParseValue(json1, ref index, ref success);
        }

        /// <summary>
        /// Converts a Hashtable / ArrayList object into a JSON string
        /// 
        /// </summary>
        /// <param name="json">A Hashtable / ArrayList</param>
        /// <returns>
        /// A JSON encoded string, or null if object 'json' is not serializable
        /// </returns>
        public static string JsonEncode(object json)
        {
            StringBuilder builder = new StringBuilder(2000);
            if (!JSON.SerializeValue(json, builder))
                return (string)null;
            return builder.ToString();
        }

        protected static Hashtable ParseObject(char[] json, ref int index, ref bool success)
        {
            Hashtable hashtable = new Hashtable();
            JSON.NextToken(json, ref index);
            bool flag = false;
            while (!flag)
            {
                switch (JSON.LookAhead(json, index))
                {
                    case 0:
                        success = false;
                        return (Hashtable)null;
                    case 6:
                        JSON.NextToken(json, ref index);
                        continue;
                    case 2:
                        JSON.NextToken(json, ref index);
                        return hashtable;
                    default:
                        string str = JSON.ParseString(json, ref index, ref success);
                        if (!success)
                        {
                            success = false;
                            return (Hashtable)null;
                        }
                        if (JSON.NextToken(json, ref index) != 5)
                        {
                            success = false;
                            return (Hashtable)null;
                        }
                        object obj = JSON.ParseValue(json, ref index, ref success);
                        if (!success)
                        {
                            success = false;
                            return (Hashtable)null;
                        }
                        hashtable[(object)str] = obj;
                        continue;
                }
            }
            return hashtable;
        }

        protected static ArrayList ParseArray(char[] json, ref int index, ref bool success)
        {
            ArrayList arrayList = new ArrayList();
            JSON.NextToken(json, ref index);
            bool flag = false;
            while (!flag)
            {
                switch (JSON.LookAhead(json, index))
                {
                    case 0:
                        success = false;
                        return (ArrayList)null;
                    case 6:
                        JSON.NextToken(json, ref index);
                        continue;
                    case 4:
                        JSON.NextToken(json, ref index);
                        goto label_9;
                    default:
                        object obj = JSON.ParseValue(json, ref index, ref success);
                        if (!success)
                            return (ArrayList)null;
                        arrayList.Add(obj);
                        continue;
                }
            }
        label_9:
            return arrayList;
        }

        protected static object ParseValue(char[] json, ref int index, ref bool success)
        {
            switch (JSON.LookAhead(json, index))
            {
                case 1:
                    return (object)JSON.ParseObject(json, ref index, ref success);
                case 3:
                    return (object)JSON.ParseArray(json, ref index, ref success);
                case 7:
                    return (object)JSON.ParseString(json, ref index, ref success);
                case 8:
                    return (object)JSON.ParseNumber(json, ref index, ref success);
                case 9:
                    JSON.NextToken(json, ref index);
                    return (object)true;
                case 10:
                    JSON.NextToken(json, ref index);
                    return (object)false;
                case 11:
                    JSON.NextToken(json, ref index);
                    return (object)null;
                default:
                    success = false;
                    return (object)null;
            }
        }

        protected static string ParseString(char[] json, ref int index, ref bool success)
        {
            StringBuilder stringBuilder = new StringBuilder(2000);
            JSON.EatWhitespace(json, ref index);
            char ch1 = json[index++];
            bool flag = false;
            while (!flag && index != json.Length)
            {
                char ch2 = json[index++];
                switch (ch2)
                {
                    case '"':
                        flag = true;
                        goto label_19;
                    case '\\':
                        if (index != json.Length)
                        {
                            switch (json[index++])
                            {
                                case '"':
                                    stringBuilder.Append('"');
                                    continue;
                                case '\\':
                                    stringBuilder.Append('\\');
                                    continue;
                                case '/':
                                    stringBuilder.Append('/');
                                    continue;
                                case 'b':
                                    stringBuilder.Append('\b');
                                    continue;
                                case 'f':
                                    stringBuilder.Append('\f');
                                    continue;
                                case 'n':
                                    stringBuilder.Append('\n');
                                    continue;
                                case 'r':
                                    stringBuilder.Append('\r');
                                    continue;
                                case 't':
                                    stringBuilder.Append('\t');
                                    continue;
                                case 'u':
                                    if (json.Length - index >= 4)
                                    {
                                        uint result = 0;
                                        // ISSUE: explicit reference operation
                                        // ISSUE: cast to a reference type
                                        // ISSUE: explicit reference operation
                                        //if ((int) ((sbyte) @success = (sbyte) uint.TryParse(new string(json, index, 4), NumberStyles.HexNumber, (IFormatProvider) CultureInfo.InvariantCulture, out result)) == 0)
                                        //return "";
                                        stringBuilder.Append(char.ConvertFromUtf32((int)result));
                                        index += 4;
                                        continue;
                                    }
                                    goto label_19;
                                default:
                                    continue;
                            }
                        }
                        else
                            goto label_19;
                    default:
                        stringBuilder.Append(ch2);
                        continue;
                }
            }
        label_19:
            if (flag)
                return stringBuilder.ToString();
            success = false;
            return (string)null;
        }

        protected static double ParseNumber(char[] json, ref int index, ref bool success)
        {
            JSON.EatWhitespace(json, ref index);
            int lastIndexOfNumber = JSON.GetLastIndexOfNumber(json, index);
            int length = lastIndexOfNumber - index + 1;
            double result;
            success = double.TryParse(new string(json, index, length), NumberStyles.Any, (IFormatProvider)CultureInfo.InvariantCulture, out result);
            index = lastIndexOfNumber + 1;
            return result;
        }

        protected static int GetLastIndexOfNumber(char[] json, int index)
        {
            int index1 = index;
            while (index1 < json.Length && "0123456789+-.eE".IndexOf(json[index1]) != -1)
                ++index1;
            return index1 - 1;
        }

        protected static void EatWhitespace(char[] json, ref int index)
        {
            while (index < json.Length && " \t\n\r".IndexOf(json[index]) != -1)
                ++index;
        }

        protected static int LookAhead(char[] json, int index)
        {
            int index1 = index;
            return JSON.NextToken(json, ref index1);
        }

        protected static int NextToken(char[] json, ref int index)
        {
            JSON.EatWhitespace(json, ref index);
            if (index == json.Length)
                return 0;
            char ch = json[index];
            ++index;
            switch (ch)
            {
                case '"':
                    return 7;
                case ',':
                    return 6;
                case '-':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    return 8;
                case ':':
                    return 5;
                case '[':
                    return 3;
                case ']':
                    return 4;
                case '{':
                    return 1;
                case '}':
                    return 2;
                default:
                    --index;
                    int num = json.Length - index;
                    if (num >= 5 && (int)json[index] == 102 && ((int)json[index + 1] == 97 && (int)json[index + 2] == 108) && ((int)json[index + 3] == 115 && (int)json[index + 4] == 101))
                    {
                        index += 5;
                        return 10;
                    }
                    if (num >= 4 && (int)json[index] == 116 && ((int)json[index + 1] == 114 && (int)json[index + 2] == 117) && (int)json[index + 3] == 101)
                    {
                        index += 4;
                        return 9;
                    }
                    if (num < 4 || (int)json[index] != 110 || ((int)json[index + 1] != 117 || (int)json[index + 2] != 108) || (int)json[index + 3] != 108)
                        return 0;
                    index += 4;
                    return 11;
            }
        }

        protected static bool SerializeValue(object value, StringBuilder builder)
        {
            bool flag = true;
            if (value is string)
                flag = JSON.SerializeString((string)value, builder);
            else if (value is Hashtable)
                flag = JSON.SerializeObject((Hashtable)value, builder);
            else if (value is ArrayList)
                flag = JSON.SerializeArray((ArrayList)value, builder);
            else if (JSON.IsNumeric(value))
                flag = JSON.SerializeNumber(Convert.ToDouble(value), builder);
            else if (value is bool && (bool)value)
                builder.Append("true");
            else if (value is bool && !(bool)value)
                builder.Append("false");
            else if (value == null)
                builder.Append("null");
            else
                flag = false;
            return flag;
        }

        protected static bool SerializeObject(Hashtable anObject, StringBuilder builder)
        {
            builder.Append("{");
            IDictionaryEnumerator enumerator = anObject.GetEnumerator();
            bool flag = true;
            while (enumerator.MoveNext())
            {
                string aString = enumerator.Key.ToString();
                object obj = enumerator.Value;
                if (!flag)
                    builder.Append(", ");
                JSON.SerializeString(aString, builder);
                builder.Append(":");
                if (!JSON.SerializeValue(obj, builder))
                    return false;
                flag = false;
            }
            builder.Append("}");
            return true;
        }

        protected static bool SerializeArray(ArrayList anArray, StringBuilder builder)
        {
            builder.Append("[");
            bool flag = true;
            for (int index = 0; index < anArray.Count; ++index)
            {
                object obj = anArray[index];
                if (!flag)
                    builder.Append(", ");
                if (!JSON.SerializeValue(obj, builder))
                    return false;
                flag = false;
            }
            builder.Append("]");
            return true;
        }

        protected static bool SerializeString(string aString, StringBuilder builder)
        {
            builder.Append("\"");
            foreach (char ch in aString.ToCharArray())
            {
                switch (ch)
                {
                    case '"':
                        builder.Append("\\\"");
                        break;
                    case '\\':
                        builder.Append("\\\\");
                        break;
                    case '\b':
                        builder.Append("\\b");
                        break;
                    case '\f':
                        builder.Append("\\f");
                        break;
                    case '\n':
                        builder.Append("\\n");
                        break;
                    case '\r':
                        builder.Append("\\r");
                        break;
                    case '\t':
                        builder.Append("\\t");
                        break;
                    default:
                        int num = Convert.ToInt32(ch);
                        if (num >= 32 && num <= 126)
                        {
                            builder.Append(ch);
                            break;
                        }
                        builder.Append("\\u" + Convert.ToString(num, 16).PadLeft(4, '0'));
                        break;
                }
            }
            builder.Append("\"");
            return true;
        }

        protected static bool SerializeNumber(double number, StringBuilder builder)
        {
            builder.Append(Convert.ToString(number, (IFormatProvider)CultureInfo.InvariantCulture));
            return true;
        }

        /// <summary>
        /// Determines if a given object is numeric in any way
        ///             (can be integer, double, null, etc).
        /// 
        ///             Thanks to mtighe for pointing out Double.TryParse to me.
        /// 
        /// </summary>
        protected static bool IsNumeric(object o)
        {
            if (o != null)
            {
                double result;
                return double.TryParse(o.ToString(), out result);
            }
            return false;
        }
    }
}
