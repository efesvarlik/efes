﻿// Decompiled with JetBrains decompiler
// Type: DesktopControlAPI.ApiLite
// Assembly: dcapilite, Version=3.6.2.20830, Culture=neutral, PublicKeyToken=null
// MVID: 71690669-F200-44CA-8231-045CBD76661E
// Assembly location: C:\Users\boraklibel\Desktop\dcapilite_connector\Debug\dcapilite.dll

using DesktopControlAPI.Private;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Threading;

namespace DesktopControlAPI
{
  /// <summary>
  /// Main API class.
  /// 
  /// </summary>
  /// 
  /// <summary>
  /// Main API class.
  /// 
  /// </summary>
  public class ApiLite
  {
    private readonly ReaderWriterLockSlim m_lockEvents;
    private static ulong REQID_NONE;
    private readonly KVClient m_connection;
    private NetworkLocation m_location;
    private bool m_connected;
    private ulong m_nextReqId;
    private string m_tenantId;
    private string m_userId;
    private string m_phone;
    private Dictionary<string, Call> m_calls;
    private ApiLite.EventDispatcher m_dispatcher;
    private AutoResetEvent m_dispatcherEvent;

    /// <summary>
    /// API event dispatcher. Attach your handles for each event you want to intercept.
    /// 
    /// </summary>
    public ApiLite.EventDispatcher eventDispatcher
    {
      get
      {
        return this.m_dispatcher;
      }
    }

    /// <summary>
    /// Specifies if API is logged in.
    /// 
    /// </summary>
    public bool connected
    {
      get
      {
        return this.m_connected;
      }
    }

    /// <summary>
    /// Collection of calls on user's phone.
    /// 
    /// </summary>
    public Dictionary<string, Call> calls
    {
      get
      {
        return this.m_calls;
      }
    }

    /// <summary>
    /// Event called when API successfully connected to server.
    /// 
    /// </summary>
    internal event ApiLite.onApiUp evtApiUp
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_apiUp += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_apiUp -= value;
      }
    }

    /// <summary>
    /// Event sent when API disconnected from server.
    /// 
    /// </summary>
    internal event ApiLite.onApiDown evtApiDown
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_apiDown += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_apiDown -= value;
      }
    }

    /// <summary>
    /// Event sent when new inbound call is arrived on user's phone and is ringing.
    /// 
    /// </summary>
    internal event ApiLite.onCallOffered evtCallOffered
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_callOffered += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_callOffered -= value;
      }
    }

    /// <summary>
    /// Event sent when outbound call is initiated. The outbound call is requested by calling CallDial() method.
    /// 
    /// </summary>
    internal event ApiLite.onCallDialing evtCallDialing
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_callDialing += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_callDialing -= value;
      }
    }

    /// <summary>
    /// Event sent when call is disconnected.
    /// 
    /// </summary>
    internal event ApiLite.onCallDisconnected evtCallDisconnected
    {
      add
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_callDisconnected += value;
      }
      remove
      {
        using (RWLock._write(this.m_lockEvents))
          this.m_callDisconnected -= value;
      }
    }

    private event ApiLite.onApiUp m_apiUp;

    private event ApiLite.onApiDown m_apiDown;

    private event ApiLite.onCallOffered m_callOffered;

    private event ApiLite.onCallDialing m_callDialing;

    private event ApiLite.onCallDisconnected m_callDisconnected;

    /// <summary>
    /// Default constructor.
    /// 
    /// </summary>
    public ApiLite()
    {
      Logger.Init();
      this.m_connection = new KVClient();
      this.m_lockEvents = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
      this.m_nextReqId = 1UL;
      this.m_calls = new Dictionary<string, Call>();
      this.m_connected = false;
      this.m_dispatcherEvent = new AutoResetEvent(false);
      new Thread(new ThreadStart(this.DoDispatch)).Start();
      this.m_dispatcherEvent.WaitOne();
    }

    private void emitApiUp(EventArgs _args)
    {
      Logger.Log(0, "Emitting ApiUp");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_apiUp == null)
          return;
        this.m_apiUp((object) this, _args);
      }
    }

    private void emitApiDown(EventArgs _args)
    {
      Logger.Log(0, "Emitting ApiDown");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_apiDown == null)
          return;
        this.m_apiDown((object) this, _args);
      }
    }

    private void emitCallOffered(CallArgs _args)
    {
      Logger.Log(0, "Emitting CallOffered");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_callOffered == null)
          return;
        this.m_callOffered((object) this, _args);
      }
    }

    private void emitCallDialing(CallArgs _args)
    {
      Logger.Log(0, "Emitting CallDialing");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_callDialing == null)
          return;
        this.m_callDialing((object) this, _args);
      }
    }

    private void emitCallDisconnected(CallArgs _args)
    {
      Logger.Log(0, "Emitting CallDisconnected");
      using (RWLock._upgradeableRead(this.m_lockEvents))
      {
        if (this.m_callDisconnected == null)
          return;
        this.m_callDisconnected((object) this, _args);
      }
    }

    /// <summary>
    /// DoWork
    /// 
    /// </summary>
    private void DoDispatch()
    {
      try
      {
        this.m_dispatcher = new ApiLite.EventDispatcher(this);
        this.m_dispatcherEvent.Set();
        Dispatcher.Run();
      }
      catch (Exception ex)
      {
      }
    }

    /// <summary>
    /// Initializes the connection to server at specified location. The result will be delivered as evtApiUp or evtApiDown events.
    /// 
    /// </summary>
    public bool InitAPI()
    {
      bool flag = false;
      Logger.Log(0, "InitAPI");
      this.m_location = new NetworkLocation("localhost", (ushort) 0, false);
      this.hookTransportEvents();
      if (!this.m_connection.rawTransport.open(this.m_location))
      {
        Logger.Log(1, "Unable to open raw transport");
        this.unhookTransportEvents();
      }
      else
        flag = true;
      return flag;
    }

    /// <summary>
    /// Disconnects from servers and releases resources.
    /// 
    /// </summary>
    public void ShutdownAPI()
    {
      Logger.Log(0, "ShutdownAPI");
      this.unhookTransportEvents();
      this.m_connection.messageReceived -= new KVClient.MessageReceived(this.onMessageReceived);
      this.m_connection.rawTransport.close();
      this.m_dispatcher.Stop();
    }

    /// <summary>
    /// Starts new outbound call to specified number. If call is successfully started, the apiCallDialing event will be emitted.
    /// 
    /// </summary>
    public ulong CallDial(string _dest)
    {
      Logger.Log(0, "CallDial: " + _dest);
      KVList packet = this.createPacket("dial");
      packet.addString("destination", _dest);
      return this.sendRequest(packet);
    }

    private KVList createPacket(string request)
    {
      KVList kvList = new KVList();
      kvList.addString("action", request);
      kvList.addString("request_id", this.m_nextReqId++.ToString());
      return kvList;
    }

    private ulong sendRequest(KVList data)
    {
      this.m_connection.send(data);
      return data.getUInt64Value("request_id");
    }

    private void hookTransportEvents()
    {
      this.m_connection.rawTransport.ready += new RawTransport.onEvent(this.onTransportReady);
      this.m_connection.rawTransport.disconnected += new RawTransport.onFailure(this.onTransportDisconnected);
    }

    private void unhookTransportEvents()
    {
      this.m_connection.rawTransport.ready -= new RawTransport.onEvent(this.onTransportReady);
      this.m_connection.rawTransport.disconnected -= new RawTransport.onFailure(this.onTransportDisconnected);
    }

    private void onTransportReady(object _sender, EventArgs _args)
    {
      Logger.Log(0, "onTransportReady");
      this.m_connection.messageReceived += new KVClient.MessageReceived(this.onMessageReceived);
      KVList _message = new KVList();
      _message.addString("action", "protocol_id");
      _message.addString("protocol", "api");
      string str = string.Format(".NET API Lite, version {0}, build date {1}", (object) Assembly.GetExecutingAssembly().GetName().Version, (object) File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location));
      _message.addString("version", str);
      this.m_connection.send(_message);
      this.emitApiUp(new EventArgs());
    }

    private void onTransportDisconnected(object _sender, RawTransport.FailureArgs _args)
    {
      Logger.Log(0, "onTransportDisconnected");
      this.m_connection.messageReceived -= new KVClient.MessageReceived(this.onMessageReceived);
      this.m_connected = false;
      this.m_calls.Clear();
      this.emitApiDown(new EventArgs());
    }

    private void onTransportClosed(object _sender, RawTransport.FailureArgs _args)
    {
      Logger.Log(0, "onTransportClosed");
      this.unhookTransportEvents();
    }

    private void onMessageReceived(object _sender, KVClient.MessageArgs _args)
    {
      string stringValue = _args.message.getStringValue("action");
      Logger.Log(0, "onMessageReceived: " + _args.message.ToString());
      switch (stringValue)
      {
        case "welcome":
          this.m_tenantId = _args.message.getStringValue("tenant_id");
          this.m_userId = _args.message.getStringValue("user_id");
          this.m_phone = _args.message.getStringValue("phone");
          break;
        case "call_offered":
          Call _call1 = new Call();
          _call1.m_direction = CallDirection.Inbound;
          _call1.m_id = _args.message.getStringValue("call_id");
          _call1.m_ANI = _args.message.getStringValue("ANI");
          _call1.m_DNIS = _args.message.getStringValue("DNIS");
          _call1.m_callerName = _args.message.getStringValue("caller_name");
          _call1.m_serviceName = _args.message.getStringValue("service_name");
          _call1.m_agentId = this.m_userId;
          _call1.m_agentPhone = this.m_phone;
          _call1.m_attachedData = _args.message.getKVListValue("attached_data");
          this.m_calls[_call1.m_id] = _call1;
          this.emitCallOffered(new CallArgs(_call1, ApiLite.REQID_NONE));
          break;
        case "call_dialing":
          Call _call2 = new Call();
          _call2.m_direction = CallDirection.Outbound;
          _call2.m_id = _args.message.getStringValue("call_id");
          _call2.m_ANI = _args.message.getStringValue("ANI");
          _call2.m_DNIS = _args.message.getStringValue("DNIS");
          _call2.m_callerName = _args.message.getStringValue("caller_name");
          _call2.m_serviceName = _args.message.getStringValue("service_name");
          _call2.m_agentId = this.m_userId;
          _call2.m_agentPhone = this.m_phone;
          this.m_calls[_call2.m_id] = _call2;
          this.emitCallDialing(new CallArgs(_call2, ApiLite.REQID_NONE));
          break;
        case "call_disconnected":
          if (this.m_calls.ContainsKey(_args.message.getStringValue("call_id")))
          {
            Call _call3 = this.m_calls[_args.message.getStringValue("call_id")];
            this.m_calls.Remove(_call3.m_id);
            this.emitCallDisconnected(new CallArgs(_call3, ApiLite.REQID_NONE));
            break;
          }
          this.emitCallDisconnected(new CallArgs(new Call()
          {
            m_id = _args.message.getStringValue("call_id"),
            m_agentId = this.m_userId,
            m_agentPhone = this.m_phone
          }, ApiLite.REQID_NONE));
          break;
      }
    }

    /// <summary>
    /// The event dispatcher class intercepts and re-emits all events emitted by API, while guaranteeing that the events will be delivered on the thread on which the API has been created. Event handlers registered with the event dispatcher class created on the GUI thread may directly update elements of the application's GUI.
    /// 
    /// </summary>
    public class EventDispatcher
    {
      private readonly ApiLite m_core;
      private readonly Dispatcher m_dispatcher;
      private readonly ReaderWriterLockSlim m_lockEvents;

      /// <summary>
      /// Event called when API successfully connected to server.
      /// 
      /// </summary>
      public event ApiLite.onApiUp evtApiUp
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_apiUp == null)
              this.m_core.evtApiUp += new ApiLite.onApiUp(this.onApiUp);
            this.m_apiUp += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_apiUp -= value;
            if (this.m_apiUp != null)
              return;
            this.m_core.evtApiUp -= new ApiLite.onApiUp(this.onApiUp);
          }
        }
      }

      /// <summary>
      /// Event sent when API disconnected from server.
      /// 
      /// </summary>
      public event ApiLite.onApiDown evtApiDown
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_apiDown == null)
              this.m_core.evtApiDown += new ApiLite.onApiDown(this.onApiDown);
            this.m_apiDown += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_apiDown -= value;
            if (this.m_apiDown != null)
              return;
            this.m_core.evtApiDown -= new ApiLite.onApiDown(this.onApiDown);
          }
        }
      }

      /// <summary>
      /// Event sent when new inbound call is arrived on user's phone and is ringing.
      /// 
      /// </summary>
      public event ApiLite.onCallOffered evtCallOffered
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_callOffered == null)
              this.m_core.evtCallOffered += new ApiLite.onCallOffered(this.onCallOffered);
            this.m_callOffered += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_callOffered -= value;
            if (this.m_callOffered != null)
              return;
            this.m_core.evtCallOffered -= new ApiLite.onCallOffered(this.onCallOffered);
          }
        }
      }

      /// <summary>
      /// Event sent when outbound call is initiated. The outbound call is requested by calling CallDial() method.
      /// 
      /// </summary>
      public event ApiLite.onCallDialing evtCallDialing
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_callDialing == null)
              this.m_core.evtCallDialing += new ApiLite.onCallDialing(this.onCallDialing);
            this.m_callDialing += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_callDialing -= value;
            if (this.m_callDialing != null)
              return;
            this.m_core.evtCallDialing -= new ApiLite.onCallDialing(this.onCallDialing);
          }
        }
      }

      /// <summary>
      /// Event sent when call is disconnected.
      /// 
      /// </summary>
      public event ApiLite.onCallDisconnected evtCallDisconnected
      {
        add
        {
          using (RWLock._write(this.m_lockEvents))
          {
            if (this.m_callDisconnected == null)
              this.m_core.evtCallDisconnected += new ApiLite.onCallDisconnected(this.onCallDisconnected);
            this.m_callDisconnected += value;
          }
        }
        remove
        {
          using (RWLock._write(this.m_lockEvents))
          {
            this.m_callDisconnected -= value;
            if (this.m_callDisconnected != null)
              return;
            this.m_core.evtCallDisconnected -= new ApiLite.onCallDisconnected(this.onCallDisconnected);
          }
        }
      }

      private event ApiLite.onApiUp m_apiUp;

      private event ApiLite.onApiDown m_apiDown;

      private event ApiLite.onCallOffered m_callOffered;

      private event ApiLite.onCallDialing m_callDialing;

      private event ApiLite.onCallDisconnected m_callDisconnected;

      internal EventDispatcher(ApiLite _core)
      {
        this.m_dispatcher = Dispatcher.CurrentDispatcher;
        this.m_lockEvents = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        this.m_core = _core;
      }

      internal void Stop()
      {
        this.m_dispatcher.InvokeShutdown();
      }

      private void onApiUp(object _sender, EventArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitApiUp(_sender, _args);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new ApiLite.onApiUp(this.emitApiUp), new object[2]
          {
            _sender,
            (object) _args
          });
      }

      private void onApiDown(object _sender, EventArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitApiDown(_sender, _args);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new ApiLite.onApiDown(this.emitApiDown), new object[2]
          {
            _sender,
            (object) _args
          });
      }

      private void onCallOffered(object _sender, CallArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitCallOffered(_sender, _args);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new ApiLite.onCallOffered(this.emitCallOffered), new object[2]
          {
            _sender,
            (object) _args
          });
      }

      private void onCallDialing(object _sender, CallArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitCallDialing(_sender, _args);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new ApiLite.onCallDialing(this.emitCallDialing), new object[2]
          {
            _sender,
            (object) _args
          });
      }

      private void onCallDisconnected(object _sender, CallArgs _args)
      {
        if (this.m_dispatcher.CheckAccess())
          this.emitCallDisconnected(_sender, _args);
        else
          this.m_dispatcher.BeginInvoke((Delegate) new ApiLite.onCallDisconnected(this.emitCallDisconnected), new object[2]
          {
            _sender,
            (object) _args
          });
      }

      private void emitApiUp(object _sender, EventArgs _args)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_apiUp == null)
            return;
          this.m_apiUp(_sender, _args);
        }
      }

      private void emitApiDown(object _sender, EventArgs _args)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_apiDown == null)
            return;
          this.m_apiDown(_sender, _args);
        }
      }

      private void emitCallOffered(object _sender, CallArgs _args)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_callOffered == null)
            return;
          this.m_callOffered(_sender, _args);
        }
      }

      private void emitCallDialing(object _sender, CallArgs _args)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_callDialing == null)
            return;
          this.m_callDialing(_sender, _args);
        }
      }

      private void emitCallDisconnected(object _sender, CallArgs _args)
      {
        using (RWLock._upgradeableRead(this.m_lockEvents))
        {
          if (this.m_callDisconnected == null)
            return;
          this.m_callDisconnected(_sender, _args);
        }
      }
    }

    /// <summary>
    /// Event called when API successfully connected to server.
    /// 
    /// </summary>
    public delegate void onApiUp(object _sender, EventArgs _args);

    /// <summary>
    /// Event sent when API disconnected from server.
    /// 
    /// </summary>
    public delegate void onApiDown(object _sender, EventArgs _args);

    /// <summary>
    /// Event sent when new inbound call is arrived on user's phone and is ringing.
    /// 
    /// </summary>
    public delegate void onCallOffered(object _sender, CallArgs _args);

    /// <summary>
    /// Event sent when outbound call is initiated. The outbound call is requested by calling CallDial() method
    /// 
    /// </summary>
    public delegate void onCallDialing(object _sender, CallArgs _args);

    /// <summary>
    /// Event sent when call is disconnected.
    /// 
    /// </summary>
    public delegate void onCallDisconnected(object _sender, CallArgs _args);
  }
}
